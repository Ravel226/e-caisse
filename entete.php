<?php
/** Partie commune à tous les secteurs
 */
?>
<!DOCTYPE html >
<html>
    <head>
       <meta charset="utf-8">
        <TITLE><?php echo $dossier." - ".$_SESSION["nom_".$_SESSION[$dossier]]?></TITLE>
        <link rel='stylesheet' type="text/css" href='/Magasin.css' />
        <link rel="stylesheet" type="text/css" href="<?php echo $dossier?>.css">
        <link rel="stylesheet" type="text/css" href="/keypad.css">
        <script type="text/javascript" src="/Magasin.js"></script>
        <script type="text/javascript" src="/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo $dossier?>.js"></script>
        <style type="text/css">
        body{
        background-color:<?php echo $_SESSION['fondF_'.$_SESSION[$dossier]]?>;
        }
        #menu{
        background-color:<?php echo $_SESSION['fondC_'.$_SESSION[$dossier]]?>;
        }
        #bandeau{
        background-color:<?php echo $_SESSION['fondC_'.$_SESSION[$dossier]]?>;
        }
        #affichage{
        background-color:<?php echo $_SESSION['fondC_'.$_SESSION[$dossier]]?>;
        }
        h3{
        background-color:<?php echo $_SESSION['titre_'.$_SESSION[$dossier]]?>;
        }
        h4{
        background-color:<?php echo $_SESSION['titre_'.$_SESSION[$dossier]]?>;
        }
        .titre{
        color:<?php echo $_SESSION['titre_'.$_SESSION[$dossier]]?>;
        }
        .surligne{
        background-color:<?php echo $_SESSION['surligne_'.$_SESSION[$dossier]]?>;
        }
        .ombre{
        color:<?php echo $_SESSION['surligne_'.$_SESSION[$dossier]]?>;
        text-shadow: 0 0 20px black;
        }
        </style>
        <script type="text/javascript">
/* $(window).bind('beforeunload', function(e) { 
    e.preventDefault();
    return "Unloading this page may lose data. What do you want to do..."
}); */
$(document).ready(function(){
    $('.vertical').click(function(){
    $('.vertical').removeClass('ombre');
    $(this).addClass('ombre');
    });
});
        </script>
    </head>
    <body style="background-color:<?php echo $_SESSION['fondF_'.$_SESSION[$dossier]]?>" onkeydown="return (event.keyCode != 116)">
    <!-- <body style="background-color:<?php echo $_SESSION['fondF_'.$_SESSION[$dossier]]?>" onkeydown="return ((event.keyCode != 116) || ( event.keyCode != 82 && ctrlKeyDown))"> -->
    <!-- <body style="background-color:<?php echo $_SESSION['fondF_'.$_SESSION[$dossier]]?>"> -->