-- MariaDB dump 10.19  Distrib 10.5.13-MariaDB, for Linux (x86_64)
--
-- ------------------------------------------------------
-- Server version	10.5.13-MariaDB
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Affichage`
--

DROP TABLE IF EXISTS Affichage;
CREATE TABLE Affichage (
  aff_id int(2) NOT NULL AUTO_INCREMENT,
  aff_parent int(2) NOT NULL DEFAULT 0,
  aff_nom varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  aff_ordre int(2) DEFAULT NULL,
  PRIMARY KEY (aff_id),
  KEY aff_parent (aff_parent)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Articles`
--

DROP TABLE IF EXISTS Articles;
CREATE TABLE Articles (
  art_id int(6) NOT NULL AUTO_INCREMENT,
  art_cb bigint(13) unsigned zerofill DEFAULT NULL,
  art_rayon int(2) DEFAULT NULL,
  art_editeur int(4) DEFAULT NULL,
  art_stk decimal(8,2) NOT NULL,
  art_seuil tinyint(1) NOT NULL,
  art_tva tinyint(1) DEFAULT 9,
  art_ttc decimal(6,2) NOT NULL,
  art_pht decimal(9,4) DEFAULT NULL,
  art_remise decimal(7,2) NOT NULL,
  art_aveccb smallint(1) DEFAULT 0,
  art_statut tinyint(1) NOT NULL DEFAULT 1,
  art_mp enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  art_unite tinyint(1) NOT NULL DEFAULT 1,
  art_pseudo tinyint(2) NOT NULL DEFAULT 1,
  art_creation date DEFAULT NULL,
  PRIMARY KEY (art_id),
  UNIQUE KEY art_cb (art_cb),
  KEY art_editeur (art_editeur),
  KEY fk_articlesRayon (art_rayon),
  KEY fk_articlesTva (art_tva),
  KEY fk_articlesUnitevente (art_unite)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Table structure for table `Codes`
--

DROP TABLE IF EXISTS Codes;
CREATE TABLE Codes (
  cod_id int(11) NOT NULL AUTO_INCREMENT,
  cod_nom varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  cod_type int(1) NOT NULL,
  cod_description varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  cod_ordre int(2) NOT NULL DEFAULT 0,
  PRIMARY KEY (cod_id),
  KEY cod_type (cod_type)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Table structure for table `Commandes_2022`
--

DROP TABLE IF EXISTS Commandes_2022;
CREATE TABLE Commandes_2022 (
  com_id int(10) NOT NULL AUTO_INCREMENT,
  com_numero int(5) DEFAULT NULL,
  com_article int(10) NOT NULL,
  com_pht decimal(8,4) NOT NULL DEFAULT 0.0000,
  com_quantite decimal(6,2) NOT NULL,
  com_ttc decimal(5,2) NOT NULL DEFAULT 0.00,
  com_remise decimal(5,2) DEFAULT NULL,
  com_tva tinyint(1) DEFAULT NULL,
  com_utilisateur tinyint(1) NOT NULL,
  com_statut tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (com_id),
  KEY com_article (com_article),
  KEY com_utilisateur (com_utilisateur),
  KEY fk_commandeTva (com_tva)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Commentaire_com_2022`
--

DROP TABLE IF EXISTS Commentaire_com_2022;
CREATE TABLE Commentaire_com_2022 (
  cmt_id int(4) NOT NULL AUTO_INCREMENT,
  cmt_commande int(4) NOT NULL,
  cmt_nom text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (cmt_id),
  KEY cmt_commande (cmt_commande)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Comptes`
--

DROP TABLE IF EXISTS Comptes;
CREATE TABLE Comptes (
  cpt_id int(4) NOT NULL AUTO_INCREMENT,
  cpt_nom varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  cpt_adr1 varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  cpt_adr2 varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  cpt_cp varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  cpt_ville varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  cpt_pays varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  cpt_tel varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  cpt_fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  cpt_compte enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  cpt_remise decimal(4,2) NOT NULL DEFAULT 0.00,
  cpt_mail varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  cpt_creation timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (cpt_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Config`
--

DROP TABLE IF EXISTS Config;
CREATE TABLE Config (
  con_id int(1) NOT NULL,
  con_mode int(1) NOT NULL,
  con_favoris tinyint(1) NOT NULL DEFAULT 1,
  con_debut year(4) NOT NULL,
  con_annees int(2) NOT NULL,
  con_caisse decimal(5,2) NOT NULL DEFAULT 0.00,
  con_arrondi enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  con_don enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  con_articledon int(5) NOT NULL,
  con_prefixfacture varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  con_prefixdevis varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  con_prefixjournal varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  con_inventaire tinyint(2) DEFAULT NULL,
  con_livre decimal(4,2) NOT NULL COMMENT 'remise maxi sur les livres',
  con_secteur int(2) NOT NULL COMMENT 'secteur livre',
  con_token varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Corrections_2021`
--

DROP TABLE IF EXISTS Corrections_2021;
CREATE TABLE Corrections_2021 (
  cor_article int(6) NOT NULL DEFAULT 0,
  cor_commande int(1) NOT NULL,
  cor_stkpre int(1) NOT NULL,
  cor_vente int(1) NOT NULL,
  cor_stock decimal(8,2) NOT NULL,
  cor_pht decimal(9,4) DEFAULT NULL,
  KEY cor_article (cor_article)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Ctypes`
--

DROP TABLE IF EXISTS Ctypes;
CREATE TABLE Ctypes (
  ctp_id int(11) NOT NULL AUTO_INCREMENT,
  ctp_nom varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (ctp_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Depot`
--

DROP TABLE IF EXISTS Depot;
CREATE TABLE Depot (
  dep_id int(6) NOT NULL AUTO_INCREMENT,
  dep_article int(5) NOT NULL,
  dep_stk decimal(6,2) NOT NULL DEFAULT 0.00,
  dep_vente decimal(6,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (dep_id),
  UNIQUE KEY dep_article (dep_article) USING BTREE,
  CONSTRAINT fk_depotArticle FOREIGN KEY (dep_article) REFERENCES Articles (art_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Detail_facture_2022`
--

DROP TABLE IF EXISTS Detail_facture_2022;
CREATE TABLE Detail_facture_2022 (
  def_id int(5) NOT NULL AUTO_INCREMENT,
  def_facture int(5) NOT NULL,
  def_tva int(2) NOT NULL DEFAULT 0,
  def_tvavaleur decimal(8,2) NOT NULL DEFAULT 0.00,
  def_artvaleur decimal(8,2) NOT NULL DEFAULT 0.00,
  def_codeana int(5) NOT NULL,
  def_codeent int(5) DEFAULT NULL,
  PRIMARY KEY (def_id),
  KEY def_facture (def_facture),
  KEY def_tva (def_tva)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Table structure for table `Devis_2021`
--

DROP TABLE IF EXISTS Devis_2021;
CREATE TABLE Devis_2021 (
  dev_id int(11) NOT NULL AUTO_INCREMENT,
  dev_compte int(11) NOT NULL,
  dev_facture int(11) DEFAULT NULL,
  dev_date date NOT NULL,
  PRIMARY KEY (dev_id),
  KEY dev_compte (dev_compte) USING BTREE,
  KEY fk_devisFacture (dev_facture)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Devis_2022`
--

DROP TABLE IF EXISTS Devis_2022;
CREATE TABLE Devis_2022 (
  dev_id int(11) NOT NULL AUTO_INCREMENT,
  dev_compte int(11) NOT NULL,
  dev_facture int(11) DEFAULT NULL,
  dev_date date NOT NULL,
  PRIMARY KEY (dev_id),
  KEY dev_compte (dev_compte) USING BTREE,
  KEY fk_devisFacture (dev_facture)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Editeur_serveur`
--

DROP TABLE IF EXISTS Editeur_serveur;
CREATE TABLE Editeur_serveur (
  eds_id int(11) NOT NULL AUTO_INCREMENT,
  eds_editeur int(4) DEFAULT NULL,
  eds_serveur int(4) DEFAULT NULL,
  eds_remise decimal(5,2) DEFAULT 30.00,
  eds_utilisateur tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (eds_id),
  KEY eds_editeur (eds_editeur),
  KEY eds_serveur (eds_serveur)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Editeurs`
--

DROP TABLE IF EXISTS Editeurs;
CREATE TABLE Editeurs (
  edi_id int(4) NOT NULL AUTO_INCREMENT,
  edi_pays tinyint(1) DEFAULT NULL,
  edi_nom varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_adr1 varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_adr2 varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_cp varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_ville varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_tel varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_fax varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_utilisateur tinyint(3) NOT NULL DEFAULT 1,
  edi_type tinyint(1) NOT NULL DEFAULT 0,
  edi_client varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_contact varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_mail varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_commande varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_site varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  edi_etat tinyint(1) NOT NULL DEFAULT 1,
  edi_port tinyint(2) NOT NULL DEFAULT 0,
  edi_code varchar(7) COLLATE utf8_unicode_ci DEFAULT '0',
  PRIMARY KEY (edi_id),
  KEY edi_utilisateur (edi_utilisateur),
  KEY edi_etat (edi_etat),
  CONSTRAINT fk_editeurUtilisateur FOREIGN KEY (edi_utilisateur) REFERENCES Utilisateurs (uti_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Etiquettes`
--

DROP TABLE IF EXISTS Etiquettes;
CREATE TABLE Etiquettes (
  eti_id int(11) NOT NULL AUTO_INCREMENT,
  eti_article int(5) DEFAULT NULL,
  eti_quantite int(3) DEFAULT NULL,
  eti_utilisateur tinyint(2) DEFAULT NULL,
  PRIMARY KEY (eti_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Exclus`
--

DROP TABLE IF EXISTS Exclus;
CREATE TABLE Exclus (
  exc_id int(1) NOT NULL AUTO_INCREMENT,
  exc_nom varchar(50) NOT NULL,
  PRIMARY KEY (exc_id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `Externe`
--

DROP TABLE IF EXISTS Externe;
CREATE TABLE Externe (
  ext_id int(11) NOT NULL AUTO_INCREMENT,
  ext_ip varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  ext_poste varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  ext_auto tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (ext_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Factures_2022`
--

DROP TABLE IF EXISTS Factures_2022;
CREATE TABLE Factures_2022 (
  fac_id int(11) NOT NULL AUTO_INCREMENT,
  fac_ticket int(11) DEFAULT NULL,
  fac_cp int(4) DEFAULT NULL,
  fac_imp enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (fac_id),
  KEY fac_cp (fac_cp),
  KEY fac_ticket (fac_ticket)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Favoris`
--

DROP TABLE IF EXISTS Favoris;
CREATE TABLE Favoris (
  fav_id int(4) NOT NULL AUTO_INCREMENT,
  fav_article int(4) NOT NULL,
  fav_parent int(2) NOT NULL,
  PRIMARY KEY (fav_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Fonctions`
--

DROP TABLE IF EXISTS Fonctions;
CREATE TABLE Fonctions (
  fon_id tinyint(4) NOT NULL AUTO_INCREMENT,
  fon_nom varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (fon_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Formesociale`
--

DROP TABLE IF EXISTS Formesociale;
CREATE TABLE Formesociale (
  fos_id tinyint(1) NOT NULL AUTO_INCREMENT,
  fos_nom varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  fos_long varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (fos_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Imprimantes`
--

DROP TABLE IF EXISTS Imprimantes;
CREATE TABLE Imprimantes (
  imp_id tinyint(1) NOT NULL AUTO_INCREMENT,
  imp_nom varchar(30) CHARACTER SET latin1 NOT NULL,
  imp_desc varchar(250) CHARACTER SET latin1 NOT NULL,
  imp_param text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (imp_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Inventaire`
--

DROP TABLE IF EXISTS Inventaire;
CREATE TABLE Inventaire (
  inv_id int(5) NOT NULL AUTO_INCREMENT,
  inv_cb bigint(13) unsigned zerofill NOT NULL,
  inv_utilisateur tinyint(2) NOT NULL,
  inv_date datetime NOT NULL,
  PRIMARY KEY (inv_id),
  KEY bil_cb (inv_cb),
  KEY fk_inventaireUtilisateur (inv_utilisateur),
  CONSTRAINT fk_inventaireUtilisateur FOREIGN KEY (inv_utilisateur) REFERENCES Utilisateurs (uti_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Journal_factures_2022`
--

DROP TABLE IF EXISTS Journal_factures_2022;
CREATE TABLE Journal_factures_2022 (
  jrn_id int(4) NOT NULL AUTO_INCREMENT,
  jrn_facture int(4) NOT NULL,
  jrn_date date DEFAULT NULL,
  jrn_annee year(4) DEFAULT NULL,
  jrn_numero varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  jrn_enregistrement timestamp NOT NULL DEFAULT current_timestamp(),
  jrn_export tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (jrn_id),
  UNIQUE KEY jrn_facture (jrn_facture)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Mode_reglement`
--

DROP TABLE IF EXISTS Mode_reglement;
CREATE TABLE Mode_reglement (
  mdr_id tinyint(1) NOT NULL AUTO_INCREMENT,
  mdr_nom varchar(20) NOT NULL,
  mdr_abrege varchar(1) NOT NULL,
  mdr_decaissement tinyint(1) NOT NULL DEFAULT 1,
  mdr_correction enum('0','1') NOT NULL,
  mdr_etat tinyint(1) NOT NULL DEFAULT 1,
  mdr_ordre tinyint(1) NOT NULL,
  mdr_couleur varchar(6) NOT NULL,
  mdr_arrondi tinyint(1) NOT NULL,
  mdr_fond int(5) NOT NULL,
  mdr_seuil int(3) NOT NULL,
  mdr_code varchar(7) NOT NULL,
  mdr_droits tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (mdr_id),
  KEY mdr_abrege (mdr_abrege)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `Promo`
--

DROP TABLE IF EXISTS Promo;
CREATE TABLE Promo (
  pro_id int(6) NOT NULL AUTO_INCREMENT,
  pro_article int(5) NOT NULL,
  pro_mode int(1) NOT NULL DEFAULT 1,
  pro_valeur decimal(8,2) DEFAULT NULL,
  pro_date timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (pro_id),
  UNIQUE KEY pro_article (pro_article) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Pseudos`
--

DROP TABLE IF EXISTS Pseudos;
CREATE TABLE Pseudos (
  pse_id int(11) NOT NULL AUTO_INCREMENT,
  pse_nom varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  pse_stock enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  pse_remise enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (pse_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Rayons`
--

DROP TABLE IF EXISTS Rayons;
CREATE TABLE Rayons (
  ray_id int(2) NOT NULL AUTO_INCREMENT,
  ray_secteur tinyint(2) NOT NULL DEFAULT 0,
  ray_tva tinyint(1) NOT NULL DEFAULT 0,
  ray_nom varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'nouveau',
  ray_abrege varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  ray_ent int(6) NOT NULL DEFAULT 0,
  ray_sort tinyint(2) NOT NULL DEFAULT 0,
  ray_ana int(6) NOT NULL DEFAULT 0,
  ray_fav tinyint(1) NOT NULL DEFAULT 0 COMMENT 'indique si le rayon est dans les favoris',
  PRIMARY KEY (ray_id),
  KEY fk_rayonSecteur (ray_secteur),
  CONSTRAINT fk_rayonSecteur FOREIGN KEY (ray_secteur) REFERENCES Secteurs (sec_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 ROW_FORMAT=COMPACT;

--
-- Table structure for table `Resume_commande_2022`
--

DROP TABLE IF EXISTS Resume_commande_2022;
CREATE TABLE Resume_commande_2022 (
  rsc_id int(5) NOT NULL AUTO_INCREMENT,
  rsc_date date NOT NULL,
  rsc_serveur int(10) NOT NULL,
  rsc_etat tinyint(1) NOT NULL DEFAULT 1,
  rsc_util tinyint(2) NOT NULL,
  rsc_ttc decimal(8,2) NOT NULL DEFAULT 0.00,
  rsc_tax decimal(8,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (rsc_id),
  KEY fk_resumecommandeUtilisateur (rsc_util),
  KEY fk_resumecommandeServeur (rsc_serveur)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Resume_detail_2022`
--

DROP TABLE IF EXISTS Resume_detail_2022;
CREATE TABLE Resume_detail_2022 (
  red_id int(11) NOT NULL AUTO_INCREMENT,
  red_jour int(11) NOT NULL,
  red_mode int(3) NOT NULL,
  red_valeur decimal(8,2) NOT NULL,
  PRIMARY KEY (red_id),
  KEY red_jour (red_jour)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Resume_jour_2022`
--

DROP TABLE IF EXISTS Resume_jour_2022;
CREATE TABLE Resume_jour_2022 (
  rsj_id int(11) NOT NULL AUTO_INCREMENT,
  rsj_date date NOT NULL,
  rsj_statut enum('0','1','2','3') DEFAULT '2',
  rsj_valeur decimal(7,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (rsj_id),
  KEY resj_statut (rsj_statut),
  KEY rsj_date (rsj_date)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Table structure for table `Resume_ticket_2022`
--

DROP TABLE IF EXISTS Resume_ticket_2022;
CREATE TABLE Resume_ticket_2022 (
  rst_id int(11) NOT NULL AUTO_INCREMENT,
  rst_num int(4) NOT NULL DEFAULT 0,
  rst_utilisateur tinyint(3) NOT NULL DEFAULT 0,
  rst_etat varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  rst_etatS tinyint(1) DEFAULT NULL,
  rst_total decimal(18,2) NOT NULL,
  rst_totalS decimal(18,2) NOT NULL,
  rst_validation datetime DEFAULT NULL,
  rst_mod enum('0','1') COLLATE utf8_unicode_ci DEFAULT '0',
  rst_dtm_log datetime DEFAULT NULL,
  PRIMARY KEY (rst_id),
  KEY fk_resumeticketModereglement (rst_etatS),
  KEY fk_resumeticketUtilisateur (rst_utilisateur)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Secteurs`
--

DROP TABLE IF EXISTS Secteurs;
CREATE TABLE Secteurs (
  sec_id tinyint(2) NOT NULL AUTO_INCREMENT,
  sec_nom varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  sec_abrege varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  sec_couleur varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  sec_ordre int(2) NOT NULL DEFAULT 0,
  sec_etat tinyint(1) NOT NULL DEFAULT 1,
  sec_tva tinyint(1) DEFAULT NULL,
  PRIMARY KEY (sec_id),
  KEY sec_tva (sec_abrege),
  KEY sec_tva_2 (sec_tva),
  CONSTRAINT fk_secteurTva FOREIGN KEY (sec_tva) REFERENCES Tva (tva_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 ROW_FORMAT=COMPACT;

--
-- Table structure for table `Societe`
--

DROP TABLE IF EXISTS Societe;
CREATE TABLE Societe (
  soc_id tinyint(1) NOT NULL AUTO_INCREMENT,
  soc_nom varchar(100) CHARACTER SET latin1 NOT NULL,
  soc_formesociale varchar(10) CHARACTER SET latin1 NOT NULL,
  soc_raisonsociale varchar(100) CHARACTER SET latin1 NOT NULL,
  soc_adr1 varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  soc_adr2 varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  soc_cp varchar(15) CHARACTER SET latin1 NOT NULL,
  soc_ville varchar(50) CHARACTER SET latin1 NOT NULL,
  soc_pays varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  soc_capital bigint(13) NOT NULL,
  soc_rcs varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  soc_tel varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  soc_fax varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  soc_siret varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  soc_ape varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  soc_banque varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  soc_rib varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  soc_cle varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  soc_tva varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  soc_iban varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  soc_bic varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (soc_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Soldes`
--

DROP TABLE IF EXISTS Soldes;
CREATE TABLE Soldes (
  sol_id int(11) NOT NULL AUTO_INCREMENT,
  sol_debut date DEFAULT NULL,
  sol_fin date DEFAULT NULL,
  PRIMARY KEY (sol_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Statut`
--

DROP TABLE IF EXISTS Statut;
CREATE TABLE Statut (
  sta_id int(1) NOT NULL AUTO_INCREMENT,
  sta_nom varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (sta_id) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Stock_2021`
--

DROP TABLE IF EXISTS Stock_2021;
CREATE TABLE Stock_2021 (
  stk_id int(4) NOT NULL AUTO_INCREMENT,
  stk_article int(4) NOT NULL,
  stk_stk decimal(8,2) NOT NULL,
  stk_pht decimal(8,4) NOT NULL,
  PRIMARY KEY (stk_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Tickets_2022`
--

DROP TABLE IF EXISTS Tickets_2022;
CREATE TABLE Tickets_2022 (
  tic_id int(11) NOT NULL AUTO_INCREMENT,
  tic_num int(11) NOT NULL,
  tic_article int(11) DEFAULT NULL,
  tic_quantite decimal(6,2) DEFAULT NULL,
  tic_quantiteS decimal(6,2) DEFAULT NULL,
  tic_prix decimal(6,2) NOT NULL,
  tic_prixS decimal(6,2) NOT NULL,
  tic_pht decimal(6,2) NOT NULL,
  tic_tt decimal(6,2) DEFAULT NULL,
  tic_tva decimal(4,2) NOT NULL,
  tic_ntva int(1) NOT NULL,
  tic_cp int(5) NOT NULL DEFAULT 0,
  tic_devis int(5) NOT NULL DEFAULT 0,
  PRIMARY KEY (tic_id),
  KEY tic_num (tic_num),
  KEY tic_article (tic_article),
  KEY tic_ntva (tic_ntva),
  KEY tic_cp (tic_cp),
  KEY tic_devis (tic_devis)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Titres`
--

DROP TABLE IF EXISTS Titres;
CREATE TABLE Titres (
  tit_id int(10) NOT NULL AUTO_INCREMENT,
  tit_nom varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  tit_recherche varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  tit_article int(5) NOT NULL,
  tit_niveau tinyint(1) NOT NULL,
  PRIMARY KEY (tit_id),
  KEY tit_article (tit_article,tit_niveau)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='table regroupant les champs de recherche, titre, ref, refin,auteur';

--
-- Table structure for table `Tva`
--

DROP TABLE IF EXISTS Tva;
CREATE TABLE Tva (
  tva_id tinyint(1) NOT NULL DEFAULT 0,
  tva_nom decimal(4,2) NOT NULL,
  tva_etat tinyint(1) NOT NULL DEFAULT 1,
  tva_description varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  tva_couleur varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  tva_ordre tinyint(2) NOT NULL,
  tva_code int(3) NOT NULL DEFAULT 0,
  tva_codeachat int(3) DEFAULT NULL,
  tva_article int(6) DEFAULT NULL,
  PRIMARY KEY (tva_id),
  KEY tva_etat (tva_etat),
  KEY tva_code (tva_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Unites_vente`
--

DROP TABLE IF EXISTS Unites_vente;
CREATE TABLE Unites_vente (
  unv_id tinyint(1) NOT NULL AUTO_INCREMENT,
  unv_nom varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  unv_abrege varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (unv_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `Utilisateurs`
--

DROP TABLE IF EXISTS Utilisateurs;
CREATE TABLE Utilisateurs (
  uti_id tinyint(3) NOT NULL AUTO_INCREMENT,
  uti_nom varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  uti_fondF varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#ffffff',
  uti_fondC varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#ffffff',
  uti_titre varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#ffffff',
  uti_surligne varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#ffffff',
  uti_fonction tinyint(1) DEFAULT NULL,
  uti_etat tinyint(1) DEFAULT 0,
  uti_pass varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  uti_email varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  uti_droits tinyint(1) NOT NULL DEFAULT 0,
  uti_clavier tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (uti_id),
  KEY fk_utilisateurFonction (uti_fonction)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
