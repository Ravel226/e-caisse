-- MariaDB dump 10.19  Distrib 10.5.13-MariaDB, for Linux (x86_64)
--
-- ------------------------------------------------------
-- Server version	10.5.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `Affichage`
--

/*!40000 ALTER TABLE Affichage DISABLE KEYS */;
INSERT INTO Affichage (aff_id, aff_parent, aff_nom, aff_ordre) VALUES (1,0,'Articles par Code',1),
(2,0,'livres',2),
(3,0,'Alimentaire',1);
/*!40000 ALTER TABLE Affichage ENABLE KEYS */;

--
-- Dumping data for table `Articles`
--

/*!40000 ALTER TABLE Articles DISABLE KEYS */;
INSERT INTO Articles (art_id, art_cb, art_rayon, art_editeur, art_stk, art_seuil, art_tva, art_ttc, art_pht, art_remise, art_aveccb, art_statut, art_mp, art_unite, art_pseudo, art_creation) VALUES (1,9782070137978,2,2,1.00,0,3,15.90,9.2750,30.00,0,1,'0',1,1,'2022-01-05'),
(2,9782266173018,4,3,0.00,0,3,7.50,4.3750,30.00,0,1,'0',1,1,'2022-01-05'),
(3,9782374350196,3,4,1.00,0,3,26.80,15.6333,30.00,0,1,'0',1,1,'2022-01-05'),
(4,2001234500036,5,5,0.00,0,2,5.00,2.5000,0.00,0,1,'0',1,1,'2022-01-06'),
(5,2001234500029,6,5,0.00,0,2,4.50,2.6000,0.00,0,1,'0',1,1,'2022-01-06'),
(6,2001234500111,4,8,0.00,0,2,0.00,0.0000,0.00,0,1,'0',1,2,'2022-01-21'),
(7,2001234500128,8,8,0.00,0,3,0.00,0.0000,0.00,0,1,'0',1,2,'2022-01-21'),
(11,2001234500043,7,5,0.00,0,2,4.20,2.5000,0.00,0,1,'0',1,1,'2022-01-07'),
(12,2001234500050,7,5,0.00,0,2,3.50,2.0000,0.00,0,1,'0',2,1,'2022-01-07'),
(13,2001234500067,7,5,0.00,0,2,3.80,2.1700,0.00,0,1,'0',2,1,'2022-01-07'),
(14,2001234500074,6,6,0.00,0,2,4.50,2.6000,0.00,0,1,'0',1,1,'2022-01-07'),
(15,2001234500081,6,6,0.00,0,2,1.00,0.5000,0.00,0,1,'0',1,1,'2022-01-07'),
(16,2001234500098,6,6,0.00,0,2,5.95,0.4500,0.00,0,1,'0',1,1,'2022-01-07'),
(17,2001234500104,1,NULL,0.00,0,0,0.00,0.0000,0.00,0,7,'0',1,3,'2022-01-07'),
(18,9782213706122,4,7,0.00,0,3,32.01,18.6725,30.00,0,1,'0',1,1,'2022-01-18'),
(21,2001234500135,11,9,0.00,0,3,137.00,80.0000,0.00,0,1,'0',1,1,'2022-01-22'),
(22,2001234500142,12,9,0.00,0,2,137.00,80.0000,0.00,0,1,'0',1,1,'2022-01-22');
/*!40000 ALTER TABLE Articles ENABLE KEYS */;

--
-- Dumping data for table `Codes`
--

/*!40000 ALTER TABLE Codes DISABLE KEYS */;
/*!40000 ALTER TABLE Codes ENABLE KEYS */;

--
-- Dumping data for table `Commandes_2022`
--

/*!40000 ALTER TABLE Commandes_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Commandes_2022 ENABLE KEYS */;

--
-- Dumping data for table `Commentaire_com_2022`
--

/*!40000 ALTER TABLE Commentaire_com_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Commentaire_com_2022 ENABLE KEYS */;

--
-- Dumping data for table `Comptes`
--

/*!40000 ALTER TABLE Comptes DISABLE KEYS */;
INSERT INTO Comptes (cpt_id, cpt_nom, cpt_adr1, cpt_adr2, cpt_cp, cpt_ville, cpt_pays, cpt_tel, cpt_fax, cpt_compte, cpt_remise, cpt_mail, cpt_creation) VALUES (1,'Remise personnalisée','','',NULL,'',NULL,NULL,NULL,'0',0.00,NULL,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE Comptes ENABLE KEYS */;

--
-- Dumping data for table `Config`
--

/*!40000 ALTER TABLE Config DISABLE KEYS */;
INSERT INTO Config (con_id, con_mode, con_favoris, con_debut, con_annees, con_caisse, con_arrondi, con_don, con_articledon, con_prefixfacture, con_prefixdevis, con_prefixjournal, con_inventaire, con_livre, con_secteur, con_token) VALUES (0,1,1,2022,0,500.00,'0','1',1,'Lib-000','Dev-000','L-000',12,5.00,4,'');
/*!40000 ALTER TABLE Config ENABLE KEYS */;

--
-- Dumping data for table `Corrections_2021`
--

/*!40000 ALTER TABLE Corrections_2021 DISABLE KEYS */;
/*!40000 ALTER TABLE Corrections_2021 ENABLE KEYS */;

--
-- Dumping data for table `Ctypes`
--

/*!40000 ALTER TABLE Ctypes DISABLE KEYS */;
INSERT INTO Ctypes (ctp_id, ctp_nom) VALUES (1,'Entree'),
(2,'Sortie'),
(3,'Analytique'),
(4,'Reglement'),
(5,'Tva');
/*!40000 ALTER TABLE Ctypes ENABLE KEYS */;

--
-- Dumping data for table `Depot`
--

/*!40000 ALTER TABLE Depot DISABLE KEYS */;
/*!40000 ALTER TABLE Depot ENABLE KEYS */;

--
-- Dumping data for table `Detail_facture_2022`
--

/*!40000 ALTER TABLE Detail_facture_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Detail_facture_2022 ENABLE KEYS */;

--
-- Dumping data for table `Devis_2021`
--

/*!40000 ALTER TABLE Devis_2021 DISABLE KEYS */;
/*!40000 ALTER TABLE Devis_2021 ENABLE KEYS */;

--
-- Dumping data for table `Devis_2022`
--

/*!40000 ALTER TABLE Devis_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Devis_2022 ENABLE KEYS */;

--
-- Dumping data for table `Editeur_serveur`
--

/*!40000 ALTER TABLE Editeur_serveur DISABLE KEYS */;
INSERT INTO Editeur_serveur (eds_id, eds_editeur, eds_serveur, eds_remise, eds_utilisateur) VALUES (1,1,1,0.00,4),
(2,2,2,30.00,4),
(3,3,3,30.00,4),
(4,4,2,30.00,4),
(5,5,5,0.00,3),
(6,6,6,0.00,3),
(7,7,7,30.00,4),
(8,8,8,0.00,4),
(9,9,9,0.00,9);
/*!40000 ALTER TABLE Editeur_serveur ENABLE KEYS */;

--
-- Dumping data for table `Editeurs`
--

/*!40000 ALTER TABLE Editeurs DISABLE KEYS */;
INSERT INTO Editeurs (edi_id, edi_pays, edi_nom, edi_adr1, edi_adr2, edi_cp, edi_ville, edi_tel, edi_fax, edi_utilisateur, edi_type, edi_client, edi_contact, edi_mail, edi_commande, edi_site, edi_etat, edi_port, edi_code) VALUES (1,NULL,'Pilon',NULL,NULL,NULL,NULL,NULL,NULL,2,0,NULL,NULL,NULL,NULL,NULL,0,0,'0'),
(2,NULL,'Gallimard',NULL,NULL,NULL,NULL,NULL,NULL,4,0,NULL,NULL,NULL,NULL,NULL,0,0,'0'),
(3,NULL,'Plon',NULL,NULL,NULL,NULL,NULL,NULL,4,0,NULL,NULL,NULL,NULL,NULL,0,0,'0'),
(4,NULL,'Saint Simon',NULL,NULL,NULL,NULL,NULL,NULL,4,0,NULL,NULL,NULL,NULL,NULL,0,0,'0'),
(5,NULL,'ProductionLocale',NULL,NULL,NULL,NULL,NULL,NULL,3,1,NULL,NULL,NULL,NULL,NULL,0,0,'0'),
(6,NULL,'Super Marché',NULL,NULL,NULL,NULL,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,0,0,'0'),
(7,NULL,'Fayard',NULL,NULL,NULL,NULL,NULL,NULL,4,0,NULL,NULL,NULL,NULL,NULL,0,0,'0'),
(8,NULL,'interne',NULL,NULL,NULL,NULL,NULL,NULL,4,0,NULL,NULL,NULL,NULL,NULL,0,0,'0'),
(9,NULL,'Chanel',NULL,NULL,NULL,NULL,NULL,NULL,9,0,NULL,NULL,NULL,NULL,NULL,0,0,'0');
/*!40000 ALTER TABLE Editeurs ENABLE KEYS */;

--
-- Dumping data for table `Etiquettes`
--

/*!40000 ALTER TABLE Etiquettes DISABLE KEYS */;
/*!40000 ALTER TABLE Etiquettes ENABLE KEYS */;

--
-- Dumping data for table `Exclus`
--

/*!40000 ALTER TABLE Exclus DISABLE KEYS */;
INSERT INTO Exclus (exc_id, exc_nom) VALUES (1,'En vente'),
(2,'Non défini'),
(4,'Nouveau'),
(3,'Épuisé'),
(5,'Ponctuel'),
(6,'Trop vieux'),
(7,'Port achat');
/*!40000 ALTER TABLE Exclus ENABLE KEYS */;

--
-- Dumping data for table `Externe`
--

/*!40000 ALTER TABLE Externe DISABLE KEYS */;
/*!40000 ALTER TABLE Externe ENABLE KEYS */;

--
-- Dumping data for table `Factures_2022`
--

/*!40000 ALTER TABLE Factures_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Factures_2022 ENABLE KEYS */;

--
-- Dumping data for table `Favoris`
--

/*!40000 ALTER TABLE Favoris DISABLE KEYS */;
INSERT INTO Favoris (fav_id, fav_article, fav_parent) VALUES (6,2,2),
(7,3,2),
(8,1,2),
(9,19,2);
/*!40000 ALTER TABLE Favoris ENABLE KEYS */;

--
-- Dumping data for table `Fonctions`
--

/*!40000 ALTER TABLE Fonctions DISABLE KEYS */;
INSERT INTO Fonctions (fon_id, fon_nom) VALUES (1,'Administration'),
(2,'Ventes'),
(3,'Saisie'),
(4,'Gestion');
/*!40000 ALTER TABLE Fonctions ENABLE KEYS */;

--
-- Dumping data for table `Formesociale`
--

/*!40000 ALTER TABLE Formesociale DISABLE KEYS */;
INSERT INTO Formesociale (fos_id, fos_nom, fos_long) VALUES (1,'SCI','Société civile immobilière'),
(2,'SARL','Société à responsabilité limitée'),
(3,'SNC','Société en nom collectif'),
(4,'SA','Société anonyme'),
(5,'SAS','Société par actions simplifiée'),
(6,'GIE','Groupement d&#39;intérêt économique'),
(7,'AS','Association');
/*!40000 ALTER TABLE Formesociale ENABLE KEYS */;

--
-- Dumping data for table `Imprimantes`
--

/*!40000 ALTER TABLE Imprimantes DISABLE KEYS */;
INSERT INTO Imprimantes (imp_id, imp_nom, imp_desc, imp_param) VALUES (1,'TM-T20II','ticket caisse',''),
(2,'PDF','factures',''),
(3,'PDF','code barre',''),
(4,'PDF','pdf','');
/*!40000 ALTER TABLE Imprimantes ENABLE KEYS */;

--
-- Dumping data for table `Inventaire`
--

/*!40000 ALTER TABLE Inventaire DISABLE KEYS */;
/*!40000 ALTER TABLE Inventaire ENABLE KEYS */;

--
-- Dumping data for table `Journal_factures_2022`
--

/*!40000 ALTER TABLE Journal_factures_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Journal_factures_2022 ENABLE KEYS */;

--
-- Dumping data for table `Mode_reglement`
--

/*!40000 ALTER TABLE Mode_reglement DISABLE KEYS */;
INSERT INTO Mode_reglement (mdr_id, mdr_nom, mdr_abrege, mdr_decaissement, mdr_correction, mdr_etat, mdr_ordre, mdr_couleur, mdr_arrondi, mdr_fond, mdr_seuil, mdr_code, mdr_droits) VALUES (1,'Espèces','L',1,'0',1,3,'adf6b5',0,500,0,'25',0),
(2,'Chèque','C',1,'1',1,4,'92d99c',0,0,0,'26',0),
(3,'Carte Banc.','B',1,'1',1,1,'d2f4d5',0,0,0,'27',0),
(4,'Carte B. SC','S',1,'1',2,2,'b5d8b9',0,0,0,'0',0),
(5,'Différé','D',1,'0',1,5,'beddc2',0,0,0,'28',1);
/*!40000 ALTER TABLE Mode_reglement ENABLE KEYS */;

--
-- Dumping data for table `Promo`
--

/*!40000 ALTER TABLE Promo DISABLE KEYS */;
/*!40000 ALTER TABLE Promo ENABLE KEYS */;

--
-- Dumping data for table `Pseudos`
--

/*!40000 ALTER TABLE Pseudos DISABLE KEYS */;
INSERT INTO Pseudos (pse_id, pse_nom, pse_stock, pse_remise) VALUES (1,'article normal','1','1'),
(2,'pseudo générique','0','1'),
(3,'pseudo non remisable','0','0'),
(4,'pseudo remisable','0','0'),
(5,'article non remisable','1','0');
/*!40000 ALTER TABLE Pseudos ENABLE KEYS */;

--
-- Dumping data for table `Rayons`
--

/*!40000 ALTER TABLE Rayons DISABLE KEYS */;
INSERT INTO Rayons (ray_id, ray_secteur, ray_tva, ray_nom, ray_abrege, ray_ent, ray_sort, ray_ana, ray_fav) VALUES (1,1,0,'Port',NULL,0,0,0,0),
(2,4,0,'Romans',NULL,0,0,0,0),
(3,4,0,'Spiritualité',NULL,0,0,0,0),
(4,4,0,'Histoire',NULL,0,0,0,0),
(5,3,0,'Boulangerie',NULL,0,0,0,0),
(6,3,0,'Confiserie',NULL,0,0,0,0),
(7,3,0,'Légume',NULL,0,0,0,0),
(8,6,0,'Interne',NULL,0,0,0,0),
(9,2,0,'DVD',NULL,0,0,0,0),
(10,2,0,'CD classique',NULL,0,0,0,0),
(11,5,0,'Femme',NULL,0,0,0,0),
(12,5,0,'Homme',NULL,0,0,0,0);
/*!40000 ALTER TABLE Rayons ENABLE KEYS */;

--
-- Dumping data for table `Resume_commande_2022`
--

/*!40000 ALTER TABLE Resume_commande_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Resume_commande_2022 ENABLE KEYS */;

--
-- Dumping data for table `Resume_detail_2022`
--

/*!40000 ALTER TABLE Resume_detail_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Resume_detail_2022 ENABLE KEYS */;

--
-- Dumping data for table `Resume_jour_2022`
--

/*!40000 ALTER TABLE Resume_jour_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Resume_jour_2022 ENABLE KEYS */;

--
-- Dumping data for table `Resume_ticket_2022`
--

/*!40000 ALTER TABLE Resume_ticket_2022 DISABLE KEYS */;
/*!40000 ALTER TABLE Resume_ticket_2022 ENABLE KEYS */;

--
-- Dumping data for table `Secteurs`
--

/*!40000 ALTER TABLE Secteurs DISABLE KEYS */;
INSERT INTO Secteurs (sec_id, sec_nom, sec_abrege, sec_couleur, sec_ordre, sec_etat, sec_tva) VALUES (1,'Article à 0%','A0','c5baba',1,1,0),
(2,'Audio/Vidéo','AV','dcd7d7',2,1,3),
(3,'Alimentaire','AL','fffdfd',3,1,2),
(4,'Livres','L','e7e7e7',4,1,3),
(5,'Parfumerie','P','e2dddd',5,1,2),
(6,'Objets','O',NULL,6,1,3);
/*!40000 ALTER TABLE Secteurs ENABLE KEYS */;

--
-- Dumping data for table `Societe`
--

/*!40000 ALTER TABLE Societe DISABLE KEYS */;
INSERT INTO Societe (soc_id, soc_nom, soc_formesociale, soc_raisonsociale, soc_adr1, soc_adr2, soc_cp, soc_ville, soc_pays, soc_capital, soc_rcs, soc_tel, soc_fax, soc_siret, soc_ape, soc_banque, soc_rib, soc_cle, soc_tva, soc_iban, soc_bic) VALUES (1,'ASSOCIATION test','7','','lieu','rue','','','',0,'','','',' ','','','','','','','');
/*!40000 ALTER TABLE Societe ENABLE KEYS */;

--
-- Dumping data for table `Soldes`
--

/*!40000 ALTER TABLE Soldes DISABLE KEYS */;
/*!40000 ALTER TABLE Soldes ENABLE KEYS */;

--
-- Dumping data for table `Statut`
--

/*!40000 ALTER TABLE Statut DISABLE KEYS */;
INSERT INTO Statut (sta_id, sta_nom) VALUES (1,'En vente'),
(2,'Dépôts'),
(3,'Épuisé'),
(4,'Nouveau'),
(5,'Ponctuel'),
(6,'Trop vieux'),
(7,'Port achat');
/*!40000 ALTER TABLE Statut ENABLE KEYS */;

--
-- Dumping data for table `Stock_2021`
--

/*!40000 ALTER TABLE Stock_2021 DISABLE KEYS */;
/*!40000 ALTER TABLE Stock_2021 ENABLE KEYS */;

--
-- Dumping data for table `Tickets_2022`
--

/*!40000 ALTER TABLE Tickets_2022 DISABLE KEYS */;
INSERT INTO Tickets_2022 (tic_id, tic_num, tic_article, tic_quantite, tic_quantiteS, tic_prix, tic_prixS, tic_pht, tic_tt, tic_tva, tic_ntva, tic_cp, tic_devis) VALUES (1,314721999,12,0.89,0.89,3.50,3.50,2.00,3.12,5.50,2,0,0);
/*!40000 ALTER TABLE Tickets_2022 ENABLE KEYS */;

--
-- Dumping data for table `Titres`
--

/*!40000 ALTER TABLE Titres DISABLE KEYS */;
INSERT INTO Titres (tit_id, tit_nom, tit_recherche, tit_article, tit_niveau) VALUES (14,'Le collier rouge','le collier rouge',1,1),
(15,'Mes voyages avec Hérodote','mes voyages avec herodote',2,1),
(16,'Les chrétiens - Comment ils ont changé le monde','les chretiens - comment ils ont change le monde',3,1),
(17,'Tom Holland','tom holland',3,4),
(18,'Ryszard Kapuscinski','ryszard kapuscinski',2,4),
(19,'M2','m2',2,3),
(20,'L3','l3',3,3),
(22,'L1','l1',1,3),
(23,'PAIN 1KG','pain 1kg',4,1),
(24,'ConfitureMaison','confituremaison',5,1),
(25,'OIGNONS','oignons',11,1),
(26,'COURGETTES','courgettes',12,1),
(27,'TOMATES','tomates',13,1),
(28,'Noix fourrées','noix fourrees',14,1),
(29,'Lait (1 litre)','lait (1 litre)',15,1),
(30,'Lait (6x1)','lait (6x1)',16,1),
(31,'Port sans TVA','port sans tva',17,1),
(32,'Une terre promise','une terre promise',18,1),
(33,'Barack Obama','barack obama',18,4),
(34,'Un livre','un livre',6,1),
(35,'Un objet','un objet',7,1),
(36,'Rufin Jean-Christophe','rufin jean-christophe',1,4),
(37,'EAU DE PARFUM VAPORISATEUR N5','eau de parfum vaporisateur n5',21,1),
(38,'125530','125530',21,2),
(39,'Bleu de chanel','bleu de chanel',22,1),
(40,'107180','107180',22,2);
/*!40000 ALTER TABLE Titres ENABLE KEYS */;

--
-- Dumping data for table `Tva`
--

/*!40000 ALTER TABLE Tva DISABLE KEYS */;
INSERT INTO Tva (tva_id, tva_nom, tva_etat, tva_description, tva_couleur, tva_ordre, tva_code, tva_codeachat, tva_article) VALUES (0,0.00,1,'','caf6ee',1,0,NULL,1),
(1,2.10,1,'','b9e3df',2,32,NULL,2),
(2,5.50,1,'','c7f2ec',3,29,NULL,5),
(3,20.00,1,'','d4f0ec',5,30,NULL,7),
(4,10.00,0,'','a8d5db',4,31,NULL,6);
/*!40000 ALTER TABLE Tva ENABLE KEYS */;

--
-- Dumping data for table `Unites_vente`
--

/*!40000 ALTER TABLE Unites_vente DISABLE KEYS */;
INSERT INTO Unites_vente (unv_id, unv_nom, unv_abrege) VALUES (1,'Objet indivisible',''),
(2,'Kilogramme','Kg'),
(3,'Litre','Lit');
/*!40000 ALTER TABLE Unites_vente ENABLE KEYS */;

--
-- Dumping data for table `Utilisateurs`
--

/*!40000 ALTER TABLE Utilisateurs DISABLE KEYS */;
INSERT INTO Utilisateurs (uti_id, uti_nom, uti_fondF, uti_fondC, uti_titre, uti_surligne, uti_fonction, uti_etat, uti_pass, uti_email, uti_droits, uti_clavier) VALUES (1,'Administrateur','#ff7f20','#fff1b9','#dc7894','#fffa00',1,0,NULL,NULL,0,0),
(2,'Gestion','#cbcfb3','#0fcf00','#44ff00','#f5ff00',4,0,NULL,NULL,0,0),
(3,'Alimentaire','#9f5f00','#ffdfc3','#fe9d00','#ffffff',3,0,NULL,NULL,0,0),
(4,'LIVRES','#7f9d70','#c9d9d9','#f1f4cb','#0059bd',3,0,NULL,NULL,0,0),
(5,'Artisanat','#f7a382','#58d317','#faec63','#f7fe00',3,0,NULL,NULL,0,0),
(6,'Bernard','#562a0c','#e6ba85','#d42133','#f8df08',2,0,NULL,NULL,1,0),
(7,'Pierre','#f6ca3f','#19933b','#f87128','#ffe800',2,0,'81dc9bdb52d04dc20036dbd8313ed055',NULL,1,0),
(8,'Olivier','#16745e','#b8a99c','#5a7611','#d9e419',2,1,NULL,NULL,0,0),
(9,'Parfumerie','#33cc76','#c6f26d','#49c7ec','#f002b1',3,0,NULL,NULL,0,0);
/*!40000 ALTER TABLE Utilisateurs ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
