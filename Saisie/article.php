<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$filtre = filter_input(INPUT_GET, "f", FILTER_SANITIZE_STRING);
$req = filter_input(INPUT_GET, "id", FILTER_SANITIZE_STRING);
$uti = filter_input(INPUT_GET, "uti", FILTER_SANITIZE_STRING);
$nom = filter_input(INPUT_GET, "nom", FILTER_SANITIZE_STRING);
if ($req == "") {
    $req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
}
if ($req == "undefined") {
    //redirection anormal depuis la page d'envoi de la commande 
?>
    <script>
        $("#popup_g").empty();
        $("#popup_g").css("visibility", "hidden");
    </script>
    <img style="float:right" id="fermer" src="/images/annuler.png" onclick='$("#popup_g").empty();$("#popup_g").css("visibility","hidden");'>
<?php
    exit;
}
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";

connexobjet();
if (isset($_SESSION[$dossier])) {
    $_SESSION['aide_' . $_SESSION[$dossier]] = 'S1_2';
}
if ($filtre == 4) { //c'est un auteur
    $an = ANNEE;
    $req_ = utf8_decode($req);

    include $incpath . "Saisie/ventes_annuelles.php";

    $req_livresauteur = "CREATE TEMPORARY TABLE Livres_auteur AS SELECT art_id AS Vart_id, Vt4_nom  FROM Titres JOIN Vtit4 ON Vt4_recherche = tit_recherche JOIN Articles ON art_id = tit_article WHERE Vt4_article = '$req' AND tit_niveau = 4";
    $r_livresauteur = $idcom->query($req_livresauteur);
    if ($idcom->error) {
        echo "<br>" . $idcom->errno . " " . $idcom->error . "<br>";
    }

    $req_max = "SELECT MAX(ven_ct) as maxi                     
                    FROM Articles
                    LEFT JOIN Ventes ON ven_article = art_id
                    JOIN Editeurs ON edi_id = art_editeur
                    JOIN Editeur_serveur ON eds_editeur=edi_id
                        WHERE edi_id =$req
                            AND art_statut = '1'";
    $r_max = $idcom->query($req_max);
    $rq_max = $r_max->fetch_object();

    $req_recher = "SELECT Vt1_nom,
                        art_id,
                        Vt4_nom,
                        art_stk,
                        unv_abrege,
                        art_unite,
                        ven_ct,
                        rsc_etat,
                        com_quantite,
                        com_id,
                        SUM(com_quantite) AS com_quantite
                            FROM Livres_auteur
                            JOIN Vtit1 ON Vt1_article = Vart_id
                            JOIN Articles ON art_id = Vart_id
                            LEFT JOIN Ventes ON ven_article = art_id
                            JOIN Unites_vente ON unv_id = art_unite
                            LEFT JOIN Commandes_$an ON com_article=art_id
                            LEFT JOIN Resume_commande_$an ON rsc_id=com_numero
                                GROUP BY art_id";
    $r_recher = $idcom->query($req_recher);
    if ($idcom->error) {
        echo "<br>" . $idcom->errno . " " . $idcom->error . "<br>";
    }
    //$nb = $r_recher->num_rows;   
?>
    <script type="text/javascript" src="/js/jquery.tablesorter.js"></script>
    <style>
        table.tablesorter tbody td {
            height: 30px;
            font-size: 12px;
            text-align: left;
            color: #3D3D3D;
            padding: 4px;
            background-color: #f7ffd3;
            vertical-align: top;
        }

        table.tablesorter tbody tr.odd td {
            text-align: left;
            border-top: solid 1px;
            background-color: #f8f8f4;
        }

        table.tablesorter thead tr .headerSortDown,
        table.tablesorter thead tr .headerSortUp {
            background-color: #8dbdd8;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $("table").tablesorter();
        });
    </script>
    <?php
    $nb = $r_recher->num_rows;
    //if ($nb == 0) echo $req_recher;
    $resu = $r_recher->fetch_object();
    $r_recher->data_seek(0);
    $nom = "de " . $resu->Vt4_nom;
    include "liste_articles.php"; ?>
    </tbody>
    </table>
<?php
} else { //modif (id,tb,vl,cp,my)
?>
    <script>
        $(document).ready(function() {
            $('#detail_article input').on('keydown', function(event) {
                $(this).addClass('jaune');
                if (event.which == 13) {
                    if ($(this).attr('type') == 'text') {
                        if ($(this).attr('id') != 'rem') {
                            var id = $(this).attr('id').split('_');
                            // alert(id[1]+' '+$('#promo').val());
                            if ((id[1] == 34) && ($('#promo').val() == 1)) {
                                if ($(this).val() < $('#prix_mini').html()) {
                                    alert('On ne peut vendre l\'article à perte,\n minimum ' + $('#prix_mini').html());
                                    return 0;
                                }
                            }
                            if ($(this).val() != '') {
                                modif(id[2], id[1], this.value, id[0], '1');
                            }
                        }
                    }
                    $(this).removeClass('jaune');
                }
            });

            //saisie quantité commande
            $('#com_<?php echo $req ?>').on('keydown', function(event) {
                $(this).addClass('jaune');
                if (event.which == 13) {
                    if ($(this).val() != '') {
                        charge('/Saisie/commandes/insert', '&art=<?php echo $req ?>&qt=' + $(this).val(), 'mysql');
                    }
                    $(this).removeClass('jaune');
                }
            });

            $("#detail_article input[type=checkbox]").click(function() {
                if ($(this).prop('checked') == true) {
                    modif($('#article').html(), 11, 1, $(this).attr('id'), '1');
                } else {
                    modif($('#article').html(), 11, 0, $(this).attr('id'), '1');
                }
                setTimeout(function() {
                    charge('article', $('#article').html(), 'panneau_g')
                }, 300);
            });
        });

        function favoris(ac, id, vl) {
            if (ac == 0) {
                art = $('#article').html();
                charge('favoris_ins', art + '&vl=' + vl, 'favoris');
            } else {
                if (vl == 'Favoris') {
                    modif(id, 29, '', 'id', 2);
                } else {
                    modif(id, 29, vl, 'parent', 1);
                }
            }
        }
        // id,tb,vl,cp,my
        function promo() { //ligne 180 detail_article
            if ($('#promo').val() == 0) { //supression de la promo;
                charge('promo', 0 + '&art=' + $('#article').html(), 'prom');
            } else if ($('#promo').val() == 1) { //mis en promo;
                charge('promo', 1 + '&art=' + $('#article').html(), 'prom');
                setTimeout(function() {
                    charge('article', $('#article').html(), 'panneau_g')
                }, 300);
            } else if ($('#promo').val() == 2) { //mis en solde; ne peut être supprimé
                $('#prom').html('Le passage en Soldes est irréversible,<br> êtes-vous sûr de le vouloir ?<br><button onclick="charge(\'article\',$(\'#article\').html(),\'panneau_g\')">Annuler</button> <button onclick="charge(\'promo\', 2 + \'&art=\' + $(\'#article\').html(), \'prom\');">Soldes</button>');
            }
        }

        function depot(id) {
            if ($("#depot").val() == 2) {
                modif(id, 30, '', 'article', $("#depot").val());
            } else {
                modif('', 30, id, 'article', $("#depot").val());
            }
        }
        if ($('#popup_g').css('visibility') == 'visible') {
            $("#popup_g #fermer").css("visibility", "visible");
        } else if ($('#popup_g').css('visibility') == 'hidden') {
            $("#fermer").css("visibility", "hidden");
        }

        function sup() {
            if ($('#fermer').parent().attr('id') == 'popup_g') {
                charge('suppression', <?php echo $req ?>, 'popup_g');
            } else {
                charge('suppression', <?php echo $req ?>, 'panneau_g');
            }
        }
    </script>

    <img style="position:absolute;right:10px;" id="fermer" src="/images/annuler.png" onclick='$("#popup_g").empty();$("#popup_g").css("visibility","hidden");'>
    <?php
    if (($uti != '') && ($uti != $_SESSION[$dossier])) {
        //recherche de la protection par mot de passe de l'utilisateur
        $prot = $idcom->query("SELECT * FROM Utilisateurs WHERE uti_id = " . $uti . " AND uti_pass IS NOT NULL");
        if ($prot->num_rows == 0) {
            echo "<h3><img src='/images/attention.png'> Cet article existe mais il est du domaine de <big>" . $nom . "</big></h3>";
        } else {
            echo "<h3><img src='/images/attention.png'> Cet article existe mais il est du domaine de <big>" . $nom . "</big><br> Il est protégé par un mot de passe</h3>";
            exit;
        }
    }
    $_SESSION['article_' . $_SESSION[$dossier]] = $req;
    include "detail_article.inc.php";
    
    //------------------------historique de l'article------------------------------------
    //**********ventes sur un an*****************************
    $creation = substr($resu->art_creation, 0, 4);
    $an = ANNEE;
    ?>

    <div style='width:70%;float:right'>
    <?php
    if ($resu->art_pseudo == 2) {
        echo "<h3>Il n'y a pas d'historique sur les pseudos génériques</h3>";
        exit;
    }
    ?>
        <h3>Historique des ventes <span id="nbv"></span></h3>
        <div class="historique">
            <?php
            //recherche des ventes depuis l'historique défini
            $na = 0;

            for ($i = $config['debut']; $i <= date("Y"); $i++) { //on exclus les articles en comptes LENGTH(tic_num) < 10
                $req_vente = "SELECT SUM(tic_quantite) AS ct, art_unite FROM Tickets_$i JOIN Articles ON tic_article = art_id WHERE tic_article = $req AND LENGTH(tic_num) < 9 ";
                $r_vente = $idcom->query($req_vente);
                $rq_vente = $r_vente->fetch_object();

                $encompte = ($i == date("Y")) ? " (" . $art_encompte . ")" : "";
                $vente = ($rq_vente->art_unite == 1) ? sprintf('%d', $rq_vente->ct) : $rq_vente->ct;

                if ($rq_vente->ct > 0) {
                    echo "<b class='pointer' onclick=\"charge('vente_annuelle','" . $i . "&art=" . $req . "&typ=ven','panneau_d')\">" . $i . "</b> " . $vente . $encompte . "<br>";
                } else {
                    "<b>" . $i . "</b> " . $vente . $encompte . "<br>";
                }
                $na += $vente;
                if ($i == date("Y")) {
                    $vent_A = $vente;
                }
            }
            ?>
        </div>

        <h3>Historique des commandes <span id="nbc"></span></h3>
        <div class="historique">
            <?php
            $nc = 0;
            $com_A = 0;
            //recherche des ventes depuis l'historique défini
            for ($i = $config['debut']; $i <= date("Y"); $i++) {
                $req_vente = "SELECT SUM(com_quantite) AS ct, art_unite
                        FROM Commandes_$i
                        JOIN Articles ON art_id = com_article
                        JOIN Resume_commande_$i ON rsc_id = com_numero
                            WHERE com_article = $req AND rsc_etat = 3";
                $r_vente = $idcom->query($req_vente);
                $rq_vente = $r_vente->fetch_object();

                $ctvente = ($rq_vente->art_unite == 1) ? sprintf('%d', $rq_vente->ct) : $rq_vente->ct;

                if ($ctvente > 0) {
                    echo "<b class='pointer' onclick=\"charge('vente_annuelle','" . $i . "&art=" . $req . "&typ=com','panneau_d')\">" . $i . "</b> " . $ctvente . "<br>";
                } else {
                    "<b>" . $i . "</b> " . $ctvente . "<br>";
                }
                $nc += $ctvente;
                if ($i == date("Y")) {
                    $com_A = $ctvente;
                }
                if ($com_A == '') {
                    $com_A = 0;
                }
            }

            if ($resu->stk_stk == "") {
                $stk = 0;
            } elseif ($resu->art_unite == 1) {
                $stk = sprintf('%d', $resu->stk_stk);
            } else {
                $stk = $resu->stk_stk;
            }
            // echo "<br>C ".$com_A."<br>V ".$vent_A."<br>Stk ".$stock."<br>Stk1 ".$stk."";
            $resultat = $com_A - ($vent_A + $stock - $stk);
            $err = ($resultat != 0) ? ' style="background-color:red"' : '';
            //recherche si commande en cours
            $req_com = "SELECT com_quantite, rsc_etat
                    FROM Commandes_" . ANNEE . "
                    JOIN Resume_commande_" . ANNEE . " ON rsc_id = com_numero
                        WHERE com_article = $req
                        AND rsc_etat < 3";
            $r_com = $idcom->query($req_com);
            $etat = 0;
            $com_quantite = "";
            if ($r_com->num_rows > 0) {
                $rq_com = $r_com->fetch_object();
                $com_quantite = ($resu->art_unite == 1) ? sprintf('%d', $rq_com->com_quantite) : $rq_com->com_quantite;
                $etat = $rq_com->rsc_etat;
            }
            if ($etat == 2) {
            ?>
                <label>En réception : <span class='mille'><?php echo $com_quantite ?></span></label>
            <?php
            } else {
            ?>
                <label>Commander : <input type='text' class='mille' id='com_<?php echo $req ?>' value='<?php echo $com_quantite ?>'></label>
            <?php
            }

            ?>
        </div>
    </div>
    <div style='width:30%;float:left'>
        <h3>Analyse <?php echo date("Y") ?></h3>
        <table class="droite" width="80%">
            <TR>
                <TH>Commandé</TH>
                <TD><?php echo $com_A ?></TD>
            </TR>
            <?php
            if ($resu->art_unite == 1) {
            ?>
                <TR>
                    <TH>Vendu</TH>
                    <TD><?php echo sprintf("%d", ($vent_A)) ?></TD>
                </TR>
                <TR>
                    <TH>Stock actuel</TH>
                    <TD><?php echo $stock ?></TD>
                </TR>
                <TR>
                    <TH>Stock <?php echo (date("Y") - 1) ?></TH>
                    <TD><?php echo sprintf("%d", $stk) ?></TD>
                </TR>
            <?php
            } else {
            ?>
                <TR>
                    <TH>Vendu</TH>
                    <TD><?php echo $vent_A ?></TD>
                </TR>
                <TR>
                    <TH>Stock actuel</TH>
                    <TD><?php echo $stock ?></TD>
                </TR>
                <TR>
                    <TH>Stock <?php echo (date("Y") - 1) ?></TH>
                    <TD><?php echo $stk ?></TD>
                </TR>
            <?php
            }
            ?>
            <TR <?php echo $err ?>>
                <TH>Resultat</TH>
                <TD><?php echo ($resultat) ?></TD>
            </TR>
        </table>
    </div>
    <div id="graph_vente" style="text-align:center;clear:both">

        <?php
        if (($na == 0) && ($nc == 0) && ($resu->art_stk == 0)) {
        ?>
            <p>Cet article n'a pas d'historique, vous pouvez le mettre en liste noir en changeant son statut ou le supprimer définitivement.</p>
            <center><button onclick="sup()">Supprimer définitivement</button></center>
        <?php
        } else {
        ?>
            <?php
            //recherches des ventes sur un an
            include "ventes.svg.php"; ?>

            <script>
                $("#nbv").html('(<?php echo $na ?>)')
                $("#nbc").html('(<?php echo $nc ?>)')
            </script>
        <?php
        }
        if (($pilon == 0) && ($stock != 0)) {
        ?>
    </div>
    <center><input type="button" value='Mettre au pilon' onclick="charge('commandes/insert_pilon',<?php echo $req ?>,'graph_vente');$(this).css('visibility','hidden')"></center>
<?php
        }
    }
?>
<script>
    $("#panneau_g").height($("#affichage").height() - 10);
    if ($("#liste").height() > 10) {
        $('#popup_g').css('backgroundColor', '<?php echo $_SESSION['fondC_' . $_SESSION[$dossier]] ?>');
        $('#popup_g').height($("#affichage").height() - 100);
        imgleft = $('#popup_g').width() - 50 + 'px';
        $('#fermer').css("margin-left", imgleft);
        $('#fermer').css('display', 'block');
    }
</script>