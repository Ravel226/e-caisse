<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "query", FILTER_SANITIZE_STRING);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
// print_r($_SESSION);
$an=ANNEE;
//commandes vides
$req_recher="(select edi_nom,
                    rsc_etat,
                    rsc_date,
                    rsc_id,
                    0 AS `Vat_nbarticle`,
                    com_numero,
                    3 AS `Vat_tht`
                        FROM Resume_commande_$an
                        LEFT JOIN Commandes_$an ON com_numero = rsc_id
                        JOIN Editeurs ON rsc_serveur = edi_id
                            WHERE rsc_util = $_SESSION[$dossier]
                            AND rsc_etat = 1
                            AND com_numero IS NULL
                                ORDER BY rsc_id)
                UNION ALL
                (select edi_nom,
                rsc_etat,
                rsc_date,
                com_numero,
                SUM(`com_quantite`) AS `Vat_nbarticle`,
                SUM(`com_quantite` * `com_ttc`) AS `Vat_ttc`,
                SUM(`com_quantite` * `Articles`.`art_pht`) AS `Vat_tht`
                    from `Commandes_$an`
                        JOIN `Articles` on `art_id` = `com_article`
                        JOIN Resume_commande_$an ON rsc_id = com_numero
                        JOIN Editeurs ON rsc_serveur = edi_id
                            WHERE com_utilisateur = $_SESSION[$dossier]
                                AND com_article != 1
                                AND rsc_etat = 1 group by `com_numero`
                                    ORDER BY rsc_id)";
$r_recher=$idcom->query($req_recher);
$nb = $r_recher->num_rows;
if ($nb == 0) {
    ?>
    <script>$("#panneau_d").empty();</script>
    <h3>Pas de commande en attente</h3>
    <?php
    exit;
}
?>
<h3 id="liste"><?php echo $nb?> Commandes en attente d'envoi</h3>
<style>
table#attente tr{
height : 25px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
    $('#attente td').click(function(){
    if ($(this).html().slice(0,8) == '<img src') {
        if ($(this).parent().attr('qt') == '0') {
            if (confirm("Voulez vraiment supprimer la commande "+$(this).parent().attr('id'))) {
                    modif($(this).parent().attr('id'),23,'','id',2);
                    setTimeout(function(){force('commandes/attente','','panneau_g')},300);
            }

        } else {
            if (confirm("Voulez vraiment supprimer la commande "+$(this).parent().attr('id')+" avec se(s) article(s)")) {
                    modif($(this).parent().attr('id'),23,'','id',2);
                    modif($(this).parent().attr('id'),16,'','numero',2);
                    setTimeout(function(){force('commandes/attente','','panneau_g')},300);
                }
            }

        } else {
        $('#attente td').css('fontWeight','normal');
        $(this).css('fontWeight','bold');
        charge('commandes/commande',$(this).parent().attr('id'),'panneau_d') ;
        }
    });
    $("#panneau_g").height($("#affichage").height()-10);
    });

$("#panneau_d").empty();
</script>
<table id="attente" style='width:90%;margin-left:5%' class="tablesorter">
<thead>
<tr><TH>N°</TH><TH>Date</TH><TH>Serveurs</TH><TH>Nb art.</TH><TH>Valeur HT</TH></tr>
</thead>
<tbody>
<?php
$n=0;
$tt_articles=0;
while ($resu=$r_recher->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    $nb_articles = sprintf("%d", $resu->Vat_nbarticle);
    $val = ($resu->Vat_tht == 3)? '':monetaireF($resu->Vat_tht);
    echo '<tr style="background-color:'.$coul.'" id="'.$resu->rsc_id.'" qt="'.$nb_articles.'" style="cursor:pointer">
    <td><img src="/images/button_drop.png"> '.$resu->rsc_id.'</td>
    <td>'.dateFR($resu->rsc_date).'</td><td>'.$resu->edi_nom.'</td>
    <td>'.$nb_articles.'</td>
    <td style="text-align:right">'.$val.'</td></tr>';
    $tt_articles += $nb_articles;
    $n++;
}
?>
</table>
<?php
if (($_SESSION[$dossier] == 1)&&($tt_articles > 0)) {
    echo
    '<a href="commandes/liste_livres.php?req=<?php echo $req?>"><button style="margin:20px">Liste CSV pour tous les livres en attente</button></a>';
}

