<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
$conf= filter_input(INPUT_GET, "conf", FILTER_SANITIZE_STRING);
if (!$conf) {
    ?>
    <center><img src='/images/attention.png'>Êtes-vous sûr de vouloir mettre cet article au pilon.<br>
    <input type="button" value='Confirmer' onclick="charge('commandes/insert_pilon','<?php echo $req?>&conf=ok','graph_vente')"></center>
    <?php
} elseif ($conf =='ok') {
        include $incpath."mysql/connect.php";
        connexobjet();
        include $incpath."php/fonctions.php";
        
        //recherche de l'editeur Pilon
        $req_pilon = 'SELECT edi_id FROM Editeurs WHERE edi_nom ="Pilon"';
        $r_pilon = $idcom->query($req_pilon);
        if ($r_pilon->num_rows == 0) {
            $idcom->query('INSERT INTO Editeurs (edi_nom) VALUES("Pilon")');
            $pilon = $idcom->insert_id;
        } else {
            $rq_pilon =$r_pilon->fetch_object();
            $pilon = $rq_pilon->edi_id;
        }
        

        //principe : les articles sont mis en négatif dans une unique commande annuelle
        //qui est validé comme reçue à la création
        //on recherche la commande de l'année, s'il n'y en a pas on la crée
        echo $req_com = "SELECT * FROM Resume_commande_".ANNEE." WHERE rsc_serveur = ".$pilon;
        echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em>";
        $r_com = $idcom->query($req_com);
        if ($r_com->num_rows == 0) {
            $insert = "INSERT INTO Resume_commande_".ANNEE." (rsc_date, rsc_serveur, rsc_etat, rsc_util) VALUES(NOW(),$pilon,3,29)";
            $r_com = $idcom->query($insert);
            $id_com = $idcom->insert_id;
        // echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em>";
        } else {
            $rq_com =$r_com->fetch_object();
            $id_com = $rq_com->rsc_id;
        }
        //insertion de l'article
        $req_art = "INSERT INTO Commandes_".ANNEE." (com_utilisateur,com_numero, com_article, com_pht, com_quantite, com_ttc, com_remise, com_tva) 
    SELECT 29,".$id_com.",".$req.",art_pht, (art_stk*-1), art_ttc, art_remise, art_tva FROM Articles WHERE art_id = ".$req;
        $r_art = $idcom->query($req_art);
        //ajustement du stock et du statut de l'article
        $req_stk = 'UPDATE Articles SET art_stk = 0, art_statut = 0 WHERE art_id = '.$req;
        $r_stk = $idcom->query($req_stk); ?>
    <script>
    charge('article',<?php echo $req?>,'panneau_g');
    </script>
    <?php
    }
