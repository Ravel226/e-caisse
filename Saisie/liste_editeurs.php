<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}

$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";

$req_recher="SELECT edi_nom, eds_editeur, edi_etat FROM Editeur_serveur
                        JOIN Editeurs ON eds_editeur = edi_id
                          WHERE edi_utilisateur = ".$_SESSION[$dossier]."
                          AND edi_etat = 0
                            ORDER BY edi_nom ";
$r_recher=$idcom->query($req_recher);
$nb = $r_recher->num_rows;
?>
<script>
$(document).ready(function() {
    $('#liste_ed button').on('click',function(){
        if ($('#liste_ed').parent().attr('id')=='popup_g') {
            charge('detail_editeur','000','panneau_d');
            $('#popup_g').css('visibility','hidden');
        }
    });
});
</script>
<img style="float: right; margin: 0 0 0 20px;" id="fermer" src="/images/annuler.png" onclick='$("#popup_g").css("visibility","hidden");$("#popup_g").empty()'>
<script>function aller(id){
modif( '<?php echo $req?>',11,id,'editeur',1);
    charge('article',<?php echo $req?>,'panneau_g');
    $('#popup_g').css('visibility','hidden');
    }
</script>
<style>
#generique tbody td {
    cursor : pointer;
}
</style>
<div id="liste_ed" style="float:left;width:400px">
<table class="generique"><TR><TH>Choisissez l'éditeur</TH></TR>
<tr>
<thead>
<TD><button>Nouvel éditeur</button></TD><td></td></tr>
<tr><td></td><td></td></tr>
</thead>
<tbody>
<?php
$n=0;
while ($resu=$r_recher->fetch_object()) {
    $coul=($n%2 == 0)?$coulCC:$coulFF;
    echo '<tr style="background-color:'.$coul.'"><td>'.$resu->edi_nom.' ('.$resu->eds_editeur.")</td>
    <td><img src='../images/valider.png' width='20' height='20' onclick=\"aller(".$resu->eds_editeur.")\" ></td></tr>\n";
    $n++;
}
?>
</tbody>
</table>
</div>
<script>$('#popup_g').css('visibility','visible');
$('#liste_ed').height($('#panneau_g').height() -60)
</script>
