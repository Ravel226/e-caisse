<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$filtre = filter_input(INPUT_GET, "f", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$serveur = filter_input(INPUT_GET, "serveur", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

// if(isset($filtre)) {
//     $comp = "&f=serveur";
// }
$supression = '';
$serveurauto = '';
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
require $incpath."php/config.php";
connexobjet();
if (isset($serveur)) {
    //modification du serveur, et des commandes s'il en existe
    $req_update = "UPDATE Editeur_serveur SET eds_serveur = $serveur WHERE eds_editeur = $req";
    $r_update = $idcom->query($req_update);
    //recherche de la date de création de l'éditeur et mise à jours des tables resumes_commandes

}
if ($req == "000") {
    $req_editeur = "INSERT INTO Editeurs(edi_nom,edi_utilisateur,edi_type,edi_etat) VALUES('Nouveau'," . $_SESSION[$dossier] . ",0,1)";
    $r_editeur = $idcom->query($req_editeur);
    $n_editeur = $idcom->insert_id;
    $req_editeur = 'INSERT INTO Editeur_serveur(eds_editeur,eds_serveur,eds_remise) VALUES(' . $n_editeur . ',' . $n_editeur . ',0.00)';
    $r_editeur = $idcom->query($req_editeur);
    $req = $n_editeur;
}
$req_recher = "SELECT Editeurs.*, Vadi_ct, eds_id, eds_serveur, eds_remise,
                   (SELECT COUNT(eds_id) FROM Editeur_serveur WHERE eds_serveur = $req GROUP BY eds_serveur) AS ctE
                    FROM Editeurs
                      LEFT JOIN Varticles_editeurs ON Vadi_editeur=edi_id
                      LEFT JOIN Editeur_serveur ON eds_editeur=edi_id 
                        WHERE edi_id = $req";
$r_recher = $idcom->query($req_recher);
$resu = $r_recher->fetch_object();
//echo $resu->edi_id." = ".$resu->eds_serveur;
//si  $resu->edi_id = $resu->eds_serveur, il est son propre serveur;
$id_edi = ($resu->edi_id == $resu->eds_serveur) ? $resu->eds_serveur : $resu->edi_id;
echo "<br>" . $id_edi;
if ($id_edi == '') {
    $id_edi = $req;
}
if ($resu->Vadi_ct != 0) {
    $lien = ' (' . $resu->Vadi_ct . ' articles) <img onclick="charge(\'articles_editeur\',' . $resu->edi_id . ',\'art_edi\')" src="/images/voir.png" width="20" height="20" />';
} else {
    $lien = '';
    //on donne la possibilité de supprimer
    $supression = '<button onclick="">Supprimer cet éditeur</button>';
}
/*  on recherche les éditeurs associés */
// echo $resu->ctE;
if ($resu->ctE > 1) {
    $ctE = " <button onclick='charge(\"editeurs\",\"E" . $id_edi . "\",\"panneau_g\")'>" . $resu->ctE . " éditeurs associés</button> ";
    $supression = '';
} else {
    $ctE = '';
}

?>
<style>
    .jaune {
        background-color: yellow;
    }
</style>
<script>
    function uti(val) {
        var pass = val.split('_');

        if (pass[1] == 1) {
            $('#protege').css('display', 'block');
        } else {
            $('#protege').css('display', 'none');
            // charge('change_domaine',this.value+'&edi=".$req."','mysql');
        }
    }
    $(document).ready(function() {
        $('#editeur input').on('keydown', function(event) {
            $(this).addClass('jaune');
            $id = '';
            if (event.which == 13) {
                alert($(this).attr('id'));
                if ($(this).attr('id') == 'password') {
                    //on vérifie la validité du mot de pass
                    //             $.ajax({ 
                    //             type: "POST",
                    //             url: "/verif_pass.php",
                    //             data : "nom="+nom+"&pass="+pass,
                    //             dataType : "html",
                    //             //affichage de l'erreur en cas de problème
                    //             error:function(msg){
                    //             alert( "erreur non définie");
                    //             },
                    //             success:function(data) {
                    //             $("#connexion").append(data);
                    //             }
                    // });
                    alert($(this).val());
                }
                if ($(this).attr('type') == 'text') {
                    if ($(this).attr('id') == 'remise') {
                        <?php
                        if ($resu->eds_id != '') { //serveur
                            ?>
                            modif(<?php echo $resu->eds_id ?>, 14, $(this).val(), 'remise', 1);
                            <?php
                        }
                        ?>
                    } else { //editeur
                        modif(<?php echo $resu->edi_id ?>, 13, $(this).val(), $(this).attr('id'), 1);
                    }

                    $(this).removeClass('jaune');
                }
            }
        });

    });
</script>
<h3><?php echo "(N° " . $id_edi . ") " . $resu->edi_nom . $lien . $ctE ?></h3>
<table id='editeur' style="margin-top:15px">
    <TR>
        <Th>Code Client</Th>
        <TD><input id='client' class="_demi" type="text" value="<?php echo $resu->edi_client ?>"><?php echo $resu->edi_creation ?></TD>
    </TR>
    <TR>
        <Th>Nom</Th>
        <TD><input id='nom' type="text" value="<?php echo $resu->edi_nom ?>"></TD>
    </TR>
    <TR>
        <Th>Adresse 1</Th>
        <TD><input id='adr1' type="text" value="<?php echo $resu->edi_adr1 ?>"></TD>
    </TR>
    <TR>
        <Th>Adresse 2</Th>
        <TD><input id='adr2' type="text" value="<?php echo $resu->edi_adr2 ?>"></TD>
    </TR>
    <TR>
        <Th>Code postal</Th>
        <TD><input id='cp' class="_demi" type="text" value="<?php echo $resu->edi_cp ?>"></TD>
    </TR>
    <TR>
        <Th>Ville</Th>
        <TD><input id='ville' type="text" value="<?php echo $resu->edi_ville ?>"></TD>
    </TR>
    <TR>
        <Th>Téléphone</Th>
        <TD><input id='tel' class="_demi" type="text" value="<?php echo $resu->edi_tel ?>"></TD>
    </TR>
    <TR>
        <Th>Fax</Th>
        <TD><input id='fax' type="text" class="_demi" value="<?php echo $resu->edi_fax ?>"></TD>
    </TR>
    <TR>
        <Th>Contact</Th>
        <TD><input id='contact' type="text" value="<?php echo $resu->edi_contact ?>"></TD>
    </TR>
    <TR>
        <Th>Email contact</Th>
        <TD><input id='mail' type="text" value="<?php echo $resu->edi_mail ?>"></TD>
    </TR>
    <TR>
        <Th>Email commande</Th>
        <TD><input id='commande' type="text" value="<?php echo $resu->edi_commande ?>"></TD>
    </TR>
    <TR>
        <Th>Site web</Th>
        <TD><input id='site' type="text" value="<?php echo $resu->edi_site ?>">
            <?php
            if ($resu->edi_site) {
                echo "<img src='/images/web.png' width='20' height='20' onclick=\"window.open('http://" . $resu->edi_site . "')\">";
            }
            ?>
        </TD>
    </TR>
    <TR>
        <Th>État</Th>
        <TD>
            <?php
            $sel = ($resu->edi_etat == 1) ? "selected" : "";
            ?>
            <select onchange="modif (<?php echo $req ?>,13,this.value,'etat','1')">
                <option value="0">Actif</option>
                <option value="1" <?php echo $sel ?>>Inactif</option>
            </select>
        </TD>
    </TR>
    <TR>
        <Th>Type</Th>
        <TD>
            <?php
            $sel = ($resu->edi_type == 1) ? "selected" : "";
            ?>
            <select onchange="modif (<?php echo $resu->edi_id ?>,13,this.value,'type','1')">
                <option value="0">Commande</option>
                <option value="1" <?php echo $sel ?>>Mouvement de stock</option>
            </select>
        </TD>
    </TR>
    <?php
    if ($filtre != "serveur") {
        ?>
        <Th>Serveurs</Th>
        <TD>
            <select onchange="modif (<?php echo $req ?>,14,this.value,'serveur','1')">
                <option value="<?php echo $resu->edi_id ?>">Lui même</option>
                <?php
                $req_serveur = "SELECT edi_nom, edi_type, edi_id, eds_id, eds_serveur, eds_editeur, COUNT(eds_editeur) AS ctE, edi_code, uti_nom, uti_fonction, edi_utilisateur
                    FROM Editeurs
                        LEFT JOIN Editeur_serveur ON eds_serveur=edi_id
                        JOIN Utilisateurs ON uti_id = edi_utilisateur
                        WHERE edi_utilisateur = " . $_SESSION[$dossier] . "
                        GROUP BY eds_serveur
                            ORDER BY edi_nom";
                $r_serveur = $idcom->query($req_serveur);
                $utilisateur = '';
                $info = '';
                while ($rq_serveur = $r_serveur->fetch_object()) {
                    if ($resu->eds_serveur == $rq_serveur->eds_serveur) {
                        $sel = "selected";

                        if ($rq_serveur->edi_id == $resu->edi_id) {
                            //il est son propre serveur
                            $serveurauto = 1;
                            // echo $rq_serveur->edi_id." == ".$resu->edi_id;
                        }
                    } else {
                        $sel = "";
                    }
                    echo "<option value='" . $rq_serveur->edi_id . "'" . $sel . ">" . $rq_serveur->edi_nom . "</option>\n";
                    $fonction = $rq_serveur->uti_fonction;
                } ?>
            </select>
        </TD>
        </TR>
        <tr>
            <th>Domaine</th>
            <td>
                <?php
                if ($serveurauto == 1) {
                    //il est sont propre serveur, on modifie directement
                    $info = ''; ?>
                    <select onchange="modif (<?php echo $resu->edi_id ?>,14,this.value,'utilisateur','1')">
                    <?php
                } else {
                    //on affiche une info
                    $info = "<tr><td colspan=2>Cet éditeur passe par un serveur. Au changement de domaine, il sera indiqué comme indépendant. Il faudra l'associer à un serveur s'il ne l'est pas lui-même.</td></tr>";
                    echo "<select onchange=charge('change_domaine',this.value+'&edi=" . $req . "','mysql')>";
                    //changement de domaine, changement dans Editeurs_serveur
                }
                $req_domaine = "SELECT uti_nom, uti_id, uti_pass FROM Utilisateurs
                                        JOIN Fonctions ON fon_id = uti_fonction
                                        WHERE fon_id = " . $fonction . "
                                        ORDER BY Uti_nom";
                $r_domaine = $idcom->query($req_domaine);
                while ($rq_domaine = $r_domaine->fetch_object()) {
                    $sel = ($rq_domaine->uti_id == $_SESSION[$dossier]) ? ' selected' : '';
                    if (($rq_domaine->uti_pass != '') && ($rq_domaine->uti_id != $_SESSION[$dossier])) {
                        echo "<option value='" . $rq_domaine->uti_id . "_1'" . $sel . ">" . $rq_domaine->uti_nom . "</option>\n";
                    } else {
                        echo "<option value='" . $rq_domaine->uti_id . "_0'" . $sel . ">" . $rq_domaine->uti_nom . "</option>\n";
                    }
                } ?></select>
            </td>
        </tr>
        <tr>
            <th></th>
            <td></td>
        </tr>
        <tr style='display:none' id="protege">
            <th>Compte protégé<br>mot de passe</th>
            <td><input id='password' type='password'></input></td>
        </tr>
        <?php
        // echo $serveurauto;
        echo $info;
        if ($resu->edi_type == 0) {
            ?>
            <TR>
                <TH>Remise</TH>
                <TD><input id='remise' class="_demi" type="text" value="<?php echo $resu->eds_remise ?>">%</TD>
            </TR>
            <?php
        }
    } else { //propre aux serveurs : port et tva_code
        $req_tva = 'SELECT * FROM Tva WHERE tva_etat = "1"';
        $r_tva = $idcom->query($req_tva);
        echo "<tr><th>Port</th>";
        echo "<td><select onchange=\"modif($resu->edi_id,13,this.value,'port','1')\">";
        while ($rq_tva = $r_tva->fetch_object()) {
            $tva = ($rq_tva->tva_id == $resu->edi_port) ? " selected" : "";
            echo '<option value="' . $rq_tva->tva_id . '"' . $tva . '>' . $rq_tva->tva_nom . '</option>';
        }
        "</select></td></tr>";
        echo "<tr><th>Code Éditeur</th><td><input id='code' class=\"_demi\" type=\"text\" value='" . $resu->edi_code . "'></td></tr>";
    }
    ?><tr>
        <td colspan=2><?php echo $supression ?></td>
    </tr>
</table>
<hr>
<div id="art_edi"></div>
<?php
$editeur = $resu->edi_id;
// if ($resu->edi_id == $resu->eds_serveur) $filtre="serveur";
// print_r($_SESSION);
$tt = 0.00;
$tableau = '';
if ($resu->edi_id == $resu->eds_serveur) {
    //on recherche l'historique des commandes
    for ($i = date('Y'); $i >= $config['debut']; $i--) {
        $req_commande = "SELECT COUNT(rsc_serveur) as CT FROM Resume_commande_$i WHERE rsc_serveur = $editeur
                GROUP BY rsc_serveur";
        $r_commande = $idcom->query($req_commande);
        if ($r_commande->num_rows > 0) {
            $resu = $r_commande->fetch_object();
            $tableau .= "<b>" . $i . "</b> " . $resu->CT . "<br>";
            $tt = $resu->CT + $tt;
        }
    }
    if ($tt != 0) {
        echo "<h3>" . $tt . " Commandes</h3>";
        echo "<div style='-moz-columns: 3;column-rule: 3px solid rgba(0,0,0,.4)'>" . $tableau . "</div>";
    }
    exit;
    if (($ctE == "") && ($tt == 0)) {
        ?>
        Pas d'article ni de commande pour ce serveur, il pourrait-être supprimé
        <img onclick="suppression( '<?php echo $editeur ?>',13,'','',2);setTimeout(function(){charge('editeurs','','panneau_g')},300);" src="../images/supprimer.png" width="20" height="20">
        <?php
    }
}
?>