<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p,$i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
//
$req_Vserveur = "CREATE TEMPORARY TABLE Vserveurs AS select edi_id AS Vser_id,edi_nom AS Vser_nom,count(edi_id) AS Vser_ct from Editeurs join Editeur_serveur on eds_serveur = edi_id WHERE edi_utilisateur = ".$_SESSION[$dossier]." group by eds_serveur";
$r_Vserveur = $idcom->query($req_Vserveur);
// echo $r_Vserveur->num_rows;

$req_recher="SELECT edi_nom, eds_editeur, edi_etat, COUNT(art_id) AS ct, Vser_nom, Vser_ct, eds_serveur
                                        FROM Editeur_serveur 
                                            JOIN Editeurs ON eds_editeur = edi_id 
                                            JOIN Vserveurs ON Vser_id = eds_serveur 
                                            LEFT JOIN Articles ON art_editeur = edi_id
                                                WHERE edi_utilisateur = ".$_SESSION[$dossier]."
                                                AND edi_etat= 0
                              GROUP BY edi_id
                                ORDER BY Vser_nom, edi_nom ";
$r_recher=$idcom->query($req_recher);
// $resu=$r_recher->fetch_object();
$nb = $r_recher->num_rows;

?>
<script>
$(document).ready(function() {
    $('#liste_editeur td').on('click',function(){
        if ($(this).hasClass('pointer')) {
        charge('articles_editeur',$(this).attr('id')+"&nb="+$(this).attr('nb'),'panneau_d');
        }        
    });
});
</script>
<h3 id="liste"><?php echo $nb?> éditeurs actifs</h3>

<table id="liste_editeur" cellpadding="0" cellspacing="0" border="1" class="tablesorter">
<thead><tr><TH>Serveur</TH><TH>Éditeurs</TH><TH>Nb d'articles</TH></tr></thead>
<tbody>
<?php
$n=0;
$n_serveur = 0;
$serveur = "";
while ($resu=$r_recher->fetch_object()) {
    if ($serveur != $resu->Vser_nom) {
        $n_serveur++;
        $nom_s= $resu->Vser_nom;
    } else {
        $nom_s ='';
    }
    if ($n_serveur == 1) {
        $coul_s=' rowspan="'.$resu->Vser_ct.'", class="serveur1"';
    } elseif ($n_serveur == 2) {
        $n_serveur = 0;
        $coul_s=' rowspan="'.$resu->Vser_ct.'", class="serveur2"';
    }

    if ($n%2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }
    if ($resu->edi_etat == '1') {
        $coul = $_SESSION['surligne_'.$_SESSION[$dossier]];
    }
    // echo '<tr><td'.$coul_s.'>'.$nom_s.'</td><td id="'.$resu->eds_editeur.'" nb="'.$resu->ct.'" class="pointer" style="background-color:'.$coul.'"</td>'.$resu->edi_nom.'<td>'.$resu->ct.'</td></tr>';
    echo '<tr>';
    if($nom_s !='') echo '<td'.$coul_s.'>'.$n.$nom_s.'</td>';
    echo '<td id="'.$resu->eds_editeur.'" nb="'.$resu->ct.'" class="pointer" style="background-color:'.$coul.'"</td>'.$resu->edi_nom.'<td>'.$resu->ct.'</td></tr>';
    $serveur= $resu->Vser_nom;
    $n++;
}
?></tbody>
</table>
<script>
$("#panneau_g").height($("#affichage").height()-10);
</script>
