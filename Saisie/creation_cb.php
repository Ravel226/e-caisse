<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$edi= filter_input(INPUT_GET, "edi", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
if ((strlen($req) != 13) &&(!is_numeric($req))) {
    echo "erreur de saisie";
} else {
    //vérification de la validité
    ean13($req);
    //recherche si l'article existe déjà si oui on redirige, si non on crée
    $req_recher="SELECT art_id FROM Articles WHERE art_cb =  $req";
    $r_recher=$idcom->query($req_recher);
    // echo	$r_recher->num_rows;
    if ($r_recher->num_rows == 1) {
        ?>
        <script>
        charge('rechercheCB',<?php echo $req?>,'panneau_g');
        </script>
        <?php
    } else {
        if (isset($edi)) {//l'editeur à été sélectionne
            if ((substr($req, 0, 3) == '978')||(substr($req, 0, 3) == '979')) {
                $art_tva = 2;
            }//5.5%
            else {
                $art_tva = 3;
            }//20%
            $ch = "INSERT INTO Articles (art_cb, art_creation,art_statut,art_pseudo,art_unite,art_stk,art_tva,art_ttc,art_remise,art_pht,art_seuil,art_editeur)
                            VALUES($req,NOW(),0,1,1,0,$art_tva,0.00,0.00,0.00,0,".$edi.")";
            $idcom->query($ch);
            // 	$idcom->errno." ".$idcom->error;
            $art_id = $idcom->insert_id;
            //insertion d'un titre
            $ch = "INSERT INTO Titres (tit_nom,tit_recherche, tit_niveau, tit_article) VALUES('Nouveau','Nouveau',1,$art_id)";
            $idcom->query($ch); ?>
            <script>
            charge('article',<?php echo $art_id?>,'panneau_g');
            </script>
            <?php
        } else {
            include('analyse_cb.php');
            // 		print_r(checkCB($req));
            // 		exit;
            $debut = '';
            $n_editeur = checkCB($req)['editeur'];
            $groupe = checkCB($req)['groupe'];
            $recherche = checkCB($req)['recherche'];
            if ((substr($req, 0, 3) == '978')||(substr($req, 0, 3) == '979')) {// c'est un livre, art_tva = 2
                $art_tva = 2;//5.5%
                $groupe.$debut == 978;
            } else {
                $art_tva = 3;//20%
                $debut == "";
            }
            //recherche de l"diteur
            $req_editeur="SELECT art_editeur, edi_nom, edi_id FROM Articles JOIN Editeurs ON edi_id = art_editeur WHERE art_cb LIKE '$recherche%' GROUP BY art_editeur";
            $r_editeur=$idcom->query($req_editeur);
            // 			echo $r_editeur->num_rows;exit;
            if ($r_editeur->num_rows == 0) {
                //creation d'un article 'vierge'
                echo $ch = "INSERT INTO Articles (art_cb, art_creation,art_statut,art_pseudo,art_unite,art_stk,art_tva,art_ttc,art_remise,art_pht,art_seuil)
                                    VALUES($req,NOW(),0,1,1,0,$art_tva,0.00,0.00,0.00,0)";
                $idcom->query($ch);
                // 	$idcom->errno." ".$idcom->error;
                $art_id = $idcom->insert_id;
                //insertion d'un titre
                /*echo*/ $ch = "INSERT INTO Titres (tit_nom,tit_recherche, tit_niveau, tit_article) VALUES('Nouveau','Nouveau',1,$art_id)";
                $idcom->query($ch); ?>
            <script>
            charge('article',<?php echo $art_id?>,'panneau_g');
            </script>
            <?php	// 				exit;
            } elseif ($r_editeur->num_rows == 1) {
                // 				exit;
                $rq_editeur = $r_editeur->fetch_object();
                $ch = "INSERT INTO Articles (art_cb, art_creation,art_statut,art_pseudo,art_unite,art_stk,art_tva,art_ttc,art_remise,art_pht,art_seuil,art_editeur)
                                VALUES($req,NOW(),0,1,1,0,$art_tva,0.00,0.00,0.00,0,".$rq_editeur->art_editeur.")";
                $idcom->query($ch);
                // 	$idcom->errno." ".$idcom->error;
                $art_id = $idcom->insert_id;
                //insertion d'un titre
                $ch = "INSERT INTO Titres (tit_nom,tit_recherche, tit_niveau, tit_article) VALUES('Nouveau','Nouveau',1,$art_id)";
                $idcom->query($ch); ?>
            <script>
            charge('article',<?php echo $art_id?>,'panneau_g');
            </script>
            <?php
            } elseif (substr($req, 0, 2) == '97') {
                //plusieurs editeurs associé au cb, liste de choix
                echo '<h3> Choisissez un des éditeurs associés à ce numéro</h3>';
                while ($rq_editeur = $r_editeur->fetch_object()) {
                    echo "<button style='width:200px' onclick=\"charge('creation_cb','".$req."&edi=".$rq_editeur->edi_id."','creationCB')\">".$rq_editeur->edi_nom."</button><br>";
                }
                // 				exit;
            } else {
                $ch = "INSERT INTO Articles (art_cb, art_creation,art_statut,art_pseudo,art_unite,art_stk,art_tva,art_ttc,art_remise,art_pht,art_seuil,art_editeur)
                            VALUES($req,NOW(),4,1,1,0,3,0.00,0.00,0.00,0,NULL)";
                $idcom->query($ch);
                $idcom->errno." ".$idcom->error;
                $art_id = $idcom->insert_id;
                //insertion d'un titre
                $ch = "INSERT INTO Titres (tit_nom,tit_recherche, tit_niveau, tit_article) VALUES('Nouveau','Nouveau',1,$art_id)";
                $idcom->query($ch);
                $idcom->errno." ".$idcom->error;
                // 				exit;
                ?>
        <script>
        charge('article',<?php echo $art_id?>,'panneau_g');
        </script>
            <?php
            }
        }
    }
}
    echo $req;
// 	exit;
?>
<script>
$('#panneau_g').height($('#affichage').height());
</script>


