<?php
if (!isset($req)) {
    session_start();
    $req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    if (!isset($incpath)) {
        $p=preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath="";
        for ($i=1;$i<sizeof($p)-1;$i++) {
            $incpath='../'.$incpath;
        }
        unset($p,$i);
    }
    require $incpath."mysql/connect.php";
    require $incpath."php/fonctions.php";
    connexobjet();
    //recupération de la date de création de l'article
    $req_creation="SELECT YEAR(art_creation) AS art_creation FROM Articles WHERE art_id=$req";
    $r_creation=$idcom->query($req_creation);
    $rq_creation=$r_creation->fetch_object();
    $creation=$rq_creation->art_creation;
}

if (!$an) {
    $an=ANNEE;
}
$debut = $an-1;
$tab_vl=array("0","0","0","0","0","0","0","0","0","0","0","0","0");
if ($debut < $config['debut']) { //première année de l'application
    $debut = $config['debut'];
    $req_vente="SELECT SUM(tic_quantite) AS vl,
                                             MONTH(rst_validation) AS mois,  
                                             YEAR(rst_validation) AS an,
                                            art_unite FROM Tickets_".$an." 
                                                JOIN Resume_ticket_".$an." ON rst_id=tic_num
                                                JOIN Articles ON art_id = tic_article  
                                                    WHERE tic_article = $req AND MONTH(rst_validation) >= '01' 
                                                        GROUP BY MONTH(rst_validation) ";
} else {
    $deb = date('n');
    $req_vente="(SELECT SUM(tic_quantite) AS vl, 
                                            (MONTH(rst_validation) - $deb) AS mois,
                                            YEAR(rst_validation) AS an,
                                            art_unite
                                                FROM Tickets_".$debut." 
                                                    JOIN Resume_ticket_".$debut." ON rst_id=tic_num
                                                    JOIN Articles ON art_id = tic_article 
                                                        WHERE tic_article = $req 
                                                            AND MONTH(rst_validation) >= $deb  
                                                                GROUP BY MONTH(rst_validation)) 
                            UNION ALL 
                                (SELECT SUM(tic_quantite) AS vl,
                                                (MONTH(rst_validation) + (12 -$deb)) AS mois,  
                                                YEAR(rst_validation) AS an,
                                                art_unite
                                                    FROM Tickets_".$an." 
                                                        JOIN Resume_ticket_".$an." ON rst_id=tic_num
                                                        JOIN Articles ON art_id = tic_article
                                                            WHERE tic_article = $req 
                                                                AND MONTH(rst_validation) <= $deb 
                                                                    GROUP BY MONTH(rst_validation)) ";
}
$deb = date('n');
$r_vente=$idcom->query($req_vente);
while ($rq_vente=$r_vente->fetch_object()) {
    if ($rq_vente->art_unite == 1) {
        $vl = sprintf("%d", $rq_vente->vl);
    } else {
        $vl = $rq_vente->vl;
    }
    $tab_vl[$rq_vente->mois]= $vl;
}
$suiv = '';
$pre = '';
if ($creation) {
    if (($an <= date("Y"))&&($an > ($creation+1))) {
        $pre = "<button onclick=\"charge('ventes.svg','".$req."&an=".($an - 1)."','graph_vente')\"><</button>";
    }
    if ($an < date("Y")) {
        $suiv = "<button onclick=\"charge('ventes.svg','".$req."&an=".($an + 1)."','graph_vente')\">></button>";
    }
}
// echo $resu->art_stk;
?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
<?php
if ($debut == $an) {//première année de l'utilisation l'application
    echo '<h3> Ventes sur l\'année '.$an.'</h3>';
} else {
    echo '<h3>'.$pre.' Ventes sur 12 mois : '.$debut.'/'.$an.' '.$suiv.'</h3>';
}
?>


<svg width="400px" height="110px" xml:lang="fr" xmlns="http://www.w3.org/2000/svg" id="svg_<?php echo $i?>" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
      <linearGradient id="MonDegrade">
        <stop offset="5%" stop-color="#6FF" />
        <stop offset="95%" stop-color="#066" />
      </linearGradient>
    </defs>
<rect x="0" y="0" width="400" height="110" style="fill:white;stroke:black;stroke-width:1px;"/>
<?php
$g=30;
$vlmax=max($tab_vl);
if ($vlmax != 0) {
    $echelle = 80 / $vlmax;
} else {
    $echelle = 0;
}
for ($l = 0; $l < 13; $l++) {
    if ($tab_vl[$l] != 0) {
        $valeur=$tab_vl[$l];
    } else {
        $valeur = '';
    }
    $hauteur=$tab_vl[$l]*$echelle;
    $positionH=109-$hauteur;
    if ($l%2 != 0) {
        $decal=12;
    } else {
        $decal = 0;
    }
    $n_mois= $l+$deb;
    if ($n_mois > 12) {
        $n_mois = $n_mois-12;
    }
    echo '<text class="valVente" x="'.(($g*$l)+19).'" y="'.(10+$decal).'">'.$mois[$n_mois].'</text>'."\n";
    echo '<line x1="'.(($g*$l)+19).'" x2="'.(($g*$l)+19).'" y1="25" y2="109" style="fill:none;stroke:black;stroke-width:1px;stroke-dasharray:4,4;"/>'."\n";
    echo '<rect fill="url(#MonDegrade)" x="'.(($g*$l)+10).'" y="'.$positionH.'" width="20" height="'.$hauteur.'" />'."\n";
    echo '<text class="valVente" x="'.(($g*$l)+19).'" y="108">'.$valeur.'</text>'."\n";
}
?>
</svg>
