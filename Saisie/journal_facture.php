<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$com= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
if ($an == '') {
    $an=ANNEE;
}
// Factures en attente de validation
?>

</style>
<?php
//------------------------------on verifie si le journal a déjà été entré
$req_verif = "SELECT * FROM Journal_factures_$an WHERE jrn_facture = $com";
$r_verif = $idcom->query($req_verif);
$rq_verif =$r_verif->fetch_object();

$req_recher="SELECT rsc_ttc,
                    rsc_id,
                    rsc_serveur,
                    rsc_date, 
                    edi_nom 
                        FROM Resume_commande_$an 
                        JOIN Editeurs ON edi_id = rsc_serveur
                            WHERE rsc_id = $com";
$r_recher=$idcom->query($req_recher);
$rq_recher = $r_recher->fetch_object();

if ($r_verif->num_rows > 0) {
    ?>
    <script>
    $(document).ready(function() {
        $('#journal input[date]').change(function() {
            if($(this).attr('type') == 'date') {
                modif(<?php echo $rq_verif->jrn_id?>,25,$(this).val(),'date','1&an=<?php echo $an?>');
            }
        });
        $('#journal input').on('keydown',function(event){
        $(this).addClass('jaune');
            if (event.which == 13) {
                if($(this).attr('type') == 'text') {
                    $(this).addClass('jaune');
                    if($(this).attr('id')=='valeurF') {
                        var ttverif = 0.00;
                        //verification du total
                        $('.tt').each(function(){
                            ttverif += +$(this).val();                    
                        })
                        resul = ttverif.toFixed(2);
                        if (resul != $('#valeurF').val()) {
                            $('#valeurF').css('backgroundColor','orange');
                            $('#ctrl').html(resul);
                        } else {
                            $('#ctrl').html('');
                            $('#valeurF').css('backgroundColor','');
                        }
                    
                        $(this).removeClass('jaune');
                modif(<?php echo $rq_recher->rsc_id?>,23,this.value,'ttc','1&an=<?php echo $an?>');
                    } else if ($(this).attr('id')=='numero'){
                        modif(<?php echo $rq_recher->rsc_id?>,25,this.value,'numero','1&an=<?php echo $an?>');
                    }
                    // 
                }
            }
        });
    });
    </script>
    <?php
    // echo $an."...........";
    echo "<h3>Modification du journal de facture ".$rq_verif->jrn_id." pour la commande ".$rq_recher->rsc_id."</h3>"; ?>
    <table id='journal' class='generique'><TR><TH>Date de facture</TH><TH>Valeur TTC</TH><TH>N° facture</TH><th></th></TR>
    <tr>
    <TD><input type="date" id='date_f'  placeholder='<?php echo $rq_verif->jrn_date?>' value='<?php echo $rq_verif->jrn_date?>'>
    </TD>
    <TD><input class="demi" type="text" onclick="this.value=''" id='valeurF' value="<?php echo $rq_recher->rsc_ttc?>">
    </TD>
    <TD><input id='numero' type="text" value="<?php echo $rq_verif->jrn_numero?>">
    </TD><td><img src="/images/edit.png" width="30px" onclick="charge('detail_commande_validee','<?php echo $rq_recher->rsc_id."&an=".$an?>','detail_fact')"></td>
</tr>
</table>
<div id='export'>
    <?php
    include "detail_facture.php"; ?>
</div>
<div id="detail_fact"></div>
    <?php
    exit;
}
// --------------------------------------------fin de l'edition

?>

<h3>Journal de facture pour la commande <?php echo $rq_recher->rsc_id." de ".$rq_recher->edi_nom?></h3>
<table class='generique'>
<TR><TH>Date de facture</TH><TH>Valeur TTC</TH><TH>N° facture</TH><th></th></TR>
<tr>
    <TD><input type="date" id='date_f' value='<?php echo $rq_recher->rsc_date?>'>
    </TD>
    <TD><input class="demi" type="text" onclick="this.value=''" value="<?php echo $rq_recher->rsc_ttc?>">
    </TD>
    <TD><input id='numero' type="text" value="" onchange="verif()">
    </TD><td><img src="/images/edit.png" width="30px" onclick="charge('detail_commande_validee','<?php echo $rq_recher->rsc_id.'&an='.$an?>','detail_fact')">
</tr>
<tr><TD colspan="3" align="right"><button style="visibility:hidden;" id='bt_valider' onclick="charge('creation_journal','<?php echo $rq_recher->rsc_id?>&dat='+$('#date_f').val()+'&num='+$('#numero').val(),'panneau_d')">Valider</button></TD></tr>
</table>
<div id='export'>
</div>
<div id="detail_fact"></div>
<?php


//on vérifie s'il y a plusieurs commandes en attente de facturations, si oui on propose la fusion avec celle affichée
$req_fusion = 'SELECT rsc_id FROM Resume_commande_'.$an.' 
                        LEFT JOIN Journal_factures_'.$an.' ON jrn_facture = rsc_id 
                            WHERE rsc_serveur = '.$rq_recher->rsc_serveur.' 
                                AND rsc_etat="3"
                                AND jrn_facture IS NULL';
$r_fusion = $idcom->query($req_fusion);
// echo $com;
if ($r_fusion->num_rows > 0) {
    while ($rq_fusion =$r_fusion->fetch_object()) {
        if ($rq_fusion->rsc_id != $com) {
            ?>
        <h3>Fusion de commande</h3>
        Cliquer sur le bouton ci-dessous pour insérer les articles de la commande <?php echo $rq_fusion->rsc_id?> dans la commande <?php echo $com?><br>
        <button onclick="$('#confirmation').css('display','block');$(this).css('display','none')"><?php echo $rq_fusion->rsc_id?></button><br>
        <div id='confirmation' style='display:none;font-weight:bold'>La commande <?php echo $rq_fusion->rsc_id?> sera définitivement supprimée.<br><br>
        <button onclick="charge('fusion_commandes','<?php echo $com."&anc=".$rq_fusion->rsc_id."&an=".date('Y')?>','panneau_g')">Suppression de la commande <?php echo $rq_fusion->rsc_id?></button>
        </div>
        <?php
        }
    }
}

?>
<script>
function verif() {
    if (($('#date_f').val() != '')&&($('#numero').val() != '')) {
        $('#bt_valider').css('visibility','visible');
    }
}
</script>