<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
?>
<script>
$(document).ready(function() {
    function ctrl() {
    cp = 0;
    $("table#inventaire tbody input[type=checkbox]").each(function() {
        if($(this).prop('checked')==true) {
        cp++;
        }
    });
    if (cp == 0) {
            $('#export').css('visibility','hidden');
        } else {

        $('#export').css('visibility','visible');
        }
    }                        

    ctrl();
    $('table#inventaire tbody td').click(function() {
        id = $(this).parent().attr('id');
        if($(this).html() == '<input type="checkbox">') {
            if ($(this).children().prop('checked') == false) {
                ctrl();
            } else {
                ctrl();
            }
        } else if($(this).attr('id') != 'fin'){ 
            $('table#inventaire tr').css('fontWeight','normal');
            $(this).parent().css('fontWeight','bold');
            if ($("#etendue").prop('checked') == false) {
                charge('articles_rayons',id+'&ac=1','panneau_d');
            } else {
                charge('articles_rayons',id,'panneau_d'); 
            }
        }
    });
});
function envoi(id) {
        var valeurs=[];
            $("#inventaire tbody input:checkbox").each(function(){
            if($(this).prop('checked') == true) {
                valeurs.push($(this).parent().parent().attr('id'));
            }
        });
        if ($("#etendue").prop('checked') == false) {
            valeurs =  valeurs+'&et=0';
        } else {
            valeurs =  valeurs+'&et=1';
        }
        if(id == 1) {
            charge('export_rayons',valeurs,'mysql');
        } else {
            window.open('imp_theorique.php?req='+valeurs);
            // charge('imp_theorique',valeurs,'panneau_d');
        }
    }        
$('#chk').click(function(){
    if ($(this).html() == 'Tout cocher') {
        $("#inventaire tbody input[type=checkbox]").prop('checked',true);
        $('#chk').html('Tout décocher');
        $('#export').css('visibility','visible');
    } else  {
        $("#inventaire tbody input[type=checkbox]").prop('checked',false);
        $('#chk').html('Tout cocher');
        $('#export').css('visibility','hidden');
    }
});

$('#etendue').click(function(){
   if($(this).prop('checked')==true) {
       $('#stk').html('Tous les articles');
   } else {
    $('#stk').html('Articles en stock');
   }
})
</script>
<style>

 .inv_art p{
text-indent:30px;
height:20px;
}
</style>
<h3 id='liste'>Inventaire théorique</h3>

<table id='inventaire'>
<thead>
<tr><TH colspan="3">Il y a <span id='nb'></span> articles en stock</TH></tr>
<tr><TD></TD><TD colspan="2"></TD></tr>
<TR><TH>Rayons</TH><TH>Nb articles</TH><th></th></TR>
</thead>
<tbody>
<?php
$nb_articles = 0;
$req_rayon = "SELECT Var_raynom,
                    Var_rayon,
                    COUNT(Var_article) AS articles
                    FROM Varticles_rayon 
                        WHERE Var_utilisateur = $_SESSION[$dossier] AND Var_stk != 0.00 
                            GROUP BY Var_rayon";
$r_rayon=$idcom->query($req_rayon);
$n = 0;
while ($rq_rayon = $r_rayon->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;

    echo '<tr class="rayon" style="background-color:'.$coul.'" id="'.$rq_rayon->Var_rayon.'"><td>'.$rq_rayon->Var_raynom.'</td><td>'.$rq_rayon->articles.'</td><td><input type="checkbox"></td></tr>'."\n";
    $n++;
    $nb_articles += $rq_rayon->articles;
}
?>
</tbody>
<tfoot>
<tr><td id='fin'>
<div id='export' style='float:left;width:150px'><span id='export'> <button onclick='envoi(1)'>Export csv</button> <button onclick='envoi(2)'>Pdf</button></span> 
</span></div> 
<button style='float:right' id='chk'>Tout cocher</button>
</td>
<td style='text-align:right'><span id='stk' for="etendue">En stock</span><input style='float:right' id='etendue' type='checkbox'></td><td></td></tr>
</tfoot>
</table>
<script>
$('#nb').html(<?php echo $nb_articles?>);
$("#panneau_g").height($('#affichage').height()-10);
</script>
