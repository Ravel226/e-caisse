<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "id", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

if ($req == "") {
    $req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
 
$req_recher="SELECT sec_id,
                    sec_nom,
                    ray_nom,
                    ray_id,
                    COUNT(Var_article) AS ct FROM Secteurs
                      JOIN Rayons ON ray_secteur = sec_id
                      JOIN Varticles_rayon ON Var_rayon = ray_id
                        WHERE Var_utilisateur = $_SESSION[$dossier]
                            GROUP BY ray_id
                              ORDER BY sec_id";
$r_recher=$idcom->query($req_recher);
if ($idcom->error) {
    echo $idcom->errno." ".$idcom->error."<br>";
}
?>
<script>
// function voir_rayon(id) {
//   charge('articles_rayons',id,'panneau_d');
// }
// <script>
$(document).ready(function() {
    $('table#rayons td').on('click',function(){
        $('#rayons td').css('fontWeight','normal');
        $(this).css('fontWeight','bold');
        charge('articles_rayons',$(this).attr('id'),'panneau_d');
    });
    });
</script>
<h3 id="liste">Articles par Rayons</h3>
<table id="rayons">
<?php
$n=0;
$sec = '';
while ($resu=$r_recher->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;

    if ($sec != $resu->sec_id) {
        echo "<tr><th>".$resu->sec_nom."</th></tr>";
        $n=0;
        $coul=$coulCC;
    }
    echo "<tr style='background-color:".$coul."'><td id=".$resu->ray_id."  class='pointer'>".$resu->ray_nom." (".$resu->ct.")</td></tr>";
    $sec = $resu->sec_id;
    $n++;
}
?>

<tr><TD></TD></tr>
</table>
<script>
$("#panneau_g").height($('#affichage').height()-10);
</script>
