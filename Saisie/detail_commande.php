<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
// print_r($_SESSION);
$an=ANNEE;
$req_recher="SELECT Commandes_$an.*,
                vt1_id,
                vt1_nom,
                art_id,
                art_cb,
                art_unite, 
                edi_commande, 
                com_quantite,
                com_numero, 
                art_pht,
                edi_nom, 
                rsc_date, 
                edi_nom
                rsc_id,
                art_statut 
                    FROM Resume_commande_$an
                        LEFT JOIN Commandes_$an ON rsc_id=com_numero
                        LEFT JOIN Articles on art_id = com_article
                        LEFT JOIN Vtit1 ON Vt1_article = art_id
                        JOIN Editeurs ON rsc_serveur = edi_id
                            WHERE rsc_id = $req
                                AND art_id != 1
                                ORDER BY art_statut DESC, vt1_nom";
$r_recher=$idcom->query($req_recher);
$nb = $r_recher->num_rows;

$resu=$r_recher->fetch_object();
$r_recher->data_seek(0);
// modif(id,tb,vl,cp,my)
?>
<script>
$(document).ready(function() {
    $('#commande input').on('keydown',function(event) {
        $(this).addClass('jaune');
        
        if(event.which == 13){
            if($(this).attr('type') == 'text'){
                if($(this).val() > 0) {
                    modif($(this).attr('id'),16,$(this).val(),'quantite',1);
                }
                else {
                    modif($(this).attr('id'),16,'','id',2);//suppression
                    setTimeout(function(){charge('commandes/commande','<?php echo $req?>','panneau_d')},200);
                }
            $(this).removeClass('jaune');
            }
        }
    });
    
    $('#commande tbody td').click(function(){
        if($(this).attr('type') != 'text') {
            charge('article',$(this).parent().attr('art'),'panneau_g');
        }
    });
});

</script>
<style>
.jaune{
background-color:yellow;
}
</style>
<?php

?>
<h3 id="liste"> Commande en attente n° <?php echo $req?><br> <?php echo $resu->edi_nom?> <br> <?php echo dateFR($resu->rsc_date)?></h3>

<table id="commande" style='width:90%;margin-left:5%'>
<thead>
<tr><TH>CB</TH><TH>Titre</TH><TH>Quantité</TH></tr>
</thead>
<tbody>
<?php
if ($resu->edi_commande == "") {
    $desactiver=" style='float:left;color:#ffffff'";
} else {
    $desactiver=" style='float:left'";
}
$n=0;
$tt = 0.00;
while ($resu=$r_recher->fetch_object()) {
    $num_commande=$resu->rsc_id;
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    
    $com_quantite=($resu->art_unite == 1)?sprintf("%d", $resu->com_quantite):$resu->com_quantite;
    
    if ($resu->art_statut > 0) {
        echo '<tr art='.$resu->art_id.' style="background-color:'.$coul.'"><td>'.$resu->art_cb.'</td>
        <td>&nbsp;'.$resu->Vt1_nom.'</td>';
        ?>
        <td style="text-align:center"><input id='<?php echo $resu->com_id?>' type="text" class="cent" value="<?php echo $com_quantite?>"></td></tr>
        <?php
    } else {//nouveautés        
        echo '<tr><td><input onchange="modif('.$resu->art_id.',11,this.value,cb,1)" type="text" class="_cb" value="'.$resu->art_cb.'"></td><td><input type="text" onchange="modif(\''.$resu->Vt1_nom.'\',12,this.value,nom,1)" value="'.$resu->Vt1_nom.'"></td><td><input type="text" class="_demi" onchange="modif('.$resu->com_id.',16,this.value,quantite,1)" value="'.$resu->com_quantite.'"></td></tr>';
    }
    $n++;
    $tt += $resu->art_pht * $com_quantite;
}
?>
</tbody>

<tfoot>
<tr><TH class='blanc' colspan="3">Nouveautés</TH></tr>
<tr id='insert_nouveau'></tr>
<tr><TD colspan="3"><button onclick="charge('commandes/nouvel_article',<?php echo $req?>,'insert_nouveau')">Ajouter un nouvel article</button></TD></tr>

<tr><TH colspan="3">Valeur estimé sans le port : <?php echo sprintf("%01.2f",$tt)?> €</TH></tr>

<tr>
    <td colspan="3">
        <div id='envoi'>
            <button <?php echo $desactiver?>>Liste CSV</button>
            <button onclick="charge('commandes/creation_commande',<?php echo $req?>,'mysql');$('#confirme').css('display','block');$('#envoi').css('display','none');" style="float:right">Imprimer</button>
        </div>
        <div id='confirme' style="display:none">
            <button onclick="modif(<?php echo $req?>,23,2,'etat',1);$('#panneau_g').empty();setTimeout(function(){force('/Saisie/commandes/attente','','panneau_g')},200);">Confirmer la réussite de l'envoi</button>
        </div>
    </td>
</tr>
</tfoot>
</table>
<script>
$("#panneau_d").height($("#affichage").height()-10);
$("#panneau_g").height($("#affichage").height()-10);
</script>
