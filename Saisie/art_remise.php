<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_article= "SELECT art_id,
                        art_pht,
                        art_ttc,
                        art_remise,
                        tva_nom,
                        edi_nom,
                        edi_id
                            FROM Articles
                            JOIN Tva ON art_tva = tva_id
                            JOIN Editeurs ON edi_id = art_editeur
                                WHERE art_remise != 0.00
                                AND art_stk != 0.00
                                AND edi_utilisateur = ".$_SESSION[$dossier]."
                                    ORDER BY art_editeur";
$r_article=$idcom->query($req_article);
$n = 01;
?>
<script type="text/javascript">
$(document).ready(function(){
    $('#liste_article td').click(function(){
        $('#liste_article tr').css('font-weight','normal');
        $(this).parent().css('font-weight','bold');
        if ($(this).html().slice(0,8) == '<img src') {
            vl = ($('#vl_'+$(this).parent().attr('id')).html());
            // id,tb,vl,cp,my
            modif($(this).parent().attr('id'),11,vl,'pht',1);
            charge('article',$(this).parent().attr('id'),'panneau_g');
        } else if ($(this).html().slice(0,8) == '<button>') {
            charge('detail_editeur',$(this).attr('edi'),'panneau_g');
        } else {
            charge('article',$(this).parent().attr('id'),'panneau_g');
            
        }
    });
});
$('#panneau_d').height($("#affichage").height()-10);
</script>
<style>
#remise img {
    width:15px;
    float:right;
}
#remise td {
    padding-left:5px;
}
#remise button {
    background-image:url('/images/button_edit.png');
    background-repeat:no-repeat;
    background-position:center center;
    width:20px;
    height:20px; 
}
</style>
<div id="liste_article">
<h3>Erreurs sur le taux de remise</h3>
<table id="remise" class="generique">
<thead><tr><th></th><th>N° article</th>
<th>Px ttc</th>
<th>Remise</th>
<th>Px ht</th>
<th colspan="2">px calculé</th>
<th colspan="2">Éditeur</th></tr>
</thead><tbody>
<?php
$n = 1;
while ($rq_article = $r_article->fetch_object()) {
    $ht= $rq_article->art_ttc/(1+($rq_article->tva_nom/100));
    $pht = $ht*(1-($rq_article->art_remise/100));

    if (number_format($pht, 4) != $rq_article->art_pht) {
        $coul=($n % 2 == 0)?$coulCC:$coulFF;
        echo "<tr id='".$rq_article->art_id."' style='background-color:".$coul.";'><td>".$n."</td>
        <td>".$rq_article->art_id."</td><td>".$rq_article->art_ttc."</td>
        <td>".$rq_article->art_remise."</td>
        <td>".$rq_article->art_pht."</td>
        <td id='vl_".$rq_article->art_id."'>".number_format($pht, 4)."</td>
        <td><img src='/images/valider.png'></td>
        <td>".$rq_article->edi_nom."</td><td edi='".$rq_article->edi_id."'><button></button></td></tr>";
        $n++;
    }
}
echo '<tbody></table></div>';
