<?php
// Dit au navigateur que les données retournées sont un fichier qui s'appelle $filename.csv
header("Content-disposition: attachment; filename=export_rayons.csv");
// Dit au navigateur que les données retournées sont un fichier csv.
header("Content-Type: text/csv");

if (!isset($incpath)) {
    session_start();
    if (!isset($incpath)) {
        $p=preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath="";
        for ($i=1;$i<sizeof($p)-1;$i++) {
            $incpath='../'.$incpath;
        }
        unset($p, $i);
    }
}
$ray= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$et= filter_input(INPUT_GET, "et", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tab_ray = array();
$n = 0;
$comp = "";
$req_recher = "";
foreach (explode(",", $ray) as $value) {
    $tab_ray[] = $value;
    $comp .=($n > 0)?" OR ":"";
    $comp .= "art_rayon = ".$value;
    $n++;
}
$comp1=($et == 0)?" AND art_stk > 0":"";
$req_recher ="SELECT Vt1_nom ,
    art_cb ,
    art_ttc,
    art_stk,
    ray_nom,
    sec_nom
        FROM Articles
        JOIN Vtit1 ON Vt1_article = art_id
        JOIN Rayons ON ray_id = art_rayon
        JOIN Secteurs ON sec_id = ray_secteur
            WHERE  (".$comp.")
                $comp1
                ORDER BY sec_id,
                ray_id,
                Vt1_nom";
// exit;
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$r_recher=$idcom->query($req_recher);
$memoire = "";
ob_start();
$n = 1;
$m = 0;
while ($rq_recher =$r_recher->fetch_object()) {
    echo "'".$rq_recher->Vt1_nom."',".$rq_recher->art_cb.",".$rq_recher->art_ttc.",".$rq_recher->art_stk.",'".$rq_recher->ray_nom."','".$rq_recher->sec_nom."'\n";
}
// echo $memoire;
$memoire = ob_get_contents();
$id_file=fopen("export_rayons.csv", "w+b");
$memoire = iconv("UTF-8", "Windows-1252", $memoire);
fwrite($id_file, $memoire);
fclose($id_file);
ob_end_flush();
?>
<script>
$('#export_ebp').css('visibility','hidden');
$('#mysql').empty();
location.href=('export_rayons.csv');</script>