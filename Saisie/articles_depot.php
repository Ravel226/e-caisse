<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p,$i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$an=ANNEE;
require "ventes_annuelles.php";
// calcul du maxi pour le graphe des ventes
$req_max="SELECT MAX(ven_ct) as maxi
                      FROM Varticle_editeur
                        JOIN Vtit1 ON Vt1_article = Vart_id
                        LEFT JOIN Ventes ON ven_article = Vart_id                        
                         WHERE Vart_utilisateur=$_SESSION[$dossier]
                          AND Vart_statut = 2
                          GROUP BY Vart_id
                            ORDER BY ven_ct DESC, Vart_stk";
                            $r_max=$idcom->query($req_max);
//                             echo "<br>".$idcom->errno." ".$idcom->error;
                            $rq_max=$r_max->fetch_object();
                            
                            
$req_recher="SELECT Vart_id AS art_id, 
                    Vt1_nom, 
                    Vart_stk AS art_stk, 
                    Vart_unite AS art_unite, 
                    ven_ct, 
                    rsc_etat,
                    unv_abrege, 
                    com_quantite,
                    com_id
                      FROM Varticle_editeur
                        JOIN Vtit1 ON Vt1_article = Vart_id
                        JOIN Unites_vente ON unv_id = Vart_unite
                        LEFT JOIN Ventes ON ven_article = Vart_id
                        LEFT JOIN encommande ON com_article = Vart_id
                         WHERE Vart_utilisateur=$_SESSION[$dossier]
                         AND Vart_statut = 2
                          GROUP BY Vart_id
                            ORDER BY ven_ct DESC, Vart_stk";
                    
$r_recher=$idcom->query($req_recher);
// echo "<br>".$idcom->errno." ".$idcom->error;
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
$resu=$r_recher->fetch_object();
// print_r($resu);
$nb = $r_recher->num_rows;
$r_recher->data_seek(0);
$nom = "en dépots";
require "liste_articles.php";

?>

