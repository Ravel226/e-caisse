<?php
require $incpath."php/config.php";
$req_recher = "SELECT art_id,
                        art_cb,
                        art_rayon,
                        art_editeur,
                        art_stk,
                        (SELECT SUM(tic_quantite) FROM Tickets_" . ANNEE . " WHERE tic_article = $req AND LENGTH(tic_num) = 10) AS art_cp,
                        art_statut,
                        art_aveccb,
                        art_creation,
                        art_mp,
                        art_pht,
                        art_pseudo,
                        art_remise,
                        art_seuil,
                        art_ttc,
                        art_tva,
                        art_unite,
                        edi_id, 
                        edi_nom, 
                        eds_remise, 
                        ray_id, 
                        ray_nom, 
                        sec_id, 
                        sec_nom,
                        con_secteur,
                        con_livre,
                        stk_stk, 
                        tva_nom,
                        pro_id,
                        pro_article,
                        pro_mode,
                        pro_valeur,
                        Vser_nom, 
                        Vt1_id, 
                        Vt1_nom, 
                        Vt2_id, 
                        Vt2_nom, 
                        Vt3_id, 
                        Vt3_nom, 
                        Vt4_id,
                        Vt4_nom,
                        fav_parent,
                        fav_id
                            FROM Articles
                            LEFT JOIN Vtit1 ON Vt1_article= art_id
                            LEFT JOIN Vtit2 ON Vt2_article= art_id
                            LEFT JOIN Vtit3 ON Vt3_article= art_id
                            LEFT JOIN Vtit4 ON Vt4_article= art_id
                            LEFT JOIN Rayons ON ray_id = art_rayon
                            LEFT JOIN Tva ON art_tva = tva_id
                            LEFT JOIN Statut ON sta_id = art_statut
                            LEFT JOIN Editeurs ON edi_id = art_editeur
                            LEFT JOIN Secteurs ON sec_id = ray_secteur
                            LEFT JOIN Editeur_serveur ON eds_editeur = edi_id
                            LEFT JOIN Vserveurs ON Vser_id = eds_serveur
                            LEFT JOIN Stock_" . (date('Y') - 1) . " ON art_id=stk_article
                            LEFT JOIN Promo ON pro_article = art_id
                            LEFT JOIN Favoris ON fav_article = art_id
                            JOIN Config
                                WHERE art_id =$req AND con_id = 0";
$r_recher = $idcom->query($req_recher);
if ($idcom->error) {
    echo "<br>" . $idcom->errno . " " . $idcom->error . "<br>";
}
if ($r_recher->num_rows == 0) { //l'article n'existe pas ou plus
    echo "<h3><center><img src ='/images/attention.png'></center><br>L'article demandé n'existe pas</h3>";
    exit;
}
$resu = $r_recher->fetch_object();
//si la date de création n'existe pas on met le premier de l'année de l'entrée en service de l'application
$creation = ($resu->art_creation == '') ? $_SESSION["debut"] . '-01-01' : $resu->art_creation;
?>
<table id='detail_article'>
    <tr>
        <TH>ID</TH>
        <TD><span id="article"><?php echo $resu->art_id ?></span> <span style='margin-left:50px'>
                <strong>Date de création</strong><input class="_demi" id="creation" type="date" value='<?php echo $creation ?>' onchange="modif(<?php echo $resu->art_id ?>,11,this.value,'creation','1')"></span>
        </TD>
    </tr>
    <tr>
        <TH>CB</TH>
        <TD><span id="cb" style="display:none;"><input type="text" value="" id='cb_11_<?php echo $resu->art_id ?>' style="width:150px" value=""><button onclick="charge('nouveau_cb.inc',<?php echo $resu->art_id ?>,'panneau_g')">Interne</button></span><span onclick="$('#cb').css('display','inline');$(this).css('display','none')"><img src="../images/button_edit.png" width="12" height="13"><?php echo $resu->art_cb ?></span>
        </TD>
    </tr>
    <tr>
        <TH>Titre</TH>
        <TD><input type="text" value="<?php echo stripslashes($resu->Vt1_nom) ?>" id='nom_12_<?php echo $resu->Vt1_id ?>'></TD>
    </tr>
    <tbody id='ray_sec'>
        <tr>
            <TH>Rayon</TH>
            <TD><img onclick="charge('choix_rayon','<?php echo $resu->art_id ?>','popup_g')" src="../images/edit.png" width="20" height="20" /> <SPAN id="ligne_rayon"><?php echo $resu->ray_nom ?></SPAN>
                <?php
                if ($resu->ray_nom != '') {
                    ?>
                    <img onclick="charge('articles_rayons','<?php echo $resu->ray_id ?>','panneau_d')" src="../images/voir.png" width="20" height="20" />
                    <?php
                }
                if ($config['favoris'] == 1) {
                    $fav = ($resu->fav_parent != '') ? 1 : 0; ?>
                    <div id='favoris' style="float:right"><select onchange="favoris(<?php echo $fav . ",'" . $resu->fav_id ?>',this.value)">
                            <option>Favoris</option>
                        <?php
                        $req_favoris = "SELECT aff_id, aff_nom FROM Affichage LEFT JOIN VaffichageFavoris ON VafF_parent = aff_id WHERE VafF_parent IS NULL AND aff_id !=1 ORDER BY aff_nom;";
                        $r_favoris = $idcom->query($req_favoris);
                        while ($rq_favoris = $r_favoris->fetch_object()) {
                            $sel = ($resu->fav_parent == $rq_favoris->aff_id) ? ' selected' : '';
                            echo '<option value="' . $rq_favoris->aff_id . '"' . $sel . '>' . $rq_favoris->aff_nom . '</option>';
                        }
                        echo "</select>";
                }
                ?></div>
            </TD>
        </tr>
        <tr>
            <TH>Secteur</TH>
            <TD><?php echo $resu->sec_nom ?></TD>
        </tr>
    </tbody>
    <tbody id='edi_ser'>
        <tr>
            <TH>Editeur</TH>
            <TD id='liste_editeurs'>
                <img onclick="charge('liste_editeurs',<?php echo $resu->art_id ?>,'popup_g')" src="../images/edit.png" width="20" height="20" />
                <SPAN id='ligne_editeur'><?php echo $resu->edi_nom ?></SPAN>
                <?php
                if ($resu->edi_nom != '') {
                    ?>
                    <img onclick="charge('articles_editeur','<?php echo $resu->edi_id ?>','panneau_d')" src="../images/voir.png" width="20" height="20" />
                    <?php
                }
                ?>
            </TD>
        </tr>
        <tr>
            <TH>Serveur</TH>
            <TD><?php echo $resu->Vser_nom ?></TD>
        </tr>
    </tbody>
    <tr>
        <TH>Auteur</TH>
        <?php
        if ($resu->Vt4_nom == "") {
            ?>
            <TD><img src="../images/plus.png" width="25" height="25" onclick="modif( '<?php echo $resu->art_id ?>',12,4,'niveau',0);setTimeout(function(){charge('article','<?php echo $resu->art_id ?>','panneau_g')},300);">
            <?php
        } else {
            ?>
            <TD><input type="text" value="<?php echo stripslashes($resu->Vt4_nom) ?>" id='nom_12_<?php echo $resu->Vt4_id ?>'> <img onclick="suppression( '<?php echo $resu->Vt4_id ?>',12,'','',2);setTimeout(function(){charge('article','<?php echo $resu->art_id ?>','panneau_g')},300);" src="../images/supprimer.png" width="20" height="20">
            <?php
        }
        ?>
            </TD>
    </tr>
    <tr>
        <TH>Référence</TH>
        <?php
        if ($resu->Vt2_nom == "") {
            ?>
            <TD><img src="../images/plus.png" width="25" height="25" onclick="modif( '<?php echo $resu->art_id ?>',12,2,'niveau',0);setTimeout(function(){charge('article','<?php echo $resu->art_id ?>','panneau_g')},300);">
            <?php
        } else {
            ?>
            <TD><input type="text" value="<?php echo stripslashes($resu->Vt2_nom) ?>" class="_demi" id='nom_12_<?php echo $resu->Vt2_id ?>'> <img onclick="suppression( '<?php echo $resu->Vt2_id ?>',12,'','id',2);setTimeout(function(){charge('article','<?php echo $resu->art_id ?>','panneau_g')},300);" src="../images/supprimer.png" width="20" height="20">
            <?php
        }
        ?>
            </TD>
    </tr>
    <tr>
        <TH>Réf. int.</TH>
        <?php
        if ($resu->Vt3_nom == "") {
            ?>
            <TD><img src="../images/plus.png" width="25" height="25" onclick="modif( '<?php echo $resu->art_id ?>',12,3,'niveau',0);setTimeout(function(){charge('article','<?php echo $resu->art_id ?>','panneau_g')},300);">
            <?php
        } else {
            ?>
            <TD><input type="text" value="<?php echo stripslashes($resu->Vt3_nom) ?>" id='nom_12_<?php echo $resu->Vt3_id ?>' class="_demi"> <img onclick="suppression( '<?php echo $resu->Vt3_id ?>',12,'','id',2)" src="../images/supprimer.png" width="20" height="20">
            <?php
        }
        ?></TD>
    </tr>
    <tr>
        <TH>Stock</TH>
        <TD>
            <?php
            if ($resu->art_cp == '') {
                $resu->art_cp = 0;
            }
            if ($resu->art_unite == 1) {
                $stock = sprintf("%d", $resu->art_stk);
                $art_encompte = sprintf("%d", $resu->art_cp);
            } else {
                $stock = $resu->art_stk;
                $art_encompte = $resu->art_cp;
            }
            $desactiver = (($resu->art_stk == 0) || ($resu->pro_mode == 2)) ? ' disabled' : '';
            ?>
            <input class="_demi" type="text" value="<?php echo $stock ?>" onchange="modif(<?php echo $resu->art_id ?>,11,parseFloat(this.value),'stk','1')">(<i><?php echo $art_encompte ?></i>)
            <span style="margin-left:50px"><strong>Seuil</strong><input class="pt" type="text" value="<?php echo $resu->art_seuil ?>" onchange="modif(<?php echo $resu->art_id ?>,11,this.value,'seuil','1')"></span>
            <span id='prom' style="float:right">
                <select id='promo' onchange="promo()" <?php echo $desactiver ?>>
                    <?php
                    //les articles en promo/solde sont stoqués dans Promo
                    // +-------------+--------------+------+-----+---------+----------------+
                    // | Field       | Type         | Null | Key | Default | Extra          |
                    // +-------------+--------------+------+-----+---------+----------------+
                    // | pro_id      | int(6)       | NO   | PRI | NULL    | auto_increment |
                    // | pro_article | int(5)       | NO   | UNI | NULL    |                |
                    // | pro_mode    | int(1)       | NO   |     | 1       |                |
                    // | pro_valeur  | decimal(8,2) | NO   |     | NULL    |                |
                    // +-------------+--------------+------+-----+---------+----------------+

                    $tab_promo = array('Promo/Solde', 'Promo', 'Solde');

                    for ($i = 0; $i < 3; $i++) {
                        if ($i == $resu->pro_mode) {
                            $sel = ' selected';
                            $visible = 'visible';
                        } else {
                            $sel = '';
                        }
                        echo '<option value="' . $i . '"' . $sel . '>' . $tab_promo[$i] . '</option>';
                    }
                    ?>
                </select>
                <?php
                if ($resu->pro_mode > 0) { //(id,tb,vl,cp,my)id[2], id[1], this.value, id[0], '1')
                    echo "<input id='valeur_34_" . $resu->pro_id . "' style='visibility:visible' type='text' class='demi' value='" . $resu->pro_valeur . "'>";
                } else {
                    echo "<input style='visibility:hidden' type='text' class='mille'>";
                }
                ?>
            </span>
        </TD>
    </tr>
    <tr>
        <TH>Tva</TH>
        <TD>
            <?php
            if ($config['mode'] == 2) {
                echo "<br>" . $resu->tva_nom;
            } else {
                echo '<select onchange="modif(' . $resu->art_id . ',11,this.value,\'tva\',1)">';
                $req_tva = "SELECT * FROM Tva WHERE tva_etat = 1 ORDER BY tva_ordre";
                $r_tva  = $idcom->query($req_tva);
                while ($rq_tva  = $r_tva->fetch_object()) {
                    $sel = ($rq_tva->tva_id == $resu->art_tva) ? " selected" : "";
                    echo "<option value='" . $rq_tva->tva_id . "'" . $sel . ">" . $rq_tva->tva_nom . "</option>";
                }
                echo '</select>';
            }
            ?>
            <span style="float:right"><b>Unité de vente</b>
                <select style="float:right" onchange="modif(<?php echo $resu->art_id ?>,11,this.value,'unite','1')"></span>
            <?php
            $req_unites = "SELECT * FROM Unites_vente";
            $r_unites = $idcom->query($req_unites);
            while ($rq_unites = $r_unites->fetch_object()) {
                $sel = ($rq_unites->unv_id == $resu->art_unite) ? " selected" : "";
                echo "<option value=" . $rq_unites->unv_id . " " . $sel . " >" . $rq_unites->unv_nom . "</option>";
            }
            ?>

            </select>
            <SPAN id='ligne_tva' style="visibility:hidden"><?php echo $resu->art_tva ?></SPAN>
        </TD>
    </tr>
    <tr style="border:solid 1px;border-style: dotted;">
        <TH>Prix V. TTC</TH>
        <TD style="text-align:center" id='prix'>
            <input style="float:left" class="cent" onchange="remise(this.value,'ttc')" type="text" id='ttc' value="<?php echo $resu->art_ttc ?>">
            <?php
            //le prix mini est ttc-5% pour les livre et pour les autres articles ht + tva
            if($resu->con_secteur == $resu->sec_id) {
                $prix_mini = sprintf("%01.2f", ($resu->art_ttc /(1 + ($resu->con_livre / 100)))); 
            } else {
                $prix_mini = sprintf("%01.2f", ($resu->art_pht * (1 + ($resu->tva_nom / 100))));
            }
            
            echo "<span style='display:none' id=prix_mini>" . $prix_mini . "</span>";
            //le moyen pondéré est incompatible avec la remise, on désactive l'affichage de la remise
            if ($resu->art_remise > 0.00) {
                $comp1 = 'style="background-color:transparent;border:0;height:32px"';
                $comp = '';
            } else {
                $comp = 'style="background-color:transparent;border:0;height:32px"';
                $comp1 = '';
            }
            $cheC = ($resu->art_aveccb == 1) ? " checked" : "";
            $cheP = ($resu->art_pseudo == 1) ? " checked" : "";
            ?>
            <span><strong>Remise</strong>
                <?php if ($resu->art_mp == 0) {
                    ?><input <?php echo $comp ?> placeholder='0.00' class="cent" onchange="remise(this.value,'remise')" type="text" id='rem' value="<?php echo $resu->art_remise ?>">
                    <?php
                } ?> % <em>(<?php echo $resu->eds_remise ?>)</em></span>
            <?php
            //si la remise est utilisée, le pht ne peut être modifié
            if ($resu->art_remise == 0.00) {
                $modif = " onchange=modif(" . $resu->art_id . ",11,this.value,'pht',1)";
            } else {
                $modif = "onclick=\"alert('On ne peut modifier ce prix HT lié à la remise. Modifer la remise ou la mettre à 0.00')\"";
            }
            ?>
            <span style="float:right;"><strong>Prix A. HT</strong> <input <?php echo $comp1 ?> class="demi" <?php echo $modif ?> type="text" id='pht' value="<?php echo number_format($resu->art_pht, 4) ?>"></span>
        </TD>
    </tr>
    <tr>
        <TH></TH>
        <TD id='imp_cb'>
            <span id='creecb'>
                <span><strong>art_aveccb</strong><input id='aveccb' type="checkbox" <?php echo $cheC ?>></span> <button onclick="$('#creecb').css('display','none');$('#cbquantite').css('display','block')">CréerCB</button>
            </span>
            <span id='cbquantite' style="display:none;float:right">Quantité <input value="1" id='qt' type="number" style="width:70px;"><button onclick="charge('creecb',$('#qt').val(),'imp_cb')">Ok</button>
            </span>
        </TD>
    </tr>
    <tr>
        <TH>Statut</TH>
        <?php
        if (($resu->art_statut == 0) && ($resu->art_editeur == 1)) {
            $pilon = 1;
            echo "<TD>Pilon</td>";
        } else {
            $pilon = 0;
            ?>
            <TD><select onchange="modif(<?php echo $resu->art_id ?>,11,this.value,'statut',1)">
                    <option></option>
                <?php
                $req_exc = "SELECT * FROM Statut ORDER BY sta_nom";
                $r_exc = $idcom->query($req_exc);
                while ($rq_exc = $r_exc->fetch_object()) {
                    $sel = ($rq_exc->sta_id == $resu->art_statut) ? " selected" : "";
                    echo "<option value='" . $rq_exc->sta_id . "'" . $sel . ">" . $rq_exc->sta_nom . "</option>";
                }
        }
            $cheC = ($resu->art_mp == 1) ? 'checked' : "";
        ?>
        </select><span style="margin-left:55px"><strong>M_P</strong> <input id='mp' type="checkbox" <?php echo $cheC ?>></span></TD>
    </tr>
    <tr>
        <th>Pseudo</th>
        <TD>
            <select onchange="modif(<?php echo $resu->art_id ?>,11,this.value,'pseudo',1)">
                <OPTION></option>
                <?php
                $req_pseudo = 'SELECT * FROM Pseudos ORDER BY pse_id';
                $r_pseudo = $idcom->query($req_pseudo);
                while ($rq_pseudo = $r_pseudo->fetch_object()) {
                    $sel = ($rq_pseudo->pse_id == $resu->art_pseudo) ? ' selected' : '';
                    echo "<option value='" . $rq_pseudo->pse_id . "'" . $sel . ">" . $rq_pseudo->pse_nom . "</option>";
                }
                ?>
            </select>
        </TD>
    </tr>
</table>

<script>
    function remise(val, cp) {
        if (cp == 'ttc') {
            $(this).addClass('jaune');
            modif(<?php echo $resu->art_id ?>, 11, val, 'ttc', 1);
            if ($('#rem').val() > 0) {
                charge('calcul_remise', '<?php echo $resu->art_id . "&tva=" . $resu->tva_nom ?>&ttc=' + val + '&cp=' + cp + '&rem=' + $('#rem').val(), 'prix');
            }
        } else if (cp == 'remise') {
            if ((val == '0.00') || (val == '0') || (val == '')) {

                modif(<?php echo $resu->art_id ?>, 11, 0.00, 'remise', 1);
                charge('article', <?php echo $resu->art_id ?>, 'panneau_g');
            } else {
                charge('calcul_remise', '<?php echo $resu->art_id . "&ttc=" . $resu->art_ttc . "&tva=" . $resu->tva_nom ?>&cp=' + cp + '&rem=' + val, 'prix');
            }
        }
    }

    $('#popup').css('background-color', '<?php echo $_SESSION['fondF_' . $_SESSION[$dossier]] ?>');
    if ($('#ligne_rayon').html() == "") $('#ligne_rayon').parent().addClass('erreur');
    if ($('#ligne_editeur').html() == "") $('#ligne_editeur').parent().addClass('erreur');
    if ($('#ligne_tva').html() == "") $('#ligne_tva').parent().addClass('erreur');
</script>