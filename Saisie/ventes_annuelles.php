<?php
if (!isset($an)) {
    $an=ANNEE;
}//à verifier, problème rencontré sur la version distante
$ancien=$an-1;
require "../php/config.php";
$limite=(date("Y")-1)."-".$limite=date("m")."-".$limite=date("j");
if ($ancien < $config['debut']) {//première année de l'application
    $insert="CREATE TEMPORARY TABLE Ventes AS (SELECT tic_article AS ven_article, COUNT(tic_article) AS ven_ct FROM Tickets_$an JOIN Resume_ticket_$an ON rst_id=tic_num WHERE DATE(rst_validation) > '".ANNEE."-01-01' GROUP BY tic_article)";
} else {
    $insert="CREATE TEMPORARY TABLE Ventes AS 
        (SELECT tic_article AS ven_article,
                SUM(tic_quantite) AS ven_ct
                FROM 
                ((SELECT tic_article,
                tic_quantite
                    FROM Tickets_$ancien 
                    JOIN Resume_ticket_$ancien ON rst_id=tic_num 
                        WHERE DATE(rst_validation) > '$limite' AND LENGTH(tic_num) < 10)
        UNION ALL
                (SELECT tic_article,
                    tic_quantite
                        FROM Tickets_$an
                            WHERE LENGTH(tic_num) < 10))
        AS UnionTable GROUP BY tic_article)";
}
$idcom->query($insert);
$idcom->query(
    "ALTER TABLE Ventes
    ADD KEY ven_ct (ven_ct)"
);
if ($idcom->error) {
    echo $idcom->errno." ".$idcom->error."<br>";
}
//table des commandes en cours
$req_encours = "CREATE TEMPORARY TABLE encommande AS SELECT com_article, 
        com_quantite,
        com_id,
        rsc_etat,
        eds_serveur,
        eds_editeur
            FROM Commandes_".ANNEE."
            JOIN Resume_commande_".ANNEE." ON rsc_id=com_numero 
            JOIN Editeur_serveur ON eds_serveur=rsc_serveur 
                WHERE rsc_etat = 1 OR rsc_etat = 1 AND com_article != 1";
$idcom->query($req_encours);
