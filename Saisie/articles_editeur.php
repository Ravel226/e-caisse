<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

require $incpath."Saisie/ventes_annuelles.php";

$req_max="SELECT MAX(ven_ct) as maxi                      
                          FROM Articles
                          LEFT JOIN Ventes ON ven_article = art_id
                          JOIN Editeurs ON edi_id = art_editeur
                          JOIN Editeur_serveur ON eds_editeur=edi_id
                            WHERE edi_id =$req
                              AND art_statut = '1'";
$r_max=$idcom->query($req_max);
$rq_max=$r_max->fetch_object();

$req_recher="SELECT Vt1_nom,
                    art_stk,
                    art_statut AS Vart_statut,
                    ven_ct,
                    art_id,
                    art_unite,
                    edi_nom,
                    edi_id,
                    unv_abrege,
                    com_article,
                    com_quantite,
                    com_id,
                    rsc_etat,
                    edi_type,
                    sta_nom,
                    art_statut,
                    Editeur_serveur.*
                        FROM Articles
                        JOIN Unites_vente ON unv_id = art_unite
                        JOIN Vtit1 ON Vt1_article = art_id
                        LEFT JOIN Ventes ON ven_article = art_id
                        JOIN Editeurs ON edi_id = art_editeur
                        JOIN Editeur_serveur ON eds_editeur=edi_id
                        JOIN Statut on sta_id = art_statut
                        LEFT JOIN encommande ON com_article = art_id
                            WHERE edi_id =$req
                                GROUP BY art_id
                                    ORDER BY ven_ct DESC, art_stk DESC, Vt1_nom";
                                
$r_recher=$idcom->query($req_recher);
if ($r_recher->num_rows == 0) {
    echo '<img src="/images/attention.png"> Il n\'y a pas d\'article en vente pour cet éditeur';
    exit;
}
$resu=$r_recher->fetch_object();
$type = $resu->edi_type;
$etat = 0;
//création d'une commande pour les articles en mouvement de stock se basant sur les dernière ventes depuis la dernière
if ($type == 1) {
    //recherche de la dernière commande
    $req_commande = 'SELECT rsc_date,
                            rsc_etat,
                            rsc_etat
                                FROM Resume_commande_'.ANNEE.' 
                                    WHERE rsc_serveur = '.$resu->eds_serveur.'
                                        ORDER BY rsc_id DESC LIMIT 0,1';
    $r_commande = $idcom->query($req_commande);
    if ($r_commande->num_rows != 0) {
        $rq_commande =$r_commande->fetch_object();
        $derniere_commande = $rq_commande-> rsc_date;
        $etat = $rq_commande-> rsc_etat;
    } else {
        $derniere_commande = ANNEE.'-01-01';
    }
    $serveur = $resu->eds_serveur;
} 
$nb = $r_recher->num_rows;
if ($nb == 0) {
    echo '<img src="/images/attention.png"> Il n\'y a pas d\'article en vente pour cet éditeur';
    exit;
}
$r_recher->data_seek(0);
$nom = "de ".$resu->edi_nom." (".$resu->edi_id.")";
require "liste_articles.php";

if ($type == 1) {
    if (($etat == 0)||($etat == 3)) {
        echo "<br><em>Note :</em>  Les articles en compte ne sont pas ne seront pas intégrés à la commande<br><div id='gen_com'><button onclick=\"charge('commandes/commande_mouvementstock','".$serveur."&lim=".$derniere_commande."','gen_com')\">Commander les articles vendus</button></div>";
    } else {
        echo "<h3>Il y a une commande en cours, terminez-la avant d'en faire une autre.</h3>";
    }    
}

?>

