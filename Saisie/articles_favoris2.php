<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
if($req != "") {

    //détail des articles qui s'afficheront sous le nom du Favoris
    $req_art = "SELECT Vt1_nom, Vt1_article FROM Vtit1 JOIN Articles ON art_id = Vt1_article WHERE art_rayon = $req";
    $r_art = $idcom->query($req_art);
    while($rq_art =$r_art->fetch_object()) {
        echo '<tr class="blanc" id="'.$rq_art->Vt1_article.'"><td>&nbsp;&nbsp;</td><td class="blanc">'.$rq_art->Vt1_nom.'</td></tr>';
    }
        ?>    
    <script>$(document).ready(function() {
        $('table#Favoris td.blanc').click(function(){
            $('td.blanc').css('fontWeight','normal');
            $(this).css('fontWeight','bold');
            id = $(this).parent().attr('id');            
            charge('article',id,'panneau_g');
        });
    });
    </script>
    <?php
    exit;
}
?>
<script>
$(document).ready(function() {
    $('table#Favoris tr.parent').click(function(){
        $('table#Favoris tr').css('fontWeight','normal');
        $(this).css('fontWeight','bold');
        id = $(this).attr('id');
        $('table#Favoris tr.blanc').remove();
        apres('articles_favoris<?php echo $config['favoris']?>',id,id);
    });
    $("#panneau_g").height($("#affichage").height()-10);
    if ($("#liste").height() > 10 ){
    $('#popup_g').css('backgroundColor','<?php echo $_SESSION['fondC_'.$_SESSION[$dossier]]?>');
    $('#popup_g').height($("#affichage").height()-100);
    imgleft=$('#popup_g').width()-50+'px';
    $('#fermer').css("margin-left",imgleft);
    $('#fermer').css('display','block');
    }
});
</script>
<h3>Articles en favoris</h3>
<table id="Favoris" class="generique">
<?php
$req_favoris = "SELECT ray_nom , ray_id FROM Rayons WHERE ray_fav = 1;";
$r_favoris = $idcom->query($req_favoris);
$n = 0;
while ($rq_favoris =$r_favoris->fetch_object()) {
    $coul=($n%2 == 0)?$coulCC:$coulFF;
    echo "<tr class='parent' style='background-color:".$coul."' id='".$rq_favoris->ray_id."'><TD colspan=2>".$rq_favoris->ray_nom."</TD></tr>";
    $n++;
}
?>
