<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$an= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
require $incpath."php/config.php";
connexobjet();
if ($an == '') {
    $an=ANNEE;
}
// Factures en attente de validation
?>
<style>
.clair{
background-color:<?php echo $coulCC?>;
}
.fonce{
background-color:<?php echo $coulFF?>;
}
</style>
<?php
$precedent= $an-1;
if ($config['debut'] < date('Y') ) {
    $complement = "(SELECT edi_id, 
    edi_nom,
    jrn_facture,
    rsc_id,
    rsc_date,
    rsc_ttc,
    edi_type,
    $precedent AS fac_an
        FROM Resume_commande_$precedent
        LEFT JOIN Journal_factures_$precedent ON rsc_id = jrn_facture 
        JOIN Editeurs ON edi_id = rsc_serveur 
            WHERE rsc_util = $_SESSION[$dossier]
            AND jrn_facture IS NULL
            AND rsc_etat = 3
            AND edi_type != 1)
UNION ALL";
} else {
    $complement = '';
}
$req_recher= $complement." 
            (SELECT edi_id, 
                        edi_nom,
                        jrn_facture,
                        rsc_id,
                        rsc_date,
                        rsc_ttc,
                        edi_type,
                        $an AS fac_an
                            FROM Resume_commande_$an
                            LEFT JOIN Journal_factures_$an ON rsc_id = jrn_facture 
                            JOIN Editeurs ON edi_id = rsc_serveur 
                                WHERE rsc_util = $_SESSION[$dossier]
                                AND jrn_facture IS NULL
                                AND rsc_etat = 3
                                AND edi_type != 1)			
                                    ORDER BY rsc_id";
$r_recher=$idcom->query($req_recher);
$n = 0;
if ($r_recher->num_rows > 0) {
    echo "<h3>Journal de factures</h3><table class='generique'>
    <tr><thead><th colspan = '4'>Factures non validées</th></tr>
    <tr><th>Serveur</th><th>Numéro</th><th>Valeur</th><th>Date de réception</th></tr>
    </thead><tbody>";
    
    while ($rq_recher = $r_recher->fetch_object()) {
        $coul=($n % 2 == 0)?" class='clair'":" class='fonce'";
        echo "<tr onclick = \"charge('journal_facture','".$rq_recher->rsc_id."&an=".$rq_recher->fac_an."','panneau_d')\" ".$coul."><td>".$rq_recher->edi_nom."</td><td align=right>".$rq_recher->rsc_id."</td><td align=right>".$rq_recher->rsc_ttc."</td><td align=right>".dateFR($rq_recher->rsc_date)."</td></tr>";
        $n++;
    }
    echo "</tbody></table>";
}
//factures validées
$req_recher="SELECT edi_id, 
                    edi_nom 
                        FROM Resume_commande_$an
                        LEFT JOIN Journal_factures_$an ON rsc_id = jrn_facture 
                        JOIN Editeurs ON edi_id = rsc_serveur 
                            WHERE rsc_util = $_SESSION[$dossier]
                            AND rsc_etat = 3
                                GROUP BY edi_id
                                    ORDER BY edi_nom";

$r_recher=$idcom->query($req_recher);
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_recher->num_rows;
$annees = "";
for ($i = ANNEE; $i >= $config['debut']; $i--) {
    if ($i == $an) {
        $sel = " selected";
    } else {
        $sel = '';
    }
    $annees .= "<option".$sel.">".$i."</option>";
}
?>

<h3>Les factures <select id='annees' onchange="force('factures',this.value,'panneau_g')"><?php echo $annees?></select></h3>
<select onchange="charge('factures_serveur',$('#annees').val()+'&serveur='+this.value,'liste_factures')">
<option></option>
<?php
while ($rq_recher=$r_recher->fetch_object()) {
    echo "<option value='".$rq_recher->edi_id."'>".$rq_recher->edi_nom."</option>\n";
}
?>
</select>
<div id='liste_factures'></div>
<script>
$("#panneau_d").height($('#affichage').height()-10);
</script>