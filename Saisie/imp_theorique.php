<?php
session_start();
header("Content-disposition: attachment; filename=imp_theorique.pdf");
// Dit au navigateur que les donn�es retourn�es sont un fichier csv.
header("Content-Type: application/pdf");

if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
// print_r($_SESSION);
require_once '../fpdf181/fpdf.php';
//*************attention ecodage cp-1252******************************************
class PDF_TicketsFinJournee extends FPDF
{
    // Propri�t�s priv�es
    public $Fonte_Taille = 7;
    public $Fonte_Nom = "Arial";

    public $Ligne_Hauteur = 15;
    // public $Line_Height = 10;

    public $nbColonnes;
    public $interColonne;
    public $nbLignes;
    public $Colonne_Largeur;
    public $ligne=0;
    public $colonne=0;
    public $N_Page=0;
    public $nbPages=0;
    public $Titre_Page=false;
    public $total_ligne;
    public $largeurPrix=15;
    public $largeurQte=7;


    // Give the height for a char size given.
    public function _Get_Height_Chars($pt)
    {
        // Tableau de concordance entre la hauteur des caract�res et de l'espacement entre les lignes
        return $pt*.52;
    }


    function __construct($nbColonnes, $margeG, $margeH, $interColonne)
    {
        parent::__construct();
//    $this->Set_Font_Name($this->Fonte_Nom);
//    $this->SetMargins($this->_Marge_Gauche,$this->_Marge_Haut);
        $this->SetAutoPageBreak(false);
        $this->Set_Font_Size($this->Fonte_Taille);
        $this->nbColonnes=$nbColonnes;
        $this->lMargin=$margeG;
        $this->tMargin=$margeH;
        $this->Colonne_Largeur=($this->GetPageWidth() - $this->lMargin - $this->rMargin -($nbColonnes-1)*$interColonne) / $this->nbColonnes;
        $this->nbLignes=intval(($this->GetPageHeight() - $this->tMargin- $this->bMargin) / 5) - 4;
        $this->interColonne=$interColonne;
        // $this->Open();
        $this->initPage(true);
        // print_r($this);
    }

    // M�thode qui permet de modifier la taille des caract�res
    // Cela modifiera aussi l'espace entre chaque ligne
    public function Set_Font_Size($pt)
    {
        if ($pt > 3) {
            $this->Fonte_Taille = $pt;
            $this->Ligne_Hauteur = $this->_Get_Height_Chars($pt);
            $this->SetFontSize($this->Fonte_Taille);
        }
    }

    // Method to change font name
    public function Set_Font_Name($fontname)
    {
        if ($fontname != '') {
            $this->Fonte_Nom = $fontname;
            $this->SetFont($this->Fonte_Nom);
        }
    }

    public function initColonne($nCol)
    {
        $this->colonne=$nCol;
        $this->ligne=0;
        if (($this->Titre_Page)&&($nCol>1)) {
            $this->ligne++;
        }
    }

    public function initPage($titre=false)
    {
        //init des variables de lignes et colonnes
        $this->AddPage();

        $this->ligne=0;
        $this->initColonne(1);
        $this->N_Page+=1;

        if ($titre) {
            // affichage de la ligne de titre si besoin
            $this->SetFillColor(0, 255, 255);
            $this->SetFont('Arial', 'B', 10);

            $this->SetX($this->lMargin);
            $this->SetY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->Cell($this->GetPageWidth()-$this->lMargin-$this->rMargin-22, 5, '�TAT DU STOCK au '.date("j/n/Y"), 1, 0, 'C', 1);
            $this->SetFont('Arial', 'I', 8);
            $this->Cell(22, 5, ' page '.$this->N_Page, 1, 0, 'C', 1);
            $this->ligne+=1;
            $this->Titre_Page=true;
        }
    }

    public function addLigne($type, $valeurs)
    {
        //print_r($this);
        if ($type=="titreTicket") {
            $this->SetFillColor(0, 255, 255);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur, $this->Ligne_Hauteur, $valeurs, 1, 0, 'C', 1);
        } elseif ($type=="article") {
            //affichage d'une ligne article
            $this->SetFillColor(255, 255, 255);
            $this->SetFont('Arial', 'B', 8);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur-$this->largeurQte-$this->largeurPrix, $this->Ligne_Hauteur, $valeurs['titre'], 1, 0, 'L', 1);
            $this->cell($this->largeurPrix, $this->Ligne_Hauteur, sprintf("%01.2f", $valeurs['prix'])." �", 1, 0, 'R', 1);
            $this->cell($this->largeurQte, $this->Ligne_Hauteur, $valeurs['qte'], 1, 0, 'R', 1);
            $this->total_ligne--;
        } elseif ($type=="blanche") {
            //affichage d'une ligne blanche
            $this->SetFillColor(255, 255, 255);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur-$this->largeurQte-$this->largeurPrix, $this->Ligne_Hauteur, " ", 0, 0, 'L', 1);
            $this->total_ligne--;
        }

        //on passe  la ligne
        $this->ligne+=1;

        // maintenant on test si on doit changer de colonne ou de page
        if ($this->ligne>=$this->nbLignes) {
            // on change de colonne
            $this->initColonne($this->colonne+1);


            if ($this->colonne>$this->nbColonnes) {
                //on change de page
            $this->initPage(true); // ou initPage($titrePage); si tu peux le rafficher
            }
        }
    }
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$pdfTickets=new PDF_TicketsFinJournee(2, 10, 10, 5);
/*    $r=0;*/
$ray= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$et= filter_input(INPUT_GET, "et", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tab_ray = array();
$n = 0;
$comp = "";
$req_recher = "";
foreach (explode(",", $ray) as $value) {
    $tab_ray[] = $value;
    $comp .=($n > 0)?" OR ":"";
    $comp .= "art_rayon = ".$value;
    $n++;
}
$comp1=($et == 0)?" AND art_stk != 0.00":"";
$req_rayon ="SELECT Vt1_nom ,
    art_ttc,
    art_stk,
    art_unite,
    ray_nom,
    ray_id
        FROM Articles
        JOIN Vtit1 ON Vt1_article = art_id
        JOIN Rayons ON ray_id = art_rayon
        JOIN Secteurs ON sec_id = ray_secteur
            WHERE  (".$comp.")
                $comp1
                ORDER BY sec_id,
                ray_id,
                Vt1_nom";
$i=0;
$rayon = '';
$r_rayon = $idcom->query($req_rayon);
while ($rq_rayon=$r_rayon->fetch_object()) {
    $qt=($rq_rayon->art_unite == 1)?sprintf('%d', $rq_rayon->art_stk):$rq_rayon->art_stk;
    if ($rayon!=$rq_rayon->ray_id) {
        $pdfTickets->addLigne("blanche", "");
        $pdfTickets->addLigne("titreTicket", utf8_decode($rq_rayon->ray_nom));
    }
    $pdfTickets->addLigne("article", array("titre"=>utf8_decode($rq_rayon->Vt1_nom),"prix"=>$rq_rayon->art_ttc,"qte"=>$qt));
    $rayon=$rq_rayon->ray_id;
}
 $pdfTickets->Output();
