<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= substr(filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS),1);
$comp = "";
if ($req !="") {
    $comp = ' AND eds_serveur = '.$req;
}
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
$req_recher="SELECT edi_nom, edi_id, eds_editeur, Vser_nom, edi_etat, COUNT(Vart_id) AS ct
                        FROM Editeur_serveur
                        JOIN Editeurs ON eds_editeur = edi_id
                        LEFT JOIN Varticle_editeur ON edi_id = Vart_idediteur
                        LEFT JOIN Vserveurs ON Vser_id = eds_serveur
                          WHERE edi_utilisateur = ".$_SESSION[$dossier]." ".$comp."
                            GROUP BY Vart_idediteur
                            ORDER BY edi_nom ";
$r_recher=$idcom->query($req_recher);
$resu=$r_recher->fetch_object();
$nb = $r_recher->num_rows;
$r_recher->data_seek(0);
?>
<img style="position:absolute;right:10px;" id="fermer" src="/images/annuler.png" onclick='$("#popup_g").empty();$("#popup_g").css("visibility","hidden");'>
<h3 id='liste' style="width:100%;"><?php echo $nb?> éditeurs</h3>
<table style="margin-top:0px;" id="liste_editeur" cellpadding="0" cellspacing="0" border="1" class="tablesorter">
<tr><th>Éditeurs</th><th>Nombre d'articles</th><th>Serveur</th></tr>
<tr><TD><button onclick="charge('detail_editeur','000','panneau_d')">Nouvel éditeur</button></TD><td></td></tr>

<script>
$(document).ready(function(){
    $('td').click(function(){
    $('td').removeClass('ombre');
    $(this).addClass('ombre');
    id=$(this).parent().attr('serv');
    charge('detail_editeur',id.substr(1),'panneau_d');
    });
    $('#liste_editeur td').css('cursor','pointer');

    if ($('#popup_g').css('visibility') == 'visible'){
        $("#popup_g #fermer").css("visibility","visible"); 
    } else if ($('#popup_g').css('visibility') == 'hidden'){
    $("#fermer").remove();
    }
});
</script>
<?php
$n=0;
while ($resu=$r_recher->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    if ($resu->edi_etat == '1') {
        $coul = $_SESSION['surligne_'.$_SESSION[$dossier]];
    }
    $Vser_nom=($resu->Vser_nom == $resu->edi_nom)?'':$resu->Vser_nom;
    echo '<tr serv="E'.$resu->eds_editeur.'" style="background-color:'.$coul.'"><td>'.$resu->edi_nom."</td><td>".$resu->ct."</td><td><em>".$Vser_nom."</em></td></tr>";
    $n++;
}

?>
</table>
<script>
$("#panneau_g").height($("#affichage").height()-10);
</script>
