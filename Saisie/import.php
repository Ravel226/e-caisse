<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

?>
<center><img src="/images/travaux.png"></center>
<h3>Importation de données (travail en préparation)</h3>
L'importation de données se fait au format csv avec ';' comme séparateur et sans entête.<br>
On a intérêt à soigner le fichier d'importation<br>
Il devra suivre la description suivante :<br><br>

<pre>'EAN13';'Titre';'Auteur';'Référence Éditeur';'référence interne';' Tva';' PU TTC';' PU ht';' Unité de vente';' Éditeur';' Rayon'</pre>
Note sur les champs :</pre>
<br><br>
<b>EAN13</b> doit-être un code officiel. S'il n'y en a pas, on laissera le champ vide, un code interne sera créé<br>
Les champs Auteur, Référence Éditeur, Référence interne sont obtionnels et peuvent-être vides<br>
<b>TVA</b> suivra la convention suivante : <br>
<?php
$req_tva = 'SELECT tva_id, tva_nom FROM Tva ORDER BY tva_id';
$r_tva = $idcom->query($req_tva);
while($rq_tva =$r_tva->fetch_object()) {
    echo $rq_tva->tva_id." = ".$rq_tva->tva_nom."<br>";
}
?>
<br>
<b>Unités de vente</b> :<br> 
<?php
$req_unit = 'SELECT * FROM Unites_vente';
$r_unit = $idcom->query($req_unit);
while($rq_unit =$r_unit->fetch_object()) {
    echo $rq_unit->unv_id." = ".$rq_unit->unv_nom."<br>";
}
?>