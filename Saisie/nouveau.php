<?php
session_start();
// print_r($_SESSION);
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p,$i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$art= filter_input(INPUT_GET, "art", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$nouv_id=$req;
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
?>
<script>
$(document).ready(function(){
    $('#nouveauCB').on('keydown',function(event){
      if(event.which == 13){
            charge('creation_cb',$('#nouveauCB').val(),'creationCB');
            }
        });
    });

</script>
<h3>Création d'un nouvel article</h3>
<table class="generique"><TR><TH>Code barre interne</TH><TH>Code barre officiel</TH></TR>
<tr><TD><button onclick="charge('nouveau_cb.inc','','panneau_g')">Code barre interne</button></TD><td><input type="text" placeholder='ean13' id='nouveauCB'></td></tr>
</table>
<div id='creationCB'>
</div> 
