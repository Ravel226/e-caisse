<?php
$tab = explode('/', $_SERVER['REQUEST_URI']);
$dossier = $tab[1];
require $incpath . "php/config.php";
if ($config[('debut')] == '') {
?>
    <script>
        top.location.href = '/index.php';
    </script>
    <?php
}

function convertColor($color)
{
    //convert hexadecimal to RGB
    if (!is_array($color) && preg_match("/^[#]([0-9a-fA-F]{6})$/", $color)) {
        $hex_R = substr($color, 1, 2);
        $hex_G = substr($color, 3, 2);
        $hex_B = substr($color, 5, 2);
        $RGB = hexdec($hex_R) . "," . hexdec($hex_G) . "," . hexdec($hex_B);
        return $RGB;
    } else { //convert RGB to hexadecimal
        if (!is_array($color)) {
            $color = explode(",", $color);
        }
        $hex_RGB = '';
        foreach ($color as $value) {
            $hex_value = dechex($value);
            if (strlen($hex_value) < 2) {
                $hex_value = "0" . $hex_value;
            }
            $hex_RGB .= $hex_value;
        }

        return "#" . $hex_RGB;
    }
}
// print_r($_SESSION);
// exit;
function getMidColors($rgb1, $rgb2, $nb)
{
    $rgb1 = explode(",", $rgb1);
    $rgb2 = explode(",", $rgb2);
    $rgb_mid = '';
    for ($i = 0; $i < 3; $i++) {
        for ($j = 1; $j <= $nb; $j++) {
            if ($rgb1[$i] < $rgb2[$i]) {
                $rgb_mid[$j] .= round(((max($rgb1[$i], $rgb2[$i]) - min($rgb1[$i], $rgb2[$i])) / ($nb + 1)) * $j  + min($rgb1[$i], $rgb2[$i]));
            } else {
                $rgb_mid[$j] .= round(max($rgb1[$i], $rgb2[$i]) - ((max($rgb1[$i], $rgb2[$i]) - min($rgb1[$i], $rgb2[$i])) / ($nb + 1)) * $j);
            }
            if ($i != 2) {
                $rgb_mid[$j] .= ",";
            }
        }
    }
    return $rgb_mid;
}

if ($dossier != 'php') {
    $tabF = explode(",", convertColor($_SESSION['fondF_' . $_SESSION[$dossier]]));
    $coulFF = convertColor(round((255 - $tabF[0]) / 2 + $tabF[0]) . "," . round((255 - $tabF[1]) / 2 + $tabF[1]) . "," . round((255 - $tabF[2]) / 2 + $tabF[2]));
    $tabC = explode(",", convertColor($_SESSION['fondC_' . $_SESSION[$dossier]]));
    $coulCC = convertColor(round((255 - $tabC[0]) / 2 + $tabC[0]) . "," . round((255 - $tabC[1]) / 2 + $tabC[1]) . "," . round((255 - $tabC[2]) / 2 + $tabC[2]));
}
// $tabF=explode(",", convertColor($_SESSION['fondF_'.$_SESSION[$dossier]]));
// /* print_r($tabF);
// echo $coulFF =convertColor(81,135,0); */
// echo '<br>'.$coulFF = convertColor(round((255-$tabF[0]/2)+$tabF[0]).",".round((255-$tabF[1]/2)+$tabF[1]).",".round((255-$tabF[2]/2)+$tabF[2]));
// $tabC=explode(",", convertColor($_SESSION['fondC_'.$_SESSION[$dossier]]));
// $coulCC=convertColor(round((255-$tabC[0]/2)+$tabC[0]).",".round((255-$tabC[1]/2)+$tabC[1]).",".round((255-$tabC[2]/2)+$tabC[2]));

function dateUS($date)
{
    // $tab_date=explode("-",$date);HH:MM:SS.
    $date = date("Y", $date) . "-" . date("m", $date) . "-" . date("d", $date);
    return $date;
}

function dateFR($date)
{
    $tab_date = explode("-", $date);
    if ($date != '') $date = str_pad(($tab_date[2]), 2, "0", STR_PAD_LEFT) . "-" . $tab_date[1] . "-" . $tab_date[0];
    return $date;
}

function timestampA($date)
{
    $tab_date = explode("-", $date);
    $date = mktime(0, 0, 0, $tab_date[1], $tab_date[2], $tab_date[0]);
    return $date;
}

function accent($nom)
{
    $nom = mb_strtolower($nom, 'UTF8'); //exit;
    $patterns = array('/à/', '/á/', '/â/', '/ã/', '/ä/', '/å/', '/ą/', '/č/', '/ò/', '/ó/', '/ô/', '/õ/', '/ö/', '/ø/', '/ė/', '/è/', '/é/', '/ê/', '/ë/', '/ę/', '/ç/', '/ì/', '/í/', '/î/', '/ï/', '/į/', '/ū/', '/ų/', '/ù/', '/ú/', '/û/', '/ü/', '/ÿ/', '/ñ/', '/ž/', '/š/', '/æ/', '/œ/', '/ç/');
    $replacements = array('a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'o', 'o', 'o', 'o', 'o', 'o', 'e', 'e', 'e', 'e', 'e', 'e', 'c', 'i', 'i', 'i', 'i', 'i', 'u', 'u', 'u', 'u', 'u', 'u', 'y', 'n', 'z', 's', 'ae', 'oe', 'c');
    $nom = preg_replace($patterns, $replacements, $nom);
    // echo $nom;
    return $nom;
};

// https://edmondscommerce.github.io/php/barcode/ean13-barcode-check-digit-with-php.html
function ean13($ean13)
{
    if (strlen($ean13) < 13) {
        echo "<img src = '/images/attention.png'> Le code devrait avoir 13 chiffres";
        exit;
    }
    //first change digits to a string so that we can access individual numbers
    $ean13 = (string)$ean13;
    // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
    $even_sum = $ean13[1] + $ean13[3] + $ean13[5] + $ean13[7] + $ean13[9] + $ean13[11];
    // 2. Multiply this result by 3.
    $even_sum_three = $even_sum * 3;
    // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
    $odd_sum = $ean13[0] + $ean13[2] + $ean13[4] + $ean13[6] + $ean13[8] + $ean13[10];
    // 4. Sum the results of steps 2 and 3.
    $total_sum = $even_sum_three + $odd_sum;
    // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
    $next_ten = (ceil($total_sum / 10)) * 10;
    $check_digit = $next_ten - $total_sum;
    if ($ean13[12] != $check_digit) {
        echo "<h1>Erreur de lecture ou de code<br>Il devrait-être : <br>" . substr($ean13, 0, 12) . $check_digit . "</h1>"; ?>
        <script>
            $('#insert_cb').val('')
        </script>
<?php
        exit;
    }
    return $check_digit;
}

?>