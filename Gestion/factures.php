<?php
session_start();
// print_r($_SESSION);
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
} 
$an = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
require $incpath."php/config.php";
connexobjet();
if (!$an) {
    $an = date("Y");
}
$req_comptes="SELECT cpt_nom ,
                    rst_total,
                    mdr_nom ,
                    fac_ticket,
                    rst_validation,
                    fac_id
                        FROM Factures_$an
                        JOIN Resume_ticket_$an ON fac_ticket=rst_id
                        JOIN Comptes ON fac_cp =cpt_id
                        JOIN Mode_reglement ON mdr_id =rst_etat
                            ORDER BY fac_id DESC";
$r_comptes=$idcom->query($req_comptes);
?>
<script src="/js/jquery.tablesorter.js"></script>
<script>
$('#factures td').click(function(){
    if ($(this).html().slice(0,8)!='<a href=') {
        charge('detail_facture',$(this).parent().attr('fac')+'&an=<?php echo $an?>','panneau_d');
    }
    $('#factures tr').css('fontWeight','normal');
    $(this).parent().css('fontWeight','bold');
})

$(document).ready(function(){
     $("#factures").tablesorter({ widgets: ['zebra']});
    }
);
</script>
<?php
if ($an > $config['debut']) {
    $bt1="<button class='pt_bt' style='float:left;' onclick=\"charge('factures',".($an-1).",'panneau_g')\"><&nbsp;".($an-1)."</button>";
    } else {
        $bt1="<button class='pt_bt' style='float:left'></button>";
    }

if ($an < date("Y")) {
    $bt2="<button class='pt_bt' style='float:right;' onclick=\"charge('factures',".($an+1).",'panneau_g')\">&nbsp;".($an+1)."></button>";
} else {
    $bt2="<button class='pt_bt' style='float:right'></button>";
}

?>
<h3><?php echo $bt1?>Factures de l'année <?php echo $an?><?php echo $bt2?></h3>
<center><table id='factures' class="generique" class="tablesorter">
  <thead>
  <TR>
  <TH>N°</TH><TH>Nom</TH><TH>Valeur</TH><TH>Règlement</TH><TH>Date</TH><th></th>
  </TR>
  </thead>
  <tbody>
<?php
$n=0;
while ($resu=$r_comptes->fetch_object()) {
    $coul=($n%2 == 0)? $coulCC:$coulFF;
    $tab=explode(' ', $resu->rst_validation);
    $tab_date=explode('-', $tab[0]);
    echo "<tr style='background-color:".$coul."' fac='".$resu->fac_id."'><TD>Lib".($an*1000 + $resu->fac_id)."</TD><TD>".$resu->cpt_nom."</TD><TD style='text-align:right'>".$resu->rst_total."&nbsp;€</TD><TD>".$resu->mdr_nom."</TD><TD>".$tab_date[2]."&nbsp;".$mois[$tab_date[1]*1]."</TD><td>";
    if (file_exists($incpath."pdf/factures/facture_".($an*1000 + $resu->fac_id).".pdf")) echo '<a href="'.$incpath."pdf/factures/facture_".($an*1000 + $resu->fac_id).'.pdf"><img src="../images/pdf.gif" width="14" height="14"></a>';
    echo "</td></tr>";
    $n++;
}
?>
</tbody>  
</table></center>

<script>
var b=$('#affichage').height() - 20;
$("#panneau_g").css('max-height', b);

</script>