<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p,$i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_serveur="SELECT * FROM Resume_commande_$an JOIN Editeurs ON edi_id = rsc_serveur WHERE rsc_id=$req";
$r_serveur=$idcom->query($req_serveur);
$rq_serveur=$r_serveur->fetch_object();
?>
</script>

<h3> Facture N° <?php echo $req." année ".$an?></h3>
<hr>
<?php

echo "<b>".$rq_serveur->edi_nom."</b><br>".stripslashes($rq_serveur->edi_adr1)."<br>";
if ($rq_serveur->edi_adr2 != "") {
    echo stripslashes($rq_serveur->edi_adr2)."<br>";
}
echo $rq_serveur->edi_cp." ".$rq_serveur->edi_ville;
if ($rq_serveur->edi_pays != "") {
    "<br>".$rq_serveur->edi_pays."<br>";
}
echo "<hr>";
//***********************************liste des articles de la facture****************************************


$req_commande="SELECT tit_nom, com_numero, com_ttc, com_quantite, com_remise , art_unite, com_pht
											FROM Commandes_$an 
												JOIN Titres ON tit_article = com_article 
												JOIN Articles ON art_id = com_article
													WHERE com_numero = ".$req." AND tit_niveau =1";
$r_commande=$idcom->query($req_commande);
if ($idcom->errno !="") {
    echo $idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_commande->num_rows;
?>
<style type="text/css">
#detfactures{width:90%}
#detfactures th {
text-align:center;
}
#detfactures td{
text-align:right
}
</style>
<table id="detfactures"><TR><TH>Article</TH><TH>quantité</TH><TH>Prix Publique</TH><TH>Remise<br>Prix&nbsp;achat</TH></TR>
<?php
$n = 0;
while ($rq_commande=$r_commande->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;    
    $quantite=($rq_commande->art_unite == 1)? sprintf("%d", $rq_commande->com_quantite):$rq_commande->com_quantite;
    $remise = ($rq_commande->com_remise == 0.00)?$rq_commande->com_pht." pht":$rq_commande->com_remise." rem.";
    $remise = ($remise == "0.0000 pht")?"":$remise;
    if ($rq_commande->com_ttc != 0.00) {
        echo "<tr style='background-color:".$coul.";'><td>".$rq_commande->tit_nom."</td><td>".$quantite."</td><td>".$rq_commande->com_ttc." €</td><td>".$remise."</td></tr>";
    }
    $n++;
}  
?></table>
<script>
var b=$('#affichage').height();
$("#panneau_d").css('max-height', b);
</script>