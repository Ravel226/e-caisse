<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$jour = date("Y-m-d");
$jour_1 = date("Y-m-d", mktime(0, 0, 0, date("m"), date("j") - 1, date("Y")));
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
$_SESSION['aide_' . $_SESSION[$dossier]] = 'G1';
connexobjet();
//jusqu'au 6 janvier, on vérifie si l'année précédente à bien été terminée en recherchant les dernières lignee de l'année précedente
if ((date("Y-m-d") <= date("Y-m-d", mktime(0, 0, 0, 1, 6, ANNEE)))) {
    $req_jour = "SELECT rsj_date,DATE_ADD(rsj_date, INTERVAL 1 DAY) AS jour_suivant FROM Resume_jour_" . (ANNEE - 1) . " WHERE rsj_statut = '2' ORDER BY rsj_date DESC LIMIT 0,1";
    $r_jour = $idcom->query($req_jour);
    $rq_jour = $r_jour->fetch_object();
    //si le dernier jour est < 31, l'année n'a pas été terminée
    if ($rq_jour->rsj_date != (date('Y') - 1) . "-12-31") {
        $tab_an = explode('-', $rq_jour->rsj_date);
        $an = $tab_an[0];
        $jour = $rq_jour->jour_suivant;
?>
        <style>
            table.jour {
                width: 100%
            }

            table.jour th {
                text-align: center
            }

            table.jour button {
                width: auto;
            }
        </style>

        <div id='entete'>
            <?php
            include "fin_jour.inc.php";
            ?>
        </div>
        <script>
            var w = $(window).height();
            $("#affichage").height(w - 40);
        </script>
        <div id='detail'></div>
<?php
        exit;
    } else {
        $an = ANNEE;
        //on verifie si le jour a été enregistré
        $req_jour = "SELECT rsj_date FROM Resume_jour_" . ANNEE . " WHERE rsj_statut = '2' ORDER BY rsj_date DESC LIMIT 0,1";
        $r_jour = $idcom->query($req_jour);
        $rq_jour = $r_jour->fetch_object();
        if ($r_jour->num_rows == 0) { //premier jour de l'année    
            $titre = "fin du 1er janvier";
            $jour = ANNEE . "-01-01";
        } elseif ($rq_jour->rsj_date == $jour_1) { //fin de ce jour    
            $titre = "fin du jour";
        } elseif ($rq_jour->rsj_date == $jour) {
            echo "<h3>Jour enregistré</h3>";
            exit;
        } else {
            $tab_j = explode("-", $rq_jour->rsj_date); //dernier jour enregistré
            //celui qu'on cherche est donc le suivant
            $cejour = timestampA($rq_jour->rsj_date);
            $jour = date("Y-m-d", mktime(0, 0, 0, date("m", $cejour), date("j", $cejour) + 1, date("Y", $cejour)));
            $tab_j = explode("-", $jour);
            $titre = "fin du " . $sem[date("w", timestampA($jour))] . " " . $tab_j[2] . " " . $mois[$tab_j[1] * 1] . " " . ANNEE;
        }
    }
} else {
    $an = ANNEE;
    //on verifie si le jour a été enregistré
    $req_jour = "SELECT rsj_date FROM Resume_jour_" . ANNEE . " WHERE rsj_statut = '2' ORDER BY rsj_date DESC LIMIT 0,1";
    $r_jour = $idcom->query($req_jour);
    $rq_jour = $r_jour->fetch_object();
    if ($r_jour->num_rows == 0) { //premier jour de l'année    
        $titre = "fin du 1er janvier";
        $jour = ANNEE . "-01-01";
    } elseif ($rq_jour->rsj_date == $jour_1) { //fin de ce jour    
        $titre = "fin du jour";
    } elseif ($rq_jour->rsj_date == $jour) {
        echo "<h3>Jour enregistré</h3>";
        exit;
    } else {
        $tab_j = explode("-", $rq_jour->rsj_date); //dernier jour enregistré
        //celui qu'on cherche est donc le suivant
        $cejour = timestampA($rq_jour->rsj_date);
        $jour = date("Y-m-d", mktime(0, 0, 0, date("m", $cejour), date("j", $cejour) + 1, date("Y", $cejour)));
        $tab_j = explode("-", $jour);
        $titre = "fin du " . $sem[date("w", timestampA($jour))] . " " . $tab_j[2] . " " . $mois[$tab_j[1] * 1] . " " . ANNEE;
    }
}
?>
<style>
    table.jour {
        width: 100%;
    }

    table.jour th {
        text-align: center;
    }

    table.jour button {
        width: auto;
    }
</style>

<div id='entete'>
    <?php
    require "fin_jour.inc.php";
    ?>
</div>
<script>
    $('#panneau_d').empty();
    $('.sp-container').remove();
    var w = $(window).height();
    $("#affichage").height(w - 40);
</script>
<div id='detail'></div>