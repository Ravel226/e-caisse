<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$cejour= filter_input(INPUT_GET, "jour", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
//***********************************************Attention encodage cp1252******************************
require $incpath.'/fpdf181/fpdf.php';
function jour_lettre($cejour)
{
    global $sem;
    $ce_jour=timestampA($cejour);
    $cejour=substr($sem[date("w", $ce_jour)], 0, 3)." ".date("j", $ce_jour);
    // $jour_semaine=date("w", $ce_jour);
    return $cejour;
}

class PDF_jour extends FPDF
{
    public $ligne = 0;
    protected $cemois;
    protected $Ligne_Hauteur = 15;
    public $nbLignes;
    protected $Colonne_Largeur;
    public $colonne = 1;
    public $nbColonne;
    protected $Titre_Page = false;
    public $N_Page=1;
    public $nbPages=1;
    public $totalligne;
    public $cejour;
    public $interColonne;

    function __construct($margeG, $margeH, $nbColonnes, $interColonne, $cejour)
    {
        parent::__construct();

        $this->SetAutoPageBreak(false);
        $this->SetFontSize(7);
        $this->FontSize = 7;
        $this->FontFamily = "Arial";
        $this->cejour = $cejour;

        $this->Ligne_Hauteur = $this->FontSize * .52;
                $this->nbColonnes=$nbColonnes;
                $this->lMargin=$margeG;
                $this->tMargin=$margeH;
                $this->bMargin = 10;
                $this->Colonne_Largeur=($this->GetPageWidth() - $this->lMargin - $this->rMargin -($nbColonnes-1)*$interColonne) / $this->nbColonnes;
                $this->nbLignes=($this->totalligne > 219)?73:76;  
                $this->interColonne=$interColonne;

        $this->ligne = 0;
        $this->colonne = 1;
        $this->nbColonne = $nbColonnes;
    }

    function initColonne($nCol)
    {
        $this->colonne=$nCol;
        $this->ligne=0;
        if (($this->Titre_Page)&&($nCol>1)) {
            $this->ligne++;
        }
    }

    function initPage()
    {
        $tab_jour=explode('-', $this->cejour);
        $mois=array("", "janvier", "f�vrier", "mars", "avril", "mai", "juin", "juillet", "ao�t", "septembre", "octobre", "novembre", "d�cembre");
        //init des variables de lignes et colonnes
        $this->AddPage();
        $this->ligne=0;
        $this->initColonne(1);
        //***on verifie si on est a la derniere page, si oui on retire 3 ligne en hauteur
        if ($this->nbPages==0) {//cas de la premiere page
            $this->nbPages=0;
        }
        if ($this->N_Page==$this->nbPages) {
            $this->nbLignes=$this->nbLignes-3;
        }
    
        $this->SetFillColor(0, 255, 255);
        $this->SetFont('Arial', 'B', 10);
        $this->SetX($this->lMargin);
        $this->SetY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin-2);//on laisse un leger espace
        $this->Cell($this->w - $this->lMargin - $this->rMargin -20, $this->Ligne_Hauteur, 'Tickets du '.$tab_jour[2].' '.$mois[($tab_jour[1]*1)].' '.$tab_jour[0], 1, 0, 'C', 1);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(20, $this->Ligne_Hauteur, ' page '.$this->N_Page."/".$this->nbPages, 1, 0, 'C', 1);
        $this->ligne+=1;
        $this->Titre_Page=true;
        $this->N_Page+=1;
    }
    function addLigne($type, $valeurs)
    {
        if ($type == "titre") {
            $mois=array("", "janvier", "f�vrier", "mars", "avril", "mai", "juin", "juillet", "ao�t", "septembre", "octobre", "novembre", "d�cembre");
            $this->SetFillColor(0, 255, 255);
            $this->SetFont('Arial', 'B', 14);
            $this->SetX($this->lMargin);
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->Cell($this->GetPageWidth - $this->lMargin - $this->rMargin, $this->Ligne_Hauteur + 5, 'Mois de ' . utf8_decode($mois[$this->cemois]) . ' ' . $this->_an, 1, 0, 'C', 1);
            $this->SetFont('Arial', 'I', 7);
            $this->ligne += 2;
        } elseif ($type=="titreTicket") {
            //affichage d'une ligne titre
            $this->SetFont('Arial', 'I', 7);
            $this->SetFillColor(100, 255, 100);
            //               $this->SetX($this->lMargin);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur, $this->Ligne_Hauteur, $valeurs, 1, 0, 'C', 1);
        } elseif ($type=="blanche") {
            //affichage d'une ligne blanche
            $this->SetLineWidth(0);
            $this->SetFillColor(255);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur, $this->Ligne_Hauteur, '', 0, 0, 'C', 1);
        } elseif ($type=="article") {
            //affichage d'une ligne article
            $this->SetLineWidth(0.1);
            $this->SetFillColor(255);
            $this->SetFont('Arial', 'I', 7);
            $largeurPrix=15;
            $largeurQte=7;
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur-$largeurQte-$largeurPrix, $this->Ligne_Hauteur, $valeurs['titre'], 1, 0, 'L', 1);
            $this->cell($largeurPrix, $this->Ligne_Hauteur, monetaireF($valeurs['prix'])." �", 1, 0, 'R', 1);
            $this->cell($largeurQte, $this->Ligne_Hauteur, $valeurs['qte'], 1, 0, 'R', 1);
            $this->totalligne--;
        } elseif ($type=="resume_Ticket") {
            //affichage d'une ligne titre fin de derniere page
            $cells=$valeurs['tva']+$valeurs['mdr'];//nombre de cellules utilisￜes
    
            $this->SetFillColor(255, 255, 0);
            $this->setX($this->w - $this->lMargin - $this->rMargin);
            $this->setY(277);
            $this->cell((($this->w-$this->lMargin-$this->rMargin)/$cells)*$valeurs['tva'], 5, "R�sum� du ".dateFR($this->cejour)." par taux de tva pour une valeur de ".monetaireF($valeurs['total'])." �", 1, 0, 'C', 1);
            $this->cell((($this->w-$this->lMargin-$this->rMargin)/$cells)*$valeurs['mdr'], 5, 'R�sum� par mode de r�glement', 1, 0, 'C', 1);
        } elseif ($type=="detail_resume") {
            $this->SetFillColor(255, 255, 255);
            $largeur_cell=($this->w-$this->lMargin-$this->rMargin)/COUNT($valeurs);
            //affichage d'une ligne titre
            $this->setY(282);
            $this->setX(10);
            foreach ($valeurs as $cle => $v) {
                $this->cell($largeur_cell, 5, $cle, 1, 0, 'C', 1);
            }
            //affichage des donn�es
            $this->setY(287);
            $this->setX(10);
            foreach ($valeurs as $cle => $v) {
                $this->cell($largeur_cell, 5, monetaireF($v).' �', 1, 0, 'C', 1);
            }
        }
        $this->ligne += 1;
        // maintenant on test si on doit changer de colonne ou de page
        if ($this->ligne>=$this->nbLignes) {
            // on change de colonne
            $this->initColonne($this->colonne+1);
            if ($this->colonne>$this->nbColonnes) {
                //on change de page
                $this->initPage(); // ou initPage($titrePage); si tu peux le rafficher
            }
        }

    }
}
$i=0;

// $cejour='2015-06-07';
// $cejour='2016-02-01';
$an=substr($cejour, 0, 4);
$mois=substr($cejour, 5, 2);


//Recherche des ticekts
/*echo */$req_ticket="SELECT rst_total, rst_validation, rst_mod FROM Resume_ticket_$an WHERE rst_jour = '$cejour'";
$n=0;

$result=array();
$r_ticket=$idcom->query($req_ticket);

//rechercher des articles

$req_articles="SELECT tic_id,
                    tic_num,
                    Vt1_nom,
                    tic_tva,
                    tic_quantite,
                    tic_prix,
                    tic_tt,
                    rst_id,
                    rst_total,
                    DATE_FORMAT(rst_validation, '%Hh %im %Ss') AS rst_validation,
                    mdr_nom
                        FROM Tickets_$an
                        JOIN Resume_ticket_$an ON tic_num = rst_id
                        JOIN Mode_reglement ON mdr_id = rst_etat
                        LEFT JOIN Vtit1 ON Vt1_article=tic_article
                            WHERE DATE(rst_validation) = '$cejour'
                                ORDER BY rst_id";
$r_articles=$idcom->query($req_articles);
$rq_num_articles=$r_articles->num_rows;
//********le nombre de lignes est egal a la somme de resume_ticket+nombre d'articles+la ligne de titre de chaque page+ligne blanche**************
$req_num_tickets=$r_ticket->num_rows;
$totalligne=$rq_num_articles+($req_num_tickets*2)+1;
// ($margeG, $margeH, $nbColonnes, $interColonne, $cejour)
$pdfTickets=new PDF_jour(10, 10, 3, 5, $cejour);

$pdfTickets->totalligne=$totalligne;
/*echo*/ $lignes_pr_pages=($pdfTickets->nbLignes)*$pdfTickets->nbColonnes;
/*echo "<br>".*/$nbLignesDernierePage=$totalligne % $lignes_pr_pages;
/*echo "<br>".*/$pdfTickets->nbPages=intval($totalligne / $lignes_pr_pages) + 1;

$pdfTickets->initPage();
$tva=array();
$mdr=array();
$lignejour = "";
$TTotal = 0.00;
// $n=0;
while ($rq_articles=$r_articles->fetch_object()) {
    if ($rq_articles->tic_num != $lignejour) {                     
        if ($pdfTickets->ligne > 1 ) {
            $pdfTickets->addLigne("blanche", $totalligne);
        }
        $pdfTickets->addLigne("titreTicket", $rq_articles->rst_id." / ".$rq_articles->rst_validation." / ".$rq_articles->rst_total." / ".utf8_decode($rq_articles->mdr_nom));
        $lignejour = $rq_articles->tic_num;
        $TTotal += $rq_articles->rst_total;
        @$mdr[utf8_decode($rq_articles->mdr_nom)] += $rq_articles->rst_total;
    }
    @$tva[$rq_articles->tic_tva." %"] += $rq_articles->tic_tt;

    $pdfTickets->addLigne("article", array("titre"=>substr(utf8_decode($rq_articles->Vt1_nom), 0, 30), "prix"=> $rq_articles->tic_prix, "qte"=>$rq_articles->tic_quantite));

}
//*********************affichage des lignes de resumes en bas de derniere page*************
$pdfTickets->addLigne("resume_Ticket", array("tva"=>COUNT($tva), "mdr"=>COUNT($mdr), "total"=>$TTotal));
$donnees=array_merge($tva, $mdr);
$pdfTickets->addLigne("detail_resume", $donnees);
$pdfTickets->Output("F", $incpath."pdf/jours/".$cejour.".pdf", 1);

//on recupere l'imprimante
ini_set("display_errors", 0);
//------------------------------------------------------------
$req_imp="SELECT * FROM Imprimantes WHERE imp_id=1";
$r_imp=$idcom->query($req_imp);
$rq_imp=$r_imp->fetch_object();
if ($r_imprimante-> num_rows != 0) {
    //verification de l'imprimante
    ini_set("display_errors", 0);
    $impr=array();
    exec('lpstat -v', $tab);//$tab=tableau contenant la description des imprimantes
    foreach ($tab as $v) {
        $ta=explode(" ", $v);
        array_push($impr, substr($ta[2], 0, -1));//on retire les : du nom de l'imprimante et on met dans un tableau
    }
    if (in_array($rq_imp->imp_nom, $impr)) {
        exec('lpr -P '.$rq_imp->imp_nom.' -o Resolution=600x600dpi "'.$incpath.'pdf/jours/'.$cejour.'.pdf"');
    }
}
header("Location:".$incpath."pdf/jours/".$cejour.".pdf");//sortie pdf