<?php
// include de annee.php ligne 76
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if ($req) {
    // print_r($_SESSION);
    if (!isset($incpath)) {
        $p=preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath="";
        for ($i=1;$i<sizeof($p)-1;$i++) {
            $incpath='../'.$incpath;
        }
        unset($p, $i);
    }
    include $incpath."mysql/connect.php";
    connexobjet();
    $i=$req;
    // $cem=$i."-".date('m')."-".date('j')." ".date('H').":".date('i');
    $req_ventes="SELECT SUM(tic_tt) as CT FROM Tickets_$i JOIN Resume_ticket_$i ON rst_id = tic_num  GROUP BY MONTH(rst_validation)";
    $r_ventes=$idcom->query($req_ventes);
    // $r_v = $r_ventes->num_rows;
    if ($idcom->error) {
        echo "<br>".$idcom->errno." ".$idcom->error."<br>";
    }
    $annee = array();
    $valeurV = array();
    $paq = array();
    while ($resu=$r_ventes->fetch_object()) {
        array_push($annee, $i);
        array_push($valeurV, $resu->CT);
    }
    //****************************************commandes**********************************************
    $valeurC = array();
    $req_commandes="SELECT SUM(rsc_ttc) as CT FROM Resume_commande_$i WHERE rsc_etat = 3 GROUP BY MONTH(rsc_date)";
    $r_commandes=$idcom->query($req_commandes);
    // $r_c = $r_commandes->num_rows;
    if ($idcom->error) {
        echo "<br>".$idcom->errno." ".$idcom->error."<br>";
    }
    while ($resu=$r_commandes->fetch_object()) {
        array_push($valeurC, $resu->CT);
    }
}
$r_v = $r_ventes->num_rows;
$r_c = $r_commandes->num_rows;
// echo $r_c. " ".$r_v;
?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd"><?php
//en janvier la valeur commande dépasse habituellement la valeur vente
$valV=($r_v > 0)?max($valeurV):0.00;
$valC=($r_c > 0)?max($valeurC):0.00;
// $valV=max($valeurV);
// $valC=max($valeurC);
// exit;
if ($valC > $valV) {
    $val = $valC;
} else {
    $val = $valV;
}
$arondi=round($val);
$lg=strlen($arondi);
$echelle=round($arondi, - ($lg-1));
if ($arondi - $echelle > 0) {
    $echelle = $echelle + str_pad(1, $lg, 0);
}
if ($lg <= 5) {
    $px=12;
} else {
    $px=8;
}
if ($echelle < 1000) {
    $echelle = 1000;
}
//4 zones grises en hauteur, hauteur image 150px
if ($echelle == 1000) {
    $interval = $echelle / 4;
}

?>

<div  class="annees" id="<?php echo $i?>">
<center>
<h3>Ventes / Commandes de l'année <?php echo $i?></h3>
<svg width="912px" height="200px" xml:lang="fr" xmlns="http://www.w3.org/2000/svg" id="svg_<?php echo $i?>" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
      <linearGradient id="MonDegrade">
        <stop offset="5%" stop-color="#6FF" />
        <stop offset="95%" stop-color="#066" />
      </linearGradient>
    </defs>


<rect x="0" y="0" width="910" height="200" style="fill:none;stroke:black;stroke-width:1px;"/>
<?php
//création des lignes de structure
//vertical
$g=69;
$n=0;
for ($l = 0; $l < 13; $l++) {
    if (isset($valeurV[$l])) {
        $valeur = $valeurV[$l];
    } else {
        $valeur = 0;
    }

    $haut=(160/$echelle*$valeur);
    $position=160 - $haut;
    $longueur=160-$position;
    echo '<line x1="'.$g.'" x2="'.$g.'" y1="10" y2="160" style="fill:none;stroke:black;stroke-width:1px;stroke-dasharray:4,4;"/>'."\n";
    echo '<rect fill="url(#MonDegrade)" x="'.($g+16).'" y="'.$position.'" width="40" height="'.$longueur.'" />'."\n";
    $g = $g+69;
    // echo "</g>";
}
//echelle d'affichage
$division=$echelle/5;
$ecart_division=80/6;
$n_echelle=$echelle;
$H=10;
echo "<g id='echelle'>\n";
for ($e = 0; $e < 6; $e++) {
    echo '<line x1="0" x2="10" y1="'.($H +($ecart_division*$e)).'" y2="'.($H +($ecart_division*$e)).'" style="fill:none;stroke:black;stroke-width:1px;"/>'."\n";
    echo '<text class="echelle" x="15" y="'.($H +($ecart_division*$e)+3).'">'.$n_echelle.'</text>'."\n";
    $H=16+$H;
    $n_echelle = $n_echelle-$division;
}
echo "</g>\n";
echo "<g id='ventes'>\n";
$g=69;
for ($l = 0; $l < 12; $l++) {
    if (isset($valeurV[$l])) {
        $valeur = $valeurV[$l];
    } else {
        $valeur = 0;
    }

    $haut=(160/$echelle*$valeur);
    $position=160 - $haut;
    if ($position < 10) {
        $position =10;
    }
    echo '<text class="valVente" x="'.($g+65).'" y="15">'.monetaireF($valeur).'</text>'."\n";
    if ($n%2 == 0) {
        $h=175;
    } else {
        $h=185;
    }

    echo '<text class="mois_svg" x="'.($g+34.5).'" y="'.$h.'">'.$mois[($l + 1)].'</text>'."\n";
    $n++;
    $g = $g+69;
}
echo "</g>\n";


echo "<g id='commandes'>\n";
$g=69;
$coordonnees="105,160 ";
for ($l = 0; $l < 12; $l++) {
    if (isset($valeurC[$l])) {
        $valeur = $valeurC[$l];
    } else {
        $valeur = 0;
    }
    
    $haut=(160/$echelle*$valeur);//
    $position=160 - $haut;
    $decal = $g +(69/2);
    
    echo '<text  x="'.($g+65).'" y="30">'.monetaireF($valeur).'</text>'."\n";
    if ($n%2 == 0) {
        $h=175;
    } else {
        $h=185;
    }
    $n++;
    if ($l==0) {
        $coordonnees.="105,".$position." ";
    } elseif ($position == 160) {
        $coordonnees.=",".($g - 34.5).",160 ";
    }//mois actuel et à venir
    else {
        $coordonnees.=$decal.",".$position." ";
    }
    $g = $g+69;
}
echo "<polyline points='".$coordonnees." ".($g - 34.5).",160'></g>\n";
$echelle=";"

?>
</svg>
</center></div>
