<?php
session_start();
$tab = explode('/', $_SERVER['REQUEST_URI']);
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$ceannee = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$ticket = filter_input(INPUT_GET, "tic", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
$_SESSION['aide_' . $_SESSION[$dossier]] = 'G2';

connexobjet();
$req_tic = "SELECT tic_quantite,
                  tic_prix,
                  tic_quantiteS,
                  tic_prixS,
                  Vt1_nom,
                  DATE(rst_validation) AS rst_jour,
                  rst_etat,
                  mdr_correction,
                  fac_id,
                  tic_id,
                  tic_tt,
                  art_pseudo,
                  art_unite
                    FROM Tickets_$ceannee
                    JOIN Resume_ticket_$ceannee ON rst_id=tic_num
                    JOIN Mode_reglement ON mdr_id = rst_etat
                    LEFT JOIN Vtit1 ON Vt1_article = tic_article                    
                    JOIN Articles ON art_id = tic_article
                    LEFT JOIN Factures_$ceannee ON fac_ticket = rst_id
                      WHERE tic_num =$ticket";
$r_tic = $idcom->query($req_tic);
if ($r_tic->num_rows == 0) {
    echo "<h3>Détail du ticket " . $ticket . "<br><img src='/images/attention.png'><br>Les données du ticket ne sont pas accessibles.</h3>";
    exit;
}
$rq_tic = $r_tic->fetch_object();
$r_tic->data_seek(0);
$clos = 0;
//on verifie si la journée est close
$req_jour = "SELECT * FROM Resume_jour_$ceannee WHERE rsj_date = '" . $rq_tic->rst_jour . "' AND rsj_statut = 2";
$r_jour = $idcom->query($req_jour);
// echo "<br>".$r_jour->num_rows;
if ($r_jour->num_rows > 0) {
    $clos = 1;
}

//si c'est une carte bancaire ou un chèque
//si la journée n'a pas été close,
//on permet la modification des quantités ou prix pour ajuster en fonction de la valeur de la carte bancaire ou chèque réellement reçu
// exit;
?>
<style>
    .coulFF {
        background-color: <?php echo $coulFF ?>
    }

    .coulCC {
        background-color: <?php echo $coulCC ?>
    }

    .corr {
        background-color: orange
    }
</style>
<script>
    function ticQ(id) {
        modif(id, 20, $('#Tq_' + id).val() + '&orq=' + $('#Tq_' + id).attr('orq'), 'quantite', 1); //modification de la ligne du ticket
        charge('recalcul_ticket', '<?php echo $ticket ?>', 'mysql'); //modification du resume ticket
        charge('detail_ticket', '<?php echo date('Y') . "&tic=" . $ticket ?>', 'panneau_d');
        charge('fin_jour', '', 'panneau_g');
    }

    function ticP(id) {
        modif(id, 20, $('#Tp_' + id).val(), 'prix', 1); //modification de la ligne du ticket
        charge('recalcul_ticket', '<?php echo $ticket ?>', 'mysql'); //modification du resume ticket
        charge('detail_ticket', '<?php echo date('Y') . "&tic=" . $ticket ?>', 'panneau_d');
        charge('fin_jour', '', 'panneau_g');
    }

    function annul(id) {
        $('#annulation').html("<h3>Voulez-vous vraiment annuler cette vente ?</h3><button class='fgauche' onclick=\"$('#annulation').html('')\">Non</button><button onclick=\"charge('annulation_ticket',<?php echo $ticket?>,'panneau_d')\" class='fdroite'>Oui</button>");
        
    }
</script>
<h3>Détail du ticket <?php echo $ticket ?><a href="/Ventes/impression_ticket.php?req=<?php echo $ticket . "&an=" . $ceannee ?>"><img src="/images/imp.png" style="float:right"></a></h3>
<div id='resume_d'>
    <table class="generique">
        <thead>
            <TR>
                <TH>Article</TH>
                <TH>Quantité</TH>
                <TH>Prix unitaire</TH>
                <TH>Total</TH>
            </TR>
        </thead>
        <tbody>
            <?php
            $n = 0;
            $TT = 0.00;
            while ($rq_tic = $r_tic->fetch_object()) {
                if ((($rq_tic->tic_prix != $rq_tic->tic_prixS) || ($rq_tic->tic_quantite != $rq_tic->tic_quantiteS)) && ($rq_tic->art_pseudo == '1')) {
                    $coul = " class='corr'"; //indication de modification; exclusion des pseudos articles
                } elseif ($n % 2 == 0) {
                    $coul = " class='coulCC'";
                } else {
                    $coul = " class='coulFF'";
                }

                if ($rq_tic->art_unite == 1) {
                    $tic_quantite = sprintf('%d', $rq_tic->tic_quantite);
                    $tic_quantiteS = sprintf('%d', $rq_tic->tic_quantiteS);
                } else {
                    $tic_quantite = $rq_tic->tic_quantite;
                    $tic_quantiteS = $rq_tic->tic_quantiteS;
                }
                $difP = ($rq_tic->tic_prix != $rq_tic->tic_prixS) ? "(" . $rq_tic->tic_prixS . ") / " : "";
                $difQ = ($tic_quantite != $tic_quantiteS) ? "(" . $tic_quantiteS . ") / " : "";
                if (($rq_tic->mdr_correction == "1") && ($rq_tic->fac_id == '')) { //possibilité de correction
                    if (($rq_tic->rst_jour == date("Y-m-d")) && ($clos != 1)) {
                        if ($n == 0) {
                            echo "<tr" . $coul . "><th colspan='4' style='border:solid 1px;padding:5px;background-color:yellow'>Attention : Il est possible de modifier la quantité ou le prix pour ajuster la vente à la valeur réellement reçue mais les modifications seront visibles</th></tr>\n";
                        }
                        echo '<tr' . $coul . ' id="T_' . $rq_tic->tic_id . '"><TD>' . $rq_tic->Vt1_nom . '</TD>';
            ?>
                        <TD><input onchange="ticQ(<?php echo $rq_tic->tic_id ?>)" id='Tq_<?php echo $rq_tic->tic_id ?>' orq="<?php echo $tic_quantite ?>" type="text" class="mille" value="<?php echo $tic_quantite ?>"></TD>
                        <TD><input onchange="ticP(<?php echo $rq_tic->tic_id ?>)" type="text" class="cent" value="<?php echo sprintf('%01.2f', $rq_tic->tic_prix) . "\" id='Tp_" . $rq_tic->tic_id ?>'"></TD>
                        <TD><?php echo $rq_tic->tic_tt ?>&nbsp;€</TD>
                        </tr>
            <?php
                    } else {
                        echo '<tr' . $coul . '><TD>' . $rq_tic->Vt1_nom . '</TD><TD>' . $difQ . $tic_quantite . '</TD><TD>' . $difP . sprintf('%01.2f', $rq_tic->tic_prix) . '</TD><TD>' . $rq_tic->tic_tt . '&nbsp;€</TD></tr>';
                    }
                    $facture = $rq_tic->fac_id;
                } else {
                    echo '<tr' . $coul . '><TD>' . $rq_tic->Vt1_nom . '</TD><TD class="mille">' . $difQ . $tic_quantite . '</TD><TD class="cent">' . $difP . sprintf('%01.2f', $rq_tic->tic_prix) . '</TD><TD>' . $rq_tic->tic_tt . '&nbsp;€</TD></tr>';
                }
                $n++;
                $TT += $rq_tic->tic_tt;
                $jour = $rq_tic->rst_jour;
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <TD colspan="3">Total vente</TD>
                <th><?php echo sprintf('%01.2f', $TT) ?> €</th>
            </tr>
            <?php
            if ($jour == date("Y-m-d")) {
                ?>
                <tr>
                    <TD colspan="4" id='annulation'><button onclick="annul(<?php echo $ticket ?>)">Annuler ce ticket</button></TD>
                </tr>
                <?php
            }
            ?>
        </tfoot>

    </table>
    <?php
    // //on vérifie s'il y a une facture associée
    $req_comptes = "SELECT cpt_nom, rst_validation, mdr_nom, rst_total, fac_id FROM Factures_$ceannee JOIN Comptes ON fac_cp =cpt_id JOIN Resume_ticket_$ceannee ON rst_id = fac_ticket JOIN Mode_reglement ON mdr_id = rst_etat WHERE rst_id = $ticket";
    $r_comptes = $idcom->query($req_comptes);
    if ($r_comptes->num_rows != 0) {
        $rq_comptes = $r_comptes->fetch_object();
        // +----------------------+-----------+-----------+------------+---------------------+--------+
        // | cpt_nom              | rst_total | mdr_nom   | fac_ticket | rst_validation      | fac_id |
        // +----------------------+-----------+-----------+------------+---------------------+--------+
        // | SAS La Froidfontaine |     71.90 | Chèque    |       1034 | 2016-02-27 14:43:08 |      2 |
        // +----------------------+-----------+-----------+------------+---------------------+--------+
    ?>
        <hr>
        <table class="generique" id='factures'>
            <thead>
                <TR>
                    <TH>N° Facture</TH>
                    <TH>Destinaire</TH>
                    <TH>Mode de règlement</TH>
                    <TH>Date d'émission</TH>
                    <th></th>
                </TR>
            </thead>
            <tr class='coulCC'>
                <TD>Lib<?php echo $ceannee * 1000 + $rq_comptes->fac_id ?></TD>
                <TD><?php echo $rq_comptes->cpt_nom ?></TD>
                <TD><?php echo $rq_comptes->mdr_nom ?></TD>
                <TD><?php echo $rq_comptes->rst_validation ?></TD>
                <th><?php
                    if (file_exists($incpath . "pdf/factures/facture_" . ($ceannee * 1000 + $rq_comptes->fac_id) . ".pdf")) {
                        echo '<img id=' . ($ceannee * 1000 + $rq_comptes->fac_id) . ' src="../images/pdf.gif" width="14" height="14">';
                    }
                    ?></th>
            </tr>
        </table>
    <?php
    }
    ?>
</div>
<script>
    $("#panneau_d").height(($('#affichage').height()) - 10);
    $(document).ready(function() {
        $('#factures img').click(function() {
            // alert($(this).attr('id'));
            id = $(this).attr('id');
            window.open("<?php echo $incpath ?>pdf/factures/facture_" + id + ".pdf");
            // charge('article',art[1],'panneau_g');

        });
    });
</script>
