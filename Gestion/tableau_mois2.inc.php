<?php
session_start();
$p = preg_split("[/]", $_SERVER['PHP_SELF']);
$incpath = "";
for ($i = 1; $i < sizeof($p) - 1; $i++) {
    $incpath = '../' . $incpath;
}
unset($p, $i);
//affichage par secteurs
$tab = explode('/', $_SERVER['REQUEST_URI']);
$dossier = $tab[1];
$_SESSION['aide_' . $_SESSION[$dossier]] = 'G3';

require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
connexobjet();
// exit;
function jour_lettre($cejour)
{
    global $sem;
    $ce_jour = timestampA($cejour);
    $cejour = $sem[date("w", $ce_jour)] . "&nbsp;" . str_pad(date("j", $ce_jour), 2, "0", STR_PAD_LEFT);
    return $cejour;
}

$cemois = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an = filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if (!$cemois) {
    $cemois = date('n');
}
if (!$an) {
    $an = date('Y');
}
if ($cemois < 13) {
    $jour = $an . "-" . $cemois . "-01";
    $_mois = $cemois;
} elseif ($cemois < 138) { //accès depuis les graphiques des années, conversion des coordonnées en valeur utilisable
    $jour = $an . '-01-01';
    $_mois = '01';
} elseif ($cemois < 207) {
    $jour = $an . '-02-01';
    $_mois = '02';
} elseif ($cemois < 276) {
    $jour = $an . '-03-01';
    $_mois = '03';
} elseif ($cemois < 346) {
    $jour = $an . '-04-01';
    $_mois = '04';
} elseif ($cemois < 414) {
    $jour = $an . '-05-01';
    $_mois = '05';
} elseif ($cemois < 484) {
    $jour = $an . '-06-01';
    $_mois = '06';
} elseif ($cemois < 553) {
    $jour = $an . '-07-01';
    $_mois = '07';
} elseif ($cemois < 622) {
    $jour = $an . '-08-01';
    $_mois = '08';
} elseif ($cemois < 690) {
    $jour = $an . '-09-01';
    $_mois = '09';
} elseif ($cemois < 760) {
    $jour = $an . '-10-01';
    $_mois = '10';
} elseif ($cemois < 829) {
    $jour = $an . '-11-01';
    $_mois = '11';
} else {
    $jour = $an . '-12-01';
    $_mois = '12';
}
$cemois = $_mois;
if ((isset($ceannee) == date("Y")) && ($cmois > date("n"))) {
    echo "<h1>Désolé, il n'y a pas encore de donnée</h1>";
    exit;
}

function dimanche($cejour)
{
    $ce_jour = timestampA($cejour);
    $jour_semaine = date("w", $ce_jour);
    return $jour_semaine;
}
?>

<script>
    function edi_jour(jour) {
        $('#affichage').html('<div id = "panneau_g"></div><div id = "panneau_d"></div>');
        charge('detail_jour', jour, 'panneau_g');
    }

    function sup_jour(jour) {
        if (confirm("Voulez-vous vraiment supprimer depuis le " + jour)) {
            $('#affichage').empty();
            $('#affichage').html('<div id = "panneau_g"></div><div id = "panneau_d"></div>');
            charge('supression_jour', jour, 'panneau_g');
        }
    }

    function cor(id) {
        $('mysql').css('visibility', 'visible');
        $('mysql').html(id);
    }
</script>
<?php
class PDF_TotalMensuel
{
    public $ligne = 0;
    protected $an;
    protected $cemois;
    protected $colonne = 1;
    public $nbColonne;
    public $tab_couleur;

    function __construct($margeG, $margeH, $nbS, $nbM, $an, $cemois)
    {
        $this->cemois = $cemois;
        $this->an = $an;
        $this->_nbS = $nbS; //nombre de secteurs
        $this->_nbM = $nbM; //nombre de mode de reglement
        $this->nbColonne = $nbS + $nbM + 2;
    }
    function addLigne($type, $valeurs)
    {

        if ($type == "titre") {
            if (!isset($incpath)) {
                $p = preg_split("[/]", $_SERVER['PHP_SELF']);
                $incpath = "";
                for ($i = 1; $i < sizeof($p) - 1; $i++) {
                    $incpath = '../' . $incpath;
                }
                unset($p, $i);
            }

            $img = (file_exists($incpath . "pdf/mois/" . $this->an . '_' . $this->cemois . ".pdf")) ? '<a href = "/pdf/mois/' . $this->an . '_' . $this->cemois . '.pdf"><img src = "../images/pdf.gif" width = "14" height = "14" style = "float:right"></a>' : "<a href = 'PDF_Mensuel.php?an=" . $this->an . "&mois=" . $this->cemois . "&main=1'><img src = '/images/imp.png' style = 'float:right' /></a>";
            $mode = '<button style="height:20px;width:60px;border:outset 2px;float:right;margin-right:10px" onclick="charge(\'tableau_mois1.inc\',\'' . $this->cemois . '&an=' . $this->an . '\',$(this).parent().parent().parent().parent().parent().parent().attr(\'id\'))">TVA</button> ';
            $mois = array("", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
            echo "<center><table id = 'resume' ><tr><th style = 'background-color:rgb(255, 245, 210)' colspan = '" . $this->nbColonne . "'>Mois de  " . $mois[$this->cemois * 1] . " " . $this->an . $img . $mode . "</td></tr>";
        } elseif ($type == "categorie") {
            $long = array('1', $this->_nbS, $this->_nbM, '1');
            echo "<tr>";
            for ($i = 0; $i < 4; $i++) {
                switch ($i) {
                    case 1:
                        $col = $this->_nbS;
                        $coul = 1;
                        break;
                    case 2:
                        $col = $this->_nbM;
                        $coul = $this->_nbS + 1;
                        break;
                    default:
                        $col = '1';
                        $coul = 0;
                }
                echo "<th style = 'background-color:#" . $this->tab_couleur[$coul] . "' colspan = '" . $col . "'>" . $valeurs[$i] . "</th>";
            }
            echo "</tr>";
        } elseif ($type == "categorie_libelle") {
            echo "<tr>";
            for ($i = 0; $i < $this->nbColonne; $i++) {
                $imag = "<img src = '/images/secteurs/" . $valeurs[0][$i] . ".png'>";
                if (($i < ($this->_nbS + 1)) && ($i > 0)) {
                    echo "<th>" . $valeurs[1][$i] . "<br>" . $imag . "</th>";
                } else {
                    echo "<th><br>" . $valeurs[0][$i] . "</th>";
                }
            }
            echo "</tr>";
        } else if ($type == "semaine") {
            echo "<tr style = 'background-color:white'><th colspan = '" . $this->nbColonne . "'>" . $valeurs . "</th></tr>\n";
        } elseif ($type == "contenu") {
            $i = 0;
            $edit = '';
            $drop = '';
            echo "<tr>";
            foreach ($valeurs as $cle => $v) {
                if ($cle == "date") {
                    $lg = ";width:100px";
                    $_jour = $v;
                    //if ((substr($v, -2)!= '01')&&(substr(jour_lettre($v),0,5)!= 'lundi'))    $drop = "<span style = 'float:left'><img src = '/images/button_drop.png' width = '11' height = '13' onclick = 'sup_jour(\"".$v."\")'></span>";
                    $v = jour_lettre($v);
                } elseif ($cle == 'total') {
                    $edit = "<span style = 'float:left'> <img src = '/images/button_edit.png' onclick = 'edi_jour(\"" . $_jour . "\")'></span>";
                    $lg = "";
                } elseif ($cle != "date") {
                    $lg = "";
                    $v = monetaireF($v);
                    $drop = '';
                    $edit = '';
                }
                echo "<td style = 'background-color:#" . $this->tab_couleur[$i] . $lg . "'>" . $drop . $edit . $v . "</td>";
                $i++;
            }
            echo "</tr>\n";
        } elseif ($type == "decaissement") {
            $i = 0;
            echo "<tr style = 'font-style:italic'>";
            foreach ($valeurs as $cle => $v) {
                if ($v == '0.00') {
                    $v = '';
                } elseif ($v == 'Décaissem.') {
                    $v = 'Décaissem.';
                } else {
                    $v = "-" . monetaireF($v);
                }
                echo "<td>" . $v . "</td>";
                $i++;
            }
            echo "</tr>";
        } elseif ($type == "report") {
            echo "<tr>";
            $i = 0;
            foreach ($valeurs as $cle => $v) {
                if ($v == '0.00') {
                    $v = '';
                }
                echo "<td>" . $v . "</td>";
                $i++;
            }
            echo "</tr>";
        } elseif ($type == "cumul") {
            echo "<tr style = 'font-weight:bold'>";
            $i = 0;
            foreach ($valeurs as $cle => $v) { //monetaireF($valeur))
                if ($cle == 'date') {
                    $_jour = $v;
                    $v = 'Cumul C.A.';
                }
                if ($cle != "date") {
                    $v = monetaireF($v);
                }
                $edit = (($v != 0.00) && ($cle != 'total')) ? "<span style = 'float:left'> <img src = '/images/button_edit.png' onclick = 'edi_mode(\"" . $_jour . "\",\"" . $cle . "\")'></span>" : $edit = '';
                echo "<td>" . $edit . $v . "</td>";
                $i++;
            }
            echo "</tr>";
        } elseif ($type == "cumulmensuel") {
            $i = 0;
            echo "<tr style = 'font-weight:bold'>";
            foreach ($valeurs as $cle => $v) {
                if ($cle != "date") {
                    $v = monetaireF($v);
                }
                echo "<td style = 'background-color:#" . $this->tab_couleur[$i] . "'>" . $v . "</td>";
                $i++;
            }
            echo "</tr>";
        } elseif ($type == "tva") {
            $i = 0;
            echo "<tr>";
            foreach ($valeurs as $cle => $v) {
                if (($v == '0')) {
                    $v1 = '';
                } elseif ($cle == 'date') {
                    $v1 = $v;
                } else {
                    $v1 = monetaireF($v);
                }
                echo "<td>" . $v1 . "</td>";
                $i++;
            }
            echo "</tr>";
        } elseif ($type == "suppression") {
            $i = 0;
            echo "<tr style = 'background-color:white'><th colspan = '" . $this->nbColonne . "'><img src = '/images/button_drop.png' width = '11' height = '13' onclick = 'sup_jour(\"" . $valeurs . "\")'> Suppression du dernier jour à partir de " . $valeurs . "</th></tr>\n";
        }
    }
}
$i = 0;
//--------------- Si c'est le mois en cours, on affiche les colonnes définies dans la configuration------------------
// tva              tva_id + 100
// secteur     sec_id + 200
// reglement mdr_id + 300
// echo $config['mode'];
//---------------------------------affichage par secteurs--------------------------------------------
$reqcolonne = "(SELECT (sec_id + 200) AS mode_id,
                    (sec_ordre + 200) AS ordre,
                    CONCAT('sec_',sec_id) AS mode_nom,
                    sec_abrege as abrege,
                    sec_couleur as couleur,
                    tva_nom AS tva_nom,
                    2 AS type
                        FROM  Secteurs
                            LEFT JOIN Tva ON tva_id = sec_tva
                                WHERE sec_etat = '1')
                UNION ALL
                    (SELECT (mdr_id + 300) AS mode_id,
                    (mdr_ordre + 300) AS ordre,
                    mdr_nom AS mode_nom,
                    mdr_abrege as abrege,
                    mdr_couleur as couleur,
                    mdr_nom AS tva_nom,
                    3 AS type
                        FROM  Mode_reglement
                            WHERE mdr_etat = '1')
                                ORDER BY ordre";

$rcolonne = $idcom->query($reqcolonne);
if ($idcom->errno == 1146) {
    echo "Il n'y a pas de données à afficher, l'année ne semble pas commencée. Passez par fin du jour pour régulariser la situation.";
}

$nbcolonne = $rcolonne->num_rows + 2;

$tab_couleur = array("ffffff");
// $type = array();
$tab_mois = array(); //tableau vide qui permet d'insérer une ligne complete générée en ligne ???
$ligne = array("statut", "date"); //sera completé en ligne ???
$categorie_libelle = array("Date");
$categorie_abrege = array("Date");
$result = array(); //tableau générale des lignes du mois
$tab_decaissement = array();
$tab_repport = array();
$tab_Totalmois = array();
$tab_tvasecteur = array('Date');
$nbS = '';
$nbM = '';
while ($rqcolonne = $rcolonne->fetch_object()) {
    $modeid = "_" . $rqcolonne->mode_id;
    $ligne[] = $modeid; //remplissage tableau php ligne
    $tab_couleur[] = $rqcolonne->couleur;
    $categorie_libelle[] = $rqcolonne->mode_nom;
    $categorie_abrege[] = $rqcolonne->abrege;
    if ($rqcolonne->mode_id < 300) {
        $tab_tvasecteur[] = $rqcolonne->tva_nom;
        $nbS++;
    } elseif ($rqcolonne->mode_id > 299) {
        $nbM++;
    }
}
$tab_couleur[] = 'ffffff';
$pdfTickets = new PDF_TotalMensuel(10, 10, $nbS, $nbM, $an, $cemois);
$pdfTickets->tab_couleur = $tab_couleur;

array_push($categorie_libelle, "Total");
array_push($ligne, "total");

$pdfTickets->addLigne("titre", mktime(0, 0, 0, $cemois, 1, $an));

$jour_ab = array("Dim ", "Lun ", "Mar ", "Mer ", "Jeu ", "Ven ", "Sam ", "Dim ");
$pdfTickets->addLigne("categorie", array("", "Ventes journalières par secteurs", "Ventes journalières par mode de règlement", ""));
$pdfTickets->addLigne("categorie_libelle", array($categorie_libelle, $categorie_abrege));

//permet l'accès de contrôle du mois avec libreoffice
if ((!$an) && (!$cemois)) {
    $an = date('Y');
    $cemois = date('n');
    echo '<html>
    <head>
      <TITLE></TITLE>
       <meta http-equiv = "Content-Type" content = "text/html; charset = utf-8"/>
    </style>
    </head><body>';
}
$tabligne = array_fill_keys($ligne, '0.00'); //remplissage avec une valeur par défaut
$tab_Totalsemaine = $tabligne;
$tab_Totalmois = $tabligne;

//*******************Recherche les données du mois entregistrées et création du tableau php par ligne contenant le tableau initial $tabligne
/*echo "mois ".*/
$req_mois = "SELECT rsj_date ,rsj_id, rsj_statut ,  SUM(red_valeur) AS valeur, red_mode  FROM Resume_jour_$an LEFT JOIN Resume_detail_$an ON rsj_id = red_jour WHERE MONTH(rsj_date) = $cemois AND red_mode > 199 GROUP BY red_mode ,rsj_id ORDER BY rsj_id";
$n = 0;
// $i = 0;
$result = array();
$lignejour = '';
$r_mois = $idcom->query($req_mois);
while ($rq_mois = $r_mois->fetch_object()) {
    if ($rq_mois->rsj_id != $lignejour) {                                                        //
        $result[$n] = array_merge($tab_mois, $tabligne);           //
        $lignejour = $rq_mois->rsj_id;                           //
        $l = $n;                                                   //
        $n++;
    }
    $result[$l]['date'] = $rq_mois->rsj_date;
    $result[$l]["_" . $rq_mois->red_mode] = $rq_mois->valeur; //mise à jour de la ligne du jour
    $result[$l]['statut'] = $rq_mois->rsj_statut;
}
//*************************************lecture de la table result************************************

$r = 0; //numero de la ligne
$c = 0; //numero de la cellule
$_jour = '';
foreach ($result as $cle => $v) {
    if ($v["statut"] == 1) { //décaissement suivi de cumul        
        unset($v['statut']); //supression de la case statut qui n'apparaît pas sur le tableau
        $_jour = $v['date'];
        $v['date'] = 'Décaissem.';
        $decc = $v;
        unset($decc['date']);
        $v['total'] = array_sum($decc);
        //cacul du total décaissement
        $pdfTickets->addLigne("decaissement", $v);
        unset($tab_Totalsemaine['statut']);
        $tab_Totalsemaine['date'] = $_jour; //'Cumul C.A.';
        //calcul du total cumul semaine
        $tab_Total = $tab_Totalsemaine;
        unset($tab_Total['date']);
        $tab_Totalsemaine['total'] = array_sum($tab_Total) / 2;
        //après la ligne décaissement insertion de la ligne des cumul
        $pdfTickets->addLigne("cumul", $tab_Totalsemaine);
        foreach ($tab_Totalsemaine as $cle => $cel) { //remise à 0 de $tab_Totalsemaine            
            $tab_Totalsemaine[$cle] = '0.00';
        }
    } elseif ($v["statut"] == 2) { //contenu    
        $_jour = $v['date'];
        unset($v['statut']); //supression de la case statut qui n'apparaît pas sur le tableau
        $vt = $v;
        unset($vt['date']); //supression de la case date pour faire le total
        $v['total'] = monetaireF(array_sum($vt) / 2); //calcul du total de la ligne qu'on met dans la case total
        foreach ($vt as $cle => $cel) { //addition des lignes pour afficher les cumuls            
            $tab_Totalsemaine[$cle] += $cel;
            $tab_Totalmois[$cle] += $cel;
        }
        $pdfTickets->addLigne("contenu", $v);
        $dernier_jour = date("w", timestampA($v['date']));
        $derniere_date = $v['date']; //sera utilisé pour lister les ventes par semaine d'une semaine non complète
    } elseif ($v["statut"] == 3) { //report        
        $_jour = $v['date'];
        //le report étant en debut de semaine, on le fait précéder de la ligne du n° de semaine
        $pdfTickets->addLigne("semaine", 'Semaine ' . date("W", timestampA($v['date'])));
        unset($v['statut']); //supression de la case statut qui n'apparaît pas dur le tableau
        $v['date'] = 'Report';
        $pdfTickets->addLigne("report", $v);
    }
}
//supression du dernier jour. Si c'est un lundi, on supprime ne supprime pas les données report créees le dimanche
$pdfTickets->addLigne("suppression", $_jour);
//cumul si ce n'est pas la fin de la semaine
if (isset($dernier_jour) != 0) {
    unset($tab_Totalsemaine['statut']);
    $tab_Totalsemaine['total'] = array_sum($tab_Totalsemaine) / 2;
    $tab_Totalsemaine['date'] = $derniere_date;
    $pdfTickets->addLigne("cumul", $tab_Totalsemaine);
    $pdfTickets->addLigne("semaine", '&nbsp;');
}
unset($tab_Totalmois['statut']);
$tab_Totalmois['date'] = "Cumul C.A.";
$pdfTickets->addLigne("semaine", 'Données comptables : Cumul mensuel / TVA');
$pdfTickets->addLigne("categorie", array("", "Ventes journalières par Secteurs", "Ventes journalières par mode de règlement", ""));
$pdfTickets->addLigne("categorie_libelle", array($categorie_libelle, $categorie_abrege));

//***********************************calcul du total mois************************************
$tab_Total = $tab_Totalmois;
unset($tab_Total['date']);
$tab_Totalmois['total'] = array_sum($tab_Total) / 2;
$pdfTickets->addLigne("cumulmensuel", $tab_Totalmois);
//***********************************calcul du tva du mois************************************
$n = 0;
$tt_tva = '0.00';
$tt_tva = '0.00';
foreach ($tab_Totalmois as $cle => $v) {
    if (is_numeric(substr($cle, 1, 3))) { //exclusion de date et total        
        if (substr($cle, 1, 3) < 300) { //exclusion des modes de règlement            
            if ($tab_tvasecteur[$n] == "0.00") {
                $tab_Totalmois[$cle] = "0.00";
            } else {
                $val_tva =     $tab_Totalmois[$cle] = $v - ($v / (1 + ($tab_tvasecteur[$n] / 100)));
                $tt_tva += $val_tva;
            }
        } else {
            $tab_Totalmois[$cle] = "0";
        }
    }
    $n++;
}
$tab_Totalmois['date'] = "Valeur TVA";
$tab_Totalmois['total'] = $tt_tva;

$pdfTickets->addLigne("tva", $tab_Totalmois);

echo "</table></center>";
$n = 0;
?>
<script>
    <?php
    if (isset($suite) == 1) { //cloture du jour, on fait la sortie pdf et papier
    ?>
        location.href = '/Gestion/PDF_jour.php?jour=<?php echo $jour ?>';
    <?php
    }
    ?>
    var p = $("#resume");
    var position = p.position();
    $("#edi_mode").css('left', position.left);
</script>