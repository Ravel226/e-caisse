function voir(page, div) {
    $('#affichage').empty();
    $('#affichage').html('<div id=panneau_d></div><div id=panneau_g></div>');
    charge(page, '', div);
}

function col(page, req, div) {
    $('#affichage').empty();
    $('#affichage').html('<div id=colonnes></div>');
    charge(page, req, div);
}

function voire(page, div) {
    $('#affichage').empty();
    $('#affichage').html('<div id=panneau_d></div><div id=panneau_g></div>');
    charge(page, '', div);
}

function charge(page, req, div) {
    $('#mysql').css('visibility', 'visible');
    $.ajax({
        type: "GET",
        url: page + ".php?req=" + req,
        dataType: "html",
        //affichage de l'erreur en cas de problème
        error: function (msg, string) {
            alert("Error !: " + string);
        },
        success: function (data) {
            $('#mysql').css('visibility', 'hidden');
            //alert(data);
            //on met à jour le div suggest avec les données reçus
            $("#" + div).empty();
            $("#" + div).append(data);
        }
    });
}

function recharge() {
    window.location.reload();
}
/*my : 0=insert,1=update,2=delete
  tb=table
  cp=champ
  vl=valeur
*/
function modif(id, tb, vl, cp, my) {
    $('#mysql').css('visibility', 'visible');
    $.ajax({
        type: "GET",
        url: "/mysql/mysql.php?my=" + my + "&id=" + id + "&tb=" + tb + "&vl=" + vl + "&cp=" + cp,
        dataType: "html",
        //affichage de l'erreur en cas de problème
        error: function (msg, string) {
            alert("Error !: " + string);
        },
        success: function (data) {
            $("#mysql").empty();
            $("#mysql").append(data);
        }
    });
}

function imp(id) {
    location.href = "etiquettes/etiq_cb.php?par=" + id;
}

function edi_mode(jour, mode) {
    $("#edi_mode").css('visibility', 'visible');
    charge('edi_mode', jour + '&mod=' + mode, 'edi_mode');
}
// --- end of code popup section ---
function suppression(id, tb, vl, cp, my) {
    if (confirm('Voulez-vous vraiment supprimer cet élément ?')) {
        modif(id, tb, vl, cp, my);
    }
}