<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}

//***********************************************Attention encodage utf8******************************

$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$cemois= filter_input(INPUT_GET, "mois", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$main= filter_input(INPUT_GET, "main", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

if (!isset($req)) {
    $req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
require $incpath."php/config.php";
connexobjet();
// print_r($_SESSION);
require_once $incpath.'/fpdf181/fpdf.php';
   function jour_lettre($cejour)
   {
       global $sem;
       $ce_jour=timestampA($cejour);
       $cejour=substr($sem[date("w", $ce_jour)], 0, 3)." ".str_pad(date("j", $ce_jour), 2, "0", STR_PAD_LEFT);
       // $jour_semaine=date("w", $ce_jour);
       return $cejour;
   }


class PDF_TotalMensuel extends FPDF
{

    // Properties

    public $ligne = 0;
    protected $an;
    protected $cemois;
    protected $Ligne_Hauteur = 15;
    // protected $_nbLignes;
    protected $Colonne_Largeur;
    protected $colonne = 1;
    public $nbColonne;
    // protected $_Titre_Page = false;

    public function __construct($margeG, $margeH, $nbT, $nbM, $an, $cemois)
    {
        parent::__construct();

        $this->SetAutoPageBreak(false);
        $this->SetFontSize(7);
        $this->FontSize = 7;
        $this->FontFamily = "Arial";

        $this->Ligne_Hauteur = $this->FontSize * .47;
        $this->AddPage('L', 'A4');

        $this->lMargin = $margeG;
        $this->tMargin = $margeH;
        $this->Colonne_Largeur = ($this->w - $this->lMargin - $this->rMargin);
        $this->ligne = 0;
        $this->colonne = 1;
        $this->cemois = $cemois;
        $this->an = $an;
        $this->_nbT = $nbT;
        $this->_nbM = $nbM;
        $this->nbColonne = $nbT + $nbM + 2;
    }

    // Give the height for a char size given.
    protected function _get_height_chars($pt = null)
    {

        // Tableau de concordance entre la hauteur des caractères et de l'espacement entre les lignes
        if ($pt === null) {
            $pt = $this->FontSize;
        }

        return $pt * .52;
    }

    public function addLigne($type, $valeurs)
    {
        //creation du tableau de couleur blanc=255, gris foncé=210. Si plus de 5 case, on alterne clair foncé
        $tab_couleur=array(255);
        $eclair=round(55/$this->_nbT);
        $dep=255;
        for ($i=0; $i< $this->_nbT; $i++) {//couleur tva/secteur
            if ($i % 2 == 0) {
                $dep = $dep-($eclair * 2);
            } else {
                $dep = $dep+$eclair;
            }
            array_push($tab_couleur, $dep);
        }
        $eclair=round(55/$this->_nbM);
        $dep=255;
        for ($i=0; $i< $this->_nbM; $i++) {//couleur mode de reglement
            $dep = $dep-$eclair;
            array_push($tab_couleur, $dep);
        }
        array_push($tab_couleur, 255);

        if ($type == "titre") {
            $mois = array("", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
            $this->SetFillColor(0, 255, 255);
            $this->SetFont('Arial', 'B', 12);
            $this->SetX($this->lMargin);
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->Cell($this->w - $this->lMargin - $this->rMargin, $this->Ligne_Hauteur + 2, 'Mois de ' . utf8_decode($mois[$this->cemois*1]) . ' ' . $this->an, 1, 0, 'C', 1);
            $this->SetFont('Arial', 'I', 7);
            $this->ligne += 1;
        } elseif ($type == "categorie") {
            $largeurCell = $this->Colonne_Largeur/$this->nbColonne;
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            // 					$coul = array('255', '255', '240', '225', '210', '255');
            $long = array('1', $this->_nbT, $this->_nbM , '1');
            for ($i = 0; $i < 4; $i ++) {
                $this->SetFillColor(255);
                $this->Cell($largeurCell*$long[ $i ], $this->Ligne_Hauteur, utf8_decode($valeurs[ $i ]), 1, 0, 'C', 1);
            }
        } elseif ($type == "categorie_libelle") {
            $largeurCell = $this->Colonne_Largeur/$this->nbColonne;
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            // 					$long = array('1', '1', '1', '1', '1', '1', '2', '2', '2', '1');
            for ($i = 0; $i < $this->nbColonne; $i ++) {
                $this->SetFillColor($tab_couleur[$i]);
                $this->Cell($largeurCell, $this->Ligne_Hauteur, utf8_decode($valeurs[ $i ]), 1, 0, 'C', 1);
            }
        } elseif ($type == "libelle") {
            //affichage d'une ligne article
            $largeurCell = $this->Colonne_Largeur/12;
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            $coul = array('255', '240', '230', '225', '210', '195', '240', '240', '225', '225', '210', '210', '255');
            for ($i = 0; $i < 12; $i ++) {
                $this->SetFillColor($coul[ $i ]);
                $this->Cell($largeurCell, $this->Ligne_Hauteur, $valeurs[ $i ], 1, 0, 'C', 1);
            }
        } elseif ($type == "semaine") {
            //affichage d'une ligne blanche
            $this->SetFont('Arial', '', 7);
            $this->SetFillColor(255, 255, 255);
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            $this->Cell($this->Colonne_Largeur, $this->Ligne_Hauteur, utf8_decode($valeurs), 1, 0, 'C', 1);
        } elseif ($type == "contenu") {
            //affichage d'une ligne article
            $this->SetFont('Arial', '', 8);
            $largeurCell = $this->Colonne_Largeur/$this->nbColonne;
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            $i=0;
            foreach ($valeurs as $cle => $v) {//monetaireF($valeur))
                $this->SetFillColor($tab_couleur[$i]);
                if ($cle != "date") {
                    $v = monetaireF($v);
                }
                $this->Cell($largeurCell, $this->Ligne_Hauteur, $v, 1, 0, 'R', 1);
                $i++;
            }
        } elseif ($type == "decaissement") {
            //affichage d'une décaissement
            $this->SetFont('Arial', 'I', 7);
            $largeurCell = $this->Colonne_Largeur/$this->nbColonne;
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            $i=0;
            foreach ($valeurs as $cle => $v) {
                // 						$this->SetFillColor($tab_couleur[$i]);
                if ($v == '0.00') {
                    $v='';
                } elseif ($v == 'Décaissem.') {
                    $v='Décaissem.';
                } else {
                    $v="-".monetaireF($v);
                }
                $this->Cell($largeurCell, $this->Ligne_Hauteur, utf8_decode($v), 1, 0, 'C', 1);
                $i++;
            }
        } elseif ($type == "report") {
            //affichage de la ligne report
            $this->SetFont('Arial', 'I', 7);
            $largeurCell = $this->Colonne_Largeur/$this->nbColonne;
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            $i=0;
            foreach ($valeurs as $cle => $v) {
                // 						$this->SetFillColor($tab_couleur[$i]);
                if ($v == '0.00') {
                    $v='';
                }
                $this->Cell($largeurCell, $this->Ligne_Hauteur, utf8_decode($v), 1, 0, 'C', 1);
                $i++;
            }
        } elseif ($type == "cumul") {
            //affichage d'une ligne article
            $this->SetFont('Arial', 'B', 7);
            $largeurCell = $this->Colonne_Largeur/$this->nbColonne;
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            $i=0;
            foreach ($valeurs as $cle => $v) {//monetaireF($valeur))
                $this->SetFillColor($tab_couleur[$i]);
                if ($cle != "date") {
                    $v = monetaireF($v);
                }
                $this->Cell($largeurCell, $this->Ligne_Hauteur, $v, 1, 0, 'R', 1);
                $i++;
            }
        } elseif ($type == "cumulmensuel") {
            //affichage d'une ligne article
            $this->SetFont('Arial', 'B', 7);
            $largeurCell = $this->Colonne_Largeur/$this->nbColonne;
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            $i=0;
            foreach ($valeurs as $cle => $v) {
                $this->SetFillColor($tab_couleur[$i]);
                if ($cle != "date") {
                    $v = monetaireF($v);
                }
                $this->Cell($largeurCell, $this->Ligne_Hauteur, $v, 1, 0, 'R', 1);
                $i++;
            }
        } elseif ($type == "tva") {
            //affichage d'une ligne article
            $this->SetFont('Arial', 'B', 7);
            $largeurCell = $this->Colonne_Largeur/$this->nbColonne;
            $this->SetY(($this->ligne) * $this->Ligne_Hauteur + $this->tMargin);
            $this->SetX(($this->colonne - 1) * $this->Colonne_Largeur + $this->lMargin + ($this->colonne - 1));
            $i=0;
            foreach ($valeurs as $cle => $v) {
                $this->SetFillColor($tab_couleur[$i]);
                // 						$v1= $v;
                if (($v =='0')) {
                    $v1 = '';
                } elseif ($cle == 'date') {
                    $v1 = $v;
                } else {
                    $v1= monetaireF($v);
                }
                // 						else ($v1 = $v);
                $this->Cell($largeurCell, $this->Ligne_Hauteur, $v1, 1, 0, 'R', 1);
                $i++;
            }
        }
        //on passe  la ligne
        $this->ligne += 1;
    }
}

/*    $r=0;*/
    $i=0;
  //--------------- Si c'est le mois en cours, on affiche les colonnes définies dans la configuration------------------
if ($config['mode'] == 1) {
    if ($an == date("Y")) {
        // 	echo "<h1>Année en cours</h1>";
        echo	 $reqcolonne="(SELECT (tva_id + 100) AS mode_id, 
															(tva_ordre + 100) AS ordre, 
															CONCAT('TVA ', tva_nom) AS mode_nom, 
															tva_couleur as couleur, 
															tva_nom AS tva_nom, 
															1 AS type
																FROM Tva WHERE tva_etat =1)
											UNION ALL
															(SELECT (sec_id + 200) AS mode_id, 
															(sec_ordre + 200) AS ordre, 
															sec_nom AS mode_nom, 
															sec_couleur as couleur, 
															sec_nom AS tva_nom, 
															2 AS type
																FROM  Secteurs
																		WHERE sec_etat = '1')
																				ORDER BY ordre";
    } else { //sinon, on recherche ce qui a été utilisé dans le mois recherché
        // 	echo "<h1>Année en passée</h1>";
        
        echo "colonnes ".$reqcolonne="(SELECT (tic_ntva + 100) AS mode_id, 
															(tva_ordre + 100) AS ordre, 
															CONCAT('TVA ', tic_tva) AS mode_nom, 
															tva_couleur as couleur, 
															1 AS type
																FROM Tickets_$an
																JOIN Resume_ticket_$an ON tic_num = rst_id
																JOIN Tva ON tva_id = tic_ntva
																	WHERE MONTH(rst_jour)=$cemois
																		GROUP BY tic_tva)
											UNION ALL
											(SELECT (mdr_id + 200) AS mode_id, 
															(mdr_ordre + 200) AS ordre, 
															mdr_nom AS mode_nom, 
															mdr_couleur as couleur, 
															2 AS type
															FROM Resume_ticket_$an JOIN Mode_reglement ON mdr_abrege = rst_etat WHERE MONTH(rst_jour)=$cemois GROUP BY rst_etat)";
    }
} else { //---------------------------------affichage par secteurs--------------------------------------------
// 		if($an == date("Y"))
    // 		{
    // 	echo "<h1>Année en cours</h1>";
    $reqcolonne="(SELECT (sec_id + 200) AS mode_id, 
															(sec_ordre + 200) AS ordre, 
															sec_abrege AS mode_nom, 
															sec_couleur as couleur, 
															tva_nom AS tva_nom, 
															2 AS type
																FROM  Secteurs
																	LEFT JOIN Tva ON tva_id=sec_tva
																		WHERE sec_etat = '1')
											UNION ALL
															(SELECT (mdr_id + 300) AS mode_id, 
															(mdr_ordre + 300) AS ordre, 
															mdr_nom AS mode_nom, 
															mdr_couleur as couleur, 
															mdr_nom AS tva_nom, 
															3 AS type
																FROM  Mode_reglement
																		WHERE mdr_etat = '1')	
											ORDER BY ordre";
    // 											exit;
// 		}
// 	else
// 		{ //sinon, on recherche ce qui a été utilisé dans le mois recherché
    // 	echo "<h1>Année en passée</h1>";
        
    /* echo "colonnes ".*//*$reqcolonne="(SELECT (tic_ntva + 100) AS mode_id,
                                                            (tva_ordre + 100) AS ordre,
                                                            CONCAT('TVA ', tic_tva) AS mode_nom,
                                                            tva_couleur as couleur,
                                                            1 AS type
                                                                FROM Tickets_$an
                                                                JOIN Resume_ticket_$an ON tic_num = rst_id
                                                                JOIN Tva ON tva_id = tic_ntva
                                                                    WHERE MONTH(rst_jour)=$cemois
                                                                        GROUP BY tic_tva)
                                            UNION ALL
                                            (SELECT (mdr_id + 200) AS mode_id,
                                                            (mdr_ordre + 200) AS ordre,
                                                            mdr_nom AS mode_nom,
                                                            mdr_couleur as couleur,
                                                            2 AS type
                                                            FROM Resume_ticket_$an JOIN Mode_reglement ON mdr_abrege = rst_etat WHERE MONTH(rst_jour)=$cemois GROUP BY rst_etat)";
        }*/
}
$rcolonne = $idcom->query($reqcolonne);
if ($idcom->errno == 1146) {
    echo "Il n'y a pas de données à afficher, l'année ne semble pas commencée. Passez par fin du jour pour régulariser la situation.";
}

$nbcolonne = $rcolonne->num_rows + 2;

$tab_couleur=array("", "");
// $type=array();
$tab_mois=array();//tableau vide qui permet d'insérer une ligne complete générée en ligne ???
$ligne=array("statut", "date");//sera completé en ligne ???
$categorie_libelle=array("Date");
$result=array();//tableau générale des lignes du mois
$tab_decaissement=array();
$tab_repport=array();
$tab_Totalmois=array();
$tab_tvasecteur=array("");
$nbT = "";
$nbM = "";
while ($rqcolonne=$rcolonne->fetch_object()) {
    $modeid="_".$rqcolonne->mode_id;
    array_push($ligne, $modeid);//remplissage tableau php ligne
    array_push($tab_couleur, $rqcolonne->couleur);
    // 	array_push($type, $rqcolonne->type);
    array_push($categorie_libelle, $rqcolonne->mode_nom);
    
    if ($rqcolonne->mode_id < 300) {
        array_push($tab_tvasecteur, $rqcolonne->tva_nom);
        $nbT++;
    } elseif ($rqcolonne->mode_id > 299) {
        $nbM++;
    }
}
// print_r($tab_tvasecteur);
// exit;
$pdfTickets=new PDF_TotalMensuel(10, 6, $nbT, $nbM, $an, $cemois);

array_push($categorie_libelle, "Total");
array_push($ligne, "total");


// print_r($ligne_mois);
$pdfTickets->addLigne("titre", mktime(0, 0, 0, $cemois, 1, $an));
$jour_ab=array("Dim ", "Lun ", "Mar ", "Mer ", "Jeu ", "Ven ", "Sam ", "Dim ");
$pdfTickets->addLigne("categorie", array("", "Ventes journalières par Secteurs", "Ventes journalières par mode de règlement", ""));
$pdfTickets->addLigne("categorie_libelle", $categorie_libelle);
//   $pdfTickets->addLigne("libelle", array("Date", "0.00", "2.10", "5.50", "10.00", "20.00", "jour", "Total", "jour", "Total", "jour", "Total", "Total jour"));*/

if (!$an) {
    $an = date('Y');
}
if (!$cemois) {
    $cemois = date('m');
}

// =array_merge($Totalsemaine, $ligne);
$tabligne = array_fill_keys($ligne, '0.00');//remplissage avec une valeur par défaut
$tab_Totalsemaine = $tabligne;
// print_r($tab_Totalsemaine);exit;
$tab_Totalmois = $tabligne;

// $tab_cumul =$tabligne;

//Recherche les données du mois entregistrées et on création du tableau php par ligne contenant le tableau initial $tabligne
/*echo "mois ".*/$req_mois="SELECT rsj_date , rsj_id, rsj_statut ,  SUM(red_valeur) AS valeur, red_mode  FROM Resume_jour_$an LEFT JOIN Resume_detail_$an ON rsj_id = red_jour WHERE MONTH(rsj_date) = $cemois AND red_mode > 199 GROUP BY red_mode , rsj_id ORDER BY rsj_id";
// exit;
$n=0;
$lignejour = 0;
// $i=0;
$result=array();
$r_mois=$idcom->query($req_mois);
while ($rq_mois=$r_mois->fetch_object()) {
    if ($rq_mois->rsj_id != $lignejour) {                         //ajout de ligne/jour au tableau result
                                                            //
    $result[$n]=array_merge($tab_mois, $tabligne);           //
    $lignejour = $rq_mois->rsj_id;                           //
    $l=$n;                                                   //
    $n++;
    }
    $result[$l]['date']=$rq_mois->rsj_date;
    if ($rq_mois->red_mode != '') {
        $result[$l]["_".$rq_mois->red_mode]=$rq_mois->valeur; //mise à jour de la ligne du jour
    }
    $result[$l]['statut']=$rq_mois->rsj_statut;
}

//*************************************lecture de la table result************************************
// exit;
$r=0;//numero de la ligne
$c=0;//numero de la cellule
foreach ($result as $cle => $v) {
    if ($v["statut"] == 1) {//décaissement suivi de cumul
        unset($v['statut']);//supression de la case statut qui n'apparaît pas sur le tableau
        $v['date']='Décaissem.';
        $decc=$v;
        unset($decc['date']);
        $v['total']=array_sum($decc);
        //cacul du total décaissement
        $pdfTickets->addLigne("decaissement", $v);
        unset($tab_Totalsemaine['statut']);
        $tab_Totalsemaine['date']='Cumul C.A.';
        //calcul du total cumul semaine
        $tab_Total=$tab_Totalsemaine;
        unset($tab_Total['date']);
        $tab_Totalsemaine['total']=array_sum($tab_Total)/2;
        //après la ligne décaissement insertion de la ligne des cumul
        $pdfTickets->addLigne("cumul", $tab_Totalsemaine);
        foreach ($tab_Totalsemaine as $cle => $cel) {//remise à 0 de $tab_Totalsemaine
            $tab_Totalsemaine[$cle] = '0.00';
        }
    } elseif ($v["statut"] == 2) {//contenu
        unset($v['statut']);//supression de la case statut qui n'apparaît pas dur le tableau
            $vt=$v;
        @$v['date'] = jour_lettre($v[date]);
        unset($vt['date']);//supression de la case statut qui n'apparaît pas dur le tableau
            $v['total']=array_sum($vt)/2;//calcul du total de la ligne qu'on met dans la case total
            foreach ($vt as $cle => $cel) {//addition des lignes pour afficher les cumuls
                $tab_Totalsemaine[$cle] += $cel;
                $tab_Totalmois[$cle] += $cel;
            }
        $pdfTickets->addLigne("contenu", $v);
    } elseif ($v["statut"] == 3) {//report
        //le report étant en debut de semaine, on le fait précéder de la ligne du n° de semaine
        $pdfTickets->addLigne("semaine", 'Semaine '.date("W", timestampA($v['date'])));
        unset($v['statut']);//supression de la case statut qui n'apparaît pas dur le tableau
        $v['date']= 'Report';
        $pdfTickets->addLigne("report", $v);
    }
}
  
  $pdfTickets->addLigne("semaine", '');
  $pdfTickets->addLigne("semaine", 'Résultat mensuel');
  $pdfTickets->addLigne("categorie_libelle", $categorie_libelle);

unset($tab_Totalmois['statut']);
$tab_Totalmois['date']="Cumul C.A.";
//calcul du total mois
$tab_Total=$tab_Totalmois;
unset($tab_Total['date']);
$tab_Totalmois['total']=array_sum($tab_Total)/2;
$pdfTickets->addLigne("cumulmensuel", $tab_Totalmois);
// print_r($tab_Totalmois)."<br>";
//ligne des valeurs TVA
// $tab_Totalmois['date']="TVA";
// print_r($categorie_libelle);
// print_r($tab_tvasecteur);
//***********************************calcul du tva du mois************************************
$n=0;
$tt_tva =0.00;
foreach ($tab_Totalmois as $cle => $v) {
    if (is_numeric(substr($cle, 1, 3))) {//exclusion de date et total
        if (substr($cle, 1, 3) < 300) {//exclusion des modes de règlement
// 	echo $n." ".$cle." => ".$v." / ".substr($cle, 1, 3)." / ".$tab_Totalmois[$cle]." / ".$tab_tvasecteur[$n]."\n";
            if ($tab_tvasecteur[$n] == "0.00") {
                $tab_Totalmois[$cle] = "0.00";
                $val_tva = 0.00;
            } else {
                $val_tva=	$tab_Totalmois[$cle]= $v - ($v /(1+ ($tab_tvasecteur[$n]/100)));
            }
            /*echo "\n".*/$tt_tva += $val_tva;
        } else {
            $tab_Totalmois[$cle] = "0.00";
        }
    }
    $n++;
}
// 	print_r($tab_Totalmois);
// 	exit;
$tab_Totalmois['date']="Valeur TVA";
$tab_Totalmois['total']=$tt_tva;
$pdfTickets->addLigne("tva", $tab_Totalmois);

//
if ($main == 1) {
    $pdfTickets->Output();
} else {
    $pdfTickets->Output("F", $incpath."pdf/mois/".$an."_".$cemois.".pdf", 1);
    $req_imp="SELECT * FROM Imprimantes WHERE imp_id=1";
    $r_imp=$idcom->query($req_imp);
    $rq_imp=$r_imp->fetch_object();
    //verification de l'imprimante
    $impr=array();
    exec('lpstat -v', $tab);//$tab=tableau contenant la description des imprimantes
    foreach ($tab as $v) {
        $ta=explode(" ", $v);
        array_push($impr, substr($ta[2], 0, -1));//on retire les : du nom de l'imprimante et on met dans un tableau
    }
    if (in_array($rq_imp->imp_nom, $impr)) {
        exec('lpr -P '.$rq_imp->imp_nom.' -o Resolution=600x600dpi "'.$incpath.'pdf/mois/'.$an."_".$cemois.".pdf");
    } else {
        header("Location:".$incpath."pdf/mois/".$an."_".$cemois.".pdf");//sortie pdf
    }
}
