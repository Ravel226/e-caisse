<?php
/**Fichier include de fin_jour.php ligne 42*/
if (!isset($titre)) {//on vient de corriger un ticket
    $tab_j=explode("-", $jour);
    global $sem;
    $titre = "fin du ".$sem[date("w", timestampA($jour))]." ".$tab_j[2]." ".$mois[$tab_j[1]*1]." ".$tab_j[0];
    $an=$tab_j[0];
}
//recherche des pseudo article livre et objet pour correction avant validation de la journée
$req_pseudo = "SELECT tic_num, Vt1_nom FROM Tickets_$an
                                            JOIN Resume_ticket_$an ON rst_id = tic_num
                                            JOIN Articles ON art_id = tic_article
                                            JOIN Vtit1 ON Vt1_article = tic_article
                                                WHERE DATE(rst_validation) LIKE '".$jour."'
                                                    AND art_pseudo = 4";
$r_pseudo=$idcom->query($req_pseudo);

$req_vente="SELECT SUM(tic_tt) AS TT , rst_etat, mdr_nom, mdr_id 
                        FROM Resume_ticket_$an 
                            JOIN Tickets_$an ON rst_id = tic_num  
                            JOIN Mode_reglement ON mdr_id = rst_etat 
                                WHERE DATE(rst_validation) = '$jour'
                                    GROUP BY rst_etat 
                                        ORDER BY mdr_abrege DESC";
$r_vente=$idcom->query($req_vente);
$nd_reg=$r_vente->num_rows;
$req_utilisateur="SELECT uti_nom FROM Utilisateurs WHERE uti_etat = 1 AND uti_fonction = 2";
$r_utilisateur=$idcom->query($req_utilisateur);
if ($r_utilisateur->num_rows > 0) {
    $rq_utilisateur = $r_utilisateur->fetch_object();
    $clore="<h4>".$rq_utilisateur->uti_nom." connecté</h4>";
} else {
    $clore='<button onclick="charge(\'Cloture\',\''.$jour.'\',\'affichage\')">Clore la journée</button>';
}

//recherche du nombre de ticket par mode de reglement
$bouton_mode = '';
$T = 0.00;
$n = 0;
$req_tr="SELECT COUNT(rst_etat ) AS qt, rst_etat 
                FROM Resume_ticket_$an  
                    WHERE DATE(rst_validation) LIKE '".$jour."%' 
                        GROUP BY rst_etat ";
$r_tr=$idcom->query($req_tr);
while ($rq_vente=$r_vente->fetch_object()) {
    while ($rq_tr=$r_tr->fetch_object()) {
        if ($rq_vente->rst_etat == $rq_tr->rst_etat) {
            $qt = $rq_tr->qt;
        }
    }
    $bouton_mode .= "<TH><button  onclick='charge(\"detail_mode\",\"".$jour."-".$rq_vente->mdr_id."\",\"detail\")'>".$rq_vente->mdr_nom." (".$qt.")</button><br><span id='m_".$rq_vente->mdr_id."'>".monetaireF($rq_vente->TT)."</span> €</TH>";
    $T +=$rq_vente->TT;
    $r_tr->data_seek(0);
    $n++;
}
?>
<h3><?php echo $titre?></h3>
<div><?php echo $clore?><span style="float:right;font-weight:bold">Total <?php echo sprintf('%01.2f', $T)?> €</span></div>
<center><table class='jour' style="background-color:<?php echo $coulCC?>"><TR><thead>
<?php
echo $bouton_mode;
?></thead></TR>
<?php
if ($r_pseudo->num_rows != 0) {
    $s=($r_pseudo->num_rows > 1)?"des 'pseudos'":"un 'pseudo'";
    $rq_pseudo=$r_pseudo->fetch_object();
    echo "<tfoot><tr><TD colspan='".$n."' style='text-align:center'><button onclick=\"charge('pseudo_jour','".$jour."','panneau_d')\">Il y a ".$s." à corriger</button></TD></tr>";
}

?>
</tfoot>
</table></center>