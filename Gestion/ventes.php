<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$filtre= filter_input(INPUT_GET, "f", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$req= filter_input(INPUT_GET, "id", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if ($req == "") {
    $req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}
require $incpath."mysql/connect.php";
require $incpath."php/config.php";
connexobjet();
$cemois=date('m')."-".date('H')."-".date('i');
$cetheure=date('H')."-".date('i');
?>
<h3>Ventes depuis du 1er janvier au <?php echo date("j")." ".$mois[date('n')]." ".date('Y').", ".date('H').":".date('i')?></h3>
<?php

$debut=$config['debut'];
$annee = array();
$valeur = array();
$paq = array();
for ($i = date('Y');$i >= $debut;$i--) {
    $cem=$i."-".date('m')."-".date('d')." ".date('H').":".date('i');
    $req_vente="SELECT IFNULL(SUM(rst_total),0) as CT FROM Resume_ticket_$i WHERE rst_validation <= '$cem' GROUP BY DATE(YEAR(rst_validation))";
    $r_vente=$idcom->query($req_vente);
    if($idcom->error) echo "<br>".$idcom->errno." ".$idcom->error."<br>";
    $resu=$r_vente->fetch_object();
    $vente=$resu->CT;
    array_push($annee, $i);
    array_push($valeur, $vente);
    array_push($paq, easter_date($i));
} //exit;
$val=max($valeur);//."<br>";
$nb_annee = count($annee);
$haut=40*$nb_annee - 20;  
//exit;
//http://www.journaldunet.com/developpeur/tutoriel/xml/041209-xml-svg-filtres-1.shtml
?>
<center>
<?php
// if(strstr($_SERVER['HTTP_USER_AGENT'],"Firefox") != FALSE)
echo '<div style="width:550px;padding-top:10px;height:20px;font-weight:bold"><span style=float:left><em>Jour de Pâques</em></span><span style=float:right><em>Valeur TTC</em></span></div>';
?>

<?xml version="1.0" encoding="utf-8"?> <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN" "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
<svg width="550px" height="<?php echo ($haut+50)?>px" xml:lang="fr" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<defs>
<filter id="filtre" filterUnits="userSpaceOnUse">
    <feGaussianBlur in="SourceAlpha" stdDeviation="4" result="blur"/>
    <feOffset in="blur" dx="4" dy="4" result="offsetBlur"/>
    <feComposite in="blur" in2="SourceAlpha" operator="in" result="specOut"/>
    <feComposite in="SourceGraphic" in2="specOut" operator="arithmetic" k1="0" k2="1" k3="1" k4="0" result="litPaint"/>
    <feMerge>
    <feMergeNode in="offsetBlur"/>
    <feMergeNode in="litPaint"/>
    </feMerge>
</filter>

</defs>
<!-- <g fill="url(#MonDegrade)" > -->

<?php
// exit;
$decal=30;
for ($i=0;$i<$nb_annee;$i++) {
  $long = 450 / $val * $valeur[$i];
  echo '<rect filter=url(#filtre) x="0" y="'.$decal.'" width="'.$long.'" height="20" style="fill:cornsilk;stroke:slategray;stroke-width:2px;"/><text class="end" x="10" y="'.($decal+15).'">'.date("d",$paq[$i])."-".date("m",$paq[$i])."-".date("Y",$paq[$i]).'</text><text x="'.($long+10).'" y="'.($decal+15).'">'.monetaireF($valeur[$i]).' €</text>'."\n";
  $decal = $decal+40;
}
?>
</svg>
</center>
