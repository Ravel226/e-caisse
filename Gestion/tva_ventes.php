<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$an = date("Y");
$req_art="SELECT art_id,
                Vt1_nom,
                ray_nom,
                sec_nom,
                tic_ntva,
                tic_tva,
                sec_tva,
                tva_nom,
                tic_prix,
                tic_quantite,
                art_unite,
                tic_num,
                tic_cp
                    FROM Tickets_$an
                    JOIN Articles ON art_id = tic_article
                    JOIN Rayons ON ray_id = art_rayon
                    JOIN Secteurs ON sec_id = ray_secteur
                    JOIN Vtit1 ON Vt1_article = tic_article
                    JOIN Tva ON tva_id = sec_tva
                        WHERE sec_tva != tic_ntva";
$r_art=$idcom->query($req_art);
$nb = $r_art->num_rows;

$req_tva = "SELECT tva_id, tva_nom FROM Tva ORDER BY tva_id";
$r_tva=$idcom->query($req_tva);
while ($rq_tva = $r_tva->fetch_object()) {
    $tab_tva[]=$rq_tva->tva_nom;
}
?>


<h3>Contrôle de cohérence des taux de tva ( <?php echo $nb?> erreurs )</h3>
<center><table id='factures' class="generique">
  <thead>
  <TR>
  <TH>Article N°</TH><TH>N° ticket</TH><TH>Titre</TH><TH>Tva ticket</TH><TH>Tva secteur</TH><TH>Secteur</TH><th>Rayon</th><th>Prix</th><th>Quantité</th><th>Écart</th>
  </TR>
  </thead>
  <tbody>
<?php
$n = 0;
while ($resu=$r_art->fetch_object()) {
    $coul=($n%2 == 0)?$coulCC:$coulFF;
    $quantite=($resu->art_unite == 1)?sprintf('%d',$resu->tic_quantite):$resu->tic_quantite;
    $ecart = ($resu->tic_prix - ($resu->tic_prix / (1+($resu->tva_nom / 100))))-($resu->tic_prix - ($resu->tic_prix / (1+($resu->tic_tva / 100))));
    $numero_ticket = ($resu->tic_num > 1000000000)?date('j/n/Y', $resu->tic_num): $resu->tic_num;
    echo "<tr style='background-color:".$coul."'><TD>".$resu->art_id."</TD><TD>".$numero_ticket."</TD><TD>".stripslashes($resu->Vt1_nom)."</TD><TD>".$tab_tva[$resu->tic_ntva]."&nbsp;%</TD><TD>".$tab_tva[$resu->sec_tva]."&nbsp;%</TD><TD>".$resu->sec_nom." </TD><td>".$resu->ray_nom."</td><td>".$resu->tic_prix."</td><td>".$quantite."</td><td>".monetaireF($ecart)."</td></tr>";
    $n ++;
}
?>
</tbody>  
</table></center>

<script>
var b=$('#affichage').height() - 20;
$("#panneau_g").css('max-height', b);

</script>