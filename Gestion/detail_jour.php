<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tab0=explode(" ", $req);
// print_r($tab0);
$req=$tab0[0];
$tab=explode("-", $req);
$cemois=$tab[1];
$ceannee=$tab[0];
$cejour=$tab[2];
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

?>
<style>
table.tablesorter tbody td {
  font-size: 14px;
  font-family:arial;
  text-align:left;
  color: #3D3D3D;
  padding: 10px;
  background-color: <?php echo $coulFF?>;
  vertical-align: top;
 }
table.tablesorter tbody tr.odd td {
  text-align:left;
  border-top:solid 1px;
  background-color:<?php echo $coulCC?>;
     }
table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
    background-color: #8dbdd8;
    }
</style>
<script src="/js/jquery.tablesorter.js"></script>
<script>
$('#panneau_d').empty();
$(document).ready(function(){
    $("#jour").tablesorter({ widgets: ['zebra']});
    }
);
</script>

<h3>Détail du <?php echo dateFR($req)?></h3>

<div id="liste">
<h3 style="cursor:pointer" id='titre_mois'><?php echo $mois[$cemois*1]?></h3>
<div id='liste_jour' style="display: none;">
<?php
for ($i=1;$i<=date('t', timestampA($req));$i++) {
    if (str_pad($i, 2, "0", STR_PAD_LEFT) == $cejour) {
        echo "<div style='text-align:center'>".str_pad($i, 2, "0", STR_PAD_LEFT)."</div>";
    } else {
        echo "<button onclick=\"charge('detail_jour','".$ceannee."-".$cemois."-".str_pad($i, 2, "0", STR_PAD_LEFT)."','panneau_g')\">".str_pad($i, 2, "0", STR_PAD_LEFT)."</button><br>";
    }
    if (($cemois == date("m"))&&($i  >= date("j"))) {
        break;
    }
}
echo "</div></div>";
if (file_exists($incpath."pdf/jours/".$req.".pdf")) {
    echo '<a href="/pdf/jours/'.$req.'.pdf"><img src="../images/pdf.gif" width="14" height="14" style="float:right"></a>';
} else {
    echo '<a href="PDF_jour.php?jour='.$req.'"><img src="/images/imp.png" style="float:right"></a>';
}
?>

<table id='jour' style="width:auto;float:right;margin-right:10px" class="tablesorter">
    <thead>
        <TR><TH>Ticket</TH><TH>Valeur</TH><TH>Heure</TH><TH>Vendeur</TH><TH>Règlement</TH></TR>
    </thead>
<tbody>
<?php
$tt = 0.00;
$tt_enregistre = 0.00;
$req_jour="SELECT tic_num,
                rst_id,
                rst_num,
                SUM(tic_tt) AS valeur,
                rst_total,
                RIGHT(rst_validation,8) AS rst_validation,
                uti_nom,
                mdr_nom
                FROM Resume_ticket_$ceannee
                    LEFT JOIN Tickets_$ceannee ON rst_id = tic_num
                    LEFT JOIN Mode_reglement ON mdr_id = rst_etat
                    JOIN Utilisateurs ON rst_utilisateur = uti_id
                        WHERE DATE(rst_validation) = '$req' GROUP BY rst_id";
$r_jour=$idcom->query($req_jour);
while ($rq_jour=$r_jour->fetch_object()) {
    echo "<TR onclick='charge(\"detail_ticket\",\"".$ceannee."&tic=".$rq_jour->rst_id."\",\"panneau_d\")'>
        <td>".$rq_jour->rst_id."/".$rq_jour->rst_num."</td>
        <td style='text-align:right'>".monetaireF($rq_jour->valeur)."</td>
        <td>".$rq_jour->rst_validation."</td>
        <td>".$rq_jour->uti_nom."</td>
        <td>".$rq_jour->mdr_nom."</td>
    </TR>";
    $tt+=$rq_jour->valeur;
    $tt_enregistre +=$rq_jour->rst_total;
}
echo "<tr><td><strong>Total</strong></td><td>".monetaireF($tt)."</td><td></td><td></td><td></td></tr>";
if ($tt != $tt_enregistre) {
    echo "<tr><td colspan='5'><strong>Erreur sur le total</strong> ".$tt." != ".$tt_enregistre."</td></tr>";
}
?></tbody>
</table>

<script>
$( "#titre_mois" ).click(function() {
    $( "#liste_jour" ).slideToggle( "slow", function() {
        // Animation complete.
    });
});
$("#panneau_g").css('max-height', $('#affichage').height()-10);
$("#panneau_d").css('max-height', $('#affichage').height()-10);
$("#liste").css('left', $('#affichage').css('left'));
$("#liste").css('max-height', $('#affichage').height()-70);
</script>