<?php
session_start();
$tab=explode('/', $_SERVER['REQUEST_URI']);
$dossier=$tab[1];
// print_r($_SESSION);
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_facture="SELECT * FROM Factures_$an WHERE fac_id=$req";
$r_facture=$idcom->query($req_facture);
$resu=$r_facture->fetch_object();

?>
<h3> Facture N° Lib<?php echo $an.str_pad($resu->fac_id, 3, "0", STR_PAD_LEFT)." année ".$an?>, ticket N° <?php echo $resu->fac_ticket?></h3>
<hr>
<?php

$compte="SELECT * FROM Comptes WHERE cpt_id=".$resu->fac_cp;
$r_compte=$idcom->query($compte);
$rq_compte=$r_compte->fetch_object();
echo "<b>".$rq_compte->cpt_nom."</b><br>".stripslashes($rq_compte->cpt_adr1)."<br>";
if ($rq_compte->cpt_adr2 != "") {
    echo stripslashes($rq_compte->cpt_adr2)."<br>";
}
echo $rq_compte->cpt_cp." ".$rq_compte->cpt_ville;
if ($rq_compte->cpt_pays != "") {
    "<br>".$rq_compte->cpt_pays."<br>";
}
echo "<hr>";
//***********************************liste des articles de la facture****************************************


$req_articles="SELECT Vt1_nom,
                        tic_num, 
                        tic_prix, 
                        tic_quantite, 
                        art_unite,
                        tic_id,
                        tic_tt
                            FROM Tickets_$an 
                            JOIN Vtit1 ON tic_article = Vt1_article 
                            JOIN Articles ON art_id = Vt1_article
                                WHERE tic_num = ".$resu->fac_ticket;
$r_articles=$idcom->query($req_articles);
if ($idcom->errno !="") {
    echo $idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_articles->num_rows;

?>
<style type="text/css">
#detfactures{width:90%;margin-left:50px}
#detfactures th {
text-align:center;
}
#detfactures td{
text-align:right
}
</style>
<table class='generique'><thead><TR><TH>Article</TH><TH>quantité</TH><TH>Prix TTC</TH></TR></thead><tbody>
<?php
$n = 0;
$tt = 0.00;
while ($rq_articles=$r_articles->fetch_object()) {
//     print_r($rq_articles);
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    if ($rq_articles->art_unite == 1) {
        $tic_quantite = sprintf('%d', $rq_articles->tic_quantite);
    } else {
        $tic_quantite = $rq_articles->tic_quantite;
    }

    echo "<tr style='background-color:".$coul.";'><td>".$rq_articles->tic_id." ".$rq_articles->Vt1_nom."</td><td class='droite'>".$tic_quantite."</td><td class='droite'>".$rq_articles->tic_prix."&nbsp;€</td></tr>";
    $tt += $rq_articles->tic_tt;
    $n++;
}
  echo "</tbody><tfoot><tr><th colspan = 2>Total</th><th>".monetaireF($tt)." €</th></tr></tfoot>";

?></table>
<?php
    if (file_exists($incpath."pdf/factures/facture_".($an*1000 + $req).".pdf")) {
        echo $fac= '<a href="'.$incpath."pdf/factures/facture_".($an*1000 + $req).'.pdf"><img src="../images/pdf.gif" width="14" height="14"></a> ';
    } else {
        echo '<img src="../images/imp.png"></a>';
    }

?>
