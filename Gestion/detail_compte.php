<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$sup= filter_input(INPUT_GET, "sup", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
if ($sup) {
    $idcom->query("DELETE FROM Comptes WHERE cpt_id=$sup");
    echo '<h1>Le compte à bien été supprimé</h1>';
    ?>
    <script>
    $('#C_<?php echo $sup?>').empty();
    </script>
    <?php
    exit;
}
if ($req == "000") {//creation de compte
    $insert=" INSERT INTO Comptes (cpt_nom) VALUE ('NOUVEAU')";
    $r_comptes=$idcom->query($insert);
    $req=$idcom->insert_id;
}

$req_comptes="SELECT * FROM Comptes WHERE cpt_id=$req";
$r_comptes=$idcom->query($req_comptes);
$resu=$r_comptes->fetch_object();
?>
<h3><?php echo $resu->cpt_nom." N° ".$resu->cpt_id?></h3>
<?php
require '/inc/detail_compte.inc.php';

//***********************************historique des factures****************************************

//on recherche toutes les factures de ce comptes
require $incpath."php/config.php";
$deb = $config['debut'];

for ($i=$deb; $i <= ANNEE; $i++) {
    $req_facture.="SELECT fac_cp, rst_id, $i AS fac_annee, fac_id, fac_ticket, rst_total ,DATE(rst_validation) AS rst_jour, mdr_nom, uti_nom 
								FROM Factures_$i 
									JOIN Resume_ticket_$i ON rst_id=fac_ticket 
									JOIN Mode_reglement ON mdr_abrege = rst_etat 
									JOIN Utilisateurs ON uti_id=rst_utilisateur 
										WHERE fac_cp=$req";
    if ($i < ANNEE) {
        $req_facture.=" UNION ALL ";
    }
}
//***********************************historique des ventes****************************************

//***********************************historique des commandes****************************************
$req_facture.=" ORDER BY fac_annee DESC, rst_id DESC";
// echo $req_facture;
$r_facture=$idcom->query($req_facture);
if ($idcom->errno !="") {
    echo $idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_facture->num_rows;
if (($nb == 0) && ($resu->cpt_compte == 0) ) {//5,10,20%
    echo "Ce compte n'a pas de facture associée, il peut être supprimé";
    ?><br>
    <button onclick="charge('comptes','&sup=<?php echo $resu->cpt_id?>','panneau_d')">Supprimer ce compte</button>
    <?php
} else {
    echo "<h3>Il y a $nb facture".($nb > 1?"s":"")." sur ce compte</h3>";
    ?>
    <style type="text/css">
    #factures td{
    text-align:right
    }
    </style>
    <center><table id="factures" style="width:90%"><TR><TH>N° ticket</TH><TH>N° Facture</TH><TH>Date</TH><TH>px TTC</TH><TH>Règlement</TH><th>Vendeur</th></TR>
    <?php
    $n=0;
    while ($rq_facture=$r_facture->fetch_object()) {
        if (file_exists($incpath."pdf/factures/facture_".($rq_facture->fac_annee*1000 + $rq_facture->fac_id).".pdf")) {
            echo '<a href="'.$incpath."pdf/factures/facture_".($rq_facture->fac_annee*1000 + $rq_facture->fac_id).'.pdf"><img src="../images/pdf.gif" width="14" height="14"></a>';
        }
        $coul=($n%2 == 0)?$coulCC:$coulFF;
        echo "<tr onclick=\"charge('detail_facture','".$rq_facture->fac_id."&an=".$rq_facture->fac_annee."','tableDetailCompte');\" style='background-color:".$coul.";'><td>".$fac.$rq_facture->fac_id."/".$rq_facture->rst_id."</td><td>facture_".(($rq_facture->fac_annee*1000)+$rq_facture->fac_id)."</td><td>".dateFR($rq_facture->rst_jour)."</td><td>".$rq_facture->rst_total." €</td><td>".$rq_facture->mdr_nom."</td><td>".$rq_facture->uti_nom."</td></tr>";
        $n++;
    }
}
?></table></center>

<script>
var b=$('#panneau_g').height();
$("#panneau_d").css('max-height', b);
</script>
