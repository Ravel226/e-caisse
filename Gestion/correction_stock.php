<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
require $incpath . "definitions.inc.php";
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
connexobjet();
$an = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
// $an = 2019;
if ($an == "") {
    $an = date("Y") - 1;
}
$precedent = $an - 1;

$req_erreur = "SELECT Vart_titre,
                    Vart_cb,
                    Vart_unite, 
                    cor_stkpre,
                    cor_commande, 
                    cor_vente, 
                    cor_stock, 
                    cor_pht, 
                    ((cor_commande + cor_stkpre) - cor_vente)-cor_stock AS cor_quantite, 
                        uti_nom 
                        FROM Corrections_$an 
                        JOIN Varticle_editeur ON cor_article = Vart_id 
                        JOIN Utilisateurs ON uti_id=Vart_utilisateur
                        JOIN Articles ON art_id = Vart_id
                            WHERE art_pseudo = '1' 
                                ORDER BY Vart_utilisateur, Vart_titre";

$r_erreur = $idcom->query($req_erreur);
if ($idcom->error) {
    echo "<br>" . $idcom->errno . " " . $idcom->error . "<br>";
}

$nb = $r_erreur->num_rows;
if ($an > 2017) {
    $pre = '<button onclick="charge(\'correction_stock\', \'' . ($an - 1) . '\', \'affichage\')">' . ($an - 1) . '</button>';
} else {
    $pre = '';
}
if (($an < 2017) && ($an >= (date('Y') - 1))) {
    $suiv = '<button onclick="charge(\'correction_stock\', \'' . ($an + 1) . '\',\'affichage\')">' . ($an - 1) . '</button>';
} else {
    $suiv = '';
}

?>

<h2 align="center"><?php echo $pre ?> Correctons stock <?php echo $an . " (" . $nb . ")" ?> <?php echo $suiv ?></h2>

<center>
    <table class='generique' style="width:600px">
        <thead>
            <TR>
                <Th>Titre</Th>
                <Th>PU HT</Th>
                <Th>Qt</Th>
                <th>Total HT</th>
            </TR>
        </thead>
        <?php
        $stt = 0.00;
        $TT = 0.00;
        $utilisateur = '';
        $n = 0;

        while ($rq_erreur = $r_erreur->fetch_object()) {            
            if ($rq_erreur->Vart_unite == 1) {
                $cor_quantite = sprintf('%d', $rq_erreur->cor_quantite);
            } else {
                $cor_quantite = $rq_erreur->cor_quantite;
            }

            if ($utilisateur != $rq_erreur->uti_nom) {
                if ($n > 0) {
                    echo "<tr align='right'><th colspan='3'>sous total : " . $utilisateur . "</th><th>" . monetaireF($stt) . "</th></tr>";
                }
                echo "<tr><th colspan='4' align=center>" . $rq_erreur->uti_nom . "</th></tr>";
                $utilisateur = $rq_erreur->uti_nom;
                $stt = "0.00";
            }
            $coul = ($n % 2 == 0) ? $coulCC : $coulFF;

            $tt = $rq_erreur->cor_quantite * $rq_erreur->cor_pht;

            // echo "<br>" . 
            echo "<tr style='background-color:" . $coul . "'>
    <td>" .$n." ". $rq_erreur->Vart_titre . "</td>
    <td class='droite'>" . monetaireF($rq_erreur->cor_pht) . "</td>
    <td class='droite'>" . $cor_quantite . "</td>
    <td class='droite'>" . monetaireF($tt) . " </td></tr>";
            // echo "<br>".$tt
            $n++;
            $stt += $tt;            
            $TT += $tt;
        }
        ?>
        <tfoot>
            <tr>
                <th colspan="3" align="right">sous-total : <?php echo $utilisateur ?></th>
                <th colspan="2"><?php echo monetaireF($stt) ?> € HT</th>
            </tr>
            <tr>
                <th colspan="3" align="right">Valeur perte estimée :</th>
                <th colspan="2"><?php echo monetaireF($TT) ?> € HT</th>
            </tr>
        </tfoot>
    </table>
</center>