<?php
if (!isset($incpath)) {
    session_start();
    if (!isset($incpath)) {
        $p=preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath="";
        for ($i=1;$i<sizeof($p)-1;$i++) {
            $incpath='../'.$incpath;
        }
        unset($p, $i);
    }
    $date= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    include $incpath."mysql/connect.php";
    include $incpath."php/fonctions.php";
    connexobjet();

    if (!$date) {
        $date = ANNEE."-".date('m')."%";
    }
}
$an = substr($date, 0, 4);

echo $req_recher="SELECT def_tvavaleur,
                    def_artvaleur,
                    edi_nom,
                    rsc_ttc,
                    rsc_id,
                    Journal_factures_$an.*,
                    edi_code,                    
                    tva_code,
                    tva_codeachat,
                    Vce_nom,
                    Vana_nom,
                    Vct_nom
                        FROM Detail_facture_$an
                        JOIN Journal_factures_$an ON def_facture = jrn_facture
                        JOIN Resume_commande_$an ON rsc_id = jrn_facture
                        JOIN Editeurs ON edi_id = rsc_serveur
                        JOIN Tva ON tva_id = def_tva
                        LEFT JOIN Codes ON tva_code = cod_id
                        LEFT JOIN Vcodes_entree ON Vce_id = def_codeent
                        LEFT JOIN Vcodes_tvadeductible ON Vct_id = tva_codeachat                 
                        JOIN Vanalytique ON Vana_id = def_codeana
                            WHERE DATE(jrn_date ) LIKE '$date%'
                            AND jrn_export = 0
                                ORDER BY jrn_date, rsc_id , tva_code DESC";
$r_recher=$idcom->query($req_recher);

$editeur = "";
ob_start();
$n = 1;
$m = 0;
// $ligne_csv = array('','','','','','');
// $csv = array();
$mode = 1;//valeur fictive de départ
$journal = "";
while ($rq_recher =$r_recher->fetch_object()) {
    // array_push($csv, $ligne_csv);
    // print_r($rq_recher);
    if ($rq_recher->rsc_ttc > 0.00) {
        if ($journal != $rq_recher->jrn_id) {
            echo "\"AP\"\t\"".dateFR($rq_recher->jrn_date)."\"\t".$rq_recher->jrn_numero."\t\"".$rq_recher->edi_code."\"\t\"".$rq_recher->edi_nom."\"\t\t".monetaireF($rq_recher->rsc_ttc)."\t\n";
            $journal = $rq_recher->jrn_id;
            $m++;
        }
        //pour chaque ligne retournée, deux affichées : ligne article : pht et tva,  ligne tva: pht et tva si tva != 0.00
        echo "\"AP\"\t\"".dateFR($rq_recher->jrn_date)."\"\t\t\"".$rq_recher->Vce_nom."\"\t\"".$rq_recher->edi_nom."\"\t".monetaireF($rq_recher->def_artvaleur)."\t\t".$rq_recher->Vana_nom."\n";
        if ($rq_recher->def_tvavaleur != 0.00) {
            echo "\"AP\"\t\"".dateFR($rq_recher->jrn_date)."\"\t\t\"".$rq_recher->Vct_nom."\"\t\"".$rq_recher->edi_nom."\"\t".monetaireF($rq_recher->def_tvavaleur)."\t\t\n";
        }
    } else { //avoir
        if ($journal != $rq_recher->jrn_id) {
            echo "\"AP\"\t\"".dateFR($rq_recher->jrn_date)."\"\t".$rq_recher->jrn_numero."\t\"".$rq_recher->edi_code."\"\t\"".$rq_recher->edi_nom."\"\t".monetaireF($rq_recher->rsc_ttc*-1)."\t\t\n";
            $journal = $rq_recher->jrn_id;
            $m++;
        }
        //pour chaque ligne retournée, deux affichées : ligne article : pht et tva,  ligne tva: pht et tva si tva != 0.00
        echo "\"AP\"\t\"".dateFR($rq_recher->jrn_date)."\"\t\t\"".$rq_recher->Vce_nom."\"\t\"".$rq_recher->edi_nom."\"\t\t".monetaireF($rq_recher->def_artvaleur*-1)."\t".$rq_recher->Vana_nom."\n";
        if ($rq_recher->def_tvavaleur != 0.00) {
            echo "\"AP\"\t\"".dateFR($rq_recher->jrn_date)."\"\t\t\"".$rq_recher->Vct_nom."\"\t\"".$rq_recher->edi_nom."\"\t\t".monetaireF($rq_recher->def_tvavaleur*-1)."\t\n";
        }
    }
}
$memoire = ob_get_contents();
$id_file=fopen("exportachat_ebp.csv", "w+b");
$memoire = iconv("UTF-8", "Windows-1252", $memoire);
fwrite($id_file, $memoire);
fclose($id_file);
ob_end_flush();
//mise à jour de la base de données
$req_update = "UPDATE Journal_factures_$an SET jrn_export = 1 WHERE jrn_export = 0 AND DATE(jrn_date ) LIKE '$date%'";
$r_update = $idcom->query($req_update);
?>
<script>
$('#export_ebp').css('visibility','hidden');
$('#mysql').empty();
location.href=('exportachat_ebp.csv');</script>