<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$cpt = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$req_comptes="SELECT cpt_id, cpt_nom,cpt_adr1, cpt_ville FROM Comptes WHERE cpt_nom LIKE '$cpt%' ORDER BY cpt_nom";
$r_comptes=$idcom->query($req_comptes);
  $n = 0;
  ?>
<script>
$(document).ready(function(){
    $('.generique tr').click(function(){
    $('.generique tr').css('font-weight','normal');
    $(this).css('font-weight','bold');
    charge('comptes',$(this).attr('id'),'panneau_d');
    });
});
</script>
<table class="generique">
<thead><tr><th>Nom</th><th>Adresse</th><th>Ville</th></tr></thead><tbody>
<?php
while ($resu=$r_comptes->fetch_object()) {
    $coul=($n%2 == 0)? $coulCC:$coulFF;
    echo '<tr style="background-color:'.$coul.'" id="'.$resu->cpt_id.'"><td>'.$resu->cpt_nom."</td><td>".$resu->cpt_adr1."</td><td>".$resu->cpt_ville."</td></tr>"; 
    $n++;
}

?></tbody>
</table>
