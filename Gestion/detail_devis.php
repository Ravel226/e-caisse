<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p,$i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$req_devis="SELECT * FROM Tickets_$an WHERE tic_devis=$req";
$r_devis=$idcom->query($req_devis);
if ($r_devis->num_rows == 0) {
    echo "<img src='/images/attention.png'> Il n'y a pas de détail pour ce devis";
    exit;
}
$rq_devis=$r_devis->fetch_object();

?>
<h3> Devis N° Dev<?php echo(($an*1000)+$req)." année ".$an?></h3>
<hr>
<?php

$compte="SELECT * FROM Comptes WHERE cpt_id=".$rq_devis->tic_cp;
$r_compte=$idcom->query($compte);
$rq_compte=$r_compte->fetch_object();
echo "<b>".$rq_compte->cpt_nom."</b><br>".stripslashes($rq_compte->cpt_adr1)."<br>";
if ($rq_compte->cpt_adr2 != "") {
    echo stripslashes($rq_compte->cpt_adr2)."<br>";
}
echo $rq_compte->cpt_cp." ".$rq_compte->cpt_ville;
if ($rq_compte->cpt_pays != "") {
    "<br>".$rq_compte->cpt_pays."<br>";
}
echo "<hr>";
//***********************************liste des articles de la facture****************************************


$req_articles="SELECT Vt1_nom, tic_num, tic_prix, tic_quantite FROM Tickets_$an JOIN Vtit1 ON Vt1_article = tic_article WHERE tic_devis = ".$rq_devis->tic_devis;
$r_articles=$idcom->query($req_articles);
if ($idcom->errno !="") {
    echo $idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_articles->num_rows;

  ?>
  <style type="text/css">
  #detfactures{width:90%;margin-left:50px}
  #detfactures th {
  text-align:center;
  }
  #detfactures td{
  text-align:right
  }
  </style>
  <table id="detfactures"><TR><TH>Article</TH><TH>quantité</TH><TH>Prix</TH></TR>
  <?php
  $n = 0;
  while ($rq_articles=$r_articles->fetch_object()) {
//     print_r($rq_articles);
      if ($n%2 == 0) {
          $coul=$coulCC;
      } else {
          $coul=$coulFF;
      }
      echo "<tr style='background-color:".$coul.";'><td>".$rq_articles->Vt1_nom."</td><td>".$rq_articles->tic_quantite."</td><td>".$rq_articles->tic_prix."&nbsp;€</td></tr>";
      $n++;
  }
  
?></table>
