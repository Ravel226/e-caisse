<?php
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
    session_start();
    /*
    Creation et remplissage en fin d'inventaire de la table Correction_ANNEE 
    qui contient les erreurs de stock de l'année
    */
    include $incpath."mysql/connect.php";
    include $incpath."php/fonctions.php";
    connexobjet();
}

$req_commandes = "CREATE TEMPORARY TABLE Vcommandes AS 
                    select Commandes_".ANNEE.".com_article AS Vcom_art,
                                    SUM(com_quantite) AS Vcom_quantite 
                                        FROM Commandes_".ANNEE." 
                                        JOIN Resume_commande_".ANNEE." on rsc_id = com_numero 
                                            WHERE rsc_etat = 3
                                                GROUP BY com_article";
$idcom->query($req_commandes);
echo $idcom->errno." ".$idcom->error."<br>";
$idcom->query("ALTER TABLE Vcommandes ADD KEY Vcom_art (Vcom_art)");
// echo $idcom->errno." ".$idcom->error."<br>";
//Les ventes
 $req_ventes = "CREATE TEMPORARY TABLE `Vvente` AS 
			SELECT `tic_article` AS `Vven_art`,
					SUM(`tic_quantite`) AS `Vven_quantite` 
						FROM `Tickets_".ANNEE."` 
							GROUP BY `tic_article`";
$idcom->query($req_ventes);
echo $idcom->errno." ".$idcom->error."<br>";
$idcom->query("ALTER TABLE `Vvente` ADD KEY Vven_art (Vven_art)");
//a verifier, exclusion des dépots
$Corrections = "CREATE TEMPORARY TABLE `Corrections` AS 
						SELECT art_id,
							Vt1_nom,
							art_pht,
							art_stk,
							art_unite,
							0 AS ven_qt,
							0 AS vcom_qt,
							0 AS stk 
								FROM Articles 
								JOIN Editeurs ON edi_id = art_editeur 
								JOIN Vtit1 ON Vt1_article = art_id 
								LEFT JOIN Depot ON dep_article = art_id 
									WHERE dep_article IS NULL";
$idcom->query($Corrections);
echo $idcom->errno." ".$idcom->error."<br>";
$idcom->query("ALTER TABLE `Corrections` ADD KEY art_id (art_id)");
// $idcom->query($Corrections);
// $nb = $r_erreur->num_rows;
//commandes
$precedent= ANNEE - 1;
$idcom->query("UPDATE Corrections, Vcommandes SET vcom_qt = Vcom_quantite WHERE Vcom_art = art_id");
// /*$r_erreur=*/$idcom->query($req_erreur);
$idcom->query("UPDATE Corrections, `Vvente` SET ven_qt = Vven_quantite WHERE Vven_art = art_id");
// /*$r_erreur=*/$idcom->query($req_erreur);
$idcom->query("UPDATE Corrections, Stock_$precedent SET stk = stk_stk WHERE stk_article = art_id");
$idcom->query('DROP TABLE `Corrections_'.ANNEE.'`');
$CorrectionsFinal = "CREATE TABLE `Corrections_".ANNEE."` AS 
					SELECT art_id AS cor_article,
							vcom_qt AS cor_commande,
							stk AS cor_stkpre,
							ven_qt AS cor_vente,
							art_stk AS cor_stock,
							art_pht AS cor_pht
								FROM Corrections
									WHERE ((vcom_qt + stk) - ven_qt)!= art_stk";
$idcom->query($CorrectionsFinal);
$idcom->query("ALTER TABLE `Corrections_".ANNEE."` ADD KEY cor_article (cor_article)");// echo "<br>".$idcom->errno." ".$idcom->error;
//
/*$req_erreur="SELECT art_id, Vt1_nom, vcom_qt,stk,ven_qt, art_stk,art_unite FROM Corrections WHERE ((vcom_qt + stk) - ven_qt)!= art_stk";
$r_erreur=$idcom->query($req_erreur);
$nb = $r_erreur->num_rows;*/
