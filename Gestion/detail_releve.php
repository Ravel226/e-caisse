<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_comptes="SELECT * FROM Comptes WHERE cpt_id=$req";
$r_comptes=$idcom->query($req_comptes);
$resu=$r_comptes->fetch_object();
$an=date("Y");
// print_r($resu);
?>
<script>
function sup_c(id){
$('#confirme').css('visibility','visible');
// modif(id,20,'','id',2);
// setTimeout(charge('detail_releve','<?php echo $req?>','panneau_d'),500);
}
</script>
<h3> Relevé pour <?php echo $resu->cpt_nom." N° ".$resu->cpt_id?></h3>
<?php
//***********************************liste des articles en compte****************************************
$req_articles="SELECT Vt1_nom,
                      tic_id,
                      tic_num,
                      tic_prix,
                      tic_quantite,
                      tic_devis ,
                      art_unite,
                      tic_tt
                        FROM Tickets_$an 
                          JOIN Articles ON art_id = tic_article 
                          JOIN Vtit1 ON Vt1_article = tic_article
                            WHERE tic_num > 100000 AND tic_cp = $req 
                              ORDER BY tic_devis DESC, tic_article";
$r_articles=$idcom->query($req_articles);
if ($idcom->errno !="") {
    $idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_articles->num_rows;
if ($nb == 0) {
    echo "<h3>Il n'y a pas  de donnée à afficher</h3>";
    exit;
}
$rq_articles=$r_articles->fetch_object();
$tic_devis = $rq_articles->tic_devis;
if ($tic_devis != 0) {
    $devis =' et un devis <a href="/pdf/devis/devis_'.(($an * 1000) +$rq_articles->tic_devis).'.pdf"> <img src="/images/pdf.gif"></a>';
}

echo "<h3 id='releve'>Il y a $nb article".($nb > 1?"s":"")." en attente sur ce compte <a href = 'etat.php?cp=".$req."'><img src = '/images/imp.png' style = 'float:right' /></a></h3>";
?>
<style type="text/css">
#factures th {
text-align:center;
}
#factures td{
text-align:right;
padding:3px 10px 3px 10px;
}
</style>
<table id="factures" class="generique"><thead><TR><th></th><TH>Article</TH><TH>quantité</TH><TH>Prix</TH><TH>Date</TH></TR></thead><tbody>
<?php
$r_articles->data_seek(0);
$n_devis = '';
$n = 0;
$tt = 0.00;
while ($rq_articles=$r_articles->fetch_object()) {
    if ($n_devis != $rq_articles->tic_devis) {
        if ($n) {
            echo "<tr><th colspan='3'>Total devis n° ".$n_devis."<a href=\"/pdf/devis/devis_".(($an * 1000) +$n_devis).".pdf\"> <img src=\"/images/pdf.gif\"></a></th><th>".$stt." €</th></tr>";
        }
        $stt = 0.00;
        $n_devis = $rq_articles->tic_devis;
    }
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    if ($rq_articles->art_unite == 1) {
        $quantite = sprintf('%d', $rq_articles->tic_quantite);
    } else {
        $quantite = $rq_articles->tic_quantite;
    }
    
    echo "<tr style='background-color:".$coul.";'><td><img src='/images/button_drop.png' onclick='sup_c(".$rq_articles->tic_id.")'></td><td>".$rq_articles->Vt1_nom."</td><td>".$quantite."</td><td>".$rq_articles->tic_prix."&nbsp;€</td><td>".date("d/m/Y H:i:s", $rq_articles->tic_num)."</td></tr>";
    $n++;
    $stt += $rq_articles->tic_tt;
    $tt += $rq_articles->tic_tt;
    $tic_devis = $rq_articles->tic_devis;
}
if ($n_devis != 0) {
    echo "<tr><th colspan='3'>Total devis n° ".$n_devis."<a href=\"/pdf/devis/devis_".(($an * 1000) +$n_devis).".pdf\"> <img src=\"/images/pdf.gif\"></a></th><th>".$stt." €</th></tr>";
} elseif ($stt != $tt) {
    echo "<tr><th colspan='3'>Total hors devis</th><th>".$stt." €</th></tr>";
}
echo "</tbody><tfoot><tr><th colspan=3>Total en compte</th><th>".monetaireF($tt)." €</th><th></th></tr></tfoot>";
?>
</table>

<script>
$("#panneau_d").height($("#affichage").height()-10);
</script>