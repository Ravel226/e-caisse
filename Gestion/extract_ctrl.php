<?php
session_start();
define('ARCHIV_CSV_UPLD_FPN', 'archiveUpld.csv');

/**
 * Choisir un fichier à contrôler
 * #e-Caisse_CTRL#
 */
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}

$psAct=filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$psHash= filter_input(INPUT_GET, "h", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

switch ($psAct) {
    case 'Ctrl':
?>
        <style>
        label {
            width: 8em;
            display: block;
            float: left;
            text-align: right;
        }
        </style>
        <h3>Fichier à contrôler</h3>
        <form method="post" action="extract_upload.php"  enctype="multipart/form-data" target="_blank" onsubmit="target_popup(this)">
            <label>Fichier : </label><input type="file" name="fileToUpload" id="fileToUpload"/>
            <input type="hidden" name="h" value="<?=$psHash?>"/>
            <input type="submit" value="Contrôler"/>
            </p>
        </form>
        <script>
        function target_popup(form) {
            window.open('', 'formpopup', 'top=100,left=100,location=no,menubar=no,toolbar=no,width=400,height=400,resizeable,scrollbars');
            form.target = 'formpopup';
        }
        </script>
<?php
    break;
}
?>
