<?php
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$secteur= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$annee= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require_once $incpath."php/config.php";
if ($secteur!="") {
    include $incpath."chart/secteur.php";
    exit;
}
$comp = "";
$an = ($annee != "")?$annee:date('Y');
if ($dossier == "Saisie") {
    $comp = "WHERE Vart_utilisateur = ".$_SESSION[$dossier];
}
$req_secteurs = "SELECT COUNT(Vart_id) AS ct, 
                      ROUND(SUM(tic_prix * tic_quantite)) AS vl,
                      sec_nom,
                      sec_couleur,
                      sec_id
                        FROM Tickets_".$an."
                          JOIN Varticle_editeur ON Vart_id = tic_article
                          JOIN Rayons ON ray_id = Vart_rayon
                          JOIN Secteurs ON sec_id = ray_secteur
                          $comp
                            GROUP BY ray_secteur 
                            ORDER BY vl";
$r_secteurs = $idcom->query($req_secteurs);
$n = 0;
$couleur = "";
$secteur = "";
$vl = "";
$id_secteur="0,";
while ($rq_secteurs =$r_secteurs->fetch_object()) {
    if ($n > 0) {
        $couleur .= ",'#".$rq_secteurs->sec_couleur."'";
        $secteur .= ",'".$rq_secteurs->sec_nom."'";
        $vl .= ",".$rq_secteurs->vl;
        $id_secteur .=",".$rq_secteurs->sec_id;
    } else {
        $couleur .= "'#".$rq_secteurs->sec_couleur."'";
        $secteur .= "'".$rq_secteurs->sec_nom."'";
        $vl .= $rq_secteurs->vl;
        $id_secteur .= $rq_secteurs->sec_id;
    }
    $n++;
}
// echo $couleur;
// echo "<br>".$secteur;
// echo "<br>".$vl;exit;
// https://apexcharts.com/
// exit;
?>
<!-- <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Simple Pie</title> -->

    <link href="/chart/styles.css" rel="stylesheet" />

    <style>
      
        #chart {
      padding: 0;
      max-width: 500px; 
      margin: 35px auto;
    }
      
    </style>
    <script src="/js/vue.min.js"></script>
    <script src="/js/apexcharts.js"></script>
    <script src="/js/vue-apexcharts.js"></script>
    <script src="/js/jquery.js"></script>

    <script>
      
      // Replace Math.random() with a pseudo-random number generator to get reproducible results in e2e tests
      // Based on https://gist.github.com/blixt/f17b47c62508be59987b
      var _seed = 42;
      Math.random = function() {
        _seed = _seed * 16807 % 2147483647;
        return (_seed - 1) / 2147483646;
      };
    </script>
<?php
if ($an > $config['debut']) {
    $bt1="<button class='pt_bt' style='float:left;' onclick=\"charge('secteurs','&an=".($an-1)."','panneau_g')\"><&nbsp;".($an-1)."</button>";
} else {
    $bt1="<button class='pt_bt' style='float:left'></button>";
}

if ($an < date("Y")) {
    $bt2="<button class='pt_bt' style='float:right;' onclick=\"charge('secteurs','&an=".($an+1)."','panneau_g')\">&nbsp;".($an+1)."></button>";
} else {
    $bt2="<button class='pt_bt' style='float:right'></button>";
}

?>
<h3><?php echo $bt1?>Ventes de l'année <?php echo $an?> par secteurs<?php echo $bt2?></h3>

    <div id="app">
      <div id="chart">
      <apexchart type="pie" width="600" :options="chartOptions" :series="series"></apexchart>
    </div>
    </div>
    <script>
        $('#panneau_d').empty();
      function chart() {
      new Vue({
        el: '#app',
        components: {
          apexchart: VueApexCharts,
        },
        data: {

            // series: [44, 55, 13, 43, 22],
          series: [<?php echo $vl?>],
          // colors: ['#c5baba','#cccccc','#dcd7d7','#d4c3c3','#fffdfd'],
          // colors: [<?php echo $couleur?>],
          chartOptions: {
            chart: {
              height: '500',
              type: 'pie',
            },
            // labels: ['Team A', 'Team B', 'Team C', 'Team D', 'Team E'],
            labels: [<?php echo $secteur?>],
            responsive: [{
              breakpoint: 400,
              options: {
                chart: {
                    height: 'auto'
                },
                legend: {
                  position: 'bottom'
                }
              }
            }]
          },
          
          
        },
        
      })
    }

    setTimeout(function() {
        chart()
    }, 100);

$(document).ready(function(){
    
    setTimeout(function() {
        
    $('.apexcharts-legend-series').click(function() {
       const sec_id = new Array(<?php echo $id_secteur ?>);
        id = $(this).attr('rel');
        // alert(id);        
        charge('/<?php echo $dossier?>/secteurs',sec_id[id]+'&an=<?php echo $an?>','panneau_d');
    });
    }, 200);
});
    </script>
<script>$('#panneau_g').css('overflow','visible')</script> 
