<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
// print_r($_SESSION);
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_coul="SELECT uti_fondF, uti_fondC, uti_titre, uti_surligne, uti_pass, uti_id
                    FROM Utilisateurs
                      WHERE uti_id = ".$_SESSION[$req];
$r_coul=$idcom->query($req_coul);
$rq_coul=$r_coul->fetch_object();
?>
<link rel='stylesheet' href='/bgrins-spectrum/spectrum.css' />
<script>
$('.sp-container').remove();
</script>
<script src='/bgrins-spectrum/spectrum.js'></script>

<script>
$(".fond").spectrum({
change: function(color) {
    col=color.toHexString();
    or=$(this).attr('id');
    col=col.replace('#','');
    if(or == 'fondF') $('body').css('backgroundColor','#'+col);
    else if(or == 'fondC'){
    $('#menu').css('backgroundColor','#'+col);
    $('#affichage').css('backgroundColor','#'+col);
    $('#bandeau').css('backgroundColor','#'+col);
    }
    else if(or == 'titre') $('h3').css('backgroundColor','#'+col);
    // $('#actualiser').css('visibility','visible');
    modif(<?php echo $_SESSION[$req]?>,1,col,or,1);
    charge('/couleurs_utilisateur',<?php echo $_SESSION[$req]?>,'mysql');
    // id,tb,vl,cp,my
    }
});
function passd() {    
    if(($('#n1').val()=='')&&($('#n2').val() == '')) {
        $('#info').html('Veuillez entrer un mot de passe');
    } else if (($('#n1').val())!= ($('#n2').val())) {
        $('#info').html('Les mots de passe ne sont pas identiques');
        $('#n1').val('');
        $('#n2').val('');
    } else if (($('#n1').val())== ($('#n2').val())) {
        $('#info').html('');
        if ($('#ancien').css('visibility')=='hidden'){//il n'y avait aps de mot de passe
        modif ('<?php echo $rq_coul->uti_id?>',1,$('#n1').val(),'pass',1);

        } 
        else if ($('#ancien').css('visibility')=='visible'){
        // // alert('ici');
        // modif ('<?php echo $rq_coul->uti_id?>',1,$('#n1').val(),'pass',1);
        $('.sp-container').remove();
        charge('/change_pass','<?php echo $rq_coul->uti_id?>&anc='+$('#ancien').val()+'&pass='+$('#n1').val(),'affichage');
        }
    }
}
    
$(document).ready(function() {
    $('input').on('keyup',function(event){
        if (isNaN($(this).val())) {
            $('#info').html('<span style="font-size:20px;color:red">Uniquement des chiffres</span>');
            $(this).val('');
        }
      });
    $('input').on('click',function(){
        $(this).val('');
    });
});

</script>
<h3>Préférences utilisateurs</h3>
<center><table>
<tr><th colspan='4'><h3>Couleurs</h3></th></tr>
<TR><TH>Fond d'écran</TH><TH>Cadres</TH><TH>Titres</TH><TH>Surlignage</TH></TR>
<tr>
<td><input type="text" value="<?php echo $rq_coul->uti_fondF?>" class="fond" id="fondF"></td>
<td><input type="text" value="<?php echo $rq_coul->uti_fondC?>" class="fond" id="fondC"></td>
<td><input type="text" value="<?php echo $rq_coul->uti_titre?>" class="fond" id="titre"></td>
<td><input type="text" value="<?php echo $rq_coul->uti_surligne?>" class="fond" id="surligne"></td>
</tr>
<tr><TD colspan="4">&nbsp;</TD></tr>
<tr><TD colspan="4" align="center"><button style='font-size:18px;padding:5px 20px 5px 20px;' onclick="recharge()">Terminer</button></TD></tr>
<tr><th colspan='4'><h3>Mot de passe</h3></th></tr>
<?php
if ($req != "Ventes") {
    include "mdp.inc.php";
} else {
    ?>
<tr><Th colspan="4" align="center">Le mot de passe des vendeurs est défini par l'Administrateur</Th></tr>
    <?php
}
?>
</table></center>
<script>
$('.sp-picker-container').width(500);
</script>
