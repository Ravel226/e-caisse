<?php
session_start();
//      my : 0=insert,1=update,2=delete
//      tab=table
//      cp=champ
//      vl=valeur
//      $orq champ optionel uilisé pour les ventes
//      $an champ uilisé pour les factures
$id = filter_input(INPUT_GET, "id", FILTER_SANITIZE_STRING);
$my = filter_input(INPUT_GET, "my", FILTER_SANITIZE_STRING);
$tb = filter_input(INPUT_GET, "tb", FILTER_SANITIZE_STRING);
$cp = filter_input(INPUT_GET, "cp", FILTER_SANITIZE_STRING);
$vl = filter_input(INPUT_GET, "vl", FILTER_SANITIZE_STRING);
$orq = filter_input(INPUT_GET, "orq", FILTER_SANITIZE_STRING);//quantite d'origine
$an = filter_input(INPUT_GET, "an", FILTER_SANITIZE_STRING);//annee
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require_once $incpath."mysql/connect.php";
// require_once $incpath."php/fonctions.php";
if (!isset($an)) {
    $an = ANNEE;
}
$idcom=connexobjet();
if (!$idcom) {
    echo "pas de connexion";
}
/**Table de suppression des accant dans les champs de recherche */
function accent($nom)
{
    $nom=mb_strtolower($nom, 'UTF8');//exit;
    $patterns = array('/à/','/á/','/â/','/ã/','/ä/','/å/','/ą/','/č/','/ò/','/ó/','/ô/','/õ/','/ö/','/ø/','/ė/','/è/','/é/','/ê/','/ë/','/ę/','/ç/','/ì/','/í/','/î/','/ï/','/į/','/ū/','/ų/','/ù/','/ú/','/û/','/ü/','/ÿ/','/ñ/','/ž/','/š/','/æ/','/œ/',"/'/");
    $replacements = array('a','a','a','a','a','a','a','c','o','o','o','o','o','o','e','e','e','e','e','e','c','i','i','i','i','i','u','u','u','u','u','u','y','n','z','s','ae','oe','’');
    $nom=preg_replace($patterns, $replacements, $nom);
    return $nom;
};
// id,tb,vl,cp,my

switch ($tb) {
case 1: $table="Utilisateurs";$pre="uti_";
    break;
case 2: $table="Societe";$pre="soc_";
    break;
case 3: $table="Formesociale";$pre="fos_";
    break;
case 4: $table="Fonctions";$pre="fon_";
    break;
case 5: $table="Imprimantes";$pre="imp_";
    break;
case 6: $table="Secteurs";$pre="sec_";
    break;
case 7: $table="Rayons";$pre="ray_";
    break;
case 8: $table="Tva";$pre="tva_";
    break;
case 9: $table="Config";$pre="con_";
    break;
case 10: $table="Fonctions";$pre="fon_";
    break;
case 11: $table="Articles";$pre="art_";
    break;
case 12: $table="Titres";$pre="tit_";
    break;
case 13: $table="Editeurs";$pre="edi_";
    break;
case 14: $table="Editeur_serveur";$pre="eds_";
    break;
case 15: $table="Statut";$pre="sta_";
    break;
case 16: $table="Commandes_".$an;$pre="com_";
    break;
case 17: $table="Commentaire_com_".ANNEE;$pre="cmt_";
    break;
case 18: $table="Comptes";$pre="cpt_";
    break;
case 19: $table="Mode_reglement";$pre="mdr_";
    break;
case 20: $table="Tickets_".ANNEE;$pre="tic_";
    break;
case 21: $table="Etiquettes";$pre="eti_";
    break;
case 22: $table="Unites_vente";$pre="unv_";
    break;
case 23: $table="Resume_commande_".$an;$pre="rsc_";
    break;
case 24: $table="Inventaire";$pre="inv_";
    break;
case 25: $table="Journal_factures_".$an;$pre="jrn_";
    break;
case 26: $table="Affichage";$pre="aff_";//Boutons des Favoris dans l'interface de vente
    break;
case 27: $table="Codes";$pre="cod_";//codes analytiques
    break;
case 28: $table="Externe";$pre="ext_";//affichage déporté du prix de vente
    break;
case 29: $table="Favoris";$pre="fav_";//articles favoris pour l'interface de vente, liés à la table 26
    break;
case 30: $table="Promo";$pre="pro_";
    break;
case 31: $table="Detail_facture_".$an;$pre="def_";
    break;
case 32: $table="Pseudos";$pre="pse_";
    break;
case 33: $table="Soldes";$pre="sol_";
    break;
case 34: $table="Promo";$pre="pro_";
    break;
}
$_id="id";
$recherche = $pre."recherche";
if ($cp == "cb") {
    $ncb = $vl;
    // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
    $even_sum = $ncb[1] + $ncb[3] + $ncb[5] + $ncb[7] + $ncb[9] + $ncb[11];
    // 2. Multiply this result by 3.
    $even_sum_three = $even_sum * 3;
    // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
    $odd_sum = $ncb[0] + $ncb[2] + $ncb[4] + $ncb[6] + $ncb[8] + $ncb[10];
    // 4. Sum the results of steps 2 and 3.
    $total_sum = $even_sum_three + $odd_sum;
    // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
    $next_ten = (ceil($total_sum/10))*10;
    $check_digit = $next_ten - $total_sum;
    $Vcb = substr($ncb, 0, 12).$check_digit;
    if ($Vcb != $vl) {
        echo '<h3>Erreur de cb</h3>Valeur reçue : '.$vl.'<br>Valeur attendue :'.$Vcb;
        exit;
    } else {
        //on véifie si l'EAN13 n'est pas déjà utilisé
        $req_ean13 = 'SELECT art_id FROM Articles WHERE art_cb = '.$vl;
        $r_ean13 = $idcom->query($req_ean13);
        $rq_ean13 =$r_ean13->fetch_object();
        if ($r_ean13->num_rows != 0) {
            echo '<h3>Problème</h3>Le code barre '.$vl.' est déjà utilisé par l\'article n°'.$rq_ean13->art_id.'<br>';
        }
    }
}
// modif (id,tb,vl,cp,my)
$comp = '';
$date=date('U');
switch ($my) {
case 0:  switch ($tb) {
    case 12:  $req="INSERT INTO Titres(`tit_article`,`tit_nom`,`tit_niveau`) VALUES($id,'_',$vl)";
        break;
    /* case 16 :  $req="INSERT INTO $table(cmt_commande,$pre$cp) VALUES($id ,'$vl')";
        break; */
    case 17:  $req="INSERT INTO $table(cmt_commande,$pre$cp) VALUES($id ,'$vl')";
        break;
    default: echo $req="INSERT INTO $table($pre$cp) VALUES('$vl')";
}
    break;

case 1: switch ($tb) {
    case 1: if (($cp == "fondF")||($cp == "fondC")||($cp == "titre")||($cp == "surligne")) {
        $vl = "#".$vl;
    }
    if ($cp == "pass") {
        echo "1-".$vl=($vl == '')?null:md5($vl);
    }
        break;
    case 8: if (($id == 0)&&($cp == 'couleur')) {//couleur tva
        $id="'%'";
    }
        break;
    case 9: if ($cp == 'mode') {
        $config['mode']=$vl;
    }
        break;
    case 11: //changement de rayon. Si la tva est gérée dans les secteurs, on ajuste la tva au nouveau rayon
        if ($cp == 'rayon') {
            $req_tvaSecteur = 'SELECT sec_tva FROM Secteurs JOIN Rayons ON ray_secteur = sec_id WHERE ray_id = '.$vl;
            $r_tvaSecteur = $idcom->query($req_tvaSecteur);
            $rq_tvaSecteur =$r_tvaSecteur->fetch_object();
            $req_cor="UPDATE Articles SET art_tva = ".$rq_tvaSecteur->sec_tva." WHERE art_id = ".$id;
            $idcom->query($req_cor);
        }
        break;
    case 12: if ($cp == "nom") {
        $comp= ",$recherche='".accent($vl)."'";
    }
        break;
    case 19: if (($id == 1)&&($cp == 'couleur')) {//couleur mode reglement
        $id="'%'";
    }
        break;
    case 20: if ($cp == "prix") {//on vérifie quel est le prix minimum : pht+tva
        $req_prix="SELECT (art_pht*1+(tic_tva/100)) AS prix FROM Articles JOIN Tickets_".ANNEE." ON art_id=tic_article WHERE tic_id=$id";
        $r_prix=$idcom->query($req_prix);
        $rq_prix=$r_prix->fetch_object();
        if ($rq_prix->prix > $vl) {//on impose le prix minimum
            $vl=$rq_prix->prix;
        }
    }
    if ($cp == "quantite") {
        //gestion de la modification de quantité en fin de journée
        if ($orq < $vl) {
            $qt = "art_stk - ".($vl - $orq);
        } else {
            $qt = "art_stk + ".($orq - $vl);
        }
        $req = "UPDATE Articles JOIN Tickets_".ANNEE." ON art_id=tic_article SET art_stk = $qt WHERE tic_id=".$id;
        $idcom->query($req);
    };
    $comp = ", tic_tt = ROUND(tic_quantite * tic_prix,2)";
        break;
}

if (($cp == "pass")&&($vl == "")) {
    $req="UPDATE $table SET $pre$cp=NULL $comp WHERE $pre$_id LIKE '$id'";
} else {
    echo    $req="UPDATE $table SET $pre$cp='".addslashes($vl)."' $comp WHERE $pre$_id LIKE '$id'";
}
    break;

case 2: echo $req="DELETE FROM $table WHERE $pre$cp=$id";//supression de la ligne
    break;
}
$res=$idcom->query($req);

if (!$res) {
    ?>
    <script>$('#mysql').css('visibility','visible')</script>
    <?php
    echo $req."<br>";
    echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em>";
} else {
    ?>
    <script>
    $('.jaune').removeClass('jaune');
    <?php
    if($cp == 'remise') {
        echo "$('#vl_remise').html(".$vl.");";
    }
    ?>
    $('#mysql').css('visibility','hidden')</script>
    <?php
    echo "<br>".$req;
}
?>
