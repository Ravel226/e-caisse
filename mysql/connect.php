<?php
$tab=explode('/', $_SERVER['REQUEST_URI']);
$dossier=$tab[1];
define("ANNEE", date("Y"));
// 1R0ch3#W1squ35-
function connexobjet()
{
    global $incpath,$idcom;
    include_once $incpath."definitions.inc.php";
    @$idcom=new mysqli(SERVEUR, NOM, PASSE, BASE);
    if ($idcom->connect_error) {
        echo "Connexion impossible à la base de donnée";
        exit;
    }
    if (! $idcom->set_charset('utf8')) {
        echo 'Erreur au changement de charset pour utf8 !';
        exit();
    }
    return $idcom;
}
$mois=array("","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre");
$sem=array("dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi");
function monetaireF($vl)
{
    if ($vl != '') {
        return number_format($vl, 2, ',', ' ');
    } else {
        return '0.00';
    }
}
