<script>
$(document).ready(function() {
    $('table#tableDetailCompte input').on('keydown',function(event){
        ch = $(this).attr('alt');
        if(event.which == 13){
            // alert($(this).attr('type'));
            if($(this).attr('type') == 'text'){
                if ($(this).hasClass('jaune')) {
                    modif(<?php echo $resu->cpt_id?>,18,$(this).val(),ch,1);
                }
                if (ch == 'mail') {
                    setTimeout(function(){charge('comptes','<?php echo $resu->cpt_id?>','panneau_d')},500);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
    });
});
</script>
<?php
$tab = explode('/', $_SERVER['REQUEST_URI']);
?>
<table id="tableDetailCompte">
    <TR>
        <TH>Nom</TH><TD><input type="text" value="<?php echo $resu->cpt_nom?>" alt='nom'></TD>
    </TR>
    <TR>
        <TH>Adresse 1</TH><TD><input type="text" value="<?php echo $resu->cpt_adr1?>"  alt='adr1'></TD>
    </TR>
    <TR>
        <TH>Adresse 2</TH><TD><input type="text" value="<?php echo $resu->cpt_adr2?>"  alt='adr2'></TD>
    </TR>
    <TR>
        <TH>CP</TH><TD><input type="text" value="<?php echo $resu->cpt_cp?>"  alt='cp'></TD>
    </TR>
    <TR>
        <TH>Ville</TH><TD><input type="text" value="<?php echo $resu->cpt_ville?>"  alt='ville'></TD>
    </TR>
    <TR>
        <TH>Téléphone</TH><TD><input type="text" value="<?php echo $resu->cpt_tel?>"  alt='tel'></TD>
    </TR>
    <TR>
        <TH>Fax</TH><TD><input type="text" value="<?php echo $resu->cpt_fax?>"  alt='fax'></TD>
    </TR>
    <TR>
        <TH>Email</TH><TD><input type="text" value="<?php echo $resu->cpt_mail?>"  alt='mail'>
        <?php
        if (($resu->cpt_mail !="")&&($tab[1] != 'Ventes')) {
            ?>
            <a href="mailto:<?php echo $resu->cpt_mail?>" ><img width="40" src="/images/mail.png"></a>
            <?php
        }
        ?>
        </TD>
    </TR>
    <TR>
        <TH>Compte</TH><TD>
                <select onchange="modif(<?php echo $resu->cpt_id?>,18,this.value,'compte','1')" >
        <?php
        if ($resu->cpt_compte == 1) {
            ?>
            <option value="0">Particulier</option>
            <option value="1" selected>Professionel</option>
            <?php
        } else {
            ?>
            <option value="0" selected>Particulier</option>
            <option value="1">Professionel</option>
            <?php
        }
        ?>
        </select>
</TD>
    </TR>
    <TR>
        <TH>Remise</TH><TD><input type="text" value="<?php echo $resu->cpt_remise?>"  alt='remise'></TD>
    </TR>
</table>
