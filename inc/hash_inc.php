<?php

/**
 * Fonctions utilitaires pour gérer le hash envoyé et reçu
 * #e-Caisse_CTRL#
*/

define('CTRL_URL', 'https://ctrl.e-caisse.pro/API');

// Pour les archivages
define ('HASH_ALGO', 'sha512');
define ('ARCHIV_CSV_FILEPN', 'archive.csv');


/**
 * Lance une requête avec l'URL $pURL avec les paramètres dans le tableau associatif $pPost.
 * Retourne null si erreur, et un tableau avec les résultats de la requête si succès
 */
function mc_callUrlPost($psURL, $paPost)
{
	$ch = curl_init();
	if (! $ch ) log_message('error', " curl_init FAILED !");
	$aDefOpt = array(
		CURLOPT_HEADER => 0,
//		CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'],
		CURLOPT_FOLLOWLOCATION => 1,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_URL => $psURL,
//		CURLOPT_TIMEOUT_MS => 1000, // allows fake async : return after 1 second
//		CURLOPT_NOSIGNAL => 1,
	/*
		CURLOPT_HTTPHEADER => XXX,
	*/
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => $paPost
	);
	if (! curl_setopt_array($ch, $aDefOpt)) error_log("error: curl_setopt_array  FAILED !");
	$resCall = curl_exec($ch);
	if(curl_errno($ch) OR ! $resCall) {
		error_log("error: Call URL {$psURL} !");
		$resCall=null;
	}
	if ( empty($resCall)){
		error_log("error: curl exec EMPTY w/ {$psURL} !");
		$resCall=null;
	}
	curl_close($ch);
	return $resCall;
}

/**
 * Lance une requête GET avec l'URL $pURL.
 * Retourne null si erreur, et un tableau avec les résultats de la requête si succès
 */
function mc_callUrlGet($psURL)
{
	$ch = curl_init();
	if (! $ch ) log_message('error', " curl_init FAILED !");
	$aDefOpt = array(
		CURLOPT_HEADER => 0,
		CURLOPT_USERAGENT => $_SERVER['HTTP_USER_AGENT'],
		CURLOPT_FOLLOWLOCATION => 1,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_URL => $psURL,
		CURLOPT_TIMEOUT_MS => 1000, // allows fake async : return after 1 second
		CURLOPT_NOSIGNAL => 1,
	/*
		CURLOPT_HTTPHEADER => XXX,
	*/
	);
	if (! curl_setopt_array($ch, $aDefOpt)) error_log("error: curl_setopt_array  FAILED !");
	$resCall = curl_exec($ch);
	if(curl_errno($ch) OR ! $resCall) {
		error_log("error: Call URL {$psURL} !");
		$resCall=null;
	}
	if ( empty($resCall)){
		error_log("error: curl exec EMPTY w/ {$psURL} !");
		$resCall=null;
	}
	curl_close($ch);
	return $resCall;
}

/**
 * retourne Vrai si la date passée est valide, selon le format donnée (format SQL par défaut)
 */
function mc_validateDate($pDate, $pFormat = 'Y-m-d H:i:s') 
{
    $d = DateTime::createFromFormat($pFormat, $pDate);
    return $d && $d->format($pFormat) == $pDate;
}

/**
 * Génère un contrôle (i.e. hash) pour l'objet Ticket Résumé donné
 */
function mc_geneCtrl($pTck) 
{
	return hash('sha512', $pTck->rst_id.'|'.$pTck->rst_total.'|'.$pTck->rst_etatS.'|'.$pTck->rst_validation, false);
}

require_once($incpath."mysql/connect.php");
if (! isset($idcom) ) connexobjet();
$gsJeton = $idcom->query('SELECT con_token from Config WHERE con_id =0')->fetch_object()->con_token;
