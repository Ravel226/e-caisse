<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require_once $incpath."mysql/connect.php";
require_once $incpath."php/fonctions.php";
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
connexobjet();

$req_ticket="SELECT tic_quantite,
                    tic_prix, 
                    Vt1_nom, 
                    tic_num, 
                    mdr_nom, 
                    fac_id, 
                    tic_devis, 
                    cpt_id, 
                    cpt_nom,
                    art_unite,
                    tic_tt
                        FROM Tickets_".ANNEE."
                            JOIN Articles ON art_id = tic_article
                            JOIN Vtit1 ON Vt1_article = tic_article 
                            JOIN Resume_ticket_".ANNEE." ON rst_id = tic_num 
                            JOIN Mode_reglement ON mdr_id = rst_etat 
                            LEFT JOIN Factures_".ANNEE." ON fac_ticket = tic_num 
                            LEFT JOIN Devis_".ANNEE." ON dev_id = tic_devis 
                            LEFT JOIN Comptes ON cpt_id = dev_compte 
                                WHERE tic_num=$req";
$r_ticket=$idcom->query($req_ticket);
$nb = $r_ticket->num_rows;
if ($nb == 0) {
    echo "<h3>Il y a un problème sur ce ticket</h3>Il ne contient pas d'article";
    exit;
}
$rq_ticket=$r_ticket->fetch_object();
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
$ticket =$rq_ticket->tic_num;
/*
+--------+---------+-----------+----------+------------+
| rst_id | rst_num | rst_total | date     | mdr_nom    |
+--------+---------+-----------+----------+------------+
|    291 |       2 |      8.00 | 14:23:49 | Espèces   |
|    290 |       1 |     46.20 | 09:35:54 | Espèces   |
+--------+---------+-----------+----------+------------+
*/
?>
<div id="aff_compte"></div>
<table id='tableRevoir' alt="<?php echo $rq_ticket->tic_num?>"  class="pagination" number-per-page="5" current-page="0">
<thead>
<tr><th colspan="4" id='titre'>Ticket n° <?php echo $rq_ticket->tic_num?> réglé en <?php echo $rq_ticket->mdr_nom?></th></tr>
<tr><Th>Article</Th><Th>Prix U.</Th><Th>Qt</Th><th>Total</th></tr>
</thead>
<tbody>
<?php
$n=0;
$r_ticket->data_seek(0);
$quantite = '';
while ($rq_ticket = $r_ticket->fetch_object()) {
    $coul=($n%2 == 0)?$coulCC:$coulFF;
    $quantite=($rq_ticket->art_unite == 1)?sprintf('%d', $rq_ticket->tic_quantite):$rq_ticket->tic_quantite;
    
    echo "<tr style='background-color:".$coul."' ><td class='libelle'>".$rq_ticket->Vt1_nom."</td><td class='droite'>".$rq_ticket->tic_prix."&nbsp;€</td><td class='droite'>".$quantite." </td><td class='droite'>".($rq_ticket->tic_tt)."&nbsp;€</td></tr>";
    $n++;
    $n_facture = $rq_ticket->fac_id;
    $cpt_nom = $rq_ticket->cpt_nom;
    $tic_devis = $rq_ticket->tic_devis;
}
// exit;
?>
</tbody>
<tfoot>
<tr>
<TD colspan="4">
<?php
//on verifie si l'imprimante ticket est définie ou pdf, si définie : impression directe, si non affichage pdf
$req_imp = 'SELECT imp_nom FROM Imprimantes WHERE imp_id = 1';
$r_imp = $idcom->query($req_imp);
$rq_imp =$r_imp->fetch_object();
if ($rq_imp ->imp_nom == 'PDF') {
    ?>
    <a href="ticket_caisse.php?req=<?php echo $req?>"><button onclick='$("#facture").css("visibility","hidden")' class="boutref" style="width:45%">Imprimer</button></a>
    <?php
} else {
    ?>
    <button onclick="$('#facture').css('visibility','hidden');charge('pre_caisse',<?php echo $req?>,'mysql');"
 class="boutref" style="width:45%">Imprimer</button>
    <?php
}

if ($n_facture != 0) {
    ?>
    <a href="/pdf/factures/facture_<?php echo ((ANNEE * 1000)+$n_facture)?>.pdf"><button id="facture" class="boutpdf" style="width:45%;float:right">Facture</button></a>
    <?php
} else {
    ?>
    <button class="boutref" id="facture" style="width:45%;float:right" onclick="charge('comptes','3&tic=<?php echo $ticket?>','aff_compte');$('#tableRevoir').css('display','none')">Facture</button>
    <?php
}
?>
<button class="boutref" style="width:100%;" onclick="charge('panier','impression','panier')">Terminer</button>
</TD>
</tr>
</tfoot>
</table>
<script>
nb_ligne = Math.floor($("#affichage").height()/23)-12;
// alert(nb_ligne+' <?php echo $nb?>');
if (nb_ligne <= <?php echo ($nb -1)?>) {
$("#tableRevoir").attr('number-per-page',nb_ligne);//nb_ligne
$("#tableRevoir").pagination();
}
</script>
