<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
/**
Validation du panier :
création d'une ligne dans Resume_ticket_$an
| rst_id          | int(11)       | NO   | PRI | NULL    | auto_increment | Numéro sur l'année
| rst_num         | int(4)        | NO   |     | 0       |                | Numéro journalier du ticket
| rst_utilisateur | tinyint(3)    | NO   | MUL | 0       |                | Vendeur
| rst_etat        | varchar(1)    | NO   |     | 0       |                | Mode de reglement
| rst_etatS       | tinyint(1)    | YES  | MUL | NULL    |                | Sauvegarde du mode de reglement
| rst_total       | decimal(18,2) | NO   |     | NULL    |                | Total ticket
| rst_totalS      | decimal(18,2) | NO   |     | NULL    |                | Sauvegarde Total ticket
| rst_validation  | datetime      | YES  |     | NULL    |                | Heure d'enregistrement
| rst_mod         | enum('0','1') | YES  |     | 0       |                | Protection modification cb->caisse
| rst_dtm_log     | datetime      | YES  |     | NULL    |                | Heure d'envoi sur le serveur de contrôle

récupération de l'id et update de tickets_$an, cet id remplaçant le numero du panier
*
* #e-Caisse_CTRL#

*/
$du= filter_input(INPUT_GET, "du", FILTER_SANITIZE_FULL_SPECIAL_CHARS); //valeur du ticket
$vl= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);//valeur reglée
$mode= filter_input(INPUT_GET, "md", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

require_once $incpath."mysql/connect.php";
require_once $incpath."php/fonctions.php";
//les variables générales de l'application $config[] sont incluses dans le fichier php/fonctions.php
connexobjet();
// on vérifie si la valeur envoyé correspond au calcul sur le serveur. Le ticket à pu être modifié depuis un autre poste
//on verifie en même temps si le serveur est sous contrôle

/* echo "<br>1". */$req_valeur="SELECT SUM(tic_tt) AS vlT, (SELECT con_token FROM Config) AS ctrl FROM Tickets_".ANNEE." WHERE tic_num =".$_SESSION['panier_'.$_SESSION[$dossier]];
$r_valeur=$idcom->query($req_valeur);
$rq_valeur=$r_valeur->fetch_object();
if ($rq_valeur->vlT != $du) {
    ?>
    <script>
    charge('erreurs',1,'references');
    </script>
    <?php
    exit;
}
// print_r($_SESSION); exit;
/*
Note : le différé est traité dans un autre fichier, differe.php
- pour les reglements en espèce ou en différé, on ne tient compte que du montant dû recalculé sur le serveur
- pour les autres :
si le magasin est habilité à recevoir des dons :
  si la valeur donnée n'est pas égale à la valeur dûe,
      on ajoute le pseudo-article don/echange avec la valeur dans le ticket
s'il ne peut recevoir des dons
    on ajoute l'article le pseudo-article don/echange avec la valeur dans le ticket
    et on créer un nouveau ticket avec le pseudo-article don/echange en sortie de caisse avec la valeur et qt -1

recherche du dernier ticket enregistré pour incrémenter le numéro journalier

*/
/* echo "<br>2". */$req_numero="SELECT rst_id FROM Resume_ticket_".ANNEE." WHERE LEFT(rst_validation,10) = LEFT(NOW(),10)";
$r_numero = $idcom->query($req_numero);
$rq_numero = $r_numero->num_rows + 1;
//la valeur donnée n'est pas égale à la valeur dûe
if (($vl != $du)&&(($mode != 1)&&($mode != 5))) {
    $diff = $vl - $du;
    $du = $vl;
    if ($config['don']==1) {//accepte les dons sauf si c'est un règlement en caisse ou différé
        //echo 'ici';
        //exit;
        //insertion du pseudo-article $_SESSION['articledon']
        echo "<br>3".$req="INSERT INTO Tickets_".ANNEE." (tic_num, tic_article, tic_quantite, tic_quantiteS, tic_prix, tic_prixS, tic_pht, tic_tt, tic_tva, tic_ntva) VALUES(".$_SESSION['panier_'.$_SESSION[$dossier]].",".$config['articledon'].",1,1,$diff,$diff,$diff,$diff,0.00, 0)";
    
        $res=$idcom->query($req);
        if (!$res) {
            ?>
            <script>$('#mysql').css('visibility','visible')</script>
            <?php
            echo $idcom->errno." ".$idcom->error;
            echo "<br>".$req;
        }
    } else {
            // création d'un ticket de sortie de caisse et insertion du pseudoarticle
            echo $req_insert="INSERT INTO Resume_ticket_".ANNEE." (rst_utilisateur,rst_num, rst_etat, rst_etatS, rst_total, rst_totalS, rst_validation) VALUES($_SESSION[$dossier], $rq_numero, $mode, $mode, ($diff*-1), ($diff*-1), NOW())";
            $idcom->query($req_insert);
            $req="INSERT INTO Tickets_".ANNEE." (tic_num, tic_article, tic_quantite, tic_quantiteS, tic_prix, tic_prixS, tic_pht, tic_tt, tic_tva, tic_ntva) VALUES(".$_SESSION['panier_'.$_SESSION[$dossier]].",".$config['articledon'].",1,1,$diff,$diff,$diff,$diff,0.00, 0)";
            $idcom->query($req);
            $id_insert = $idcom->insert_id;
            $req="INSERT INTO Tickets_".ANNEE." (tic_num, tic_article, tic_quantite, tic_quantiteS, tic_prix, tic_prixS, tic_pht, tic_tt, tic_tva, tic_ntva) VALUES(".$id_insert.",".$_config['articledon'].",-1,-1,$diff,$diff,$diff,$diff,0.00, 0)";
            $idcom->query($req);
            echo " <br>erreur ".$idcom->errno." ".$idcom->error;
            $vl = $du;
    }
    
}
if ($mode == 1) {//caisse
    /* echo	"<br>4". */$req_insert="INSERT INTO Resume_ticket_".ANNEE." (rst_utilisateur,rst_num, rst_etat, rst_etatS, rst_total, rst_totalS, rst_validation) VALUES($_SESSION[$dossier], $rq_numero, $mode, $mode, $du, $du, NOW())";
    $idcom->query($req_insert);
    /*echo " <br>erreur ".*/$idcom->errno." ".$idcom->error;
    /*echo "<br>insert ".*/$id_insert = $idcom->insert_id;
} else {
    echo "<br>5".$req_insert="INSERT INTO Resume_ticket_".ANNEE." (rst_utilisateur,rst_num, rst_etat, rst_etatS, rst_total, rst_totalS, rst_validation) VALUES($_SESSION[$dossier],$rq_numero,$mode,$mode,$vl,$vl,NOW())";
    $idcom->query($req_insert);
    $idcom->errno." ".$idcom->error;
    $id_insert = $idcom->insert_id;
}
//on retire les articles du stock s'ils sont géré en stock'
/* echo	"<br>6".  */$req_stock="UPDATE Articles 
                                JOIN Tickets_".ANNEE." ON art_id = tic_article 
                                JOIN Pseudos ON pse_id = art_pseudo
                                    SET art_stk =  art_stk - tic_quantite WHERE tic_num = ".$_SESSION['panier_'.$_SESSION[$dossier]]." AND pse_stock = '1'"; 
$idcom->query($req_stock);
echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em>";
// exit;
//mise à jour du panier
/* echo	"<br>7". */$req_insert="UPDATE Tickets_".ANNEE." SET tic_num = ".$id_insert."
                                WHERE tic_num =".$_SESSION['panier_'.$_SESSION[$dossier]];
$idcom->query($req_insert);
if (strlen($rq_valeur->ctrl) == 30) {
    //************************envoi du code controle sur le serveur distant **********************************
    //~ require_once($incpath.'inc/hash_inc.php');
    //~ $theREs = mc_callUrlGet((isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME']. '/Ventes/envoi_hash.php');
    // exec('php ./envoi_hash.php  >> /dev/null &');
    //error_log("res CallURLget ='{$theREs}' \n", 3, 'debug_log.txt') ;

    //************************fin envoi du code controle sur le serveur distant**********************************
}

//creation d'une variable session pour l'impression du dernier ticket
$_SESSION['impression_'.$_SESSION[$dossier]]=$id_insert;
unset($_SESSION['panier_'.$_SESSION[$dossier]]);

// exit;
header("location:liste_paniers.php");
?>