<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require_once $incpath."mysql/connect.php";
require_once $incpath."php/fonctions.php";
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$det= filter_input(INPUT_GET, "det", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
connexobjet();
/*Liste des articles en attente dans ce compte
on vérifie d'abord les devis actifs pour ce compte(les devis refusés seront visibles en Gestion/Comptes/devis)
Si le devis a été mis dans le panier en cours, il ne doit plus être listé, son tic_num qui était timestamp(10) est passé à 9
*/
$req_devis = "SELECT SUM(tic_tt) AS tt,
                    tic_devis,
                    fac_id
                        FROM Devis_".ANNEE." 
                        JOIN Tickets_".ANNEE." ON dev_id = tic_devis  
                        LEFT JOIN Factures_".ANNEE." ON fac_ticket = tic_num 
                            WHERE tic_cp = $req 
                            AND LENGTH(tic_num)=10 
                                GROUP BY dev_id";
$r_devis=$idcom->query($req_devis);
if ($r_devis->num_rows > 0) {
    while ($rq_devis=$r_devis->fetch_object()) {
        echo '<h4><img onclick="charge(\'edit_devis\','.$rq_devis->tic_devis.',\'references\')" style="float:left;width:30px;margin-top:-8px" src="/images/edit.png">Devis N° '.((ANNEE * 1000) +$rq_devis->tic_devis).'<a href="/pdf/devis/devis_'.((ANNEE * 1000) +$rq_devis->tic_devis).'.pdf"><img style="float:right" src="/images/pdf.gif"></a></h4>';
    }
}

//-------------------------articles qui ne sont pas dans un devis
$req_ticket="SELECT tic_quantite,
                    tic_prix, 
                    Vt1_nom, 
                    tic_num, 
                    cpt_nom, 
                    cpt_id, 
                    tic_devis,
                    art_unite,
                    tic_id,
                    tic_tt
                        FROM Tickets_".ANNEE." 
                        JOIN Vtit1 ON Vt1_article = tic_article 
                        JOIN Comptes ON cpt_id = tic_cp 
                        JOIN Articles ON art_id = tic_article
                            WHERE tic_cp=$req AND LENGTH(tic_num)=10 
                            AND tic_devis = 0";
$r_ticket=$idcom->query($req_ticket);
$nb = $r_ticket->num_rows;
if ($nb > 0) {
    $rq_ticket=$r_ticket->fetch_object();
    $id_compte = $rq_ticket->cpt_id;
    $devis=$rq_ticket->tic_devis;
    if ($idcom->error) {
        echo "<br>".$idcom->errno." ".$idcom->error."<br>";
    }
    // exit;
    /*
    | tic_id | tic_num    | tic_article | tic_quantite | tic_quantiteS | tic_prix | tic_prixS | tic_pht | tic_tva | tic_ntva | tic_cp | tic_devis |
    +--------+------------+-------------+--------------+---------------+----------+-----------+---------+---------+----------+--------+-----------+
    |   6910 | 2147483647 |        3990 |            1 |             1 |     0.22 |      0.40 |    0.18 |   20.00 |        3 |      2 |         1 |
    |   6911 | 2147483647 |        3992 |            1 |             1 |     0.28 |      0.50 |    0.23 |   20.00 |        3 |      2 |         1 |
    |   6912 | 2147483647 |        3991 |            1 |             1 |     0.35 |      0.60 |    0.29 |   20.00 |        3 |      2 |         1 |
    |   6913 | 2147483647 |        4320 |            1 |             1 |     0.31 |      0.70 |    0.26 |   20.00 |        3 |      2 |         1 |
    */
    if ($nb > 0) {
        ?>
    <style>
    .pager{
    margin:0;
    }
    .page{
    padding: 0 10px 0 10px;
    margin : 0 5px 0 5px;
    }
    input[type=checkbox]{
    margin: 0 10px 0 0;
    }
    </style>
    <table id='tableRevoir'  class="pagination" number-per-page="5" current-page="0">
        <thead>
        <tr><th colspan="4"><?php echo $rq_ticket->cpt_nom?></th></tr>
            <tr><Th>Article</Th><Th>Prix U.</Th><Th>Qt</Th><th>Total</th></tr>
        </thead>
    <tbody>
    <?php
    $n=0;
        $total = 0;
        $r_ticket->data_seek(0);
        while ($rq_ticket=$r_ticket->fetch_object()) {
            $coul = ($n % 2 == 0)? $coulCC:$coulFF;
            $quantite = ($rq_ticket->art_unite == 1)?sprintf('%d', $rq_ticket->tic_quantite):$rq_ticket->tic_quantite;
            // print_r($rq_ticket);
            echo "<tr style='background-color:".$coul."' ><td><div class='libell'><input id='".$rq_ticket->tic_id."' alt='".($rq_ticket->tic_tt)."' type='checkbox' checked>".substr($rq_ticket->Vt1_nom, 0, 39)."</div></td><td class='droite'>".$rq_ticket->tic_prix."&nbsp;€</td><td class='droite'>".$quantite." </td><td class='droite'>".monetaireF($rq_ticket->tic_tt)."&nbsp;€</td></tr>\n";
            $n++;
            $total += $rq_ticket->tic_tt;
        } ?><span style='visibility:hidden' id='totalOR'><?php echo monetaireF($total)?></span>
    </tbody>
    <tfoot>
    <tr style="background-color:white"><td><input type='checkbox' id='bascule' checked></td><TH colspan="3">Total <span id='total'><?php echo monetaireF($total)?></span>&nbsp;€</TH></tr>
    </tfoot>
    </table>
        <tr>
            <TD colspan="4">
                <button id='panier_<?php echo $req?>' class="boutref" style="width:45%;" onclick="envoyer(1)">Panier</button>
                <button id='devis' class="boutref" style="width:45%;float:right;" onclick="envoyer(2)">Devis</button>
            </TD>
        </tr>
    <?php
    }
} else {
    $id_compte = 0;
}
?>
<script>
var partiel = 0;
$(document).ready(function(){
    var tt = 0.00;
    $("#tableRevoir input[type=checkbox]").on("click",(function(){
        var id = $(this).attr('id');
        if (id == 'bascule') { //
           if ($(this).prop('checked') == false) {
                $("#tableRevoir input[type=checkbox]").prop('checked',false);
//                 $("#tableRevoir input[type=checkbox]").removeAttr('checked');
                $('.boutref').css('visibility','hidden');
                $('#total').html('0.00');
                tt = 0.00;
                partiel = 1;
                
           } else {
                $("#tableRevoir input[type=checkbox]").prop('checked',true);
                $('.boutref').css('visibility','visible'); 
                $('#total').html($('#totalOR').html());
                tt = +$('#totalOR').html();
                partiel = 0;    
           } 
        } else {
            if ($(this).prop('checked') == true){
                $('.boutref').css('visibility','visible');
            } else {
                partiel = 1;
            }
            calcul();
        }
    }));
    
    function calcul() {
            tt = 0.00;
        $("#tableRevoir input[type=checkbox]").each(function() {
            id = $(this).attr('id');
            if ($.isNumeric(id)) {
                if ($(this).prop('checked') == true) {
                    tt += +$('#'+id).attr('alt');
                }
            }            
        })
    var TT = tt.toFixed(2); 
    $('#total').html(TT);
        if (tt == 0.00) {
            $('.boutref').css('visibility','hidden');
        } else {
            $('.boutref').css('visibility','visible');
        }
    }
});
function envoyer(mode){
    if (partiel == 0) {
        if (mode == 1) {
        charge('update','&cp=<?php echo $id_compte?>','panier');
        } else {
        charge('devis','&cp=<?php echo $req?>','panier');
        }
    } else {
            var tab_art='';
            $("#tableRevoir input[type=checkbox]").each(function() {
            if ($(this).prop('checked') == true) {
    //         alert($(this).attr('id'));
                id = $(this).attr('id');
                if ($.isNumeric(id)){
                tab_art +=id+'-';
                }            
            }
        });
    }
    if (mode == 1) {//panier
        charge('update','&lst='+tab_art,'panier');        
    } else {
        charge('devis','&cp=<?php echo $id_compte?>&lst='+tab_art,'panier');
        setTimeout(charge('panier','','panier'),100);
    }
}
if ($('#b_compte').attr('alt')=='panier') $('#panier_<?php echo $req?>').css('visibility','hidden');
nb_ligne = Math.floor(($("#references").height()-72)/18)-12;
if (nb_ligne <= <?php echo($nb -1)?>){
$("#tableRevoir").attr('number-per-page',nb_ligne);//nb_ligne
$("#tableRevoir").pagination();
}
</script>
