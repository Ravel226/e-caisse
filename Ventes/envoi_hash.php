<?php
/**
 * 
Principe
On recherche le premier ticket non validé, (il y a normalement le dernier ticket enregistré qui appelle cette page)
s'il n'y en a pas, on ne fait rien, si non on envoie les données du premier ticket rencontré
au retour on indique dans la table Resume_ticket_".ANNEE. que le contrôle est reçu puis
on relance ce fichier tant qu'on trouve des tickets non contrôlés.
* 
* NB: la variable globale $idcom est utilisée.
* #e-Caisse_CTRL#
*/
//if(!isset($incpath)){$p=preg_split("[/]",$_SERVER['PHP_SELF']);$incpath="";for($i=1;$i<sizeof($p)-1;$i++){$incpath='../'.$incpath;}unset($p,$i);}
// appel du script en direct, définition d'incpath en "dur"
$incpath = '../';

require_once($incpath.'inc/hash_inc.php');

//echo '<html>'; flush(); ob_flush(); // Utile si appel par curl 

if (! empty($gsJeton)) {
	$oResult=$idcom->query('SELECT rst_id, rst_total, rst_etatS, rst_validation FROM Resume_ticket_'.ANNEE.' WHERE rst_dtm_log IS NULL ORDER BY rst_id');
	while ($oTicket=$oResult->fetch_object()) {
		$controle = mc_geneCtrl($oTicket);
		$aPost=['j'=>$gsJeton, 'h'=>$controle, 'i'=>$oTicket->rst_id, 'd'=>$oTicket->rst_validation];
		$sRes = mc_callUrlPost(CTRL_URL.'/add', $aPost);
//	error_log("sRes '{$sRes}' pour id ={$oTicket->rst_id} dtm_valid={$oTicket->rst_validation}\n", 3, 'debug_log.txt') ;
		if(! is_null($sRes)) {
			$oRes = json_decode($sRes);
			if (json_last_error()== JSON_ERROR_NONE AND $oRes->dtm_log != '#ERROR#') {
				if (mc_validateDate($oRes->dtm_log)) {
					$sSql="UPDATE Resume_ticket_".ANNEE." SET rst_dtm_log = '{$oRes->dtm_log}' WHERE rst_id = ".$oTicket->rst_id;
					$idcom->query($sSql);
				}
			}
		}
	}
}

//echo 'OK</html>'; flush(); ob_flush(); // Utile si appel par curl 

?>
