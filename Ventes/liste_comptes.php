<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$cp= filter_input(INPUT_GET, "cp", FILTER_SANITIZE_FULL_SPECIAL_CHARS); //sortie de compte
$mode= filter_input(INPUT_GET, "mode", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tic=filter_input(INPUT_GET, "tic", FILTER_SANITIZE_FULL_SPECIAL_CHARS);//N) de ticket pour création facture

require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

/*
Cette page liste les comptes pour
---1 mise en compte, avec calcul de remise s'il y a
---2 paiement différé, avec calcul de remise s'il y a
---3 creation de facture sans calcul de remise car la ticket à été validé
---4 sortie de compte

*/
//Creation d'une table temporaire pour les articles en comptes
$art_compte="CREATE TEMPORARY TABLE Articles_comptes AS SELECT cpt_id AS Vac_id, cpt_nom AS Vac_nom FROM Comptes LEFT JOIN Tickets_".ANNEE." ON tic_cp = cpt_id WHERE LENGTH(tic_num)=10 GROUP BY cpt_id";
$idcom->query($art_compte);
?>
<script type="text/javascript" src="/js/simplepagination.js"></script>
<?php
if ($cp != 'cp') {
    $comp = "LEFT";
}
switch ($mode) {
case 4: $comp ='';
    break;
}
 $req_comptes="SELECT Comptes.*, Vac_id FROM Comptes $comp JOIN Articles_comptes ON Vac_id = cpt_id WHERE cpt_nom LIKE '".$req."%'  ORDER BY cpt_nom";
// //exit;
$r_comptes=$idcom->query($req_comptes);
$nb = $r_comptes->num_rows;
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
$n = 0;
$mettreencp = '';
    echo '<table id="tableCompte" class="pagination" number-per-page="6" current-page="0"><thead><tr><th>Rem.</th><th>Nom</th><th>Ville</th><th>Adresse</th><th>&nbsp;&nbsp;&nbsp;</th><th></th></thead><tbody>';
    while ($rq_comptes=$r_comptes->fetch_object()) {
        $coul=($n % 2 == 0)?$coulCC:$coulFF;
        if ($rq_comptes->Vac_id > 0) {//voir les articles déjà en compte
            $voir="class='voirC' onclick=\"charge('detail_articlescompte',".$rq_comptes->cpt_id.",'references')\"";
        } else {
            $voir='';
        }
        if ($rq_comptes->cpt_remise == 0) {//il n'y a pas de remise, on peut mettre en compte directement sinon il faut passer par l'édition pour calculer la remise
//       $mettreencp =  'class="valid" onclick="insert_compte('.$rq_comptes->cpt_id.')"';
            $remise = '';
        } else {
            $mettreencp = '';
            $remise = '<span style="font-size:15px">'.$rq_comptes->cpt_remise.'&nbsp%</span>';
        }
        //on affiche qu'une fois le compte et pas si tic_num == 9, panier ?>
            <tr style='background-color:<?php echo $coul?>'><td <?php echo $mettreencp?>><?php echo $remise?></td><TD><?php echo $rq_comptes->cpt_nom?></TD><TD><?php echo $rq_comptes->cpt_ville?></TD><TD><?php echo $rq_comptes->cpt_adr1?></TD><td <?php echo $voir?>></TD></td>
            <?php
            if ($mode != 4) {
                ?>
            <td class="edit" onclick="charge('detail_compte','cp&det=<?php echo $rq_comptes->cpt_id?>','panier')"></td>
            <?php
            } else {
                echo "<td></td>";
            } ?></tr>
            <?php
            $n++;
    }
    echo '</tbody></table>';
?>

<script>
if($('#b_compte').attr('alt') == 'comptes'){
 $('.valid').css('visibility','hidden');
 }
function insert_compte(id){
charge('update','&pan='+id,'panier');
}
nb_ligne = Math.floor($("#affichage").height()/70);

if(nb_ligne == <?php echo($nb)?>){
// alert(nb_ligne)
    hauteur_td = ($("#affichage").height()-40)/(nb_ligne);
    $("#tableCompte td").height(hauteur_td);
    }
//on tient compte de la place des boutons de navigation
if(nb_ligne <= <?php echo($nb -1)?>){
// alert(nb_ligne)
$("#tableCompte").attr('number-per-page',nb_ligne -1);
hauteur_td = ($("#affichage").height()-40)/(nb_ligne);
$("#tableCompte td").height(hauteur_td);
$("#tableCompte").pagination();
$('.pager').css('margin-top',20);
}</script>