<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$ticket= filter_input(INPUT_GET, "tic", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$det= filter_input(INPUT_GET, "det", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if (isset($det)) {
    $detail = ' AND cpt_nom LIKE '.$det;
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
if ($ticket) $_SESSION['impression_'.$_SESSION[$dossier]] = $ticket;
connexobjet();

/*
on liste la première lettre des nom des comptes.
*/
$remise_perso = '';
$nouveau = '';
switch($req) {
case 1 ://mise en compte
    $remise_perso = '<button class="boutref" style="width:350px" onclick="charge(\'detail_compte\',\'cp&det=1\',\'panier\')">Remise personnalisée</button>'; 
    echo $req_comptes="SELECT cpt_id, cpt_nom, COUNT(cpt_nom) AS ct, cpt_ville, LEFT(cpt_nom ,1) AS ref FROM Comptes  WHERE cpt_nom !='' GROUP BY ref ORDER BY ref";
    $action='Mise en comptes';//exit;
    $mode = 1;
    $nouveau = '<button class="boutref" onclick="charge(\'detail_compte\',\'000&det=1\',\'panier\')">Nouv.</button>';
    break;  
case 2 ://sortie de comptes
    /*echo*/ $req_comptes="SELECT cpt_id, cpt_nom, cpt_ville, LEFT(cpt_nom ,1) AS ref FROM Comptes LEFT JOIN Tickets_".ANNEE." ON tic_cp=cpt_id WHERE cpt_nom !='' AND tic_cp IS NOT NULL AND LENGTH(tic_num)>9 GROUP BY ref ORDER BY ref";
    $comp="&cp=cp";
    $action='Sortie des comptes';
    $mode = 4;
    break; 
case 3 : //creation facture
    $remise_perso = '<button class="boutref" style="width:350px" onclick="$(\'#tableRevoir\').css(\'display\',\'block\');$(this).css(\'display\',\'none\');$(\'#les_comptes\').css(\'display\',\'none\')">Annuler</button>';
    $req_comptes="SELECT cpt_id, cpt_nom, COUNT(cpt_nom) AS ct, cpt_ville, LEFT(cpt_nom ,1) AS ref FROM Comptes  WHERE cpt_nom !='' GROUP BY ref ORDER BY ref";
    $action='Creation facture pour le ticket <span id="n_ticket">'.$_SESSION['impression_'.$_SESSION[$dossier]].'</span>';
    $mode = 3;
    $nouveau = '<button class="boutref" onclick="charge(\'detail_compte\',\'000\',\'panier\')">Nouv.</button>';
    //on verifie d'abord s'il y a une facture déjà crée pour ce ticket ce qui ne devrait jamais arriver
    $req_facture = "SELECT * FROM Factures_".ANNEE." WHERE fac_ticket = $ticket";
    $r_facture=$idcom->query($req_facture);
    if ($r_facture->num_rows != 0) {
        $rq_facture=$r_facture->fetch_object();
        echo "<h1>La facture existe déjà <a href='/pdf/factures/facture_".$rq_facture->fac_id.".pdf'><button class='boutpdf' style='width:45%'>Facture</button></a></h1>";
        unset($_SESSION['impression_'.$_SESSION[$dossier]]);
        ?>
        <script>$("#facture").css("visibility","hidden");
        </script>
        <?php
        exit;
    }
    //on vérifie si les articles sont lié à un compte, si oui, on propose la facture pour ce compte et le choix d'au autre compte.
    /*echo*/$req_articles = "SELECT tic_cp, cpt_id, cpt_nom FROM Tickets_".ANNEE." JOIN Comptes ON cpt_id = tic_cp WHERE tic_cp > 0 AND tic_num = ".$_SESSION['impression_'.$_SESSION[$dossier]];
    $r_articles=$idcom->query($req_articles);
    if ($r_articles->num_rows > 0) {
        $rq_articles = $r_articles->fetch_object();
        //echo $rq_articles->cpt_nom;
        ?>
        <script>$("#facture").css("visibility","hidden")</script>
        <button id='imp' class='boutref' style='width:45%' onclick="charge('creation_facture','<?php echo $rq_articles->cpt_id.'&tic='.$_SESSION['impression_'.$_SESSION[$dossier]]?>','panier');$('#imp').css('visibility','hidden')">Facture <?php echo $rq_articles->cpt_nom;?></button><button class='boutref' onclick="charge('comptes',2,'references')" style='width:45%;float:right' >Autres </button>
        <?php
        unset($_SESSION['impression_'.$_SESSION[$dossier]]);
        exit;
    }
                
}
//exit;
$r_comptes=$idcom->query($req_comptes);
if ($idcom->error) echo "<br>".$idcom->errno." ".$idcom->error."<br>";
?>
<script>
// alert(<?php echo $mode?>);
$("#valider_compte").click(function(){
    charge('panier','','panier');
    });
</script>

<div id="les_comptes">
<?php echo $remise_perso?>

<h3><?php echo $action."</h3>".$nouveau?>
<?php 

while ($rq_comptes=$r_comptes->fetch_object()) {
    if ($rq_comptes->ct > 63) {
        ?>
        <script>alert('ici');</script>
        <?php
        
       
        echo '<button class="boutref" onclick="charge(\'comptes\',\''.$rq_comptes->ref.'&mode='.$mode.'&det='.$det,\'panier\')">'.$rq_comptes->ref.'</button>';
    } else {
        echo '<button class="boutref" onclick="charge(\'liste_comptes\',\''.$rq_comptes->ref.'&mode='.$mode.'\',\'panier\')">'.$rq_comptes->ref.'</button>';
    }
}
echo "</div>";
if ($req != 2 ) {
?>
<script>
$('#les_comptes .boutref').height(($('#les_comptes').height()/14));
</script>
<?php
}
?>