<?php
echo $req_verif="SELECT Tickets_".ANNEE.".*,
                    art_unite,
                    art_pseudo
                    FROM Tickets_".ANNEE." 
                    JOIN Articles ON art_id = tic_article
                        WHERE tic_article =".$art_id."
                            AND art_pseudo != 4
                            AND tic_num = ".$_SESSION['panier_'.$_SESSION[$dossier]];

$r_verif=$idcom->query($req_verif);
$rq_verif = $r_verif->fetch_object();
/*
art_unite : 
0 -> nouveaute, ne devrait pas servir dans cette page
1 -> article normal
2 -> article au poids
3 -> article au litre
les quantités ne sont pas ajouté sur les pseudos génériques(4), une nouvelle ligne est crée
*/
if ($r_verif->num_rows == 0) {
    //recherche de l'unite de vente
    $req_unite = "SELECT art_unite FROM Articles WHERE art_id = ".$art_id;
    $r_unite=$idcom->query($req_unite);
    $rq_unite = $r_unite->fetch_object();
    if ($rq_unite->art_unite == 1) {     
        if (isset($quantite)) $quantite = $quantite;
        else $quantite = 1;
        $req_insert = "INSERT INTO Tickets_".ANNEE." (tic_num, tic_article, tic_quantite, tic_quantiteS, tic_prix, tic_prixS, tic_pht, tic_tt, tic_tva, tic_ntva)
                                                    SELECT ".$_SESSION['panier_'.$_SESSION[$dossier]].",
                                                    art_id,
                                                    $quantite,
                                                    $quantite,
                                                    CASE
                                                        WHEN pro_valeur IS NOT NULL THEN pro_valeur
                                                        ELSE art_ttc
                                                    END AS art_ttc,
                                                    art_ttc,
                                                    art_pht,
                                                    CASE
                                                        WHEN pro_valeur IS NOT NULL THEN ROUND(pro_valeur * $quantite,2)
                                                        ELSE ROUND(art_ttc * $quantite,2)
                                                    END AS art_ttc,                                                    
                                                    tva_nom,
                                                    art_tva
                                                        FROM Articles
                                                        JOIN Tva ON tva_id = art_tva
                                                        LEFT JOIN Promo ON pro_article = art_id
                                                            WHERE art_id =".$art_id;
    } elseif ($rq_unite->art_unite > 1 ) {//article au poids, on ajoute 
        if (isset($quantite)) $quantite = $quantite;
        else $quantite = 0.00;
        $req_insert = "INSERT INTO Tickets_".ANNEE." (tic_num, tic_article, tic_quantite, tic_quantiteS, tic_prix, tic_prixS, tic_pht, tic_tt, tic_tva, tic_ntva) SELECT ".$_SESSION['panier_'.$_SESSION[$dossier]].", art_id, $quantite, $quantite, art_ttc, art_ttc, art_pht, ROUND(art_ttc * $quantite,2), tva_nom, art_tva FROM Articles JOIN Tva ON tva_id = art_tva WHERE art_id =".$art_id;//exit;
    }
} else {
    if ($rq_verif->art_unite > 1 ) {//article au poids, on ajoute 
        $quantite = "NULL";
        $req_insert = "INSERT INTO Tickets_".ANNEE." (tic_num, tic_article, tic_quantite, tic_quantiteS, tic_prix, tic_prixS, tic_pht, tic_tt, tic_tva, tic_ntva) SELECT ".$_SESSION['panier_'.$_SESSION[$dossier]].", art_id, $quantite, $quantite, art_ttc, art_ttc, art_pht, ROUND(art_ttc * $quantite,2), tva_nom, art_tva FROM Articles JOIN Tva ON tva_id = art_tva WHERE art_id =".$art_id;
    } else {
        $quantite = 1;
        $req_insert ="UPDATE Tickets_".ANNEE." SET tic_quantite = tic_quantite + $quantite,tic_quantiteS = tic_quantite, tic_tt = ROUND(tic_prix * tic_quantite,2)  WHERE tic_num = ".$_SESSION['panier_'.$_SESSION[$dossier]]." AND tic_article = ".$art_id;
    }
}
// echo $req_insert;
// exit;
