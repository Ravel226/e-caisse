<?php
$afficheur = isset($_SESSION['afficheur']) ? $_SESSION['afficheur'] : "";
$auto = isset($_SESSION['afficheur_auto']) ? $_SESSION['afficheur_auto'] : "";
/* Principe :
L'ip de l'afficheur externe doit-être défini en Administration. Il est normalement associé à l'ip du poste de vente.
On peut cependant l'utiliser d'un autre poste
Une variable $_SESSION['afficheur'] indique que l'afficheur est actif pour ce poste.
La variable $_SESSION['afficheur_auto'] indique si l'afficheur est activé à l'ouverture
    Le bouton de l'afficheur est une bascule : jaune, l'afficheur reçoit les données, blanc : les données ne sont pas envoyées
Si cette variable existe, la div $('#prix').on("DOMSubtreeModified",function() est surveillé en permanence
et en cas de modification, charge le fichier actualiser_externe.php?prix=prix.
Ce fichier avec fopen passe les paramètres au fichier distant actualiser.php qui écrit les paramètres dans
le fichier donnees.php
sur l'afficheur, un script python lit toutes les 200ms ce fichier et affiche la valeur si elle a changé.
*/


if ($afficheur!='') {
echo '<div style="float :right"><button id="ecran" class="blanc"></button>
    </div>';
    ?>
    <script>
        
    $(document).ready(function() {
        <?php
        if($auto) {
            ?>
            $('#ecran').addClass('jaune');
            $('#ecran').removeClass("blanc");
            <?php    
        } else {
            ?>
            $('#ecran').addClass('blanc');
            $('#ecran').removeClass("jaune");
            <?php
        }
        ?>
    $('#prix').on("DOMSubtreeModified",function(){
        actif();
    });
});
function actif() {
        if ($('#ecran').hasClass('jaune') == true) {
    charge('actualiser_externe',$('#prix').html(),'myqsl');
    }
}
$(document).ready(function() {
    $('#ecran').click(function() {
        if($( this ).hasClass("jaune") == false) {
            $( this ).removeClass("blanc");
            $( this ).addClass("jaune");
        } else {
            $( this ).removeClass("jaune");
            $( this ).addClass("blanc");
        }
    });
});
</script>    
    <?php
}
?>