<?php
if ($det) {
    if (!isset($req1)) {
        $req1 = $req;
    }
    $req=$det;
}
$req_comptes="SELECT * FROM Comptes WHERE cpt_id=$req";
$r_comptes=$idcom->query($req_comptes);
$resu=$r_comptes->fetch_object();
if (isset($req1)) {
    $req = $req1;
}
?>
<script>ntic = $("#n_ticket").html();
$('#input_remise').keyup(function(){
// alert($(this).val());
    if($(this).val() != 0.00){
        $('#bt_remise').css('visibility','visible');
        $('#valider_compte').css('visibility','hidden');
    } else {
    $('#bt_remise').css('visibility','hidden');
    $('#valider_compte').css('visibility','visible');
    }
})
</script>
<?php
if ($det == 1) {//remise personnalisée, affichage simplifié
    ?>
    <h3><span id='mode_'></span>Calcul de la remise sur le panier</h3>
    <table id="tableDetailCompte">
    <tr><TH colspan="3">Remise pour particulier : prix minimum imposé et 5 % sur les livres</TH></tr>
    <TR>
    <TH>Remise</TH><TD><input id='input_remise' type="text" class="demi" value="<?php echo $resu->cpt_remise?>" onchange="modif(1,18,this.value,'remise','1')">  
    </TD>
    <TD id="remise">
        <button onclick="charge('calcul_remise','1','remise');" class="boutref" style="width:300px">Calcul du total avec la remise de <span id='vl_remise'><?php echo $resu->cpt_remise?></span> %</button>
    </TD>
    </TR>
    </table>
    <?php
// 	exit;
} else {
    ?>
    <h3><span id='mode_'></span><?php echo $resu->cpt_nom." N° ".$resu->cpt_id?></h3>
    <?php
    //--------le détail du compte est commun avec le module Gestion
    include $incpath."inc/detail_compte.inc.php"; ?>
    <div id="remise" style="float:left;width:250px 0 0 50px;vertical-align:middle">
    <?php
    if (($resu->cpt_remise == 0.00)||(isset($_SESSION['impression_'.$_SESSION[$dossier]]))) {//ou impression d'un ticket validé
        $visible = 'hidden';
        $valider = 'visible';
    } else {
        //le prix remise est recalculé sur le serveur
        $visible = 'visible';
        $valider = 'hidden';
    } ?>
    <img onclick='$("#references").empty();validation_cp(<?php echo $resu->cpt_id?>)' id='valider_compte' style="visibility:<?php echo $valider?>" src="/images/valider.png">
    <button id='bt_remise' onclick="charge('calcul_remise','<?php echo $resu->cpt_id?>','remise');" class="boutref" style="width:200px;visibility:<?php echo $visible?>">Calcul du total avec la remise de <?php echo $resu->cpt_remise?> %</button>
    <br><button onclick="charge('calcul_remise','1','remise');" class="boutref" style="width:300px">Validation sans recalcul </button>
    </div>    
    <?php
}
?>
<script>
<?php
/*
Deux cas : on arrive de reglement ou de compte
si on arrive de compte, deux cas : 
 panier vide(sortie du compte)(1), 
 panier utilisé(mise en compte)(2)
         panier utilisé                      panier vide
             mdr                                    cp
              |                                      |
              |                                      |
          ____|____                              ____|____
        |          |                            |         |
        |          |                            |         |
(3)differe,mis en compte(1)                (2)sortie  facture(4)

*/
$nticket = "";
if (isset($_SESSION['impression_'.$_SESSION[$dossier]])) {
    $nticket = $_SESSION['impression_'.$_SESSION[$dossier]];
}
if ($req == 'cp') {//on a cliqué sur le bouton compte
    /* s'il y a un ou plusieurs articles dans le panier, alt D5 (paiement différé) = panier
    s'il est vide atl D5 = compte, cela évite d'avoir à réinterroger la base de donnée
    cas (1) :*/
    ?>
    if ($('#b_compte').attr('alt')=='panier'){
        // 	alert(1);
        $('#mode_').html('Détail pour la mise en compte de ');
        function validation_cp(id){
        charge('update','&pan=<?php echo $resu->cpt_id?>','panier');
    }
}
<?php
//  cas (2) :sortie du compte
?>
else if($('#b_compte').attr('alt')=='compte') {
// 	alert(2);
        $('#mode_').html('Détail du compte de ');
        $('#valider_compte').css('visibility','hidden');
        }
    <?php
} else { //il y a des articles dans le panier
// 	cas 3, différé on ne passe pas automatiquement au ticket suivant, le bouton terminer est pour pallier à ce problème
    ?>
    if($('#b_compte').attr('alt')=='panier'){
// 	alert(3);
        $('#mode_').html('Règlement du panier en différé pour ');
        function validation_cp(id){
        //validation du ticket, création de la facture et insertion du numéro, redirection verun nouveau ticket
        charge('validation_differe',id+"&du="+$('#prix').html(),'mysql');
        $('#panier').html("<center><button class='Bcartes' onclick=\"charge(\'liste_paniers\',\'\',\'liste_panier\')\"'>Terminer</button><center>");
        $('#references').empty();
        }
    }
    <?php
    //  cas (4) : facture
    }
    ?>	
    else if ($('#b_compte').attr('alt')=='comptes'){
// 	alert(4);
        $('#mode_').html("Création d'une facture pour ");
         function validation_cp(id){
        charge('creation_facture','<?php echo $resu->cpt_id?>&tic=<?php echo $nticket?>','panier');
        }
}

</script>
