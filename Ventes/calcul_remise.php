<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$req_comptes="SELECT cpt_remise, cpt_compte FROM Comptes WHERE cpt_id=$req";
$r_comptes=$idcom->query($req_comptes);
$rq_compte=$r_comptes->fetch_object();

if ($rq_compte->cpt_remise=="99.00") {//prix coutant = art_pht/(1+(tva/100)) pour tous les articles
    $req_vente = "UPDATE Tickets_".ANNEE." 
        JOIN Articles ON art_id = tic_article 
        JOIN Tva ON tva_id = art_tva
        JOIN Rayons ON ray_id = art_rayon
        JOIN Pseudos ON pse_id = art_pseudo
        SET tic_prix = (tic_pht*(1 + (tic_tva/100))),
            tic_tt = ROUND(tic_prix * tic_quantite,2)
                WHERE pse_remise = '1' AND tic_num = ".$_SESSION['panier_'.$_SESSION[$dossier]];
    $r_vente=$idcom->query($req_vente);
} elseif ($rq_compte->cpt_compte == '1') {//remise sans controle de min pour tous les articles pour les professionnels
    $req_vente = "UPDATE Tickets_".ANNEE." 
    JOIN Articles ON art_id = tic_article 
    JOIN Tva ON tva_id = art_tva
    JOIN Rayons ON ray_id = art_rayon
    JOIN Pseudos ON pse_id = art_pseudo
    SET tic_prix = (tic_prixS*(1-(".$rq_compte->cpt_remise."/100))), tic_tt =ROUND(tic_prix * tic_quantite,2) 
            WHERE pse_remise = '1' AND tic_num = ".$_SESSION['panier_'.$_SESSION[$dossier]];
    $r_vente=$idcom->query($req_vente);
} elseif ($rq_compte->cpt_compte == '0') {//remise pour particulier : pht+tva et 5% sur les livres
    //première partie, on vérifie si le prix remisé est inférieur au prix d'achat
    //deuxième partie :on remet à 5% les livres
    $req_vente = "UPDATE Tickets_".ANNEE." 
    JOIN Articles ON art_id = tic_article 
    JOIN Tva ON tva_id = art_tva
    JOIN Rayons ON ray_id = art_rayon
    JOIN Pseudos ON pse_id = art_pseudo
        SET tic_prix = CASE 
            WHEN (tic_prixS*(1-(".$rq_compte->cpt_remise."/100)))  < (tic_pht*(1 + (tic_tva/100))) 
            THEN (tic_pht*(1 + (tic_tva/100)))
            ELSE (tic_prixS*(1-(".$rq_compte->cpt_remise."/100)))
            END,
            tic_tt = ROUND(tic_prix * tic_quantite,2)
                WHERE pse_remise = '1' AND tic_num = ".$_SESSION['panier_'.$_SESSION[$dossier]];
    $r_vente=$idcom->query($req_vente);
    
    $req_livre = "UPDATE Tickets_".ANNEE." 
    JOIN Articles ON art_id = tic_article 
    JOIN Rayons ON ray_id = art_rayon
    JOIN Pseudos ON pse_id = art_pseudo
        SET tic_prix = tic_prixS - (tic_prixS * (select con_livre FROM Config)/100),
            tic_tt = ROUND(tic_prix * tic_quantite,2) 
            WHERE ray_secteur = 2 AND pse_remise = '1' AND tic_num = ".$_SESSION['panier_'.$_SESSION[$dossier]];
    $r_livre=$idcom->query($req_livre);
}

//calcul du total
$req_total = "SELECT SUM(tic_tt) AS TT FROM  Tickets_".ANNEE." WHERE  tic_num = ".$_SESSION['panier_'.$_SESSION[$dossier]];
$r_total=$idcom->query($req_total);
$rq_total=$r_total->fetch_object();
?>
<button onclick="charge('calcul_remise',<?php echo $req?>,'remise')" class="boutref" style="width:200px">Calcul du total avec la remise</button>
<?php
if ($req == 1) {//remise personnalisé
    echo "<img onclick=\"charge('panier','','panier')\" id='valider_compte' src=\"/images/valider.png\">";
} else {
    ?>
    <img onclick='validation_cp(<?php echo $req?>)' id='valider_compte' src="/images/valider.png">
    <?php
}
?>
<script>
$('#prix').html('<?php echo sprintf("%01.2f", $rq_total->TT)?>');
</script>