<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
//***********************************************Attention encodage cp1252******************************
// print_r($_SERVER); exit;
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
$ticket= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);//numero du ticket
$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);//numero du ticket
if( !$an) {
    $an = ANNEE;
}

connexobjet();

require $incpath.'/fpdf181/fpdf.php';
$Mois=array("", "janvier", "f�vrier", "mars", "avril", "mai", "juin", "juillet", "ao�t", "septembre", "octobre", "novembre", "d�cembre");
require "../info_facture.php";//fichier modifiable par l'utilisateur
$cemois = '';
$TT = '';
class PDF_FacturePF extends FPDF
{
    protected $Ligne_Hauteur = 15;

    function __construct($margeG, $margeH)
    {
        parent::__construct();
        $this->SetAutoPageBreak(false);
        $this->SetFontSize(7);
        $this->FontSize = 7;
        $this->FontFamily = "Arial";
        $this->Ligne_Hauteur = $this->FontSize * .47;
        $this->AddPage('P', 'A4');
        $this->lMargin = $margeG;
        $this->tMargin = $margeH;
    }

    function addLigne($type, $valeurs)
    {
        if ($type == "entete") {
            //**********************en tete******************************
            $this->SetFont('Arial', 'B', 14);
            $this->SetX($this->lMargin);
$this->MultiCell(100, 6, $valeurs['soc_nom'].'
'.$valeurs['soc_adr1'].'
'.$valeurs['soc_adr2'].'
'.$valeurs['soc_cp'].' '.$valeurs['soc_ville'].'
'.$valeurs['soc_pays']);
$this->SetFont('Arial', 'I', 8);
$this->SetX($this->lMargin);
$this->MultiCell(100, 4, 'SIRET : '.$valeurs['soc_siret'].'
N TVA : '.$valeurs['soc_tva'].'
'.$valeurs['soc_banque'].'
IBAN : '.$valeurs['soc_iban']);
$this->SetFont('Arial', 'B', 10);
$this->SetX($this->lMargin);
$this->MultiCell(100, 4, 'TEL. : '. $valeurs['soc_tel'].'
FAX. : '.$valeurs['soc_fax']);
            //****************fin de l'entete***********************
        } elseif ($type == "titre_tableau") {
            $y_axis_initial=70;
            $this->SetFillColor(255, 255, 255);
            $this->SetFont('Arial', 'B', 14);
            $this->SetY($y_axis_initial);
            $this->SetX($this->lMargin);
            $this->Cell(180, 7, 'Ticket N� '.$valeurs['num'].' du '.$valeurs['jour']." ".$valeurs['mois']." ".$valeurs['annee'], 1, 0, 'C', 1);
            if ($valeurs['nombre_page'] == 1) {
                $hauteur_tableau = 214;
            } else {
                $hauteur_tableau = 216;
            }
            $this->Line(15, 75, 15, $hauteur_tableau);
            $this->Line(110, 83, 110, $hauteur_tableau);
            $this->Line(120, 83, 120, $hauteur_tableau);
            $this->Line(140, 83, 140, $hauteur_tableau);
            $this->Line(155, 83, 155, $hauteur_tableau);
            $this->Line(175, 83, 175, $hauteur_tableau);
            $this->Line(195, 75, 195, $hauteur_tableau);
            $this->Line(15, $hauteur_tableau, 195, $hauteur_tableau);
                //deuxieme ligne, titre des colonnes
            $this->SetY($y_axis_initial+6);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(95, 6, 'D�signation', 1, 0, 'C', 1);
            $this->Cell(10, 6, 'Qte', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Prix Uni. TTC', 1, 0, 'C', 1);
            $this->Cell(15, 6, 'Rem.', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Taux TVA', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Total TTC', 1, 0, 'C', 1);
        } elseif ($type == "titre_suivant") {//page complete            
            $y_axis_initial=10;
            $hauteur_tableau = 260;
            $this->Line(15, 16, 15, $hauteur_tableau);
            $this->Line(110, 16, 110, $hauteur_tableau);
            $this->Line(120, 16, 120, $hauteur_tableau);
            $this->Line(140, 16, 140, $hauteur_tableau);
            $this->Line(155, 16, 155, $hauteur_tableau);
            $this->Line(175, 16, 175, $hauteur_tableau);
            $this->Line(195, 16, 195, $hauteur_tableau);
            $this->Line(15, $hauteur_tableau, 195, $hauteur_tableau);
            //deuxieme ligne, titre des colonnes
            $this->SetY($y_axis_initial+6);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(95, 6, 'D�signation', 1, 0, 'C', 1);
            $this->Cell(10, 6, 'Qte', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Prix Uni. TTC', 1, 0, 'C', 1);
            $this->Cell(15, 6, 'Rem.', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Taux TVA', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Total TTC', 1, 0, 'C', 1);
        } elseif ($type == "titre_dernier") {//derni�re page avec total            
            $y_axis_initial=10;
            $hauteur_tableau = 260;
            $this->Line(15, 16, 15, $hauteur_tableau);
            $this->Line(110, 16, 110, $hauteur_tableau);
            $this->Line(120, 16, 120, $hauteur_tableau);
            $this->Line(140, 16, 140, $hauteur_tableau);
            $this->Line(155, 16, 155, $hauteur_tableau);
            $this->Line(175, 16, 175, $hauteur_tableau);
            $this->Line(195, 16, 195, $hauteur_tableau);
            $this->Line(15, $hauteur_tableau, 195, $hauteur_tableau);
            //deuxieme ligne, titre des colonnes
            $this->SetY($y_axis_initial+6);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(95, 6, 'D�signation', 1, 0, 'C', 1);
            $this->Cell(10, 6, 'Qte', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Prix Uni. TTC', 1, 0, 'C', 1);
            $this->Cell(15, 6, 'Rem.', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Taux TVA', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Total TTC', 1, 0, 'C', 1);
        } elseif ($type == "article") {
            $prx_u = $valeurs["prix"];
            if ($valeurs["prix"]==$valeurs["prixS"]) {
                $remise = 0.00;
            } elseif ($valeurs["prixS"] != 0.00) {
                $remise = floor(100 - ($valeurs["prix"] * 100 / $valeurs["prixS"]));
            } else {
                $remise = 0.00;
            }
            $Total_TTC = $prx_u * $valeurs["quantite"];
            $this->SetY(82+$valeurs["dec"]);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(95, 6, " ".$valeurs["titre"], 1, 0, 'L', 1);
            $this->Cell(10, 6, $valeurs["quantite"], 1, 0, 'C', 1);
            $this->Cell(20, 6, monetaireF($prx_u)." �", 1, 0, 'R', 1);
            $this->Cell(15, 6, monetaireF($remise)." %", 1, 0, 'R', 1);//.
            $this->Cell(20, 6, monetaireF($valeurs["tva"])." %", 1, 0, 'R', 1);
            $this->Cell(20, 6, monetaireF($Total_TTC)." �", 1, 0, 'R', 1);
        } elseif ($type == "total") {
            //***************************************ligne tva*******************************
            $mar_g=$this->lMargin;
            $bas = $valeurs['bas'];
            $this->SetY($bas);
            $this->SetX($mar_g);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(60, 4, 'D�tail de la TVA', 1, 0, 'C', 1);
            $this->SetY($bas += 4);
            $this->SetX($mar_g);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(20, 4, 'Montant HT', 1, 0, 'C');
            $this->Cell(20, 4, 'Taux', 1, 0, 'C');
            $this->Cell(20, 4, 'Montant TVA', 1, 1, 'C');
            $this->Line(15, ($bas + 20), 75, ($bas +20));
            //*****************************affichage des tva utilis�s********************************
            $TTtva = '';
            foreach ($valeurs['afftva'] as $key => $value) {
                if ($value != 0.00) {
                    $tva=($value*($key/100)/1+($key/100));
                    $monTHT = $value - $tva;
                    @$TTtva += $tva;
                } else {
                    $tva =0.00;
                    $monTHT = 0.00;
                }
                $this->SetY($bas += 4);
                $this->SetX($mar_g);
                $this->SetFont('Arial', '', 7);
                $this->Cell(20, 4, monetaireF($monTHT)." �", "L", 0, 'R');
                $this->Cell(20, 4, monetaireF($key).' %', 0, 0, 'R');
                $this->Cell(20, 4, monetaireF($tva)." �", 'R', 1, 'R');
            }  
            //*****************************fin affichage des tva utilis�s********************************    
            $bas=$valeurs['bas'];
            //ligne du total
            $this->SetY($bas);
            $this->SetX($mar_g);
            $this->SetFont('Arial', 'B', 11);
            $this->Cell(140, 6);
            $this->Cell(20, 6, 'Total TTC', 1);
            $this->Cell(20, 6, monetaireF($valeurs['TT'])." �", 1, 0, 'R', 1);
            //ligne titre tva
            $bas=$bas+6;
            $this->SetY($bas);
            $this->SetX($mar_g);
            $this->SetFont('Arial', '', 11);
            $this->Cell(140, 6);
            $this->Cell(20, 6, 'Total TVA', 1, 0, 'R');
            $this->Cell(20, 6, monetaireF($TTtva)." �", 1, 0, 'R', 1);
            //cellule mode de reglement
            $bas += 6;
            $this->SetY($bas);
            $this->SetX($mar_g);
            $this->SetFont('Arial', '', 8);
            $this->Cell(60, 6);
            $this->Cell(120, 6, 'Mode de r�glement utilis� : '.$valeurs['mdr'], 'RB', 0, 'R');


        } elseif ($type == "infos_legales") {
            //position fixe
            $this->SetY(240);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', '', 7);
            $this->MultiCell(180, 3, utf8_decode($valeurs['info']), 1, 'J');       
        }
    }
}    
//coordonn�es de la soci�t�
$req_societe="SELECT * FROM Societe";
$r_societe=$idcom->query($req_societe);
$rq_societe=$r_societe->fetch_object();

$FacturePF=new PDF_FacturePF(15, 6);


//debut de facture, coordonn�es et lignes de titre
$FacturePF->addLigne("entete", array("soc_nom"=>utf8_decode($rq_societe->soc_nom), "soc_adr1"=>utf8_decode($rq_societe->soc_adr1), "soc_adr2"=>utf8_decode($rq_societe->soc_adr2), "soc_cp"=>$rq_societe->soc_cp, "soc_ville"=>utf8_decode($rq_societe->soc_ville), "soc_pays"=>$rq_societe->soc_pays, "soc_siret"=>$rq_societe->soc_siret, "soc_tva"=>$rq_societe->soc_tva, "soc_banque"=>$rq_societe->soc_banque, "soc_iban"=>$rq_societe->soc_iban, "soc_tel"=>$rq_societe->soc_tel, "soc_fax"=>$rq_societe->soc_fax));
// contenu de la vente
$req_articles="SELECT tit_nom,
                    tic_prixS,
                    tic_quantite,
                    tic_tva,
                    tic_ntva,
                    tic_prix,
                    tic_cp,
                    tic_num,
                    mdr_nom,
                    tic_tt,
                    UNIX_TIMESTAMP(rst_validation) AS rst_validation,
                    art_unite
                        FROM Tickets_".$an."
                        JOIN Titres ON tit_article = tic_article
                        JOIN Resume_ticket_".$an." ON rst_id = tic_num
                        JOIN Mode_reglement ON mdr_id = rst_etat
                        JOIN Articles ON art_id = tic_article
                            WHERE tit_niveau = 1
                            AND tic_num =".$ticket."
                                ORDER BY tic_id";
$r_articles=$idcom->query($req_articles);
$nombre_de_ligne=$r_articles->num_rows -1;
$articles=$r_articles->num_rows;
if ($articles <= 22) {
    $nombre_page = 1;
} else {
    $nombre_page = 2;
}

//************************ Ventes/impression_ticket.php?req=2420, 46 lignes

$dec=0;
$page = 1;
$numero_ligne = 1;//ligne incr�ment� de l'ensemble
$n_ligne = 1; //de la page
$rq_articles=$r_articles->fetch_object();
$num_ticket = $rq_articles->tic_num;
$mdr = utf8_decode($rq_articles->mdr_nom);

$FacturePF->addLigne("titre_tableau", array('jour'=>date('j', $rq_articles->rst_validation), 'mois'=>$Mois[date('n', $rq_articles->rst_validation)], 'annee'=>date('Y', $rq_articles->rst_validation), 'num'=>$num_ticket, 'nombre_page'=>$nombre_page));
$r_articles->data_seek(0);
// exit;
/*
Il y a toujours le cadre info sur la premi�re page. S'il n'y a <= 22 une seule page avec les totaux s'il ya plus de 22 lignes les totaux seront sur la deuxi�me page.

totaux  4 lignes
info 8 lignes
premi�re page seule 22 lignes
page1 avec suivante 26 lignes
page complete 47 lignes
page 2 et suivantes, maxi 39 lignes

----------page 1---------------
si plus <= 22, page 1 seule
si plus > 22, on insert la page suivante au dernier article ou au num�ro 26,
----------page 2 et suivantes---------------
si moins <= 39 affichage du total
si sup�rieur � 39, on insert la page suivante au dernier article ou au num�ro 39
*/
  //creation dynamique des lignes tva
$req_tva = "SELECT tva_id, tva_nom FROM Tva WHERE tva_etat = 1 ORDER BY tva_ordre";
$r_tva=$idcom->query($req_tva);
$afftva=array();
while ($rq_tva=$r_tva->fetch_object()) {
    array_push($afftva, $rq_tva->tva_nom);
}
$afftva = array_fill_keys($afftva, '0.00');
$page_encours = 1;

while ($rq_articles=$r_articles->fetch_object()) {
    if ($rq_articles->art_unite == 1) {
        $quantite = sprintf("%d", $rq_articles->tic_quantite);
    } else {
        $quantite = $rq_articles->tic_quantite;
    }

    $FacturePF->addLigne("article", array("titre"=>utf8_decode($rq_articles->tit_nom), "prixS"=>$rq_articles->tic_prixS, "prix"=>$rq_articles->tic_prix, "quantite"=>$quantite, "tva"=>$rq_articles->tic_tva, "dec"=>$dec, "numero_ligne"=>$numero_ligne));

    @$TT += $rq_articles->tic_tt;
    $dec += 6;
    if ($rq_articles->tic_ntva != 0) {
        $afftva[$rq_articles->tic_tva] += $rq_articles->tic_tt;
    }
    $reste = $nombre_de_ligne--;     
    if ($page_encours == 1) {
        if (($numero_ligne <= 22)&&($reste == 0)) {//page unique : articles/totaux/info            
            $bas=214;
            $FacturePF->addLigne('infos_legales', array("info"=>$info));
            $FacturePF->addLigne('total', array('bas'=>$bas, 'TT'=>$TT, 'afftva'=>$afftva, 'mdr'=>$mdr));
        } elseif (($numero_ligne <= 26)&&($reste == 0)) {//premi�re page : articles/info            
            if ($numero_ligne != 26) {
                $FacturePF->addLigne('finTableau', array('debut'=>($dec), 'fin'=>238));
            }
            $FacturePF->addLigne('infos_legales', array("info"=>$info));
            $FacturePF->AddPage('P', 'A4');
            $page_encours = 2;
            $dec=0;
            $bas = 260;
            $FacturePF->addLigne('titre_suivant',  array('jour'=>date('j/n/Y', $rq_articles->rst_validation), 'n_page'=>$page_encours));
        } elseif (($numero_ligne == 26)&&($reste > 0)) {//premi�re page : articles/info            
            $FacturePF->addLigne('infos_legales', array("info"=>$info));
            $FacturePF->AddPage('P', 'A4');
            $page_encours++;
            $bas = 260;
            $dec = -53;
            $numero_ligne = 1;
            $FacturePF->addLigne('titre_suivant',  array('jour'=>date('j/n/Y', $rq_articles->rst_validation), 'n_page'=>$page_encours));
        }
    } else {//toutes les pages suivantes        
        if (($numero_ligne >= 39)&&($reste == 0)) {
            if ($reste == 0) {
                if ($numero_ligne != 43) {
                    $FacturePF->addLigne('finTableau', array('debut'=>($dec), 'fin'=>270));
                }
                $FacturePF->AddPage('P', 'A4');
                $page_encours++;
                $bas = 260;
                $dec = -53;
                $numero_ligne = 1;
                $FacturePF->addLigne('titre_suivant',  array('jour'=>date('j/n/Y', $rq_articles->rst_validation), 'n_page'=>$page_encours));
            }
        } elseif ($numero_ligne == 43) {
            $FacturePF->AddPage('P', 'A4');
            $page_encours++;
            $bas = 260;
            $dec = -53;
            $numero_ligne = 1;
            $FacturePF->addLigne('titre_suivant',  array('jour'=>date('j/n/Y', $rq_articles->rst_validation), 'n_page'=>$page_encours));
        }
    }
    $numero_ligne ++;
}

$FacturePF->addLigne('total', array('bas'=>$bas, 'TT'=>$TT, 'afftva'=>$afftva, 'mdr'=>$mdr));
if($_SERVER['SCRIPT_NAME']!= "/Ventes/impression_ticket.php") {
unset($_SESSION['impression_'.$_SESSION[$dossier]]);//supression de la variable d'impression du dernier ticket
}
//****************************impression direct******************
// actuellement ce fichier n'est utilis� que par le module Gestion, la d�tection d'imprimante est d�sactiv�e
//on recupere l'imprimante
// $req_imprimante="SELECT * FROM Imprimantes WHERE imp_id=1";
// $r_imprimante=$idcom->query($req_imprimante);
// $rq_imprimante=$r_imprimante->fetch_object();
// if ($r_imprimante-> num_rows != 0) {
//     //verification de l'imprimante
//     ini_set("display_errors", 0);
//     $impr=array();
//     
//     exec('lpstat -v', $tab);//$tab=tableau contenant la description des imprimantes
//     foreach ($tab as $v) {
//         $ta=explode(" ", $v);
//         if (isset($ta[2])) {
//             array_push($impr, substr($ta[2], 0, -1));//on retire les : du nom de l'imprimante et on met dans un tableau
//         }
//     }
//     if (in_array($rq_imprimante->imp_nom, $impr)) {
//         $chemin = $incpath."pdf/jours/print.pdf";
//         $FacturePF->Output('F', $chemin);
//         $exec='lpr -P '.$rq_imprimante->imp_nom.' -o Resolution=600x600dpi '.$chemin;
//         exec($exec);
//     } else {
//         $FacturePF->Output();//sortie pdf
//     }
// } else {
    $FacturePF->Output();//sortie pdf
// }
