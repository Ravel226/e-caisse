<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p,$i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$titre=$idcom->query('SELECT ray_nom FROM Rayons WHERE ray_id='.$req)->fetch_object()->ray_nom;

$requete='SELECT Vt1_nom AS tit_nom, art_id, art_ttc, 
        art_pseudo, art_cb, art_unite,
        IF(unv_abrege != "",CONCAT("0.00 ",unv_abrege),"") AS unv_abrege
            FROM Articles 
                JOIN Vtit1 ON Vt1_article = art_id 
                JOIN Unites_vente ON art_unite = unv_id WHERE art_rayon='.$req;

//echo $requete;
$r_cartes=$idcom->query($requete);
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_cartes->num_rows;
if ($nb == 1) {//insertion direct de l'article
    $rq_cartes=$r_cartes->fetch_object()
?>
<script>charge('insert','&cb=<?php echo $rq_cartes->art_id?>','panier');</script>
<?php
exit;
}
// $rq_cartes=$r_cartes->fetch_object();
?>
<table id="tableTicket" class="pagination" number-per-page="6" current-page="0">
    <thead>
        <TR><TH colspan="2"><?php echo $titre?></TH></TR>
    </thead>
<?php
$n=0;
$art_pseudoP = 0;
$art_pseudoQT = '';
$abrege = '';
while ($rq_cartes=$r_cartes->fetch_object()) {
    // 	print_r($rq_cartes)
    if ($n % 2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }
    if ($rq_cartes->art_ttc == 0.00) {
        $prix = 'Prix à définir';
    } else {
        $prix = $rq_cartes->art_ttc." €";
    }
    
    //trois types d'articles, pseudo ou non, article au détail
    if ($rq_cartes->art_pseudo == 1) {
        if ($rq_cartes->unv_abrege != '') {
            $abrege = "<span style='color:silver'> ".$rq_cartes->unv_abrege."</span>";
        }
        echo "<tr style='background-color:".$coul."'><td>".$rq_cartes->tit_nom." (<i>".$prix."</i>)";
        if ($rq_cartes->art_unite == 1) {
            echo "<button class='plus1' onclick='plus1(".$rq_cartes->art_id.")'>+</button>";
        }
        
        echo "</td><td unite='".$rq_cartes->art_unite."' onclick='voir_clavierqt(".$rq_cartes->art_id.")' class='saisie' id='".$rq_cartes->art_id."' pseudo='".$rq_cartes->art_pseudo."'>".$abrege."</td></tr>";
        $art_pseudoQT = 0;
        $abrege = '';
    } else {
        if (file_exists($incpath."Saisie/images/".$rq_cartes->art_cb.".png")) {
            $comp = "background-image:url('".$incpath."Saisie/images/".$rq_cartes->art_cb.".png')";
        } else {
            $comp = '';
        }
        echo "<tr style='background-color:".$coul."'><td class='abbaye' style=\"padding-left:100px;".$comp."\">".$rq_cartes->tit_nom." (<i>".$prix."</i>)</td><td unite='".$rq_cartes->art_unite."' onclick='voir_clavierpi(".$rq_cartes->art_id.")' class='saisie' id='".$rq_cartes->art_id."' pseudo='".$rq_cartes->art_pseudo."'><span style='color:silver'>Valeur</span></td></tr>";
        $art_pseudoP = 1;
    }
    
    $n++;
}
?>
</table>
<script type="text/javascript" src="/js/simplepagination.js"></script>
<script>
<?php
include('tableTicketjs.php');
if ($art_pseudoQT == 0) {
    ?>
min = 'test';
function plus1(id){
    $('#'+id).html((+$('#'+id).html())+1);	
    $('#'+id).addClass('calcul');
    voir_clavierqt();
}
    
function voir_clavierqt(id){
    $("#"+id).html("");
    $( ".saisie" ).each(function() {
        $( ".saisie" ).css("backgroundColor","white");
        });
    $("#"+id).css("backgroundColor","<?php echo $_SESSION['surligne_'.$_SESSION[$dossier]]?>");
    $('#'+id).addClass('calcul');
    
    if($('#'+id).attr('unite') == 1)$('#references').html(clavierqt);
    else $('#references').html(clavierp);
    
    $('#keypad').css('top',50);
    $('#keypad').css('display','block');
    
    $("#keypad .key").click(function(){
        if($(this).html() == 'Valider'){
            var tableauCarte="";
            $(".calcul").each(function(){ 
                if($(this).html() != 0){
                    tableauCarte=tableauCarte+$(this).attr('id')+"-"+$(this).html()+"-"+$(this).attr('pseudo')+",";
                    }
                });
            charge('insert',"&carte="+tableauCarte,'panier');
            } else if ($(this).html() == 'Annuler'){
                $('#'+id).html($('#'+id).attr('Alt'));
            } else if ($(this).html() == 'Effacer'){
                $('#'+id).html('');
            } else if (($(this).html() == '0')&&($(this).attr('unite') == 1)){
                if($('#'+id).html() == ''){	
                    $('#references').html('<h1>Il ne peut y avoir une quantité 0.</h1>');
                    $('#'+id).html($('#'+id).attr('Alt'));
            } else $("#"+id).append($(this).html());
        } else {
        $("#"+id).append($(this).html());        
    }
    $('#valider').css('visibility', 'visible');
});
//gestion du bouton valider
    var tt='';
    $( ".saisie" ).each(function() {
    tt +=  $( this ).text();
    });
    if(tt =='')$('#valider').css('visibility', 'hidden');
    else $('#valider').css('visibility', 'visible');
    }

<?php
}

if ($art_pseudoP == 1) {
    ?>
function voir_clavierpi(id){		
    $(".saisie" ).each(function() {
        $(".saisie" ).css("backgroundColor","white");
        });
    $('#'+id).empty();
    $('#'+id).css('backgroundColor','<?php echo $_SESSION['surligne_'.$_SESSION[$dossier]]?>');
    $('#'+id).addClass('calcul');
// 			$('#'+id).removeClass('saisie');
    $('#references').empty();
    $('#references').html(clavierp);
    $('#keypad').css('top',50);
    $('#keypad').css('display','block');			

    $("#keypad .key").click(function(){
    
        if($(this).html() == 'Valider'){		
            var tableauCarte="";
            $(".calcul").each(function(){ 
                tableauCarte=tableauCarte+$(this).attr('id')+"-"+$(this).html()+"-"+$(this).attr('pseudo')+",";
                });
            charge('insert',"&carte="+tableauCarte,'panier');
            }			
        else if ($(this).html() == 'Annuler'){
            $('#'+id).css('color','black');
            $("#references").empty();
            charge('cb','','references');
            }
        else if ($(this).html() == 'Valider'){
            charge('update',id+'&px='+$('#n_prix').html(),'panier');
            }
        else if ($(this).html() == 'Effacer'){
            $('#'+id).html('');
            }
        else{
            $('#'+id).append($(this).html());
// 			if()
            $('#valider').css('visibility', 'visible');
            }
        })
    var tt='';
    $( ".calcul" ).each(function() {
    tt +=  $( this ).text();
    });	
    if(tt =='')$('#valider').css('visibility', 'hidden');
    else $('#valider').css('visibility', 'visible');
    }
<?php
}
?>
</script>
