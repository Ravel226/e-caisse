<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
//on vient du fichier reglement.php
//on verifie si le panier est passé par un compte
$req_attente="SELECT tic_cp, cpt_id, cpt_nom FROM Tickets_".ANNEE." JOIN Comptes ON tic_cp = cpt_id WHERE tic_num =".$_SESSION['panier_'.$_SESSION[$dossier]]."";
$r_attente=$idcom->query($req_attente);
?>

<h2>Règlement du panier en différé</h2>
Un facture est imposée. Veuillez choisir un compte existant ou en créer un nouveau. Si ce compte a une remise, elle sera appliquée.<br>
    <script>
    function refac(){
$('#panier').html('<center><button class="Bcartes" onclick="charge(\'liste_paniers\',\'\',\'liste_panier\')">Terminer</button><center>');
}
</script>
<?php
if ($r_attente -> num_rows > 0) {
    $rq_attente = $r_attente->fetch_object(); ?>
    <button class='boutref' style='width:45%' onclick="charge('validation_differe','<?php echo $rq_attente->cpt_id?>&du='+$('#prix').html(),'paniers');refac();">Facture <?php echo $rq_attente->cpt_nom; ?></button><button class='boutref' onclick="charge('comptes',3,'references')" style='width:45%;float:right' >Autres </button>

    <?php
    exit;
}
$req_comptes="SELECT cpt_id, cpt_nom, cpt_ville, LEFT(cpt_nom ,1) AS ref, COUNT(LEFT(cpt_nom ,1)) AS nb FROM Comptes WHERE cpt_nom !='' GROUP BY ref ORDER BY ref";
// //exit;
$r_comptes=$idcom->query($req_comptes);
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
echo '<h3>Comptes enregistrés</h3>';
echo '<button class="boutref" onclick="charge(\'detail_compte\',\'000\',\'panier\')">Nouv.</button>';
while ($rq_comptes=$r_comptes->fetch_object()) {
    echo '<button class="boutref" onclick="charge(\'detail_compte\',\''.$rq_comptes->ref.'\',\'panier\')">'.$rq_comptes->ref.'</button>';
}
?>
