<?php
session_start();
if(!isset($incpath)){$p=preg_split("[/]",$_SERVER['PHP_SELF']);$incpath="";for($i=1;$i<sizeof($p)-1;$i++){$incpath='../'.$incpath;}unset($p,$i);}

//***********************************************Attention encodage cp1252******************************

$cp= filter_input(INPUT_GET, "cp", FILTER_SANITIZE_STRING);
include($incpath."mysql/connect.php");
global $idcom;
connexobjet();
require_once($incpath.'/fpdf181/fpdf.php');
$Mois=array("","janvier","f�vrier","mars","avril","mai","juin","juillet","ao�t","septembre","octobre","novembre","d�cembre");
include("../info_facture.php");//fichier modifiable par l'utilisateur

class PDF_FacturePF extends FPDF {

    // Properties

    public 		$_ligne = 0;
		protected $_an;   
		protected $_cemois;
    protected $_Ligne_Hauteur = 15;
    protected $_nbLignes;
    protected $_Colonne_Largeur;
    protected $_colonne = 1;
    protected $_Titre_Page = false;

    function __construct($margeG, $margeH, $an, $cemois) {

        parent::__construct();

        $this->SetAutoPageBreak(false);
        $this->SetFontSize(7);
        $this->FontSize = 7;
        $this->FontFamily = "Arial";

        $this->_Ligne_Hauteur = $this->FontSize * .47;
        $this->AddPage('P','A4');

        $this->lMargin = $margeG;
        $this->tMargin = $margeH;
        $this->_Colonne_Largeur = ($this->w - $this->lMargin - $this->rMargin);
        $this->_ligne = 0;
        $this->_colonne = 1;
        $this->_cemois = $cemois;
        $this->_an = $an;
    }

    // Give the height for a char size given.
    protected function _get_height_chars($pt = null) {

        // Tableau de concordance entre la hauteur des caract�res et de l'espacement entre les lignes
        if ($pt === null) {
            $pt = $this->FontSize;
        }

        return $pt * .52;
    }

  function addLigne($type, $valeurs) 
		{
		if($type == "entete")
			{
			//**********************en tete******************************
			$this->SetFont('Arial','B',14);
			$this->SetX($this->lMargin);
			$this->MultiCell(100,6,$valeurs['soc_nom'].'
'.$valeurs['soc_adr1'].'
'.$valeurs['soc_adr2'].'
'.$valeurs['soc_cp'].' '.$valeurs['soc_ville'].'
'.$valeurs['soc_pays']);
			$this->SetFont('Arial','I',8);
			$this->SetX($this->lMargin);
			$this->MultiCell(100,4,'SIRET : '.$valeurs['soc_siret'].'
N TVA : '.$valeurs['soc_tva'].'
'.$valeurs['soc_banque'].'
			RIB : '.$valeurs['soc_iban']);
			$this->SetFont('Arial','B',10);
			$this->SetX($this->lMargin);
			$this->MultiCell(100,4,'TEL. : '. $valeurs['soc_tel'].'
FAX. : '.$valeurs['soc_fax']);
		//****************fin de l'entete***********************
					//**************on affiche les coordonnes de l'acheteur*****************
		
		//   print_r($rq_facture);
			$debut=30;
			$this->SetFont('Arial','B',16);
			$this->SetY(20);
			$this->SetX($mar_g+100);
			$this->Cell(100,6,'FACTURE Pro format');
			$this->SetFont('Arial','I',10);
			$this->SetY($debut);
			$this->SetX($mar_g+100);
			$this->Cell(80,6,$valeurs['cpt_nom']);
			$this->SetY($debut+6);
			$this->SetX($mar_g+100);
			if($valeurs['cpt_adr2'] !="")
				{
				$this->Cell(80,6,$valeurs['cpt_adr1']);
				$this->SetY($debut+12);
				$this->SetX($mar_g+100);
				$this->Cell(80,6,$valeurs['cpt_adr2']);
				$this->SetY($debut+18);
				$this->SetX($mar_g+100);
				$this->Cell(80,6,$valeurs['cpt_cp']);
				$this->SetY($debut+24);
				$this->SetX($mar_g+100);
				$this->Cell($debut,6,$valeurs['cpt_ville']);
				}
			else
				{
				$this->Cell(80,6,$valeurs['cpt_adr1']);
				$this->SetY($debut+12);
				$this->SetX($mar_g+100);
				$this->Cell(80,6,$valeurs['cpt_cp']);
				$this->SetY($debut+18);
				$this->SetX($mar_g+100);
				$this->Cell($debut,6,$valeurs['cpt_ville']);
				}
			}

		elseif($type == "titre_tableau")
			{
			$y_axis_initial=70;
			$this->SetFillColor(255,255,255);
			$this->SetFont('Arial','B',14);
			$this->SetY($y_axis_initial);
			$this->SetX($this->lMargin);
			$this->Cell(180,7,'Ticket N� '.$valeurs['num'].' du '.$valeurs['jour']." ".$valeurs['mois']." ".$valeurs['annee'],1,0,'C',1);
			if($valeurs['nombre_page'] == 1) $hauteur_tableau = 214;
			else $hauteur_tableau = 216;
			$this->Line(15,75,15,$hauteur_tableau);
			$this->Line(110,83,110,$hauteur_tableau);
			$this->Line(120,83,120,$hauteur_tableau);
			$this->Line(140,83,140,$hauteur_tableau);
			$this->Line(155,83,155,$hauteur_tableau);
			$this->Line(175,83,175,$hauteur_tableau);
			$this->Line(195,75,195,$hauteur_tableau);
			$this->Line(15,$hauteur_tableau,180,$hauteur_tableau);
						//deuxieme ligne, titre des colonnes
			$this->SetY($y_axis_initial+6);
			$this->SetX($this->lMargin);
			$this->SetFont('Arial','B',8);
			$this->Cell(95,6,'D�signation',1,0,'C',1);
			$this->Cell(10,6,'Qte',1,0,'C',1);
			$this->Cell(20,6,'Prix Uni. TTC',1,0,'C',1);
			$this->Cell(15,6,'Rem.',1,0,'C',1);
			$this->Cell(20,6,'Taux TVA',1,0,'C',1);
			$this->Cell(20,6,'Total TTC',1,0,'C',1);
			}
		elseif($type == "titre_suivant")
			{
			$y_axis_initial=10;
			$hauteur_tableau = 250;
			$this->Line(15,16,15,$hauteur_tableau);
			$this->Line(110,16,110,$hauteur_tableau);
			$this->Line(120,16,120,$hauteur_tableau);
			$this->Line(140,16,140,$hauteur_tableau);
			$this->Line(155,16,155,$hauteur_tableau);
			$this->Line(175,16,175,$hauteur_tableau);
			$this->Line(195,16,195,$hauteur_tableau);
			$this->Line(15,$hauteur_tableau,180,$hauteur_tableau);
						//deuxieme ligne, titre des colonnes
			$this->SetY($y_axis_initial+6);
			$this->SetX($this->lMargin);
			$this->SetFont('Arial','B',8);
			$this->Cell(95,6,'D�signation',1,0,'C',1);
			$this->Cell(10,6,'Qte',1,0,'C',1);
			$this->Cell(20,6,'Prix Uni. TTC',1,0,'C',1);
			$this->Cell(15,6,'Rem.',1,0,'C',1);
			$this->Cell(20,6,'Taux TVA',1,0,'C',1);
			$this->Cell(20,6,'Total TTC',1,0,'C',1);
			}
		elseif($type == "article")
			{
			 $prx_u = $valeurs["prix"];
      if($valeurs["prix"]==$valeurs["prixS"]) $remise = 0.00;
      elseif($valeurs["prixS"] != 0.00) $remise = floor(100 - ($valeurs["prix"] * 100 / $valeurs["prixS"])); 
      else  $remise = 0.00;
//       echo "100 - (".$valeurs["prix"]." * 100 / ".$valeurs["prixS"].")<br>";
			$Total_TTC = $prx_u * $valeurs["quantite"];
// 			exit;
			$this->SetY(82+$valeurs["dec"]);
  		$this->SetX($this->lMargin);
      $this->SetFont('Arial','B',8);
			$this->Cell(95,6," ".$valeurs["titre"],1,0,'L',1);
			$this->Cell(10,6,$valeurs["quantite"],1,0,'C',1);
			$this->Cell(20,6,monetaireF($valeurs["prixS"])." �",1,0,'R',1);
			$this->Cell(15,6,monetaireF($remise)." %",1,0,'R',1);//.
			$this->Cell(20,6,monetaireF($valeurs["tva"])." %",1,0,'R',1);
			$this->Cell(20,6,monetaireF($Total_TTC)." �",1,0,'R',1);
			//si je suis 
// 				if(($n_ligne == 21))AddPage('P','A4');

			}
		elseif($type == "total")
			{
//***************************************ligne tva*******************************
  $mar_g=$this->lMargin;
      $bas = $valeurs['bas'];
      $this->SetY($bas);
      $this->SetX($mar_g);
      $this->SetFont('Arial','B',7);
      $this->Cell(60,4,'D�tail de la TVA',1,0,'C',1);
      $this->SetY($bas += 4);
      $this->SetX($mar_g);
      $this->SetFont('Arial','B',7);
      $this->Cell(20,4,'Montant HT',1,0,'C');
      $this->Cell(20,4,'Taux',1,0,'C');
      $this->Cell(20,4,'Montant TVA',1,1,'C');
//*****************************affichage des tva utilis�********************************
      foreach($valeurs['afftva'] as $key => $value)
        {
        if($value != 0.00)
          {
          $tva=($value*($key/100)/1+($key/100));
          $monTHT = $value - $tva;
          /*echo "\n".*/$TTtva += $tva;
          }
          else
          {
          $tva =0.00;
          $monTHT = 0.00;
          }
        $this->SetY($bas += 4);
        $this->SetX($mar_g);
        $this->SetFont('Arial','',7);
        $this->Cell(20,4,monetaireF($monTHT)." �","L",0,'R');
        $this->Cell(20,4,monetaireF($key).' %',0,0,'R');
        $this->Cell(20,4,monetaireF($tva)." �",'R',1,'R');
        }   
 //*****************************fin affichage des tva utilis�********************************
     
	$bas=$valeurs['bas'];
//ligne du total
  $this->SetY($bas);
  $this->SetX($mar_g);
  $this->SetFont('Arial','B',11);
  $this->Cell(140,6);
  $this->Cell(20,6,'Total TTC',1);
  $this->Cell(20,6,monetaireF($valeurs['TT'])." �",1,0,'R',1);
//ligne titre tva
$bas=$bas+6;
  $this->SetY($bas);
  $this->SetX($mar_g);
  $this->Cell(140,6);
  $this->Cell(20,6,'Total TVA',1,0,'R');
  $this->Cell(20,6,monetaireF($TTtva)." �",1,0,'R',1);
//cellule mode de reglement
$bas += 6;
  $this->SetY($bas);
  $this->SetX($mar_g);
  $this->SetFont('Arial','',8);
  $this->Cell(60,6);
  $this->Cell(120,6,'Mode de r�glement utilis� : '.$valeurs['mdr'],'RB',0,'R');


			}
		elseif($type == "infos_legales")
			{
			  //position fixe
			$this->SetY(240);
			$this->SetX($this->lMargin);
			$this->SetFont('Arial','',7);
			$this->MultiCell(180,3,utf8_decode($valeurs['info']),1,'J');

			}
		}
	}	
//coordonn�es de la soci�t�
$req_societe="SELECT * FROM Societe";
$r_societe=$idcom->query($req_societe);
$rq_societe=$r_societe->fetch_object();



//coordonn�es de l'acheteur
$req_facture="SELECT * FROM Comptes WHERE cpt_id=$cp";
$r_facture=$idcom->query($req_facture);
$rq_facture=$r_facture->fetch_object();


$FacturePF=new PDF_FacturePF(15, 6, $an, $cemois);
$FacturePF->AliasNbPages();
//debut de facture, coordonn�es et lignes de titre
$FacturePF->addLigne("entete",array("soc_nom"=>$rq_societe->soc_nom,"soc_adr1"=>$rq_societe->soc_adr1,"soc_adr2"=>$rq_societe->soc_adr2,"soc_cp"=>$rq_societe->soc_cp,"soc_ville"=>$rq_societe->soc_ville,"soc_pays"=>$rq_societe->soc_pays,"soc_siret"=>$rq_societe->soc_siret,"soc_tva"=>$rq_societe->soc_tva,"soc_banque"=>$rq_societe->soc_banque,"soc_iban"=>$rq_societe->soc_iban,"soc_tel"=>$rq_societe->soc_tel,"soc_fax"=>$rq_societe->soc_fax,"cpt_nom"=>$rq_facture->cpt_nom,"cpt_adr1"=>utf8_decode($rq_facture->cpt_adr1),"cpt_adr2"=>utf8_decode($rq_facture->cpt_adr2),"cpt_cp"=>$rq_facture->cpt_cp,"cpt_ville"=>utf8_decode($rq_facture->cpt_ville)));
$FacturePF->addLigne("titre_tableau",array('jour'=>date('j'),'mois'=>$Mois[date('n')],'annee'=>date('Y'),'nombre_page'=>2));



// contenu de la vente
 $req_articles="SELECT tit_nom, art_ttc, tic_quantite, tic_tva, tic_prix, tic_prixS, tic_ntva FROM Tickets_2017 JOIN Titres ON tit_article = tic_article JOIN Articles ON art_id = tic_article WHERE tit_niveau = 1 AND tic_cp =".$cp." LIMIT 0,10";
$r_articles=$idcom->query($req_articles);
 $nombre_de_ligne=$r_articles->num_rows;
$dec=0; 
$page = 1;
$numero_ligne = 1;//ligne de la requete

$rq_articles=$r_articles->fetch_object();
$mdr = utf8_decode($rq_articles->mdr_nom);

$FacturePF->addLigne("titre_tableau",array('ticket'=>$rq_facture->rst_id,'jour'=>date('j'),'mois'=>$Mois[date('n')],'annee'=>date('Y'),'nombre_page'=>2,'num'=>$rq_articles->tic_num));
$r_articles->data_seek(0);
// exit;
/*
Il y a toujours le cadre info sur la premi�re page. S'il n'y a <= 22 une seule page avec les totaux s'il ya plus de 22 lignes les totaux seront sur la deuxi�me page.

totaux  4 lignes
info 8 lignes
premi�re page seule 22 lignes
page complete 47 lignes

nombre de page
si plus de 22

*/
  //creation dynamique des lignes tva
  $req_tva = "SELECT tva_id, tva_nom FROM Tva WHERE tva_id !=0 AND tva_etat = 1 ORDER BY tva_ordre";
  $r_tva=$idcom->query($req_tva);
  $afftva=array();
  while($rq_tva=$r_tva->fetch_object())
  {
  array_push($afftva,$rq_tva->tva_nom);
  }
 $afftva = array_fill_keys($afftva, '0.00');

while($rq_articles=$r_articles->fetch_object())
	{
	$FacturePF->addLigne("article",array("titre"=>utf8_decode($rq_articles->tit_nom),"prixS"=>$rq_articles->tic_prixS,"prix"=>$rq_articles->tic_prix,"quantite"=>$rq_articles->tic_quantite,"tva"=>$rq_articles->tic_tva,"dec"=>$dec,"numero_ligne"=>$numero_ligne));
//  print_r($rq_articles);
  $TT += $rq_articles->tic_prix*$rq_articles->tic_quantite; 
  if($rq_articles->tic_ntva != 0)
    {
    $afftva[$rq_articles->tic_tva] += ($rq_articles->tic_prix*$rq_articles->tic_quantite);
    }  
	$dec += 6;
	if($nombre_de_ligne <= 22)//une seule page : lignes/total/info
		{
// 		echo 1;
// 		$FacturePF->addLigne('total',$bas);
		$bas=216;
		$page_encours = 1;
// 		$FacturePF->AddPage('P','A4');
		}
	
	$n_ligne ++;//lignes par page
	}
// 	echo $n_ligne."rrrrrrrrrrr";
	if($n_ligne < 21)//une seule page
	{
	 $bas=216;
// 	  include("../info_facture.php");
	}
	else $bas = 270;
//   echo $bas;
if($page_encours == 1)
	{
// 	$FacturePF->addLigne('total',$bas);
	$FacturePF->addLigne('infos_legales',array("info"=>$info));
	} 
$FacturePF->addLigne('total',array('bas'=>$bas, 'TT'=>$TT,'afftva'=>$afftva,'mdr'=>$mdr));
// print_r($afftva);
$chemin = "/pdf/factures/facture_pf.pdf";
$FacturePF->Output('F',$chemin);//
?>
<script>
window.open('<?php echo $chemin?>');
// location.href='<?php echo $chemin?>';
</script>