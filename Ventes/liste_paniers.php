<?php
if (!isset($incpath)) {
    session_start();
    if (!isset($incpath)) {
        $p=preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath="";
        for ($i=1;$i<sizeof($p)-1;$i++) {
            $incpath='../'.$incpath;
        }
        unset($p, $i);
    }
    $req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

    include $incpath."mysql/connect.php";
    include $incpath."php/fonctions.php";

    if ($req) {
        $_SESSION['panier_'.$_SESSION[$dossier]]=$req;
    }//changement de panier
    connexobjet();
    $charge=1;
}
//recherche des paniers en cours pour cet utilisateur, le numero de départ des paniers étant :
$refD=315522000;//mktime(0,0,0,1,1,1980);
// 3155 -$_SESSION[$dossier] référence l'utilisateur, 2000 -n référence le numéro du panier
$req_panier="SELECT tic_num FROM Tickets_".ANNEE." WHERE tic_num LIKE '".(3155-$_SESSION[$dossier])."%' AND LENGTH(tic_num)=9 GROUP BY tic_num ORDER BY tic_num DESC";
$r_panier=$idcom->query($req_panier);
$nb = $r_panier->num_rows;

// le bouton de déconnexion n'est visible que s'il n'y a pas d'article dans un panier, celui affiché ou un autre
$boutons ='<div style="float:left;"><img src="/images/sortie.png" style="padding:5px 0px 0px 5px;visibility:hidden" id="deconnexion" onclick="location.href=\'/php/deconnexion.php?dos='.$dossier.'&uti='.$_SESSION[$dossier].'\'"><br>
<img src="/images/couleur.png" style="margin:0 0 0 4px;" onclick="charge(\'/preferences\',\''.$dossier.'\',\'panier\')"></div>
<div style="float:left;padding-right:10px">
<img src="/images/veille.png" style="padding:5px 0px 0px 5px;" onclick="$(\'#voile\').css(\'visibility\',\'visible\')"><br>
<button id="plein" style="margin-left: 4px;" onclick="toggleFullScreen()"></div>';

$actif = '';
$dernier_panier = "";
if ($nb == 0) {//pas de panier en cours
  //recherche des tickets réalisé ce jour par ce vendeur
    $cejour=date('Y').'-'.date('m').'-'.date('d');
    /*echo*/	$req_tickets="SELECT * FROM Resume_ticket_".ANNEE." WHERE DATE(rst_validation) = '".$cejour."' AND rst_utilisateur = ".$_SESSION[$dossier];
    $r_tickets=$idcom->query($req_tickets);
    if ($r_tickets->num_rows == 0) {
        $n_panier = 1999;
        $actif = 1;//drapeau
    } else {
        $n_panier = 1999 - $r_tickets->num_rows;
    }
    //Creation de la variable ssession
    $_SESSION['panier_'.$_SESSION[$dossier]]=(3155-$_SESSION[$dossier])."2".$n_panier;
  
    $boutons .= "<button class = 'panier_actif' onclick=\"charge('panier','','panier')\">".(2000-substr($_SESSION['panier_'.$_SESSION[$dossier]], -4))."</button>";
    $dernier_panier = $_SESSION['panier_'.$_SESSION[$dossier]] -1;
    $actif = 1;//drapeau
} else { //un ou plusieurs paniers en cours, on liste pour afficher les boutons le premier étant actif, s'il n'y en a pas d'égal à la variable $_SESSION['panier_'.$_SESSION[$dossier]], c'est qu'on vient d'en créer un nouveau, on l'ajoute
// 	echo "<br>ici";
    while ($rq_panier=$r_panier->fetch_object()) {
        if (isset($_SESSION['panier_'.$_SESSION[$dossier]])) {
            $panier = $_SESSION['panier_'.$_SESSION[$dossier]];
        } else {
            $panier = '';
        }
        if ($_SESSION['panier_'.$_SESSION[$dossier]] == $rq_panier->tic_num) {//panier en cours
            $actif = 1;//drapeau
            $boutons .= "<button class = 'panier_actif' onclick=\"charge('panier','','panier')\">".(2000-substr($rq_panier->tic_num, -4))."</button>";
            $dernier_panier = $_SESSION['panier_'.$_SESSION[$dossier]] -1;
        } else {
            $boutons .= "<button class = 'panier_attente' onclick=\"charge('liste_paniers','".$rq_panier->tic_num."','liste_panier')\">".(2000-substr($rq_panier->tic_num, -4))."</button>";
        }
        if (($actif != "1")&&(!$_SESSION['panier_'.$_SESSION[$dossier]])) {//reprise d'un panier non utilisé
            $_SESSION['panier_'.$_SESSION[$dossier]] = $rq_panier->tic_num; ?>
            <script>
            charge('liste_paniers','','liste_panier');
            </script>
            <?php
        }
    }
}
if (($actif != "1")&&($_SESSION['panier_'.$_SESSION[$dossier]])) {
    $boutons .= "<button class = 'panier_actif' onclick=\"charge('liste_paniers','','liste_panier')\">".(2000-substr($_SESSION['panier_'.$_SESSION[$dossier]], -4))."</button>";
}
  //--------------------------bouton nouveau painer-------------
$boutons .= "<button id='nouveau_panier' onclick=\"charge('liste_paniers',".$dernier_panier.",'liste_panier')\" class = 'panier_attente' style='visibility:hidden'>Nouv</button>";
echo $boutons;
?>
<script>
var bodyElement = document.getElementById("conteneur");
<?php
if (strstr($_SERVER['HTTP_USER_AGENT'],"WebKit") != FALSE) {
    ?>
    function toggleFullScreen() {
        if (!document.mozFullScreen && !document.webkitFullScreen) {
            if (bodyElement.webkitRequestFullScreen) {
            bodyElement.webkitRequestFullScreen();
            setTimeout(function(){
                $('#affichage').height($('#conteneur').height()-140);
                $('#affichage').width($('#conteneur').width()-100);
                },100);
            } else {
            bodyElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
            setTimeout(function(){
                $('#affichage').height($('#conteneur').height()-140);
                $('#affichage').width($('#conteneur').width()-100);
                },100);
            } else {
            document.webkitCancelFullScreen();
            }
        }
    }    
charge('panier','','panier');
</script> 
<?php  
} else {
    ?>
    <!-- <script> -->
    function toggleFullScreen() {
        if (!document.mozFullScreen && !document.webkitFullScreen) {
            if (bodyElement.mozRequestFullScreen) {
            bodyElement.mozRequestFullScreen();
            setTimeout(function(){
                $('#affichage').height($('#conteneur').height()-140);
                $('#affichage').width($('#conteneur').width()-100);
                },100);
            } else {
            bodyElement.mozRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        } else {
            if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
            setTimeout(function(){
                $('#affichage').height($('#conteneur').height()-140);
                $('#affichage').width($('#conteneur').width()-100);
                },100);
            } else {
            document.mozCancelFullScreen();
            }
        }
    }
    charge('panier','','panier');
</script> 
<?php
}
?>
