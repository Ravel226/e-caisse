<?php $info='LIVRAISON
Les marchandises voyagent aux risques et périls du destinataire. Aucune réclamation sur les articles n\'est recevable passé le délai de 8 jours après la livraison.
CONDITIONS DE RÈGLEMENT - DÉLAIS DE PAIEMENT
Paiement par chèque en euros tiré sur une banque domiciliée en France, par virement ou par carte bancaire.
Ne sont pas admis les chèques émis en une devise autre que l\'euro ou dont la domiciliation de la banque n\'est pas en France.
Pas d\'escompte en cas de paiement anticipé.
Dans les rapports commerciaux entre professionnels, le paiement doit intervenir à la date d\'échéance du règlement indiquée sur la facture, date qui est fixée au maximum, et selon le cas, à 45 jours fin de mois ou à 60 jours calendaires suivant la date d\'émission de la facture.
Les pénalités de retard sont exigibles dès le jour suivant la date d\'échéance du règlement figurant sur la facture sans qu\'un rappel soit nécessaire et sans mise en demeure préalable.
Le taux d\'intérêt des pénalités de retard exigibles le jour suivant la date de règlement figurant sur la facture sera égal à 3 fois le taux d\'intérêt légal.
Est en outre puni d\'une amende civile le fait de ne pas respecter les délais de paiement (article L441-6 du Code de Commerce modifié par la loi n°2008-776 du 4 août 2008).
RÉSERVE DE PROPRIÉTÉ
Les marchandises restent notre propriété jusqu\'à paiement intégral du prix facturé.'?>