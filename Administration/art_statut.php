<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$id= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_artstatut = "SELECT art_cb,
                        Vt1_nom,
                        sta_nom,
                        sta_id,
                        art_id,
                        art_stk,
                        art_statut
                            FROM Articles
                                JOIN Vtit1 ON Vt1_article  = art_id
                                JOIN Statut on sta_id = art_statut
                                    WHERE art_statut = ".$id;
$r_artstatut = $idcom->query($req_artstatut);
if ($r_artstatut->num_rows == 0) {
    echo "<img src='/images/attention.png'> Il n'y a pas d'article dans cette catégorie";
    exit;
}
$rq_artstatut =$r_artstatut->fetch_object();

$req_statut = "SELECT * FROM Statut ORDER BY sta_id, sta_nom";
$r_statut=$idcom->query($req_statut);
$combo = '';
while ($rq_statut = $r_statut->fetch_object()) {
    $combo .= "<option value='".$rq_statut->sta_id."'>".$rq_statut->sta_nom."</option>";
}
?>
<style>
#articles{width:90%}
.jaune{
background-color:yellow;
}
#articles.tablesorter tbody td {
  font-size: 11pt;
  color: #3D3D3D;
  padding: 0 4px 0 4px;
  background-color: <?php echo $coulFF?>;
  vertical-align: middle;
 }
#articles.tablesorter tbody tr.odd td {
/*   text-align:left; */
  background-color:<?php echo $coulCC?>;
  vertical-align: middle;
 }
#articles.tablesorter tbody tr.odd.orange td {
/*   text-align:left; */
  background-color:orange;
  vertical-align: middle;
 }
 #articles.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
background-color: #8dbdd8;}
</style>
<script type="text/javascript" src="/js/jquery.tablesorter.js"></script>
<script>
$(document).ready(function() {
    $( "select" ).change(function() {
        id = $(this).attr("id").substr(1);
    // alert($(this).val());
        modif (id,11,$(this).val(),"statut",1);
        charge('art_statut','<?php echo $id?>','panneau_d')
    });
    $("#articles").tablesorter({ widgets: ['zebra']});
});
</script>
<h3><?php echo $rq_artstatut->sta_nom?></h3>
<table id='articles' class="tablesorter">
<thead><tr><th>CB</th><th>Titre</th><th>Statut</th><th>Stock</th></tr></thead>
<tbody>
<?php
$r_artstatut->data_seek(0);
$tab_statut = "0,";
$tab_article = "0,";
$n = 0;
while ($rq_artstatut =$r_artstatut->fetch_object()) {
    $tab_article .=($n == 0)?$rq_artstatut->art_id:",".$rq_artstatut->art_id;
    $tab_statut .=($n == 0)?$rq_artstatut->art_statut:",".$rq_artstatut->art_statut;
    echo "<tr><td>".$rq_artstatut->art_cb."</td>
    <td>".$rq_artstatut->Vt1_nom."</td>
    <td><select id='C".$rq_artstatut->art_id."'></select></td><td>".$rq_artstatut->art_stk."</td></tr>";
    $n++;
}
?>
</tbody></table>
<script>
var tab_statut= [<?php echo $tab_statut?>];
var tab_article = [<?php echo $tab_article?>];
// alert(tab_statut.length);
var combo = "<option></option><?php echo $combo?>"; 
for(i = 1 ; i <= tab_statut.length; i++){
    // alert('#C'+tab_article[i]);
    $('#C'+tab_article[i]).html(combo);
    $('#C'+tab_article[i]+' option[value='+tab_statut[i]+']').prop('selected', true);
    // $('#'+sortie+'_'+id+' option[value='+cod+']').prop('selected', true);
}
$("#panneau_d").css('max-height', $('#affichage').height());
$("#panneau_g").css('max-height', $('#affichage').height());
</script>
