<?php
if (!isset($incpath)) {
    session_start();
    if (!isset($incpath)) {
        $p=preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath="";
        for ($i=1;$i<sizeof($p)-1;$i++) {
            $incpath='../'.$incpath;
        }
        unset($p, $i);
    }
    $date= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    include $incpath."mysql/connect.php";
    include $incpath."php/fonctions.php";
    connexobjet();

    if (!$date) {
        $date = ANNEE."-".date('m')."%";
    }//permet l'importation directe dans libreoffice
?>
<html>
    <head>
      <TITLE></TITLE>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    </head>
  <body>
<?php
}
$an =substr($date, 0, 4);

$req_recher="SELECT rst_validation,
                    rst_total,
                    tic_num,
                    tic_tva,
                    rst_total,
                    SUM(tic_quantite* tic_prix) AS tt
                        FROM Tickets_$an 
                        JOIN Resume_ticket_$an ON rst_id = tic_num
                            WHERE MONTH(rst_validation) =1
                                GROUP BY tic_ntva, tic_num
                                    ORDER BY tic_num";
// exit;
$r_recher=$idcom->query($req_recher);
$r_recher->num_rows;
?>
<center id="resultat_analyse">
<?php
if ($r_recher->num_rows == 0) {
    echo "<h1>Il n'a pas de donnée à analyser</h1>";
    exit;
}
//recherche des tautaux par tva
$ticket = '';
?>

<table><tr style='background-color:white'><th colspan="8">Export des ventes</th></tr>
<TR><TH>N° ticket</TH><TH>Jour</TH><TH>Tva</TH><TH>Total par tva</TH><th>Total</th></TR>
<?php
$n = 1;
$m = 0;
// $ligne_csv = array('','','','','','');
// $csv = array();
$mode = 1;//valeur fictive de départ
$ttr = 0;
while ($rq_recher =$r_recher->fetch_object()) {
    // array_push($csv, $ligne_csv);
    $coul=($m % 2 == 0)?$coulCC:$coulFF;
    // print_r($rq_recher);
    if ($ticket != $rq_recher->tic_num) {
        echo "<tr style='background-color:".$coul."'><td>".$rq_recher->tic_num."</td>
        <td>".$rq_recher->rst_validation."</td>
        <td></td><td></td>
        <td class='droite'>".monetaireF($rq_recher->rst_total)."</td></tr>";
        $m++;
    }
    echo "<tr style='background-color:".$coul."'>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>".$rq_recher->tic_tva."</td>
    <td class='droite'>".monetaireF($rq_recher->tt)."</td>
    <td></td></tr>";

    // $ttr += $rq_recher->rst_total;
    // $ligne_csv[$m][1]=$rq_recher->cod_nom;
    // $ligne_csv[$m][3]=$rq_recher->mdr_nom;
    $ticket = $rq_recher->tic_num;
}

?>
</table>
<div id="export_achat" style='margin:10px;background-color:white;text-align:left'><button onclick="charge('export_ebp','<?php echo $date?>','export_ebp')">Export ebp Achat</button></div>
