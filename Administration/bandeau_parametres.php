<?php
session_start();
require "../php/config.php";
?>
<script type="text/javascript">
$(document).ready(function() {
    $('.param').click(function(){
    $('.param').css('color','black');
    $(this).css('color','Blue');
    });
});
function voir(page,div) {
    $('#affichage').empty();
    $('#affichage').html('<div id=panneau_g></div><div id=panneau_d></div>');
    charge(page,'',div);
}

var w=$(window).height();
var b=$('#bandeau').height();
$("#affichage").height(w-b-40);
$('.sp-container').remove();
</script>

<h3>Paramètres</h3>
<center>
  <table style="width:100%">
    <TR>
      <TH class="param" onclick="voir('imprimantes','affichage');">Imprimantes</TH>
      <?php
      if ($config['favoris']== 1) {
      ?>
      <TH class="param" onclick="voir('favoris','panneau_g')">Favoris</TH>
      <?php
      }
      ?>
      <TH class="param" onclick="voir('secteurs','panneau_g')">Secteurs</TH>
      <TH class="param" onclick="voir('tva','panneau_g')">TVA</TH>
      <TH class="param" onclick="voir('config','panneau_g')">Configuration</TH>
      <TH class="param" onclick="voir('fonctions','panneau_g')">Fonctions</TH>
      <TH class="param" onclick="voir('statut','panneau_g')">Statuts articles</TH>
      <TH class="param" onclick="voir('pseudos','panneau_g')">Pseudos</TH>
      <TH class="param" onclick="voir('soldes','panneau_g')">Soldes</TH>
      <TH class="param" onclick="voir('mode_reglement','affichage')">Modes de règlements</TH>
      <TH class="param" onclick="voir('codes','panneau_g')">Codes analytiques</TH>
      <TH class="param" onclick="voir('unites','panneau_g')">Unités de ventes</TH>
      <TH class="param" onclick="voir('afficheurs','panneau_g')">Afficheurs externes</TH>
   </TR>
  </table>
</center>
