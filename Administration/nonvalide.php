<?php
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$valeur= filter_input(INPUT_GET, "valeur", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$mode= filter_input(INPUT_GET, "mode", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
connexobjet();

if ($valeur) {
    //mise à jour du ticket non validé et deconnexion de l'utilisateur
    $req_up="UPDATE Resume_ticket_".ANNEE." JOIN Utilisateurs ON uti_id=rst_utilisateur SET rst_etat='$mode', rst_validation = rst_jour, rst_total = $valeur, rst_totalS=$valeur, uti_etat=0 WHERE rst_id=$req";
    $idcom->query($req_up); ?>
    <script>
    charge('utilisateurs','','panneau_g');
    </script>
    <?php
    exit;
}
if ($mode=="suppression") {
    //supression des article, deconnexion de l'utilisateur, rst_utilisateur = 0
    $sup="DELETE FROM Tickets_".ANNEE." WHERE tic_num=$req";
    $idcom->query($sup);

    $req_up="UPDATE Resume_ticket_".ANNEE." JOIN Utilisateurs ON uti_id=rst_utilisateur SET rst_utilisateur=0, uti_etat=0 WHERE rst_id=$req";
    $idcom->query($req_up); ?>
    <script>
    charge('utilisateurs','','panneau_g');
    </script>
    <?php
    exit;
}
//recherche des articles non validés du vendeur
$req_vendeur="SELECT Tickets_".ANNEE.".*, uti_nom, uti_id, tic_article, uti_id, Vt1_nom
                FROM Utilisateurs
                JOIN Resume_ticket_".ANNEE." ON rst_utilisateur = uti_id
                JOIN Tickets_".ANNEE." ON rst_id = tic_num
                JOIN Vtit1 ON Vt1_article = tic_article
                  WHERE rst_id = $req";
$r_vendeur=$idcom->query($req_vendeur);
if ($r_vendeur->num_rows > 1) {
    $s="s";
}
echo "<h3>".$r_vendeur->num_rows." Article".$s." non validé".$s."</h3>";
echo "<table width=90%><TR><TH>Titre</TH><TH>Quantité</TH><TH>Prix U.</TH><TH>Total</TH></TR>";
while ($rq_vendeur=$r_vendeur->fetch_object()) {
    $tt=$rq_vendeur->tic_prix*$rq_vendeur->tic_quantite;
    echo "<tr><td>".$rq_vendeur->Vt1_nom."</td><td>".$rq_vendeur->tic_quantite."</td><td>".$rq_vendeur->tic_prix."</td><td>".$tt."</td></tr>";
    $total += $tt;
}

?>
<tr><TH colspan="3">Valeur de ce ticket non validé</TH><tH><?php echo $total?></tH></tr>
<tr><TD colspan="4"><button style="float:left" onclick="charge('nonvalide','<?php echo $req?>&mode=suppression','panneau_d')">Supprimer ce ticket</button><button style="float:right" onclick="$('#mode').css('visibility','visible')">Valider ce ticket</button>
<select onchange="charge('nonvalide','<?php echo $req."&valeur=".$total?>&mode='+this.value,'panneau_d')" style="visibility:hidden;float:right" id="mode">
<option></option>
<?php
$req_mode="SELECT * FROM Mode_reglement WHERE mdr_ac=1";
$r_mode=$idcom->query($req_mode);
while ($rq_mode=$r_mode->fetch_object()) {
    echo "<option value='".$rq_mode->mdr_abrege."'>".$rq_mode->mdr_nom."</option>\n";
}
?>
</select>
</TD></tr>
</table>