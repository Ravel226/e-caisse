<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$uti= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
//seuls les favoris n'ayant pas d'articles sont sélectionnables
$req_aff="SELECT * FROM Affichage LEFT JOIN Vfavoris ON Vfav_parent = aff_id ORDER BY aff_ordre";

$r_aff=$idcom->query($req_aff);
$nb=$r_aff->num_rows;

//construction du combo des parents et de ordre
$n =1;
$com_ordre = '<option></option>';
$com_aff='';
//taleau pour le selected des combo
$tab_ordre = "'',";
$tab_parent= "'',";
while ($rq_aff=$r_aff->fetch_object()) {
    if (($rq_aff->Vfav_parent != "")||($rq_aff->aff_id == 1)) {
        $dis='disabled';
    } else {
        $dis='';
    }
    $com_aff .="<option value='".$rq_aff->aff_id."' ".$dis.">".$rq_aff->aff_nom."</option>\n";
    $com_ordre .="<option value='".$n."'>".$n."</option>\n";
    $tab_parent.= "'".$rq_aff->aff_parent."',";
    $tab_ordre .= "'".$rq_aff->aff_ordre."',";
    $n++;
}

?>
<script>
$(document).ready(function() {
    $('table#secteurs input').on('keydown',function(event){
        if(event.which == 13){
            if($(this).attr('type') == 'text') {
                if ($(this).hasClass('jaune')) {
                    modif($(this).attr('id'),26,$(this).val(),'nom',1);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
    });
    
    $('table#secteurs img').click(function(){
        var id = $(this).attr('alt');
        if ($(this).hasClass('supprimer') == true ) {
            charge('rayons','sup&id='+id,'panneau_d');
        } else {
            window.open('upload.php?req=affichage&id='+id);
        }
    }); 
});
</script>

<img src="/images/aide.png" style="float:right" onclick="charge('aide',7,'panneau_d')">
<h3>Définition des favoris de l'interface de vente</h3>
<table id="secteurs">
  <TR><TH>Nom</TH><th>Parent</th><th>Ordre</th>
  </TR>
<?php
$n = 1;
$r_aff->data_seek(0);
while ($rq_aff=$r_aff->fetch_object()) {
    $coul=($n%2 == 0)?$coulCC:$coulFF;
?>
<TR style='background-color:<?php echo $coul?>'>
    <TD><input id='<?php echo $rq_aff->aff_id?>' type="text" style="width:200px" value="<?php echo $rq_aff->aff_nom?>"></TD>
    </TD>
    <td>  <select id='p_<?php echo $n?>' onchange="modif(<?php echo $rq_aff->aff_id?>,26,this.value,'parent','1')">
  <option value="0">Parent</option>
    <?php echo $com_aff?>
  </select></td>
    <td>  
    <?php
    $file=$incpath.'images/affichage/aff_'.$rq_aff->aff_id.'.png';
    if (file_exists($file)) {
        $img= "/images/affichage/aff_".$rq_aff->aff_id.".png";
    } else {
        $img= "/images/secteurs/inc.png";
    }    
    ?>
    <img src="<?php echo $img?>" width=50 alt="<?php echo $rq_aff->aff_id?>">
    </td>
    <td><select id='o_<?php echo $n?>' onchange="modif(<?php echo $rq_aff->aff_id?>,26,this.value,'ordre','1')">
    <?php echo $com_ordre?>
    </select></td>
  </TR>
<?php
$n++;
}
?>
</table>
<button onclick="modif('',26,1,'ordre','0');setTimeout(function(){charge('favoris','','panneau_g');$('#panneau_d').empty()},300);">Nouveau favoris</button>
</div>
<script>
var tab_parent = [<?php echo $tab_parent?>];
// alert(tab_parent.length);
var tab_ordre = [<?php echo $tab_ordre?>];
for(i = 1; i <= tab_parent.length; i++){
$('#p_'+i+' option[value='+tab_parent[i]+']').prop('selected', true);
$('#o_'+i+' option[value='+tab_ordre[i]+']').prop('selected', true);
}
$("#panneau_g").css('max-height', $('#affichage').height());
</script>
