<?php
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
$idcom=connexobjet();
/*
ce fichier gère les utilisateurs
si l'utilisateur n'est pas défini :
en fonction de la page qui charge ce script, on liste les utilisateurs de la section
on affiche la demande de mot de passe s'il y en a un de défini

les variables session de l'utilisateurs définies dans /connexion sont, explemple pour utilisateur 1
[nom_1] => LIVRES
[id_1] => 1 ne sert sans doute à rien, à vérifier
[titre_1] => #fff000
[fondF_1] => #ff7f20
[fondC_1] => #fff1b9
[surligne_1] => #0026ff
[dossier_1] => Saisie
[Saisie] => 1
*/
if (isset($_SESSION[$dossier])) {//session déjà ouverte, on redirige vers le bon dossier
    include "interface.php";
    exit;
}
//liste des utilisateurs de la fonction
if ($dossier == "Ventes") {//verification de la cloture de la journée
    $cejour = date("Y-m-d");;
    $req_resumejour = "SELECT * FROM Resume_jour_".ANNEE." WHERE rsj_date = '$cejour' AND rsj_statut = 2";
    $r_resumejour=$idcom->query($req_resumejour);
    //  echo $r_resumejour->num_rows;
    if ($r_resumejour->num_rows > 0) {   
    ?>
    <!DOCTYPE html
    >
    <html>
        <head>
        <TITLE><?php echo $dossier?></TITLE>
    </head>
    <body>
    <center>
    <h1>La journée à été validée, il n'est plus possible de faire des ventes</h1>
    <a href='/php/deconnexion.php'><img src='/images/sortie.png' style='margin-left:5px;'></a>
    </center>
    </body>
    </html>
    <?php
    exit;
    }
}
$req_util="SELECT * FROM Fonctions
                    JOIN Utilisateurs ON uti_fonction = fon_id
                    JOIN Vfonction_utlisateur ON Vfon_id = uti_fonction
                        WHERE fon_nom = '$dossier' AND uti_etat != 2 ORDER BY uti_nom";
$r_util=$idcom->query($req_util);
$nm = $r_util->num_rows;
?>
<!DOCTYPE html
>
<html>
<head>
<TITLE><?php echo $dossier?></TITLE>
<script type="text/javascript" src="/js/jquery.js"></script>
<style>
.bout {
    width:250px;
    font: 1.3em bold small-caps verdana,serif;
}
</style>
<link rel="stylesheet" type="text/css" href="/keypad.css">
<script>
function suite(nom) {
    /* cette fonction actualise seulement les variables session
    c'est la fonction reload, ligne 101 ou 108 de cette page 
    qui actualise les données */
    if ($('#P'+nom).length == 0) {
    pass = 'vide';
    } else {
    pass =$('#P'+nom).html();
    }
    $.ajax({ 
        type: "POST",
        url: "/connexion.php",
        data : "nom="+nom+"&pass="+pass,
        dataType : "html",
        //affichage de l'erreur en cas de problème
        error:function(msg){
        alert( "erreur non définie");
        },
        success:function(data) {
        $("#connexion").append(data);
        }
    });
}

function voirpass(nom) {
    if($('#P'+nom).length == 0){
        suite(nom);
    } else {
    $('#effacer').css('visibility','hidden');  
    $('#keypad_tactile').css('display','block');       
    $("#keypad_tactile .key").click(function(){
        if ($(this).html() == 'Valider'){
            suite(nom);
            }
        else if ($(this).html() == 'Effacer'){
            $("#P"+nom).html('');
            $('#valider').css('visibility', 'hidden');
            }
        else if ($(this).html() == 'Annuler'){
            $("#P"+nom).html('');
            $('#keypad_tactile').css('display','none');
            }
        else {
            $("#P"+nom).append($(this).html());
            $('#valider').css('visibility', 'visible');
            $('#effacer').css('visibility', 'visible');
        }
    });
    }
}
</script>
</head>
<body>
<?php if (isset($rq_util->uti_id)) {
    echo $_SESSION['message_'.$rq_util->uti_id];
    }
?>
<div id="connexion"></div>
<div id="keypad_tactile" style="position:absolute;left:50px;top:50px;display:none">
    <div class="keypad-row">
        <button type="button" class="key">1</button>
        <button type="button" class="key">2</button>
        <button type="button" class="key">3</button>
    </div>
    <div class="keypad-row">
        <button type="button" class="key">4</button>
        <button type="button" class="key">5</button>
        <button type="button" class="key">6</button>
    </div>
    <div class="keypad-row">
        <button type="button" class="key">7</button>
        <button type="button" class="key">8</button>
        <button type="button" class="key">9</button>
    </div>
    <div class="keypad-row">
        <div class="keypad-space"></div>
        <button type="button" class="key">0</button>
    </div>
    <div class="keypad-row">
        <div class="keypad-space"></div>
        <button type="button" onclick="$('#keypad').css('display', 'none')" style="background-color:red;text-shadow: 2px 2px white;" class="key">Annuler</button>
        <button style="background-color:yellow;text-shadow: 2px 2px white;" type="button" class="key">Effacer</button>
        <button style="background-color:green;text-shadow: 2px 2px white;visibility:hidden" id="valider" type="button" onclick="$('#keypad').css('display', 'none')" class="key">Valider</button>
    </div>
</div>


<center>
    <div align="center" id="liste" style="background-color:#FFCA00;width:300px;">
        <br>
<?php
if ($nm == 0) {
    echo "Il n'y a pas encore d'utilisateur défini pour cette section<br>";
} else {
    // print_r($_SESSION);
    while ($rq_util=$r_util->fetch_object()) {
        $connecte=($rq_util->uti_etat == 1)?" (connecté)":"";
        if ($rq_util->uti_pass != '') {
            ?>
            <input type="button" onclick="voirpass('<?php echo $rq_util->uti_nom?>')" class="bout" value="<?php echo $rq_util->uti_nom.$connecte?>">
            <span style="display:none" id='P<?php echo $rq_util->uti_nom?>'></span>
            <?php
        } else {
            ?>
            <input type="button" onclick="voirpass('<?php echo $rq_util->uti_nom?>')" class="bout" value="<?php echo $rq_util->uti_nom.$connecte?>">
            <?php
        }
    }
}
echo "<a href='/php/deconnexion.php'><img src='/images/sortie.png' style='margin-left:5px;'></a> ";
?>
</div>
</center>
<script type="text/javascript">
hh=$(window).height();
$('.bout').css('height',((hh - 100)/<?php echo $nm?>));
$('.pswd').css('marginTop',((hh - 100)/<?php echo $nm?>/2));
</script>
</body>
</html>