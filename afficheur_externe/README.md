Ce répertoire contient le féchier nécéssaire à la mise en place d'un afficheur externe sur un raspberry pi3.

**Principe :**<br >
Dans le secteur *Ventes* du poste de vente, la zone de prix est surveillée. En cas de changement, il appelle le fichier actualiser.php de l'afficheur qui écrit la date dans le fichier totalprice.txt surveillé par la programme python qui écrit la valeur sur l'écran.

Dans le secteur *Gestion*, deux boutons permettent un arrêt et un reboot à distance de l'afficheur. Il n'y a normalement pas besoin d'utiiliser un clavier ou une souris sur l'afficheur

Configuration du raspberry
==========================

Interface graphique de LXDE
---------------------------
.config/lxsession/LXDE-pi/autostart

@lxpanel --profile LXDE-pi<br > 
@pcmanfm --desktop --profile LXDE-pi<br >
 #@xscreensaver -no-splash<br > 
@point-rpi<br >
@xset s off<br >
@xset -dpms<br >
@xset s noblank<br > 
 #@unclutter -idle 0<br > 
@/home/pi/showprice2.sh
 
------------------------------------

Configuration de incron
-----------------------

incron doit-être actif

deux fichiers dans /etc/incron.d/

* arret_raspberry qui contient 
    * var/www/html/arret.txt IN_CLOSE_WRITE sudo halt -p<br >
* reboot_raspberry qui contient 
    * var/www/html/reboot.txt IN_CLOSE_WRITE sudo reboot
    
Un serveur http (lighttpd)
--------------------------

les fichiers suivants doivent être accessible en écriture par lighttpd

* reboot.txt
* arret.txt
* showprice2.txt
 


