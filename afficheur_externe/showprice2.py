#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import datetime
from tkinter import *


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PRICE_FILE = "totalprice.txt"
now = datetime.datetime.now()
PATH_TO_FILE = os.path.join(BASE_DIR, PRICE_FILE)

def get_price_str(file):
    """Read price from file

    Args:
        file (name of file): [description]

    Returns:
        price: string
    """
    price = ""
    with open(file, "r") as f:
        price = f.readline().strip() or ""
    return price


class Gui:
    def __init__(self):
        self.tmp_price_str = ""
        self.root = Tk()
        self.root.configure(background='black')
        self.root.title("Le prix total")
        self.lbl0 = Label(self.root, text="e-caisse\n"+(now.strftime("%d/%m/%Y") + "\n\n"), fg="#fff", bg="#000")
        self.lbl0.configure(font=("Arial", 15, "normal"))
        self.lbl = Label(self.root, text="", fg="#fff", bg="#000")
        self.lbl.configure(font=("Arial", 50, "bold"))
        self.lbl.config(anchor=CENTER)
        self.root.geometry("800x600")
        self.root.attributes("-fullscreen", True)
        self.root.bind(
            "<F11>",
            lambda event: self.root.attributes(
                "-fullscreen", not self.root.attributes("-fullscreen")
            ),
        )
        self.root.bind(
            "<Escape>", lambda event: self.root.attributes("-fullscreen", False)
        )
        self.readPrice()

    def run(self):
        self.lbl0.pack()
        self.lbl.pack()
        self.lbl.after(200, self.readPrice)
        self.root.mainloop()

    def readPrice(self):
        msg = get_price_str(PATH_TO_FILE)
        if msg != self.tmp_price_str:
            self.lbl["text"] = msg
            self.root.update()
            self.tmp_price_str = msg
        self.root.after(200, self.readPrice)


if __name__ == "__main__":
    Gui().run()
